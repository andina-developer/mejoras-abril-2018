<?php
/*
All the functions are in the PHP pages in the `functions/` folder.
*/

	require get_template_directory() . '/functions/cleanup.php';
	require get_template_directory() . '/functions/setup.php';
	require get_template_directory() . '/functions/enqueues.php';
	require get_template_directory() . '/functions/navbar.php';
	require get_template_directory() . '/functions/widgets.php';
	require get_template_directory() . '/functions/search-widget.php';
	require get_template_directory() . '/functions/index-pagination.php';
	require get_template_directory() . '/functions/split-post-pagination.php';
	require get_template_directory() . '/functions/feedback.php';
	require get_template_directory() . '/functions/remove-query-string.php';

	/*
	 *  Funciones personalizadas
	 */
	require get_template_directory() . '/functions/bs4navwalke.php';
	require get_template_directory() . '/functions/funciones.php';
	require get_template_directory() . '/functions/cdn.php';
	require get_template_directory() . '/functions/acciones.php';
	require get_template_directory() . '/functions/shortcodes.php';
	require get_template_directory() . '/functions/filtros.php';
	require get_template_directory() . '/functions/woocommerce.php';
	require get_template_directory() . '/functions/envios.php';



/************************
	INI: CUSTOM-DEVELOPER
*************************/

//Using this code you can activate your plugin from the functions.php
    function activate_plugin_via_php() {
        $active_plugins = get_option( 'active_plugins' );
        array_push($active_plugins, 'woocommerce/woocommerce.php'); /* Here just replace unyson plugin directory and plugin file*/
        update_option( 'active_plugins', $active_plugins );    
    }
    add_action( 'init', 'activate_plugin_via_php' );


function letsgo_needs_processing($is_not_virtual, $product, $order_id) {
	return true;
}
add_filter('woocommerce_order_item_needs_processing','letsgo_needs_processing',10,3);


function letsgo_add_user_fields($profileuser) {

	$html_tipodoc = '';
	$array_tipodoc = array('DNI','CE');
	$meta_tipodoc = get_user_meta($profileuser->ID,'tipo_documento',true);
	$meta_numdoc = get_user_meta($profileuser->ID,'numero_documento',true);

	foreach($array_tipodoc as $tipodoc) {
		$html_tipodoc .= '<option '.selected($meta_tipodoc,$tipodoc,false).'>'.$tipodoc.'</option>'."\n";
	}

	echo '<h2>Documento de Identidad</h2>';
	echo '<table class="form-table" id="fieldset-dni">
			<tbody>
				<tr>
					<th><label for="user_tipodoc">Tipo de Documento</label></th>
					<td><select name="user_tipodoc" id="user_tipodoc">'.$html_tipodoc.'</select></td>
				</tr>
				<tr>
					<th><label for="user_numdoc">Numero de Documento</label></th>
					<td><input type="text" name="user_numdoc" id="user_numdoc" value="'.$meta_numdoc.'" /></td>
				</tr>
			</tbody>
		</table>';

}

add_action( 'edit_user_profile', 'letsgo_add_user_fields', 10, 1 );
add_action( 'show_user_profile', 'letsgo_add_user_fields', 10, 1 );


function letsgo_update_user_fields($user_id) {

	if ( current_user_can('edit_user',$user_id) ) {
		if( isset($_POST['user_tipodoc']) )
			update_user_meta($user_id,'tipo_documento',$_POST['user_tipodoc']);
			
		if( isset($_POST['user_tipodoc']) )
			update_user_meta($user_id,'numero_documento',$_POST['user_numdoc']);
	}
	
}

add_action('personal_options_update','letsgo_update_user_fields',10,1);
add_action('edit_user_profile_update','letsgo_update_user_fields',10,1);

/************************
	FIN: CUSTOM-DEVELOPER
*************************/
?>