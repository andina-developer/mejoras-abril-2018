##Reparación de Bugs

1. Desde mobile, cuando se selecciona uno de los filtros (niño o niña) para ingresar a la lista de productos, no terminan de cargar los productos cuando se hace uso del scroll hacia abajo. Esto sí funciona cuando se ingresa a categorías o a la opción toys2go.pe/tienda.

2. Cuando el cliente olvida registrar un dato en el proceso de checkout, se muestra una alerta (esto está bien), pero cuando se llega al campo donde se presenta el problema, luego el proceso regresa al inicio.

3. Algunos usuarios que provienen de migración desde Magento (ejemplo: gabybuse2002@yahoo.com), no cuentan con la información actualizada de dirección o DNI. Se requiere revisión.

Este tipo de usuarios además, cuando llegan al proceso de checkout, presenta de la siguiente manera la información de dirección:

Y el campo “Seleccione” de dirección guardada, aparece vacío.

4. Cuando se realizan pagos con PayU, y PayU identifica que la transacción se realizó correctamente, automáticamente cambia el estado del pedido a Completado. Debe cambiar a Procesando.


##¿Como aplicar los cambios?

Paso 1: 
El archivo functions.php deberá ser actualizado y agregar al utlimo de todo el bloque existente todo lo contenido en el comentario "Custom Developer"

Paso 2:
Reemplazar este archivo:

" toys2go_movil / include / movil / comprar-por.php ""

(Recordar siempre hacer un backup del fichero)


##Sobre Payu
Lo contenido en functions deberia solucionar el problema, se debe probar con las llaves de prueba o las de producción. Lo que debe hacer es que despues de cada compra el estado debe ser "procesando". De existir algun error en la aplicación de la solución, capturar el error y reporarlo para proceder con la inspección. 