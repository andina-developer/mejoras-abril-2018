<?php
/*

  Plugin Name: Personalización del checkout
  Description: Plugin que nos permite personalizar el checkout de acuerdo al diseño de Toys2go
  Version: 1.0
  License: GPLv2
  License URL: http://www.gnu.org/licenses/gpl-2.0.html

 */

class beta_custom_checkout {
    private $url;
    private $urlpath;
    private $version;
    private $prefijo;
    private $tabla_departamento;
    private $tabla_provincia;
    private $tabla_distrito;
    private $tabla_direcciones;

    function __construct() {
        global $wpdb;
        $this->prefijo = $wpdb->prefix;
        $this->tabla_departamento = $wpdb->prefix . "ubigeo_departamento";
        $this->tabla_provincia = $wpdb->prefix . "ubigeo_provincia";
        $this->tabla_distrito = $wpdb->prefix . "ubigeo_distrito";
        $this->tabla_direcciones = $wpdb->prefix . "customer_address";
        date_default_timezone_set('America/Lima');
        $this->version = "1.0";
        $this->url = plugins_url(basename(__FILE__), __FILE__);
        $this->urlpath = plugins_url('', __FILE__);

        do_action( 'woocommerce_set_cart_cookies',  true );


        add_action('wp_enqueue_scripts', array($this, "agregar_js"));

        add_action('woocommerce_formulario_usuario', array($this, "custom_data_user"), 1);
        add_action('woocommerce_before_checkout_billing_form', array($this, "add_fields_before_billing"), 2);
        add_filter('woocommerce_checkout_fields', array($this, 'quitar_agregar_campos_checkout'));

        // Ajax para traer la data de la direción seleccionada
        add_action('wp_ajax_nopriv_get_data_direccion', array($this, 'get_data_direccion'));
        add_action('wp_ajax_get_data_direccion', array($this, 'get_data_direccion'));
        
        // Ajax para traer la data de la direción seleccionada
        add_action('wp_ajax_nopriv_get_data_direccion_giftlist', array($this, 'get_data_direccion_giftlist'));
        add_action('wp_ajax_get_data_direccion_giftlist', array($this, 'get_data_direccion_giftlist'));

        // Ajax para traer las provincias dado un departamento
        add_action('wp_ajax_nopriv_get_data_provincia', array($this, 'get_data_provincia'));
        add_action('wp_ajax_get_data_provincia', array($this, 'get_data_provincia'));

        // Ajax para traer los distritos dato una provincia
        add_action('wp_ajax_nopriv_get_data_distrito', array($this, 'get_data_distrito'));
        add_action('wp_ajax_get_data_distrito', array($this, 'get_data_distrito'));

        // Guardar la dirección del cliente
        add_action('woocommerce_checkout_update_order_meta', array($this, 'guardar_campos_orden'), 10, 2);        

        // Valida campos ingresados
        add_action('woocommerce_checkout_process', array($this, 'verificar_datos_checkout'));
        add_action('woocommerce_admin_billing_fields', array($this, 'agregar_metas_builling_admin'), 10, 1);
        add_action('woocommerce_admin_shipping_fields', array($this, 'agregar_metas_shipping_admin'), 10, 1);
    }

    function agregar_metas_shipping_admin($fields) {
        $fields['mensaje'] = array(
            'label' => _e('Dedicatoria:', 'b4st'),
            'type' => 'textarea',
            'show' => true,
        );
        return $fields;
    }

    function agregar_metas_builling_admin($fields) {
        $fields['comprobante'] = array(
            'label' => __('Comprobante de pago', 'woocommerce'),
            'show' => true,
        );
        /*$fields['identity_document'] = array(
            'label' => __('Documento de facturación', 'woocommerce'),
            'show' => true,
        );
        $fields['razon_social'] = array(
            'label' => __('Razón Social', 'woocommerce'),
            'show' => true,
        );*/
        return $fields;
    }

    function quitar_agregar_campos_checkout($fields)
    {
        global $wpdb;
        $current_id = get_current_user_id();
        $where_provincia = $where_distrito = "";
        $departamentos = $distritos = $provincias = array();
        $departamento_id = $provincia_id = $distrito_id = 0;
        $direccion = $referencia = $nombre = "";
        $direccion_id = "0";
        if (!empty($current_id)) {
            $direccion_id = get_user_meta($current_id, 'ultima_direccion', true);
            if (!empty($direccion_id)) {
                $query = $wpdb->prepare("select * from " . $this->tabla_direcciones . " where ID = " . intval($direccion_id) . " limit 1", '');
                $resultado = $wpdb->get_results($query);
                if (!empty($resultado)) {
                    $departamento_id = $resultado[0]->departamento_id;
                    $provincia_id = $resultado[0]->provincia_id;
                    $distrito_id = $resultado[0]->distrito_id;
                    $direccion = $resultado[0]->direccion;
                    $referencia = $resultado[0]->referencia;
                    $nombre = $resultado->nombre;
                    $where_provincia = " where idDepa = " . $resultado[0]->departamento_id;
                    $where_distrito = " where idProv = " . $resultado[0]->provincia_id;
                }
            }
        }

        echo '<input type="hidden" id="hidden_billing_departamento" name="hidden_billing_departamento" value="'.$departamento_id.'" />';
        echo '<input type="hidden" id="hidden_billing_provincia" name="hidden_billing_provincia" value="'.$provincia_id.'" />';
        echo '<input type="hidden" id="hidden_billing_distrito" name="hidden_billing_distrito" value="'.$distrito_id.'" />';
        /*echo '<input type="hidden" id="selected_billing_departamento" name="selected_billing_departamento" value="" />';
        echo '<input type="hidden" id="selected_billing_provincia" name="selected_billing_provincia" value="" />';
        echo '<input type="hidden" id="selected_billing_distrito" name="selected_billing_distrito" value="" />';*/
        // departamentos.
        $departamentos[""] = "Seleccione";
        $query = $wpdb->prepare("select idDepa as ID, departamento as nombre from " . $this->tabla_departamento, '');
        $resultado = $wpdb->get_results($query);
        if (!empty($resultado)) {
            foreach ($resultado as $itemDepartamento) {
                $departamentos[$itemDepartamento->ID] = $itemDepartamento->nombre;
            }
        }
        $fields['billing']['billing_departamento'] = array(
            'label' => __('Departamento', 'woocommerce'),
            'placeholder' => _x('', 'placeholder', 'woocommerce'),
            'required' => true,
            'class' => array('form-row-wide'),
            'clear' => false,
            'default' => $departamento_id,
            //'default' => "",
            'type' => 'select',
            'options' => $departamentos
        );

        $fields['shipping']['shipping_departamento'] = array(
            'label' => __('Departamento', 'woocommerce'),
            'placeholder' => _x('', 'placeholder', 'woocommerce'),
            'required' => true,
            'class' => array('form-row-wide'),
            'clear' => false,
            'default' => $departamento_id,
            //'default' => "",
            'type' => 'select',
            'options' => $departamentos
        );

        // Provincia
        $provincias[""] = "Seleccione";
        /*if (!empty($where_provincia)) {
            $query = $wpdb->prepare("select idProv as ID, provincia as nombre from " . $this->tabla_provincia, '');
            $resultado = $wpdb->get_results($query);
            if (!empty($resultado)) {
                foreach ($resultado as $itemProvincia) {
                    $provincias[$itemProvincia->ID] = $itemProvincia->nombre;
                }
            }
        }*/

        $fields['billing']['billing_provincia'] = array(
            'label' => __('Provincia', 'woocommerce'),
            'placeholder' => _x('', 'placeholder', 'woocommerce'),
            'required' => true,
            'class' => array('form-row-wide'),
            'clear' => false,
            'data-selected' => $provincia_id,
            'type' => 'select',
            'options' => $provincias
        );
        $fields['shipping']['shipping_provincia'] = array(
            'label' => __('Provincia', 'woocommerce'),
            'placeholder' => _x('', 'placeholder', 'woocommerce'),
            'required' => true,
            'class' => array('form-row-wide'),
            'clear' => false,
            'data-selected' => $provincia_id,
            'type' => 'select',
            'options' => $provincias
        );

        // Distritos
        $distritos[""] = "Seleccione";
        /*if (!empty($where_distrito)) {
            $query = $wpdb->prepare("select idDist as ID, distrito as nombre from " . $this->tabla_distrito, '');
            $resultado = $wpdb->get_results($query);
            if (!empty($resultado)) {
                foreach ($resultado as $itemDistrito) {
                    $distritos[$itemDistrito->ID] = $itemDistrito->nombre;
                }
            }
        }*/
        $fields['billing']['billing_distrito'] = array(
            'label' => __('Distrito', 'woocommerce'),
            'placeholder' => __('', 'placeholder', 'woocommerce'),
            'required' => true,
            'class' => array('form-row-wide'),
            'clear' => false,
            //'default' => $distrito_id,
            'type' => 'select',
            'options' => $distritos
        );

        $fields['shipping']['shipping_distrito'] = array(
            'label' => __('Distrito', 'woocommerce'),
            'placeholder' => __('', 'placeholder', 'woocommerce'),
            'required' => true,
            'class' => array('form-row-wide'),
            'clear' => false,
            //'default' => $distrito_id,
            'type' => 'select',
            'options' => $distritos
        );

        $fields['billing']['billing_referencia'] = array(
            'type' => 'text',
            'required' => true,
            'class' => array('form-row-wide'),
            'placeholder' => __(''),
            'label' => __('Referencia')
        );

        $fields['shipping']['shipping_referencia'] = array(
            'type' => 'text',
            'required' => true,
            'class' => array('form-row-wide'),
            'placeholder' => __(''),
            'label' => __('Referencia')
        );

        /*$fields['billing']['billing_tipodoc'] = array(
            'label' => __('Tipo documento', 'woocommerce'),
            'placeholder' => __('', 'placeholder', 'woocommerce'),
            'required' => true,
            'class' => array('form-row-wide'),
            'clear' => false,
            'type' => 'select',
            'options' => array("dni" => "Dni", "ruc" => "Ruc")
        );*/

        /*$fields['billing']['billing_documento'] = array(
            'type' => 'text',
            'required' => true,
            'class' => array('form-row-wide'),
            'placeholder' => __(''),
            'label' => __('Nro. documento:')
        );*/

        /*$fields['billing']['billing_razon_social'] = array(
            'type' => 'text',
            'class' => array('form-row-wide hidden'),
            'placeholder' => __(''),
            'label' => __('Raz&oacute;n social:')
        );*/

        /*$fields['billing']['billing_identity_document']['required'] = true;
        $fields['billing']['billing_identity_document']['class'] = array('form-row-wide');
        $fields['billing']['billing_identity_document']['label'] = __('Nro Documento:');*/
 
        $fields['billing']['billing_ckguardardireccion'] = array(
            'type' => 'checkbox',
            'class' => array('input-checkbox'),
            'default' => 0,
            'label' => __('<span>Guardar direccion.</span>'),
        );
        /*$fields['billing']['billing_ckcrearcuenta'] = array(
            'type' => 'checkbox',
            'class' => array('input-checkbox'),
            'default' => 0,
            'label' => __('<span>¿Crear una cuenta?</span>'),
        );*/
        $fields['billing']['billing_nomdireccion'] = array(
            'type' => 'text',
            'class' => array('form-row-wide', 'hidden'),
            'placeholder' => __(''),
            'label' => __('Guardar como:'),
            'default' => __('Mi dirección')
        );

        if (!empty($nombre)) {
            $fields['billing']['billing_nomdireccion']['default'] = $nombre;
        }
        if (!empty($referencia)) {
            $fields['billing']['billing_referencia']['default'] = $referencia;
        }
        if (!empty($direccion)) {
            $fields['billing']['billing_address_1']['default'] = $direccion;
        }
 
        $fields['billing1']['billing_ckusarmismadireccion'] = array(
            'type' => 'checkbox',
            'class' => array('input-checkbox'),
            'default' => 0,
            'label' => __('<span>Usar la dirección de envío.</span>'),
        );

        // Eliminar el campo de notas
        unset($fields['billing']['billing_company']);  //  remove field
        unset($fields['billing']['billing_country']);  //  remove field
        unset($fields['billing']['billing_state']);  //  remove field
        unset($fields['billing']['billing_city']);  //  remove field
        unset($fields['billing']['billing_postcode']);  //  remove field
        unset($fields['billing']['order_comments']);  //  remove field
        unset($fields['billing']['billing_phone']);

        unset($fields['order']['order_comments']);

        unset($fields['shipping']['shipping_company']);  //  remove field
        unset($fields['shipping']['shipping_country']);  //  remove field
        unset($fields['shipping']['shipping_state']);  //  remove field
        unset($fields['shipping']['shipping_city']);  //  remove field
        unset($fields['shipping']['shipping_postcode']);  //  remove field

        unset($fields['account']['createaccount']);  //  remove field 
        unset($fields['account']['account_password']);  //  remove field 


        // Ordenar los campos de billing
        $order = array(
            //"billing_first_name",
            //"billing_last_name",
            //"billing_tipodoc",
            //"billing_documento",
            //"billing_identity_document",
            //"billing_razon_social",
            "billing_address_1",
            "billing_address_2",
            //"billing_email",
            //"billing_phone",
            "billing_departamento",
            "billing_provincia",
            "billing_distrito",
            "billing_referencia",
            "billing_ckguardardireccion",
            //"billing_ckcrearcuenta",
            "billing_nomdireccion"
        );

        $fieldShip = array(
            //"shipping_first_name",
            //"shipping_last_name",
            "shipping_address_1",
            "shipping_address_2",
            "shipping_departamento",
            "shipping_provincia",
            "shipping_distrito",
            "shipping_referencia",
        );

        $fieldBill = array(
            "billing_ckusarmismadireccion"
        );

        foreach ($order as $field) {
            $ordered_fields[$field] = $fields["billing"][$field];
        }

        foreach ($fieldShip as $fieldShip) {
            $ordered_fields_shipping[$fieldShip] = $fields["shipping"][$fieldShip];
        }

        foreach ($fieldBill as $fieldBill) {
            $ordered_fields_shipping1[$fieldBill] = $fields["billing1"][$fieldBill];
        }

        $fields["billing"] = $ordered_fields;
        $fields["shipping"] = $ordered_fields_shipping;
        $fields["billing1"] = $ordered_fields_shipping1;

        return $fields;
    }

    function agregar_js() {
        wp_enqueue_script(get_class($this) . '_general', $this->urlpath . '/js/general.js', array('jquery'), 1.1, false);
        wp_localize_script(
            'function',
            'ajax_script',
            array('ajaxurl' => admin_url('admin-ajax.php')));
    }

    function get_data_direccion() {
        global $wpdb;
        $id = intval($_POST['id']);
        $htmlProvincia = $htmlDistrito = "";
        if (!empty($id)) {
            $query = $wpdb->prepare("select * from " . $this->tabla_direcciones . " where ID = " . intval($id) . " limit 1", '');
            $resultado = $wpdb->get_row($query);
            if (!empty($resultado)) {
                // Provincias
                $sqlProvincia = $wpdb->prepare("select idProv as ID, provincia as nombre from " .
                    $this->tabla_provincia . " where idDepa = " . $resultado->departamento_id, '');
                $provincias = $wpdb->get_results($sqlProvincia);
                if (!empty($provincias)) {
                    foreach ($provincias as $itemProvincia) {
                        $selected = ($resultado->provincia_id == $itemProvincia->ID) ? "selected = true" : "";
                        $htmlProvincia .= "<option value='" . $itemProvincia->ID . "' " . $selected . ">" . $itemProvincia->nombre . "</option>";
                    }
                }

                // Distritos
                $sqlDistritos = $wpdb->prepare("select idDist as ID, distrito as nombre from " .
                    $this->tabla_distrito . " where idProv = " . $resultado->provincia_id, '');
                $distritos = $wpdb->get_results($sqlDistritos);
                if (!empty($distritos)) {
                    foreach ($distritos as $itemDistrito) {
                        $selected = ($resultado->distrito_id == $itemDistrito->ID) ? "selected = true" : "";
                        $htmlDistrito .= "<option value='" . $itemDistrito->ID . "' " . $selected . ">" . $itemDistrito->nombre . "</option>";
                    }
                }

                $paralel_products = $wpdb->get_results("SELECT * FROM spanktg_parallel_cart WHERE quantity > 0 AND user_id = ".get_current_user_id());
                $direccion_entrega = '';
                if ( !empty($paralel_products) ) {
                    foreach ( $paralel_products as $pp ) {
                        $giftlist = new Woocommerce_Gift_Ideas_Giftlist( $pp->list_id );
                        $direccion_entrega = $giftlist->get_direccion_entrega();
                    }
                    echo json_encode([
                        "referencia" => $direccion_entrega['referencia'], 
                        "direccion" => $direccion_entrega['address_1'],
                        "numero" => $direccion_entrega['address_2'], 
                        "nombre" => '',
                        "departamento_id" => $direccion_entrega['departamento'],
                        "provincias" => $htmlProvincia, 
                        "distritos" => $htmlDistrito,
                    ]);
                } 

                echo json_encode(array("nombre" => $resultado->nombre,
                    "referencia" => $resultado->referencia, "direccion" => $resultado->direccion,
                    "numero" => $resultado->numero, "nombre" => $resultado->nombre,
                    "departamento_id" => $resultado->departamento_id,
                    "provincias" => $htmlProvincia, "distritos" => $htmlDistrito,));
            }
        }
        wp_die();
    }

    function get_data_direccion_giftlist() {
        global $wpdb;
        $htmlProvincia = $htmlDistrito = "";
        $paralel_products = $wpdb->get_results("SELECT * FROM spanktg_parallel_cart WHERE quantity > 0 AND user_id = ".get_current_user_id());
        $direccion_entrega = '';
        foreach ( $paralel_products as $pp ) {
            $giftlist = new Woocommerce_Gift_Ideas_Giftlist( $pp->list_id );
            $direccion_entrega = $giftlist->get_direccion_entrega();
        }
        
        // Provincias
        $sqlProvincia = $wpdb->prepare("select idProv as ID, provincia as nombre from " .
            $this->tabla_provincia . " where idDepa = " . $direccion_entrega['departamento'], '');
        $provincias = $wpdb->get_results($sqlProvincia);
        if (!empty($provincias)) {
            foreach ($provincias as $itemProvincia) {
                $selected = ($direccion_entrega['provincia'] == $itemProvincia->ID) ? "selected = true" : "";
                $htmlProvincia .= "<option value='" . $itemProvincia->ID . "' " . $selected . ">" . $itemProvincia->nombre . "</option>";
            }
        }

        // Distritos
        $sqlDistritos = $wpdb->prepare("select idDist as ID, distrito as nombre from " .
            $this->tabla_distrito . " where idProv = " . $direccion_entrega['provincia'], '');
        $distritos = $wpdb->get_results($sqlDistritos);
        if (!empty($distritos)) {
            foreach ($distritos as $itemDistrito) {
                $selected = ($direccion_entrega['distrito'] == $itemDistrito->ID) ? "selected = true" : "";
                $htmlDistrito .= "<option value='" . $itemDistrito->ID . "' " . $selected . ">" . $itemDistrito->nombre . "</option>";
            }
        }

        echo json_encode([
            "referencia" => $direccion_entrega['referencia'], 
            "direccion" => $direccion_entrega['address_1'],
            "numero" => $direccion_entrega['address_2'], 
            "nombre" => '',
            "departamento_id" => $direccion_entrega['departamento'],
            "provincias" => $htmlProvincia, 
            "distritos" => $htmlDistrito,
        ]);

        
        wp_die();
    }

    function verificar_datos_checkout() {
        global $wpdb;

        if ($_POST["billing_tipodoc"] == "ruc" && $_POST["billing_comprobante"] == "Boleta" /*&& $_POST["billing_razon_social"] == ""*/) {
            wc_add_notice(__('Lo sentimos, no hace falta algún dato de facturación.'), 'error');
        }

        if (isset($_POST["billing_ckusarmismadireccion"])) {
            // Será el mismo de facturación
            $_POST["shipping_first_name"] = $_POST["field_nombre_usuario"];
            $_POST["shipping_last_name"] = $_POST["field_apellido_usuario"];
            $_POST["shipping_address_1"] = $_POST["billing_address_1"];
            $_POST["shipping_address_2"] = $_POST["billing_address_2"];

            $_POST["shipping_departamento"] = $_POST["billing_departamento"];
            $_POST["shipping_provincia"] = $_POST["billing_provincia"];
            $_POST["shipping_distrito"] = $_POST["billing_distrito"];
            $_POST["shipping_referencia"] = $_POST["billing_referencia"];
        }
    }
 

    function guardar_campos_orden($order_id, $post) {
        global $wpdb;
        $current_id = (empty(get_current_user_id())) ? 0 : get_current_user_id();

        if(!empty($current_id)){

            $user_email     = trim($_POST["field_email_usuario"]);
            $user_first     = trim($_POST["field_nombre_usuario"]);
            $user_last      = trim($_POST["field_apellido_usuario"]);
            $user_pass      = trim($_POST["field_password_usuario"]);
            $pass_confirm   = trim($_POST["field_password_confirm_usuario"]);
            $tipo_docs      = trim($_POST["tipo_documento"]);
            $num_telefono   = trim($_POST["field_telefono_usuario"]);
            $num_docs       = trim($_POST["field_documento_usuario"]);

                $messages = array();

                $errors = 0;

                if(trim($user_first) == ""){
                    // Name empty
                    $errors++;
                    $messages[] = "<li><strong>El campo Nombre no puede estar vacío</strong></li>"; 
                }
                if(trim($user_last) == ""){
                    // LastName empty
                    $errors++;
                    $messages[] = "<li><strong>El campo Apellido no puede estar vacío</strong></li>"; 
                } 
                if(!is_email($user_email)) {
                    //invalid email
                    $errors++;
                    $messages[] = "<li><strong>Correo electrónico inválido</strong></li>"; 
                    //wc_add_notice(__('Correo electrónico inválido'), 'error');
                }
                if($num_docs==""){
                    $errors++;
                    $messages[] = "<li><strong>Debe ingresar su documento de identidad</strong></li>"; 
                }
                if($num_telefono==""){
                    $errors++;
                    $messages[] = "<li><strong>Debe ingresar su teléfono</strong></li>"; 
                }

                if(trim($_POST["billing_comprobante"])=="Factura" && trim($_POST["factura_ruc"])==""){
                    $errors++;
                    $messages[] = "<li><strong>Debe ingresar el RUC para la Factura</strong></li>"; 
                }
                /*if(trim($_POST["billing_comprobante"])=="Factura" && trim($_POST["factura_razon_social"])==""){
                    $errors++;
                    $messages[] = "<li><strong>Debe ingresar la Razón Social para la Factura</strong></li>"; 
                }*/

            foreach ($messages as $value) {
                        $mess .= $value;
                    }

            $mess = "<ul class='woocommerce-error'>". $mess ."</ul>";
            if( $errors > 0){

                $arr = ["result" => "failure",
                        "messages" => $mess,
                        "refresh" => false,
                        "reload" => false
                        ];

                echo json_encode($arr);

                wp_die();
                die();

            }else{

                $direccion_id = 0;
                $objeto = array(
                    'user_id' => $current_id,
                    'direccion' => trim($_POST["billing_address_1"]),
                    'numero' => trim($_POST["billing_address_2"]),
                    'departamento_id' => intval($_POST["billing_departamento"]),
                    'provincia_id' => intval($_POST["billing_provincia"]),
                    'distrito_id' => intval($_POST["billing_distrito"]),
                    //'country' => intval($_POST["billing_departamento"]),
                    //'city' => intval($_POST["billing_provincia"]),
                    //'state' => intval($_POST["billing_distrito"]),
                    'nombre' => trim($_POST["billing_nomdireccion"]),
                    'referencia' => trim($_POST["billing_referencia"])
                );
                //if (!empty($_POST["billing_ckguardardireccion"]) && !empty($_POST["ckcbdireccion"])) {
                if (!empty($_POST["billing_ckguardardireccion"]) && !empty($_POST["cbodireccion"])) {
                    //Actualizamos
                    //echo "<pre>";var_dump(array("UNO"));exit;
                    $query = $wpdb->prepare("SELECT ID FROM " . $this->tabla_direcciones .
                        " WHERE ID = " . intval($_POST["cbodireccion"]) . " limit 1;", '');
                    $item = $wpdb->get_row($query);

                    if (!empty($item)) {
                        $objeto["ID"] = intval($_POST["cbodireccion"]);
                        $wpdb->update($this->tabla_direcciones, $objeto, array('ID' => $objeto["ID"]));
                        $direccion_id = $objeto["ID"];
                    } else {
                        wc_add_notice(__('Lo sentimos, no pudimos identificar su dirección guardada.'), 'error');
                    }

                } elseif (!empty($_POST["billing_ckguardardireccion"]) && empty($_POST["cbodireccion"])) {
                    //echo "<pre>";var_dump(array("DOS"));exit;
                    // Creamos una nueva dirección
                    $wpdb->insert($this->tabla_direcciones, $objeto);
                    $direccion_id = $wpdb->insert_id;
                } else {
                    //echo "<pre>";var_dump(array("TRES"));exit;
                }
                    //die(); 
                update_post_meta($order_id, '_billing_phone', $_POST["field_telefono_usuario"]);
                update_post_meta($order_id, '_shipping_mensaje', $_POST["shipping_mensaje"]);
                update_post_meta($order_id, '_shipping_packing_img', $_POST["packing_img"]);
                // Guardar departamento, provincia, distrito y referencia
                update_post_meta($order_id, '_shipping_address_1', $_POST["shipping_address_1"]);
                update_post_meta($order_id, '_shipping_address_2', $_POST["shipping_address_2"]);
                update_post_meta($order_id, '_shipping_departamento', $_POST["shipping_departamento"]);
                update_post_meta($order_id, '_billing_departamento', $_POST["billing_departamento"]);

                update_post_meta($order_id, '_shipping_provincia', $_POST["shipping_provincia"]);
                update_post_meta($order_id, '_billing_provincia', $_POST["billing_provincia"]);

                update_post_meta($order_id, '_shipping_distrito', $_POST["shipping_distrito"]);
                update_post_meta($order_id, '_billing_distrito', $_POST["billing_distrito"]);

                update_post_meta($order_id, '_shipping_referencia', $_POST["shipping_referencia"]);
                update_post_meta($order_id, '_billing_referencia', $_POST["billing_referencia"]);

                update_post_meta($order_id, '_billing_comprobante', $_POST["billing_comprobante"]);
                if(trim($_POST["billing_comprobante"])=="Factura"){
                    update_post_meta($order_id, '_factura_ruc', $_POST["factura_ruc"]);
                    //update_post_meta($order_id, '_factura_razon_social', $_POST["factura_razon_social"]);
                }/*else{
                    update_post_meta($order_id, '_factura_ruc', "");
                    update_post_meta($order_id, '_factura_razon_social', "");
                }*/
                if ($direccion_id > 0) {
                    update_user_meta($current_id, 'ultima_direccion', $direccion_id);
                }
            }
        }else{ 

        //Creando una cuenta si no existe y si marco el check de CREAR CUENTA
        //if (!empty($_POST["billing_ckcrearcuenta"])) {
            $fecha = new DateTime();
            $user_login     = $fecha->getTimestamp();
            $user_email     = trim($_POST["field_email_usuario"]);
            $user_first     = trim($_POST["field_nombre_usuario"]);
            $user_last      = trim($_POST["field_apellido_usuario"]);
            $user_pass      = trim($_POST["field_password_usuario"]);
            $pass_confirm   = trim($_POST["field_password_confirm_usuario"]);
            $tipo_docs      = trim($_POST["tipo_documento"]);
            $num_telefono   = trim($_POST["field_telefono_usuario"]);
            $num_docs       = trim($_POST["field_documento_usuario"]);

            $messages = array();

            $errors = 0;

            if(trim($user_first) == ""){
                // Name empty
                $errors++;
                $messages[] = "<li><strong>El campo Nombre no puede estar vacío</strong></li>"; 
            }
            if(trim($user_last) == ""){
                // LastName empty
                $errors++;
                $messages[] = "<li><strong>El campo Apellido no puede estar vacío</strong></li>"; 
            }

            if(username_exists($user_login)) {
                // Username already registered
                $errors++;
                $messages[] = "<li><strong>El nombre de usuario ya existe</strong></li>"; 
                //wc_add_notice(__('El nombre de usuario ya existe'), 'error');
            }
            if(!validate_username($user_login)) {
                // invalid username
                $errors++;
                $messages[] = "<li><strong>Nombre de usuario inválido</strong></li>"; 
                //wc_add_notice(__('Nombre de usuario inválido'), 'error');
            }
            if($user_login == '') {
                // empty username
                $errors++;
                $messages[] = "<li><strong>Ingrese un nombre de usuario</strong></li>"; 
                //wc_add_notice(__('Ingrese un nombre de usuario'), 'error');
            }
            if(!is_email($user_email)) {
                //invalid email
                $errors++;
                $messages[] = "<li><strong>Correo electrónico inválido</strong></li>"; 
                //wc_add_notice(__('Correo electrónico inválido'), 'error');
            }
            if(email_exists($user_email)) {
                //Email address already registered
                $errors++;
                $messages[] = "<li><strong>El Correo electrónico ya existe</strong></li>"; 
                //wc_add_notice(__('El Correo electrónico ya existe'), 'error');
            }
            if($user_pass == '') {
                // passwords do not match
                $errors++;
                $messages[] = "<li><strong>Ingrese una contraseña</strong></li>"; 
                //wc_add_notice(__('Ingrese una contraseña'), 'error');
            }
            if($user_pass != $pass_confirm) {
                // passwords do not match
                $errors++;
                $messages[] = "<li><strong>Las contraseñas no coinciden</strong></li>"; 
                //wc_add_notice(__('Las contraseñas no coinciden'), 'error');
            }
                if($num_docs==""){
                    $errors++;
                    $messages[] = "<li><strong>Debe ingresar su documento de identidad</strong></li>"; 
                }
                if($num_telefono==""){
                    $errors++;
                    $messages[] = "<li><strong>Debe ingresar su teléfono</strong></li>"; 
                }


                if(trim($_POST["billing_comprobante"])=="Factura" && trim($_POST["factura_ruc"])==""){
                    $errors++;
                    $messages[] = "<li><strong>Debe ingresar el RUC para la Factura</strong></li>"; 
                }
                /*if(trim($_POST["billing_comprobante"])=="Factura" && trim($_POST["factura_razon_social"])==""){
                    $errors++;
                    $messages[] = "<li><strong>Debe ingresar la Razón Social para la Factura</strong></li>"; 
                }*/

            foreach ($messages as $value) {
                        $mess .= $value;
                    }

            $mess = "<ul class='woocommerce-error'>". $mess ."</ul>";

            if( $errors > 0){

                $arr = ["result" => "failure",
                        "messages" => $mess,
                        "refresh" => false,
                        "reload" => false
                        ];

                echo json_encode($arr);

                wp_die();
                die();

            } 

            else{
            // only create the user in if there are no errors
            //if($errors==0) {

                $new_user_id = wp_insert_user(array(
                        'user_login'        => $user_login,
                        'user_pass'         => $user_pass,
                        'user_email'        => $user_email,
                        'first_name'        => $user_first,
                        'last_name'         => $user_last,
                        'user_registered'   => date('Y-m-d H:i:s'),
                        'role'              => 'subscriber'
                    )
                ); 
                add_user_meta( $new_user_id, 'tipo_documento', $tipo_docs );
                add_user_meta( $new_user_id, 'numero_documento', $num_docs );
                add_user_meta( $new_user_id, 'numero_telefono', $num_telefono );

                if($new_user_id) {
                    $user = get_user_by('email', $user_email);

                    // Redirect URL //
                    if ( !is_wp_error( $user ) )
                    {
                        wp_clear_auth_cookie();
                        wp_set_current_user ( $user->ID );
                        wp_set_auth_cookie  ( $user->ID ); 
                    }
                }

                $direccion_id = 0;
                $objeto = array(
                    'user_id' => $new_user_id,
                    'direccion' => trim($_POST["billing_address_1"]),
                    'numero' => trim($_POST["billing_address_2"]),
                    'departamento_id' => intval($_POST["billing_departamento"]),
                    'provincia_id' => intval($_POST["billing_provincia"]),
                    'distrito_id' => intval($_POST["billing_distrito"]),
                    //'country' => intval($_POST["billing_departamento"]),
                    //'city' => intval($_POST["billing_provincia"]),
                    //'state' => intval($_POST["billing_distrito"]),
                    'nombre' => trim($_POST["billing_nomdireccion"]),
                    'referencia' => trim($_POST["billing_referencia"]),
                );
                if (!empty($_POST["billing_ckguardardireccion"]) && !empty($_POST["ckcbdireccion"])) {
                    //Actualizamos
                    //echo "<pre>";var_dump(array("UNO"));exit;
                    $query = $wpdb->prepare("SELECT ID FROM " . $this->tabla_direcciones .
                        " WHERE ID = " . intval($_POST["cbodireccion"]) . " limit 1;", '');
                    $item = $wpdb->get_row($query);

                    if (!empty($item)) {
                        $objeto["ID"] = intval($_POST["cbodireccion"]);
                        $wpdb->update($this->tabla_direcciones, $objeto, array('ID' => $objeto["ID"]));
                        $direccion_id = $objeto["ID"];
                    } else {
                        wc_add_notice(__('Lo sentimos, no pudimos identificar su dirección guardada.'), 'error');
                    }

                } elseif (!empty($_POST["billing_ckguardardireccion"]) && empty($_POST["ckcbdireccion"])) {
                    //echo "<pre>";var_dump(array("DOS"));exit;
                    // Creamos una nueva dirección
                    $wpdb->insert($this->tabla_direcciones, $objeto);
                    $direccion_id = $wpdb->insert_id;
                } else {
                    //echo "<pre>";var_dump(array("TRES"));exit;
                }

                update_post_meta($order_id, '_billing_phone', $_POST["field_telefono_usuario"]);
                update_post_meta($order_id, '_shipping_mensaje', $_POST["shipping_mensaje"]);
                update_post_meta($order_id, '_shipping_packing_img', $_POST["packing_img"]);
                // Guardar departamento, provincia, distrito y referencia
                update_post_meta($order_id, '_shipping_address_1', $_POST["shipping_address_1"]);
                update_post_meta($order_id, '_shipping_address_2', $_POST["shipping_address_2"]);
                update_post_meta($order_id, '_shipping_departamento', $_POST["shipping_departamento"]);
                update_post_meta($order_id, '_billing_departamento', $_POST["billing_departamento"]);

                update_post_meta($order_id, '_shipping_provincia', $_POST["shipping_provincia"]);
                update_post_meta($order_id, '_billing_provincia', $_POST["billing_provincia"]);

                update_post_meta($order_id, '_shipping_distrito', $_POST["shipping_distrito"]);
                update_post_meta($order_id, '_billing_distrito', $_POST["billing_distrito"]);

                update_post_meta($order_id, '_shipping_referencia', $_POST["shipping_referencia"]);
                update_post_meta($order_id, '_billing_referencia', $_POST["billing_referencia"]);
                update_post_meta($order_id, '_billing_comprobante', $_POST["billing_comprobante"]);
                if(trim($_POST["billing_comprobante"])=="Factura"){
                    update_post_meta($order_id, '_factura_ruc', $_POST["factura_ruc"]);
                    //update_post_meta($order_id, '_factura_razon_social', $_POST["factura_razon_social"]);
                }/*else{
                    update_post_meta($order_id, '_factura_ruc', "");
                    update_post_meta($order_id, '_factura_razon_social', "");
                }*/


                if ($direccion_id > 0) {
                    update_user_meta($current_id, 'ultima_direccion', $direccion_id);
                }
            }
        }
    }

    function get_data_provincia() {
        global $wpdb;
        $id = absint($_POST['id']);
        $seleccionado = "";
        $html = "<option value=''>Seleccione</option>";
        if (!empty($id)) {
            $query = $wpdb->prepare("select idProv as ID, provincia as nombre from " .
                $this->tabla_provincia . " where idDepa = " . intval($id), '');
            $resultado = $wpdb->get_results($query);
            if (!empty($resultado)) {
                foreach ($resultado as $itemProvincia) {
                    $selected = (!empty($seleccionado) && $seleccionado == $itemProvincia->ID) ? "selected = true" : "";
                    $html .= "<option value='" . $itemProvincia->ID . "' " . $selected . ">" . $itemProvincia->nombre . "</option>";
                }
            }
        }
        echo $html;
        wp_die();
    }

    function get_data_distrito() {
        global $wpdb;
        $id = absint($_POST['id']);
        $seleccionado = "";
        $html = "<option value=''>Seleccione</option>";
        if (!empty($id)) {
            $query = $wpdb->prepare("select idDist as ID, distrito as nombre from " .
                $this->tabla_distrito . " where idProv = " . intval($id), '');
            $resultado = $wpdb->get_results($query);
            if (!empty($resultado)) {
                foreach ($resultado as $itemDistrito) {
                    $selected = (!empty($seleccionado) && $seleccionado == $itemDistrito->ID) ? "selected = true" : "";
                    $html .= "<option value='" . $itemDistrito->ID . "' " . $selected . ">" . $itemDistrito->nombre . "</option>";
                }
            }
        }
        echo $html;
        wp_die();
    }

    function custom_data_user() {
        if ( is_user_logged_in() ) {
            $current_id = wp_get_current_user();
            $user_id    = $current_id->ID;
            $nombre     = $current_id->user_firstname;
            $apellido   = $current_id->user_lastname;
            $correo     = $current_id->user_email;
            $tipo_doc   = get_user_meta( $user_id, 'tipo_documento', true );
            $nro_doc   = get_user_meta( $user_id, 'numero_documento', true );
            $telefono   = get_user_meta( $user_id, 'numero_telefono', true );
        }
        ?>
        <div class="clearfix">
            <p class="form-row form-row-first validate-required">
                <label for="field_nombre_usuario" class="">Nombre <abbr class="required" title="obligatorio">*</abbr></label>
                <input type="text" class="input-text form-control" name="field_nombre_usuario" id="field_nombre_usuario" value="<?php echo $nombre; ?>">
            </p>

            <p class="form-row form-row-last validate-required">
                <label for="field_apellido_usuario" class="">Apellido <abbr class="required" title="obligatorio">*</abbr></label>
                <input type="text" class="input-text form-control" name="field_apellido_usuario" id="field_apellido_usuario" value="<?php echo $apellido; ?>">
            </p>

            <p class="form-row validate-required radio_personalizado inverso">
                <label class="d-inline w-auto">
                    <input type="radio" name="tipo_documento" value="DNI" <?php if( $tipo_doc == 'DNI' ){ echo 'checked'; }elseif( $tipo_doc != 'Carné de extranjería' ){ echo 'checked'; }else{ echo ''; } ?>>
                    <span><small><?php _e( 'DNI', 'b4st' ); ?></small></span>
                </label>

                <label class="d-inline w-auto">
                    <input type="radio" name="tipo_documento" value="Carné de extranjería" <?php if( $tipo_doc == 'Carné de extranjería' ){ echo 'checked'; }else{ echo ''; } ?>>
                    <span><small><?php _e( 'Carné de extranjería', 'b4st' ); ?></small></span>
                </label>
            </p>

            <p class="form-row form-row-last validate-required">
                <label for="field_documento_usuario" class="">Documento <abbr class="required" title="obligatorio">*</abbr></label>
                <input type="text" class="input-text form-control" name="field_documento_usuario" id="field_documento_usuario" value="<?php echo $nro_doc; ?>">
            </p>

            <p class="form-row form-row-last validate-required">
                <label for="field_email_usuario" class="">Correo electrónico <abbr class="required" title="obligatorio">*</abbr></label>
                <input type="text" class="input-text form-control" name="field_email_usuario" id="field_email_usuario" value="<?php echo $correo; ?>">
            </p>
            <?php if(empty($current_id)){ ?>
            <p class="form-row form-row-last validate-required">
                <label for="field_password_usuario" class="">Contraseña <abbr class="required" title="obligatorio">*</abbr></label>
                <input type="password" class="input-text form-control" name="field_password_usuario" id="field_password_usuario" value="">
            </p>

            <p class="form-row form-row-last validate-required">
                <label for="field_password_confirm_usuario" class="">Confirmar Contraseña <abbr class="required" title="obligatorio">*</abbr></label>
                <input type="password" class="input-text form-control" name="field_password_confirm_usuario" id="field_password_confirm_usuario" value="">
            </p>
            <?php } ?>
            <p class="form-row form-row-last validate-required">
                <label for="field_telefono_usuario" class="">Teléfono <abbr class="required" title="obligatorio">*</abbr></label>
                <input type="text" class="input-text form-control" name="field_telefono_usuario" id="field_telefono_usuario" value="<?php echo $telefono; ?>">
            </p>
        </div>
        <?php
    }

    function add_fields_before_billing() {
        $current_id = get_current_user_id();
        ?>
        <?php if (!empty($current_id)) { ?>
        <p class="form-row form-row-wide mt-3">
            <label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
                <input class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox"
                       id="ckcbdireccion" type="checkbox" name="ckcbdireccion" value="0" >
                <span><?php _e('Selecciona una dirección guardada', 'woocommerce'); ?></span>
            </label>
        </p>
        <p class="form-row form-row-wide" id="panel_cbodireccion">
            <?php
            global $wpdb;
            $query = $wpdb->prepare("select ID, nombre from " . $this->tabla_direcciones . " where user_id = " . $current_id, '');
            $resultado = $wpdb->get_results($query);
            // Se agrega la direccion del regalo

            ?>
            <select id="cbodireccion" name="cbodireccion" class="form-control custom-select">
                <?php
                if ( !empty($resultado) ) {
                    $cnt = 0;
                    ?>
                    <option value="" disabled="disabled">Seleccione</option>
                    <?php
                    foreach ($resultado as $itemProvincia) {
                        ?>
                        <option value="<?php echo $itemProvincia->ID; ?>" <?php echo ( $cnt == 0 ) ? 'selected' : ''; ?>>
                            <?php echo $itemProvincia->nombre ?></option>
                        <?php
                        $cnt++;
                    }
                }
                ?>
            </select>
        </p>
        <p id="p_nueva_direccion" class="form-row mt-3 mb-3">
            <a id="ingresar_nueva_direccion" href="#" class="d-inline mx-auto"><u><?php _e( 'O ingresar una dirección nueva', 'b4st' ); ?></u></a>
        </p>
    <?php } ?>

        <?php
    }
}
new beta_custom_checkout();