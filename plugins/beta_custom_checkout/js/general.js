/**
 * Created by MODO BETA on 15/11/2017.
 */

jQuery(function ($) {
    //$("#billing_departamento").val("");
    //$("#shipping_departamento").val("");
    if($("#hidden_billing_departamento").val()!=""){
       //$("#selected_billing_departamento").val($("#billing_departamento option:selected").text());
        $('#customer_details').block({
            message: null,
            overlayCSS: {
                background: '#fff',
                backgroundSize: '16px 16px',
                opacity: 0.6
            }
        });

        var data = {
            action: "get_data_provincia",
            id: $("#hidden_billing_departamento").val(),
        };

        jQuery.ajax({
            type: "POST",
            url: base_url() + "wp-admin/admin-ajax.php",
            data: data,
            success: function (html) {
                $("#billing_provincia").html(html);
                jQuery('#customer_details').unblock();
                $('#billing_provincia option[value="'+$("#hidden_billing_provincia").val()+'"]').prop("selected", true);
                //$("#selected_billing_provincia").val($("#billing_provincia option:selected").text());

                //Ahora cargar distritos
                $('#customer_details').block({
                    message: null,
                    overlayCSS: {
                        background: '#fff',
                        backgroundSize: '16px 16px',
                        opacity: 0.6
                    }
                });

                var data = {
                    action: "get_data_distrito",
                    id: $("#hidden_billing_provincia").val(),
                };

                jQuery.ajax({
                    type: "POST",
                    url: base_url() + "wp-admin/admin-ajax.php",
                    data: data,
                    success: function (html) {
                        $("#billing_distrito").html(html);
                        jQuery('#customer_details').unblock();
                        $('#billing_distrito option[value="'+$("#hidden_billing_distrito").val()+'"]').prop("selected", true);
                        //$("#selected_billing_distrito").val($("#billing_distrito option:selected").text());
                    },
                    error: function (msg) {
                        console.log(msg.statusText);
                        console.log('No se pudo obtener datos');
                        jQuery('#customer_details').unblock();
                    }
                }); 
            },
            error: function (msg) {
                console.log(msg.statusText);
                console.log('No se pudo obtener datos');
                jQuery('#customer_details').unblock();
            }
        });

        var data = {
            action: "get_data_provincia",
            id: $("#hidden_billing_departamento").val(),
        };

        jQuery.ajax({
            type: "POST",
            url: base_url() + "wp-admin/admin-ajax.php",
            data: data,
            success: function (html) {
                $("#shipping_provincia").html(html);
                jQuery('#customer_details').unblock();
                $('#shipping_provincia option[value="'+$("#hidden_billing_provincia").val()+'"]').prop("selected", true);
                //$("#selected_billing_provincia").val($("#billing_provincia option:selected").text());

                //Ahora cargar distritos
                $('#customer_details').block({
                    message: null,
                    overlayCSS: {
                        background: '#fff',
                        backgroundSize: '16px 16px',
                        opacity: 0.6
                    }
                });

                var data = {
                    action: "get_data_distrito",
                    id: $("#hidden_billing_provincia").val(),
                };

                jQuery.ajax({
                    type: "POST",
                    url: base_url() + "wp-admin/admin-ajax.php",
                    data: data,
                    success: function (html) {
                        $("#shipping_distrito").html(html);
                        jQuery('#customer_details').unblock();
                        $('#shipping_distrito option[value="'+$("#hidden_billing_distrito").val()+'"]').prop("selected", true);
                        //$("#selected_billing_distrito").val($("#billing_distrito option:selected").text());
                    },
                    error: function (msg) {
                        console.log(msg.statusText);
                        console.log('No se pudo obtener datos');
                        jQuery('#customer_details').unblock();
                    }
                }); 
            },
            error: function (msg) {
                console.log(msg.statusText);
                console.log('No se pudo obtener datos');
                jQuery('#customer_details').unblock();
            }
        });
    }

    // check if address exists to logged in user, don't show detail
    if ($('#cbodireccion').length && $('#cbodireccion').has('option').length > 0 && !$('#giftlist_address').length && !$('#giftlist_address').val().length){
        $('.woocommerce-billing-fields__field-wrapper').addClass('hidden');
    } else {
        // hides pre selected address
        $("#panel_cbodireccion").addClass("hidden");
        $("#panel_cbodireccion").fadeOut();
    }

    jQuery(document).on("click", "#ingresar_nueva_direccion", function (e) {
        e.preventDefault();
        $("#cbodireccion").prop("checked", false);
        $("#cbodireccion").attr("value", 0);
        $("#billing_address_1").val("");
        $("#billing_address_2").val("");
        $("#billing_departamento").val("");
        $("#billing_referencia").val("");
        $("#billing_nomdireccion").val("Mi dirección");
        $("#billing_provincia").html('<option value="">Seleccione</option>');
        $("#billing_distrito").html('<option value="">Seleccione</option>');
        $(".woocommerce-billing-fields__field-wrapper").css("display", "block");
        // hides pre selected address
        $("#panel_cbodireccion").addClass("hidden");
        $("#panel_cbodireccion").fadeOut();
    });

    jQuery(document).on("click", "#ckcbdireccion", function (e) {
        if (!$('#ckcbdireccion').is(':checked')) {
            $(this).attr("value", 0);
            $("#billing_address_1").val("");
            $("#billing_address_2").val("");
            $("#billing_departamento").val("");
            $("#billing_referencia").val("");
            $("#billing_nomdireccion").val("Mi dirección");
            $("#billing_provincia").html('<option value="">Seleccione</option>');
            $("#billing_distrito").html('<option value="">Seleccione</option>');
            $(".woocommerce-billing-fields__field-wrapper").css("display", "block");
            $("#p_nueva_direccion").hide();
        } else {
            $(this).attr("value", 1);
            $(".woocommerce-billing-fields__field-wrapper").css("display", "none");
            $("#p_nueva_direccion").show();
        }

        if ($("#panel_cbodireccion").hasClass("hidden")) {
            $("#cbodireccion").val("");
            $("#panel_cbodireccion").removeClass("hidden");
            $("#panel_cbodireccion").fadeIn();
        } else {
            $("#panel_cbodireccion").addClass("hidden");
            $("#panel_cbodireccion").fadeOut();
        }
    });

    $(document).on('click', '#p_nueva_direccion', function () {
        $(this).hide();
        $('#ckcbdireccion').prop('checked', false);
    });

    /*if ($("#billing_ckguardardireccion").val() == 1) {
     $("#billing_nomdireccion_field").removeClass("hidden");
     $("#billing_nomdireccion_field").fadeIn();
     }*/

    jQuery('#billing_ckguardardireccion').val("0");
    jQuery('#billing_ckguardardireccion').removeAttr("checked");
    jQuery(document).on("click", "#billing_ckguardardireccion", function (e) {
        if ($("#billing_nomdireccion_field").hasClass("hidden")) {
            $("#billing_nomdireccion_field").removeClass("hidden");
            $("#billing_nomdireccion_field").fadeIn();
            $(this).attr("value", 1);
        } else {
            $(this).attr("value", 0);
            $("#billing_nomdireccion_field").addClass("hidden");
            $("#billing_nomdireccion_field").fadeOut();
        }

    });

    $(document).on("click", "a[href=#finish]", function (e) {
        
        var form = $("#checkout").show();
        $( "input[name=comprobante_boleta]" ).rules( "add", {required: true});
        if(form.valid()){
            form.submit();
        }
    })
    $(document).on("click", "label[data-toggle=collapse]", function (e) {
        e.preventDefault();
        //var niveles = $(this).closest(".contenido").find(".niveles");
        //$(this).attr("checked", true);
        //$(niveles).slideDown();
        //var primer = $(niveles).find("input[name=rbpacking]");
        var checkpadre = "" + $(this).find('.rbpacking_padre');
        var inputcheck = $(this).find("input");
        $(inputcheck).prop("checked", true);
        $(inputcheck).attr("checked", true);

        // Quitar si no se quiere que se seleccione el primero
        var primer = $(this).parent().find("input[class=ckpacking_nivel2]");
        console.log(primer[0]);
        $(primer[0]).trigger("click");
        $("input[name=rbpacking]").prop("checked", false);
        $("input[name=rbpacking]").removeAttr("checked");
    });
    /*$(document).on("click", "input[name=payment_method]", function (e) {
        e.preventDefault();
        $("input[name=payment_method]").attr("checked", false);
        //$("input[name=payment_method]").removeAttr("checked");
        $(this).attr("checked", true);  
    });*/

    jQuery(document).on("click", ".ckpacking_nivel2", function (e) {
        //$(this).prop("checked", true);
        //$(this).prop("checked", true);
        jQuery('body').trigger('update_checkout');

    });

    jQuery(document).on("change", "#ship-to-different-address-checkbox", function (e) {
        if ($('#ship-to-different-address-checkbox').is(':checked')) {
            //$("#billing_razon_social_field").addClass("validate-required");
            $('label[for="billing_razon_social"]').append('<abbr class="required" title="obligatorio">*</abbr>')
        }
    });

    $('#billing_ckusarmismadireccion').prop('checked', true);

    jQuery(document).on("change", "#billing_ckusarmismadireccion", function (e) {
        if ($('#billing_ckusarmismadireccion').is(':checked')) {
            //$("#billing_razon_social_field").addClass("validate-required");
            $('.woocommerce-shipping-fields').css("display","none");
        }else{
            $('.woocommerce-shipping-fields').css("display","block");
        }
    });
        if ($('#billing_ckusarmismadireccion').is(':checked')) {
            //$("#billing_razon_social_field").addClass("validate-required");
            $('.woocommerce-shipping-fields').css("display","none");
        }else{
            $('.woocommerce-shipping-fields').css("display","block");
        }

    var typingTimer;                //timer identifier
    var doneTypingInterval = 2000;  //time in ms, 5 second for example
    var $input = $("#billing_cupon");

    $(document).on('keyup input', '#billing_cupon', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
        console.log('keyup');
    });

    $(document).on('keydown input', '#billing_cupon', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
        console.log('keydown');
    });

    //user is "finished typing," do something
    function doneTyping () {
      //do something
      $form = jQuery("#checkout");
        var data = {
                security:       wc_checkout_params.apply_coupon_nonce,
                coupon_code:    $form.find( 'input[name="billing_cupon"]' ).val()
            };

            $.ajax({
                type:       'POST',
                url:        wc_checkout_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'apply_coupon' ),
                data:       data,
                success:    function( code ) {
                    $( '.woocommerce-error, .woocommerce-message' ).remove();
                    $form.removeClass( 'processing' ).unblock();

                    if ( code ) {
                        $form.before( code );
                        //$form.slideUp();

                        $( document.body ).trigger( 'update_checkout', { update_shipping_method: false } );
                    }
                },
                dataType: 'html'
            });
    } 

    jQuery(document).on("change", "#billing_departamento", function (e) {
        $('#customer_details').block({
            message: null,
            overlayCSS: {
                background: '#fff',
                backgroundSize: '16px 16px',
                opacity: 0.6
            }
        });

        var data = {
            action: "get_data_provincia",
            id: $(this).val(),
        };

        jQuery.ajax({
            type: "POST",
            url: base_url() + "wp-admin/admin-ajax.php",
            data: data,
            success: function (html) {
                $("#billing_provincia").html(html);
                jQuery('#customer_details').unblock();
            },
            error: function (msg) {
                console.log(msg.statusText);
                console.log('No se pudo obtener datos');
                jQuery('#customer_details').unblock();
            }
        });

    });

    jQuery(document).on("change", "#shipping_departamento, #direccion_entrega_departamento", function (e) {
        $('#customer_details').block({
            message: null,
            overlayCSS: {
                background: '#fff',
                backgroundSize: '16px 16px',
                opacity: 0.6
            }
        });

        var data = {
            action: "get_data_provincia",
            id: $(this).val(),
        };

        jQuery.ajax({
            type: "POST",
            url: base_url() + "wp-admin/admin-ajax.php",
            data: data,
            success: function (html) {
                $("#shipping_provincia").html(html);
                $('#shipping_provincia').trigger('wc.gift-ideas.loaded.provincias');
                $("#direccion_entrega_provincia").html(html);
                jQuery('#customer_details').unblock();
            },
            error: function (msg) {
                console.log(msg.statusText);
                console.log('No se pudo obtener datos');
                jQuery('#customer_details').unblock();
            }
        });

    });

    jQuery(document).on("change", "#shipping_provincia, #direccion_entrega_provincia", function (e) {
        $('#customer_details').block({
            message: null,
            overlayCSS: {
                background: '#fff',
                backgroundSize: '16px 16px',
                opacity: 0.6
            }
        });

        var data = {
            action: "get_data_distrito",
            id: $(this).val(),
        };

        jQuery.ajax({
            type: "POST",
            url: base_url() + "wp-admin/admin-ajax.php",
            data: data,
            success: function (html) {
                $("#shipping_distrito").html(html);
                $('#shipping_distrito').trigger('wc.gift-ideas.loaded.distritos');
                $("#direccion_entrega_distrito").html(html);
                jQuery('#customer_details').unblock();
            },
            error: function (msg) {
                console.log(msg.statusText);
                console.log('No se pudo obtener datos');
                jQuery('#customer_details').unblock();
            }
        });

    });

    jQuery(document).on("change", "#billing_provincia", function (e) {
        $('#customer_details').block({
            message: null,
            overlayCSS: {
                background: '#fff',
                backgroundSize: '16px 16px',
                opacity: 0.6
            }
        });

        var data = {
            action: "get_data_distrito",
            id: $(this).val(),
        };

        jQuery.ajax({
            type: "POST",
            url: base_url() + "wp-admin/admin-ajax.php",
            data: data,
            success: function (html) {
                $("#billing_distrito").html(html);
                jQuery('#customer_details').unblock();
            },
            error: function (msg) {
                console.log(msg.statusText);
                console.log('No se pudo obtener datos');
                jQuery('#customer_details').unblock();
            }
        });
    });

    jQuery(document).on("change", ".ckpacking_nivel2", function () {
        var valor_packing = $(this).val();
        var avalor = valor_packing.split("|");
        var imagen = "";
        $("input[data-padre=" + avalor[2] + "]").each(function (index, element) {
            if ($(this).is(':checked')) {
                var vp = $(this).val();
                var vpvalor = vp.split("|");
                if (("" + vpvalor[3]) != "undefined") {
                    imagen += vpvalor[3] + "|";
                }
            }
        });
        $("#packing_img").val(imagen);
    });

    jQuery(document).on("change", "input[name=rbpacking]", function () {
        $("input[name=rbpacking_padre]").prop("checked", false);
        $("input[name=rbpacking_padre]").removeAttr("checked");
        $("input[class=ckpacking_nivel2]").prop("checked", false);
        $("input[class=ckpacking_nivel2]").removeAttr("checked");
        var valor_packing = $(this).val();
        var avalor = valor_packing.split("|");
        if (("" + avalor[3]) !== "undefined") {
            $("#packing_img").val(avalor[3]);
        } else {
            $("#packing_img").val("");
        }

        jQuery('body').trigger('update_checkout');
    });
    
    jQuery(document).on("change", "input[name=payment_method]", function () {
        $("input[name=payment_method]").removeAttr("checked");
        if($(this).attr("checked")===true){
            $(this).removeAttr("checked");
        }else{
            $(this).attr("checked", true);
        }
    });
    

    
    /*jQuery(document).on("change", ".radio_packing", function (e) {
        e.preventDefault();
        alert("CLICK");
        $(".radio_packing").removeAttr("checked");
        if($(this).attr("checked")==true){
            $(this).removeAttr("checked");
        }else{
            $(this).attr("checked", true);
        }
    });*/

    
    jQuery(document).on("change", "input.shipping_method", function () {
        $("input.shipping_method").removeAttr("checked");
        if($(this).attr("checked")===true){
            $(this).removeAttr("checked");
        }else{
            $(this).attr("checked", true);
        }
    });

    //$("input[name=billing_comprobante]:first").attr('checked', true);
    jQuery(document).on("change", "input[name=billing_comprobante]", function () {
        /*$("input[name=billing_comprobante]").removeAttr("checked");
        if($(this).attr("checked")==true){
            $(this).removeAttr("checked");
        }else{
            $(this).attr("checked", true);
        }*/
        if ($(this).val() == "Factura") {
            //$("#billing_tipodoc").val("ruc");
            //$("#billing_razon_social_field").removeClass("hidden");
            //$("#billing_razon_social_field").addClass("validate-required");
            //$('label[for="billing_razon_social"]').append('<abbr class="required" title="obligatorio">*</abbr>')
            $('#campos_factura').show();
        } else {
            //$("#billing_tipodoc").val("dni");
            //$("#billing_razon_social_field").addClass("hidden");
            //$("#billing_razon_social_field").removeClass("validate-required");
            //$('label[for="billing_razon_social"]').find(".required").remove();
            $('#campos_factura').hide();
        }
    });
        if ($(this).val() === "Factura") {
            $('#campos_factura').show();
        } else {
            $('#campos_factura').hide();
        }

    $(".create-account").hide();

    $(document).on("click", "a[href=#next]", function () {
        console.log("*****************************");
        var valor = $("#packing_img").val();
        console.log(valor);
        if ($(".pluginator-wizard-step-view-order").is(':visible')) {
            var id = $(".pluginator-wizard-step-view-order").attr("id");
            if ($.trim(valor) == "") {
                $("a[aria-controls=" + id + "]").parent().addClass("error")
                alert("Debe seleccionar un Packing");
                $("input[name=rbpacking]").parent().parent().css("color", "#a00");
                $(".contenido").css("color", "#a00")
                $("a[href=#previous]").trigger("click");
            } else {


            }
        }
    });

    jQuery(document).on("change", "#billing_tipodoc", function () {
        if ($(this).val() == "ruc") {
            $("#comprobante_factura").trigger("click");
            //$("#billing_razon_social_field").removeClass("hidden");
            //$("#billing_razon_social_field").addClass("validate-required");
            $('label[for="billing_razon_social"]').append('<abbr class="required" title="obligatorio">*</abbr>')
        } else {
            $("#comprobante_boleta").trigger("click");
            //$("#billing_razon_social_field").addClass("hidden");
            //$("#billing_razon_social_field").removeClass("validate-required");
            $('label[for="billing_razon_social"]').find(".required").remove();
        }
    });

    jQuery(document).on("change", "#cbodireccion", function (e) {
        get_primera_direccion($(this).val());
    });

    if ($("#cbodireccion").val()) {
        get_primera_direccion($("#cbodireccion").val());
    }
});

function get_primera_direccion(valor) {
    $('#customer_details').block({
        message: null,
        overlayCSS: {
            background: '#fff',
            backgroundSize: '16px 16px',
            opacity: 0.6
        }
    });
    var data = {};
    if ($('#giftlist_address').length) {
        data = {
            action: "get_data_direccion_giftlist",
        };
    } else {
        data = {
            action: "get_data_direccion",
            id: valor,
        };
    }
    
    if (valor === "") {
        $("#billing_address_1").val("");
        $("#billing_address_2").val("");
        $("#billing_departamento").val("");
        $("#billing_referencia").val("");
        $("#billing_nomdireccion").val("Mi dirección");
        $("#billing_provincia").html('<option value="">Seleccione</option>');
        $("#billing_distrito").html('<option value="">Seleccione</option>');
        jQuery('#customer_details').unblock();
        return false;
    }
    jQuery.ajax({
        type: "POST",
        url: base_url() + "wp-admin/admin-ajax.php",
        data: data,
        success: function (json) {
            var obj = JSON.parse(json);
            $("#billing_address_1").val(obj.direccion);
            $("#billing_address_2").val(obj.numero);
            $("#billing_referencia").val(obj.referencia);
            $("#billing_departamento").val(obj.departamento_id);
            $("#billing_nomdireccion").val(obj.nombre);
            $("#billing_provincia").html(obj.provincias);
            $("#billing_distrito").html(obj.distritos);
            jQuery('#customer_details').unblock();
        },
        error: function (msg) {
            console.log(msg.statusText);
            console.log('No se pudo obtener datos');
        }
    });
}

function get_provincias(valor) {

    var data = {
        action: "get_data_provincia",
        default: valor,
        id: $(this).val(),
    };
    jQuery.ajax({
        type: "POST",
        url: base_url() + "wp-admin/admin-ajax.php",
        data: data,
        success: function (html) {
            $("#billing_provincia").html(html);
        },
        error: function (msg) {
            console.log(msg.statusText);
            alert('No se pudo obtener datos');
        }
    });
}

function get_distritos(valor) {
    var data = {
        action: "get_data_distrito",
        default: valor,
        id: $(this).val(),
    };
    jQuery.ajax({
        type: "POST",
        url: base_url() + "wp-admin/admin-ajax.php",
        data: data,
        success: function (html) {
            $("#billing_distrito").html(html);
        },
        error: function (msg) {
            console.log(msg.statusText);
            console.log('No se pudo obtener datos');
            jQuery('#customer_details').unblock();
        }
    });
}

function base_url() {

    return $("#base_url").val() + "/";

}