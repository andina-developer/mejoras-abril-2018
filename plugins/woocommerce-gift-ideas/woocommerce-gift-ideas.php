<?php
/**
 * WooCommerce Gitf Ideas
 *
 * Listas de ideas de regalos, enlazadas a una fecha de cumpleaños y dirección de envío.
 * Con funcionalidad de compartir.
 *
 * @link              http://modobeta.pe
 * @since             1.0.0
 * @package           Woocommerce_Gift_Ideas
 *
 * @wordpress-plugin
 * Plugin Name:          WooCommerce Gift Ideas
 * Plugin URI:           http://modobeta.pe
 * Description:          Listas de ideas de regalos, enlazadas a una fecha de cumpleaños y dirección de envío.
 * Tags:                 woocommerce, e-commerce, ecommerce, gift, holidays, present, giftwrap, wrapping
 * Version:              1.0.0
 * Author:               Fernando Paz
 * Author URI:           http://modobeta.pe
 * License:              Private
 * License URI:          http://modobeta.pe
 * Text Domain:          woocommerce-gift-ideas
 * Domain Path:          /languages
 * Requires at least:    4.8
 * Tested up to:         4.8
 * WC requires at least: 3.2
 * WC tested up to:      3.2
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-woocommerce-gift-ideas-activator.php
 */
function activate_woocommerce_gift_ideas() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-woocommerce-gift-ideas-activator.php';
	Woocommerce_Gift_Ideas_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-woocommerce-gift-ideas-deactivator.php
 */
function deactivate_woocommerce_gift_ideas() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-woocommerce-gift-ideas-deactivator.php';
	Woocommerce_Gift_Ideas_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_woocommerce_gift_ideas' );
register_deactivation_hook( __FILE__, 'deactivate_woocommerce_gift_ideas' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-woocommerce-gift-ideas.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_woocommerce_gift_ideas() {

	$plugin = new Woocommerce_Gift_Ideas();
	$plugin->run();

}

/**
 * Check if WooCommerce is active
 */
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ), true ) ) {
	define( 'WOOCOMMERCE_GIFT_IDEAS', '1.0.0' );
	define( 'WOOCOMMERCE_GIFT_IDEAS_PATH', plugin_dir_path( __FILE__ ) );
	define( 'WOOCOMMERCE_GIFT_IDEAS_URL', plugin_dir_url( __FILE__ ) );
	run_woocommerce_gift_ideas();
}
