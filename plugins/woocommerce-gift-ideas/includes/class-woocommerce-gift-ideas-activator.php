<?php
/**
 * Fired during plugin activation
 *
 * @link       http://modobeta.pe
 * @since      1.0.0
 *
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/includes
 */

if ( ! defined( 'ABSPATH' ) ) {
	// Exit if accessed directly.
	exit;
}

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/includes
 * @author     Fernando Paz <info@beta.pe>
 */
class Woocommerce_Gift_Ideas_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
