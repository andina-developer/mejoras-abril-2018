<?php
/**
 * Funciones globales de gift ideas.
 *
 * Date: 10/22/17
 * Time: 11:10 AM
 *
 * @link       http://modobeta.pe
 * @since      1.0.0
 * @author     Modo Beta SAC <info@beta.pe>
 *
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/includes
 */

if ( ! function_exists( 'wc_gift_ideas_get_giftlist' ) ) {

	/**
	 * Obtener la lista actual en la pagina solicitada.
	 *
	 * @param string|int|null $gift_list_id Id o codigo de la lista a buscar.
	 *
	 * @return null|Woocommerce_Gift_Ideas_Giftlist|Woocommerce_Gift_Ideas_Giftlist[]
	 */
	function wc_gift_ideas_get_giftlist( $gift_list_id = null ) {
		$giftlist_manager = Woocommerce_Gift_Ideas::get_public_instance()->get_giftlist_manager();
		$gift_list        = null;
		if ( empty( $gift_list_id ) ) {
			$gift_list_id = get_query_var( 'wcglID', null );
		}

		if ( ! empty( $gift_list_id ) ) {
			$gift_list = wc_gift_ideas_get_giftlist_by_codigo_id( $gift_list_id );
		} elseif ( is_user_logged_in() ) {
			$gift_list = $giftlist_manager->obtener_giftlist_usuario( get_current_user_id() );
		}

		return $gift_list;
	}
}

if ( ! function_exists( 'wc_gift_ideas_get_giftlist_by_codigo_id' ) ) {

	/**
	 * Obtener la lista por id o codigo.
	 *
	 * @param string|int $gift_list_id Id o codigo de la lista a buscar.
	 *
	 * @return null|Woocommerce_Gift_Ideas_Giftlist
	 */
	function wc_gift_ideas_get_giftlist_by_codigo_id( $gift_list_id = null ) {
		$giftlist_manager = Woocommerce_Gift_Ideas::get_public_instance()->get_giftlist_manager();
		$gift_list        = null;
		if ( is_integer( $gift_list_id ) ) {
			$gift_list = new Woocommerce_Gift_Ideas_Giftlist( $gift_list_id );
			if ( ! $gift_list->get_id() ) {
				$gift_list = null;
			}
		}
		if ( empty( $gift_list ) ) {
			$gift_list = $giftlist_manager->obtener_giftlist_by_codigo( $gift_list_id );
		}

		if ( $gift_list ) {
			// Verificar si el usuario logeado tiene acceso a ver la lista.
			// Verificar por si la lista es compartida, no necesariamente se sabe a que usuarios se compartio.
			if (
				is_user_logged_in()
				&&
				get_current_user_id() !== (int) $gift_list->post_author
				&&
				! in_array( get_current_user_id(), $gift_list->get_compartido_con(), true )
				&&
				! $gift_list->is_compartida()
			) {
				$gift_list = null;
			}
		}

		return $gift_list;
	}
}

if ( ! function_exists( 'wc_gift_ideas_get_giftlist_by_nombres' ) ) {

	/**
	 * Obtener la lista por id o codigo.
	 *
	 * @param string|int $gift_list_id Id o codigo de la lista a buscar.
	 *
	 * @return null|Woocommerce_Gift_Ideas_Giftlist
	 */
	function wc_gift_ideas_get_giftlist_by_nombres( $giftlist_items ) {
		$giftlist_manager = Woocommerce_Gift_Ideas::get_public_instance()->get_giftlist_manager();
		$gift_list = array();
		$gift_list = wc_gift_ideas_get_giftlist_by_nombres( $gift_list_id );
		if ( is_integer( $gift_list_id ) ) {
			$gift_list = new Woocommerce_Gift_Ideas_Giftlist( $gift_list_id );
			if ( ! $gift_list->get_id() ) {
				$gift_list = null;
			}
		}
		if ( empty( $gift_list ) ) {
			$gift_list = $giftlist_manager->obtener_giftlist_by_codigo( $gift_list_id );
		}

		if ( $gift_list ) {
			// Verificar si el usuario logeado tiene acceso a ver la lista.
			// Verificar por si la lista es compartida, no necesariamente se sabe a que usuarios se compartio.
			if (
				is_user_logged_in()
				&&
				get_current_user_id() !== (int) $gift_list->post_author
				&&
				! in_array( get_current_user_id(), $gift_list->get_compartido_con(), true )
				&&
				! $gift_list->is_compartida()
			) {
				$gift_list = null;
			}
		}

		return $gift_list;
	}
}

if ( ! function_exists( 'wc_gift_ideas_get_post' ) ) {

	/**
	 * Obtener un valor de post.
	 *
	 * @param string $key     Key de $_POST.
	 * @param mixed  $default Valor por defecto.
	 *
	 * @return mixed
	 */
	function wc_gift_ideas_get_post( $key, $default = null ) {
		if ( ! isset( $_POST[ $key ] ) ) {
			return $default;
		}

		return $_POST[ $key ];
	}
}

if ( ! function_exists( 'wc_gift_ideas_get_post_sanitized' ) ) {

	/**
	 * Obtener un valor de post sanitizado.
	 *
	 * @param string $key     Key de $_POST.
	 * @param mixed  $default Valor por defecto.
	 * @param string $como    Sanitizar como?.
	 *
	 * @return mixed
	 */
	function wc_gift_ideas_get_post_sanitized( $key, $default = null, $como = 'string' ) {
		$valor = wc_gift_ideas_get_post( $key, $default );

		if ( $default !== $valor ) {
			switch ( $como ) {
				case 'bool':
				case 'boolean':
					$valor = is_string( $valor ) && 'false' === $valor ? false : (bool) $valor;
					break;
				case 'int':
				case 'integer':
					$valor = (int) $valor;
					break;
				case 'number':
				case 'float':
				case 'decimal':
					$valor = (float) $valor;
					break;
				case 'array':
					$valor = (array) $valor;
					break;
				default:
					$valor = sanitize_text_field( $valor );
					break;
			}
		}

		return $valor;
	}
}

if ( ! function_exists( 'wc_gift_ideas_address_form_field' ) ) {

	/**
	 * Outputs a address form field.
	 *
	 * @param string $key   Clave post para obtener el valor. Puede ser formato address[address-line1].
	 * @param mixed  $args  Argumentos del campo.
	 * @param string $value Valor por defecto del campo.
	 */
	function wc_gift_ideas_address_form_field( $key, $args, $value = null ) {
		$defaults = [
			'type'              => 'text',
			'label'             => '',
			'description'       => '',
			'placeholder'       => '',
			'maxlength'         => false,
			'required'          => false,
			'autocomplete'      => false,
			'id'                => str_replace( '[', '_', rtrim( $key, ']' ) ),
			'class'             => [],
			'input_class'       => [ 'form-control' ],
			'return'            => false,
			'options'           => [],
			'custom_attributes' => [],
			'validate'          => [],
			'default'           => '',
			'autofocus'         => '',
			'priority'          => '',
		];

		$args = wp_parse_args( $args, $defaults );

		if ( is_string( $args['label_class'] ) ) {
			$args['label_class'] = [ $args['label_class'] ];
		}

		if ( is_null( $value ) ) {
			$value = $args['default'];
		}

		// Custom attribute handling.
		$custom_attributes         = [];
		$args['custom_attributes'] = array_filter( (array) $args['custom_attributes'] );

		if ( $args['required'] ) {
			//$args['custom_attributes']['required'] = 'required';
		}

		if ( $args['description'] ) {
			$args['custom_attributes']['aria-describedby'] = $args['id'] . 'Help';
		}

		if ( $args['maxlength'] ) {
			$args['custom_attributes']['maxlength'] = absint( $args['maxlength'] );
		}

		if ( ! empty( $args['autocomplete'] ) ) {
			$args['custom_attributes']['autocomplete'] = $args['autocomplete'];
		}

		if ( true === $args['autofocus'] ) {
			$args['custom_attributes']['autofocus'] = 'autofocus';
		}

		if ( ! empty( $args['custom_attributes'] ) && is_array( $args['custom_attributes'] ) ) {
			foreach ( $args['custom_attributes'] as $attribute => $attribute_value ) {
				$custom_attributes[] = esc_attr( $attribute ) . '="' . esc_attr( $attribute_value ) . '"';
			}
		}

		$field           = '';
		$label_id        = $args['id'];
		$field_container = '<div class="%1$s" id="%2$s">%3$s%4$s</div>';

		switch ( $args['type'] ) {
			case 'country':
				// Obtener solo los paises disponibles para envio.
				$countries = WC()->countries->get_shipping_countries();

				if ( 1 === count( $countries ) ) {

					$field .= '<strong>' . current( array_values( $countries ) ) . '</strong>';

					$field .= '<input type="hidden" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" value="' . current( array_keys( $countries ) ) . '" ' . implode( ' ', $custom_attributes ) . ' class="country_to_state" readonly="readonly" />';

				} else {

					$field = '<select name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" class="country_to_state country_select custom-select ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" ' . implode( ' ', $custom_attributes ) . '>' . '<option value="">' . esc_html__( 'Select a country&hellip;', 'woocommerce' ) . '</option>';

					foreach ( $countries as $ckey => $cvalue ) {
						$field .= '<option value="' . esc_attr( $ckey ) . '" ' . selected( $value, $ckey, false ) . '>' . $cvalue . '</option>';
					}

					$field .= '</select>';

				}

				break;
			case 'state':
				/* Get country this state field is representing */
				$for_country = isset( $args['country'] ) ? $args['country'] : WC()->checkout->get_value( 'shipping_country' );
				$states      = WC()->countries->get_states( $for_country );

				if ( is_array( $states ) && empty( $states ) ) {
					$field_container = '<div class="%1$s" id="%2$s" style="display: none">%3$s%4$s</div>';

					$field .= '<input type="hidden" class="hidden" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" value="" ' . implode( ' ', $custom_attributes ) . ' placeholder="' . esc_attr( $args['placeholder'] ) . '" readonly="readonly" />';

				} elseif ( ! is_null( $for_country ) && is_array( $states ) ) {
					$field .= '<select name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" class="state_select custom-select ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" ' . implode( ' ', $custom_attributes ) . ' data-placeholder="' . esc_attr( $args['placeholder'] ) . '">
						<option value="">' . esc_html__( 'Select a state&hellip;', 'woocommerce' ) . '</option>';

					foreach ( $states as $ckey => $cvalue ) {
						$field .= '<option value="' . esc_attr( $ckey ) . '" ' . selected( $value, $ckey, false ) . '>' . $cvalue . '</option>';
					}

					$field .= '</select>';
				} else {
					$field .= '<input type="text" class="input-text ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" value="' . esc_attr( $value ) . '"  placeholder="' . esc_attr( $args['placeholder'] ) . '" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" ' . implode( ' ', $custom_attributes ) . ' />';
				}
				break;
			case 'textarea':
				$field .= '<textarea name="' . esc_attr( $key ) . '" class="input-text ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" id="' . esc_attr( $args['id'] ) . '" placeholder="' . esc_attr( $args['placeholder'] ) . '" ' . ( empty( $args['custom_attributes']['rows'] ) ? ' rows="2"' : '' ) . ( empty( $args['custom_attributes']['cols'] ) ? ' cols="5"' : '' ) . implode( ' ', $custom_attributes ) . '>' . esc_textarea( $value ) . '</textarea>';
				break;
			case 'checkbox':
				$field = '<label class="form-check-label ' . implode( ' ', $args['label_class'] ) . '" ' . implode( ' ', $custom_attributes ) . '>
						<input type="' . esc_attr( $args['type'] ) .
				         '" class="input-checkbox ' . esc_attr( implode( ' ', $args['input_class'] ) ) .
				         '" name="' . esc_attr( $key ) .
				         '" id="' . esc_attr( $args['id'] ) . '" value="1" ' .
				         checked( $value, 1, false ) . ' /> ' . $args['label'] . '</label>';
				break;
			case 'password':
			case 'text':
			case 'email':
			case 'tel':
			case 'number':
				$field .= '<input type="' . esc_attr( $args['type'] ) . '" class="input-text ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" placeholder="' . esc_attr( $args['placeholder'] ) . '"  value="' . esc_attr( $value ) . '" ' . implode( ' ', $custom_attributes ) . ' />';
				break;
			case 'select':
				$field   = '';
				$options = '';
				if ( ! empty( $args['options'] ) ) {
					foreach ( $args['options'] as $option_key => $option_text ) {
						if ( '' === $option_key ) {
							// If we have a blank option, select2 needs a placeholder.
							if ( empty( $args['placeholder'] ) ) {
								$args['placeholder'] = $option_text ? $option_text : __( 'Elije una opción', 'woocommerce-gift-ideas' );
							}
							$custom_attributes[] = 'data-allow_clear="true"';
						}
						$options .= '<option value="' . esc_attr( $option_key ) . '" ' . selected( $value, $option_key, false ) . '>' . esc_attr( $option_text ) . '</option>';
					}

					$field .= '<select name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" class="select custom-select ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" ' . implode( ' ', $custom_attributes ) . ' data-placeholder="' . esc_attr( $args['placeholder'] ) . '">
							' . $options . '
						</select>';
				}
				break;
			case 'radio':
				$label_id = current( array_keys( $args['options'] ) );

				if ( ! empty( $args['options'] ) ) {
					foreach ( $args['options'] as $option_key => $option_text ) {
						$field .= '<input type="radio" class="input-radio ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" value="' . esc_attr( $option_key ) . '" name="' . esc_attr( $key ) . '" ' . implode( ' ', $custom_attributes ) . ' id="' . esc_attr( $args['id'] ) . '_' . esc_attr( $option_key ) . '"' . checked( $value, $option_key, false ) . ' />';
						$field .= '<label for="' . esc_attr( $args['id'] ) . '_' . esc_attr( $option_key ) . '" class="radio form-check-label ' . implode( ' ', $args['label_class'] ) . '">' . $option_text . '</label>';
					}
				}
				break;
		}

		if ( ! empty( $field ) ) {
			$field_html = $field;

			if ( $args['description'] ) {
				$field_html .= '<small id="' . $args['id'] . 'Help" class="form-text text-muted">' . esc_html( $args['description'] ) . '</small>';
			}

			if ( $args['label'] && 'checkbox' !== $args['type'] ) {
				$label = '<label for="' . esc_attr( $label_id ) . '" class="' . esc_attr( implode( ' ', $args['label_class'] ) ) . '">' . $args['label'] . '</label>';
			}


			$field_container = apply_filters( 'wc_gift_ideas_address_form_field_container', $field_container );
			$container_id    = esc_attr( $args['id'] ) . '_field';
			$field           = sprintf(
				$field_container,
				(
				in_array( $args['type'], [ 'checkbox', 'radio' ], true ) ?
					'form-check'
					:
					'form-group'
				),
				$container_id,
				( isset( $label ) ? $label : '' ),
				$field_html
			);

			if ( 'direccion_entrega[address_1]' === $key ) {
				$field = sprintf(
					'<div class="row"><div class="col-8">%s</div>',
					$field
				);
			} elseif ( 'direccion_entrega[address_2]' === $key ) {
				$field = sprintf(
					'<div class="col-4">%s</div></div>',
					$field
				);
			}
		}

		$field = apply_filters( 'wc_gift_ideas_address_form_field_' . $args['type'], $field, $key, $args, $value );

		echo $field;
	}
}

if ( ! function_exists( 'wc_gift_ideas_get_giftlist_page_url' ) ) {

	/**
	 * Wrapper para obtener el enlace a la pagina de listado de giftlists.
	 *
	 * @param string $codigo Codigo de la lista de regalo (Opcional).
	 *
	 * @return string
	 */
	function wc_gift_ideas_get_giftlist_page_url( $codigo = null ) {
		$enlace = Woocommerce_Gift_Ideas::get_public_instance()->get_page_manager()->get_giftlist_page_url();

		if ( $enlace ) {
			$enlace .= $codigo;
		}

		return $enlace;
	}
}

if ( ! function_exists( 'wc_gift_ideas_get_giftlist_results_page_url' ) ) {

	/**
	 * Wrapper para obtener el enlace a la pagina de listado de giftlists.
	 *
	 * @param string $codigo Codigo de la lista de regalo (Opcional).
	 *
	 * @return string
	 */
	function wc_gift_ideas_get_giftlist_results_page_url( $codigo = null ) {
		$enlace = Woocommerce_Gift_Ideas::get_public_instance()->get_page_manager()->get_giftlist_results_page_url();

		if ( $enlace ) {
			$enlace .= $codigo;
		}

		return $enlace;
	}
}

if ( ! function_exists( 'wc_gift_ideas_get_giftlist_create_search_page_url' ) ) {

	/**
	 * Wrapper para obtener el enlace a la pagina de crear y buscar giftlist.
	 *
	 * @return string
	 */
	function wc_gift_ideas_get_giftlist_create_search_page_url() {
		$enlace = Woocommerce_Gift_Ideas::get_public_instance()->get_page_manager()->get_giftlist_create_search_page_url();

		return $enlace;
	}
}

if ( ! function_exists( 'wc_gift_ideas_get_giftlist_form_page_url' ) ) {

	/**
	 * Wrapper para obtener el enlace a la pagina de formulario de una giftlist.
	 *
	 * @return string
	 */
	function wc_gift_ideas_get_giftlist_form_page_url() {
		$enlace = Woocommerce_Gift_Ideas::get_public_instance()->get_page_manager()->get_giftlist_form_url();

		return $enlace;
	}
}

if ( ! function_exists( 'wc_gift_ideas_last_giftlist_code' ) ) {

	/**
	 * Wrapper para obtener el codigo de la ultima lista de regalos modificada.
	 *
	 * @return string
	 */
	function wc_gift_ideas_last_giftlist_code() {
		if ( ! is_user_logged_in() ) {
			return null;
		}

		$giftlists = Woocommerce_Gift_Ideas::get_public_instance()->get_giftlist_manager()->obtener_giftlist_usuario( get_current_user_id() );

		if ( empty( $giftlists ) ) {
			return null;
		}

		$giftlist = current( $giftlists );

		return $giftlist->get_codigo();
	}
}
