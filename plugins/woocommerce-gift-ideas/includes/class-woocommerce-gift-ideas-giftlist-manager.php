<?php
/**
 * Clase administradora de las listas de deseo.
 *
 * Date: 10/21/17
 * Time: 12:01 PM
 *
 * @link       http://modobeta.pe
 * @since      1.0.0
 * @author     Modo Beta SAC <info@beta.pe>
 *
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/includes
 */

if ( ! defined( 'ABSPATH' ) ) {
	// Exit if accessed directly.
	exit;
}

/**
 * Class Woocommerce_Gift_Ideas_List_Manager.
 *
 * @since      1.0.0
 * @author     Fernando Paz <ferpaz@beta.pe>
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/includes
 */
class Woocommerce_Gift_Ideas_Giftlist_Manager {

	/**
	 * Nombre del custom post type para almacenar las wishlist.
	 */
	const NAME = 'woo_giftidealist';

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $plugin_name The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $version The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 *
	 * @param      string $plugin_name The name of the plugin.
	 * @param      string $version     The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;

	}

	/**
	 * Crear el custom post type para almacenar las listas.
	 */
	public function create_post_type() {
		register_post_type(
			self::NAME,
			[
				'labels'      => [
					'name'          => __( 'Listas de regalos', 'woocommerce-gift-ideas' ),
					'singular_name' => __( 'Lista de regalo', 'woocommerce-gift-ideas' ),
				],
				'public'      => false,
				'has_archive' => false,
			]
		);
	}

	/**
	 * Obtener las listas de regalo de un usuario.
	 *
	 * @param int $usuario_id Id del usuario dueño de la giftlist.
	 *
	 * @return Woocommerce_Gift_Ideas_Giftlist[]
	 */
	public function obtener_giftlist_usuario( $usuario_id ) {
		if ( ! $usuario_id || ! is_int( $usuario_id ) ) {
			return [];
		}

		$query     = new WP_Query(
			[
				'post_status'    => 'publish',
				'post_type'      => self::NAME,
				'posts_per_page' => - 1,
				'author'         => $usuario_id,
			]
		);
		$giftlists = [];

		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) {
				$query->the_post();
				$giftlists[] = new Woocommerce_Gift_Ideas_Giftlist( get_the_ID() );
			}
		}

        wp_reset_postdata();

        return $giftlists;
    }

	/**
	 * Obtener la lista de regalos por el codigo.
	 *
	 * @param string $codigo Codigo de la lista.
	 *
	 * @return Woocommerce_Gift_Ideas_Giftlist|null
	 */
	public function obtener_giftlist_by_codigo( $codigo ) {
		if ( ! $codigo || ! is_string( $codigo ) || empty( $codigo ) ) {
			return null;
		}

		$query    = new WP_Query(
			[
				'post_status'    => 'publish',
				'post_type'      => self::NAME,
				'posts_per_page' => 1,
				'meta_key'       => 'codigo',
				'meta_value'     => $codigo,
			]
		);
		$giftlist = null;

		if ( $query->have_posts() ) {
			$query->the_post();
			$giftlist = new Woocommerce_Gift_Ideas_Giftlist( get_the_ID() );
			//var_dump($giftlist);
		}

        wp_reset_postdata();

		return $giftlist;
	}

	/**
	 * Eliminar una lista de regalos
	 *
	 * @param Woocommerce_Gift_Ideas_Giftlist $giftlist Lista a eliminar.
	 */
	public function delete_giftlist( $giftlist ) {
		if ( $giftlist->get_id() ) {
			wp_trash_post( $giftlist->get_id() );
		}
	}

	/**
	 * Eliminar las listas de regalos asociadas a un usuario.
	 *
	 * @param int $user_id User being deleted.
	 */
	public function delete_user_giftlists( $user_id ) {
		if ( $user_id ) {

			$query     = new WP_Query(
				[
					'post_status'    => 'publish',
					'post_type'      => self::NAME,
					'posts_per_page' => - 1,
					'author'         => $user_id,
				]
			);
			$giftlists = [];

			if ( $query->have_posts() ) {
				while ( $query->have_posts() ) {
					$query->the_post();
					$giftlists[] = new Woocommerce_Gift_Ideas_Giftlist( get_the_ID() );
				}

				array_map( [ $this, 'delete_giftlist' ], $giftlists );
			}

            wp_reset_postdata();
		}
	}

	/**
	 * Obtener una lista de deseos por nombre cumpleañero que no esten vacias.
	 *
	 * @param string $nombre Nombre del cumpleañero.
	 *
	 * @return Woocommerce_Gift_Ideas_Giftlist[]
	 */
	public function buscar_giftlist_cumpleanero( $nombre ) {
		if ( ! $nombre || ! is_string( $nombre ) || empty( $nombre ) ) {
			return [];
		}

		$meta = [ 'relation' => 'OR' ];

		$pieces = explode( ' ', $nombre );
		foreach ( $pieces as $piece ) {
			$piece = trim( $piece );
			if ( empty( $piece ) ) {
				continue;
			}
			//echo "PIECE : ".$piece;
			$meta[] = [
				'key'     => 'cumpleanero_nombre',
				'value'   => $piece,
				'compare' => 'LIKE'
			];

			$meta[] = [
				'key'     => 'cumpleanero_apellido',
				'value'   => $piece,
				'compare' => 'LIKE'
			];

			$meta[] = [
				'key'     => 'email',
				'value'   => $piece,
				'compare' => 'LIKE'
			];
		}

		//Modificando el OR de titulo o meta
		add_action( 'pre_get_posts', function( $q )
		{
		    if( $title = $q->get( '_meta_or_title' ) )
		    {
		        add_filter( 'get_meta_sql', function( $sql ) use ( $title )
		        {
		            global $wpdb;

		            // Only run once:
		            static $nr = 0; 
		            if( 0 != $nr++ ) return $sql;

		            // Modified WHERE
		            $sql['where'] = sprintf(
		                " AND ( %s OR %s ) ",
		                $wpdb->prepare( "{$wpdb->posts}.post_title like '%%%s%%'", $title),
		                mb_substr( $sql['where'], 5, mb_strlen( $sql['where'] ) )
		            );

		            return $sql;
		        });
		    }
		});

		//Haciendo el meta query
		$query    = new WP_Query(
			[
				'post_status'    => 'publish',
				'post_type'      => self::NAME,
				//'posts_per_page' =>v 1,v
			    '_meta_or_title' => $nombre, 
				'meta_query'     => $meta
			]
		);  

		$giftlist = []; 
		 if ( $query->have_posts() ) {
            while ( $query->have_posts() ) { 
                $query->the_post();
				$giftl = new Woocommerce_Gift_Ideas_Giftlist( get_the_ID() );
				if ( ! empty( $giftl->get_productos() ) ) {
					$giftlist[] = $giftl;
				} 

            }

        } 
        wp_reset_postdata();

		return $giftlist;
	}
}
