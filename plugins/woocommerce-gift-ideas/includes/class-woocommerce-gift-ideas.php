<?php
/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://modobeta.pe
 * @since      1.0.0
 *
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/includes
 */

if ( ! defined( 'ABSPATH' ) ) {
	// Exit if accessed directly.
	exit;
}

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/includes
 * @author     Fernando Paz <info@beta.pe>
 */
class Woocommerce_Gift_Ideas {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Woocommerce_Gift_Ideas_Loader $loader Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string $plugin_name The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string $version The current version of the plugin.
	 */
	protected $version;

	/**
	 * Instancia unica publica.
	 *
	 * @var Woocommerce_Gift_Ideas_Public
	 */
	private static $instance;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'WOOCOMMERCE_GIFT_IDEAS' ) ) {
			$this->version = WOOCOMMERCE_GIFT_IDEAS;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'woocommerce-gift-ideas';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
	}

	/**
	 * Obtener la instancia publica.
	 *
	 * @return Woocommerce_Gift_Ideas_Public
	 */
	public static function get_public_instance() {
		return self::$instance;
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Woocommerce_Gift_Ideas_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

	/**
	 * Añadir emails a woocommerce.
	 *
	 * @param array $email_classes Clases de emails disponibles.
	 *
	 * @return array
	 */
	public function custom_emails( $email_classes ) {
		/**
		 * Emails.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-woocommerce-gift-ideas-share-giftlist-email.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-woocommerce-gift-ideas-created-giftlist-email.php';

		static::$instance->init_emails();

		// add to the list of email classes that WooCommerce loads.
		$email_classes['Woocommerce_Gift_Ideas_Share_Giftlist_Email']   = static::$instance->init_share_giftlist_email();
		$email_classes['Woocommerce_Gift_Ideas_Created_Giftlist_Email'] = static::$instance->init_created_giftlist_email();

		return $email_classes;
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * Helpers.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'helper/class-woocommerce-gift-ideas-user-address-helper.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'helper/class-woocommerce-gift-ideas-cart-address-helper.php';

		/**
		 * Funciones necesarias globales.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/woocommerce-gift-ideas-function.php';

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-woocommerce-gift-ideas-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-woocommerce-gift-ideas-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-woocommerce-gift-ideas-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-woocommerce-gift-ideas-public.php';

		/**
		 * La clase lista de regalo.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-woocommerce-gift-ideas-giftlist.php';

		/**
		 * La clase responsables de administrar las listas de regalo.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-woocommerce-gift-ideas-giftlist-manager.php';

		/**
		 * La clase responsables de administrar las paginas de las listas de regalo.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-woocommerce-gift-ideas-page-manager.php';

		$this->loader = new Woocommerce_Gift_Ideas_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Woocommerce_Gift_Ideas_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Woocommerce_Gift_Ideas_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Woocommerce_Gift_Ideas_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public  = new Woocommerce_Gift_Ideas_Public( $this->get_plugin_name(), $this->get_version() );
		self::$instance = $plugin_public;

		$this->loader->add_action( 'init', $plugin_public->get_giftlist_manager(), 'create_post_type' );
		$this->loader->add_action( 'init', $plugin_public->get_page_manager(), 'apply_rewrite_rules', 0 );
		$this->loader->add_action( 'init', $plugin_public->get_page_manager(), 'add_rewrite_rules', 0 );
		$this->loader->add_action( 'init', $plugin_public->get_page_manager(), 'create_shortcodes', 0 );
		$this->loader->add_action( 'init', $plugin_public->get_page_manager(), 'create_custom_pages' );

		$this->loader->add_action( 'woocommerce_init', $plugin_public->get_cart_helper(), 'cargar_carrito' );
		$this->loader->add_action( 'woocommerce_before_checkout_shipping_form', $plugin_public->get_cart_helper(), 'send_to_birthday_boy_girl' );

		$this->loader->add_filter( 'language_attributes', $plugin_public->get_page_manager(), 'doctype_opengraph' );
		$this->loader->add_action( 'wp_head', $plugin_public->get_page_manager(), 'add_opengraph_meta_tags', 1 );

		$this->loader->add_action( 'query_vars', $plugin_public->get_page_manager(), 'add_query_var' );
		$this->loader->add_action( 'deleted_user', $plugin_public->get_giftlist_manager(), 'delete_user_giftlists' );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		$this->loader->add_action( 'wc_gift_ideas_giftlist_created', $plugin_public, 'send_email_giftlist_created', 10, 2 );

		$this->loader->add_filter( 'woocommerce_email_classes', $this, 'custom_emails' );
		$this->loader->add_action( 'woocommerce_email', $plugin_public, 'send_queued_emails' );
		$this->loader->add_action( 'woocommerce_after_shop_loop_item', $plugin_public, 'add_remove_product_giftlist' );
		$this->loader->add_action( 'woocommerce_after_shop_loop_item', $plugin_public, 'add_to_giftlist_button' );
		$this->loader->add_action( 'woocommerce_single_product_summary', $plugin_public, 'add_to_giftlist_button', 31 );
		$this->loader->add_action( 'woocommerce_checkout_order_processed', $plugin_public->get_cart_helper(), 'clean_attached' );
		$this->loader->add_action( 'wp_ajax_add_to_giftlist', $plugin_public, 'add_to_giftlist_dialog' );
		$this->loader->add_action( 'wp_ajax_nopriv_create_giftlist', $plugin_public, 'create_giftlist_dialog' );
		$this->loader->add_action( 'wp_ajax_create_giftlist', $plugin_public, 'create_giftlist_dialog' );
		$this->loader->add_action( 'wp_ajax_remove_from_giftlist', $plugin_public, 'remove_from_giftlist' );
		$this->loader->add_action( 'wp_ajax_remove_giftlist', $plugin_public, 'remove_giftlist' );
		$this->loader->add_action( 'wp_ajax_share_giftlist', $plugin_public, 'share_giftlist_email' );
		$this->loader->add_action( 'wp_ajax_share_giftlist_whatsapp', $plugin_public, 'share_giftlist_whatsapp' );
		$this->loader->add_action( 'wp_ajax_add_to_cart_from_giftlist', $plugin_public, 'add_to_cart_from_giftlist' );
		$this->loader->add_action( 'wp_ajax_nopriv_add_to_cart_from_giftlist', $plugin_public, 'add_to_cart_from_giftlist' );
		$this->loader->add_action( 'wp_ajax_search_giftlist', $plugin_public, 'search_giftlist' );
		$this->loader->add_action( 'wp_ajax_nopriv_search_giftlist', $plugin_public, 'search_giftlist' );

	}

}
