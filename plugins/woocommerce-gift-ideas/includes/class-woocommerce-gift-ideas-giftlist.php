<?php
/**
 * Clase modelo de las listas de regalo.
 *
 * Date: 10/21/17
 * Time: 5:02 PM
 *
 * @link       http://modobeta.pe
 * @since      1.0.0
 * @author     Fernando Paz <ferpaz@beta.pe>
 *
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/includes
 */

if ( ! defined( 'ABSPATH' ) ) {
	// Exit if accessed directly.
	exit;
}

/**
 * Class Woocommerce_Gift_Ideas_Giftlist.
 *
 * @since      1.0.0
 * @author     Fernando Paz <ferpaz@beta.pe>
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/includes
 */
class Woocommerce_Gift_Ideas_Giftlist {

	/**
	 * Es hombre el cumpleañero.
	 */
	const MASCULINO = 1;

	/**
	 * Es mujer la cumpleañera.
	 */
	const FEMENINO = 2;

	/**
	 * Ver todos.
	 */
	const TODOS = 3;

	/**
	 * Id de la lista.
	 *
	 * @var int
	 */
	private $idn;

	/**
	 * Nombre de la lista.
	 *
	 * @var string
	 */
	private $nombre;

	/**
	 * Codigo de la lista.
	 *
	 * @var string
	 */
	private $codigo;

	/**
	 * Nombre del cumpleañero.
	 *
	 * @var string
	 */
	private $cumpleanero_nombre;

	/**
	 * Apellido del cumpleañero.
	 *
	 * @var string
	 */
	private $cumpleanero_apellido;

	/**
	 * Fecha del cumpleañero.
	 *
	 * @var DateTime
	 */
	private $fecha_cumpleanos;

	/**
	 * Sexo del cumpleañero.
	 *
	 * @var int
	 */
	private $sexo_cumpleanero;

	/**
	 * Email a enviar enlace de la lista creada.
	 *
	 * @var string
	 */
	private $email;

	/**
	 * Direccion de entrega.
	 *
	 * @var mixed
	 */
	private $direccion_entrega;

	/**
	 * Compartir la lista?.
	 *
	 * @var bool
	 */
	private $compartida;

	/**
	 * Id de los usuarios con los que se compartio la lista.
	 *
	 * @var int[]
	 */
	private $compartido_con;

	/**
	 * Listado de productos en la lista.
	 *
	 * @var WC_Product[]
	 */
	private $productos;

	/**
	 * Listado de productos comprados.
	 *
	 * @var WC_Product[]
	 */
	private $comprados;

	/**
	 * Post asociado.
	 *
	 * @var WP_Post
	 */
	private $post;

	/**
	 * Cantidades de los productos indexadas por el id de producto.
	 *
	 * @var array[]
	 */
	private $cantidades_producto;

	/**
	 * Initialize the class.
	 *
	 * @since    1.0.0
	 *
	 * @param int|WP_Post $post_id Id del post u objeto del post.
	 */
	public function __construct( $post_id = null ) {

		$this->productos      			= [];
		$this->cantidades_producto      = [];
		$this->comprados      			= [];
		$this->compartida     			= true;
		$this->compartido_con 			= [];

		if ( $post_id ) {
			$this->cargar_desde_bd( $post_id );
		}

	}

	/**
	 * Magic setter.
	 *
	 * @param string $name  Nombre propiedad.
	 * @param mixed  $value Valor de la propiedad.
	 */
	public function __set( $name, $value ) {
		if ( in_array( $name, [ 'id', 'idn' ], true ) ) {
			// No permitir modificar el id.
			return;
		}

		if ( 'cumpleanero' === $name ) {
			list( $nombre, $apellido ) = explode( ' ', preg_replace( '/\s{2,}/', ' ', $value ), 2 );
			$apellido                   = $apellido ? $apellido : '';
			$this->cumpleanero_nombre   = $nombre;
			$this->cumpleanero_apellido = $apellido;
		}

		if ( 'sexo_cumpleanero' === $name ) {
			// Convertir int.
			$value = (int) $value;
		}

		if ( 'compartida' === $name ) {
			// Convertir int.
			$value = (bool) $value;
		}

		if ( 'compartido_con' === $name ) {
			// Convertir int.
			$value = array_map( 'absint', (array) $value );
		}

		if ( 'fecha_cumpleanos' === $name && ! empty( $value ) ) {
			// Convertir objeto.
			$value = date_create_from_format( 'Y-m-d', $value );
		}

		if ( in_array( $name, [ 'productos', 'comprados' ], true ) ) {
			// Convertir a productos de woocommerce.
			$value = array_map(
				function ( $producto_id ) {
					return $producto_id instanceof WC_Product ? $producto_id : wc_get_product( $producto_id );
				},
				(array) $value
			);
			$value = array_filter(
				$value,
				function ( $producto ) {
					return $producto instanceof WC_Product;
				}
			);
		}

		if ( 'direccion_entrega' === $name && ! is_array( $value ) ) {
			$value = (array) $value;
			foreach ( $value as $key => $val ) {
				$value[ $key ] = htmlentities( $val );
			}
		}

		$this->{$name} = $value;
	}

	/**
	 * Magic getter. Para acceder a los atributos del post.
	 *
	 * @param string $name Nombre propiedad.
	 *
	 * @return mixed
	 */
	public function __get( $name ) {
		if ( in_array(
			$name,
			[
				'id',
				'idn',
				'nombre',
				'codigo',
				'cumpleanero',
				'cumpleanero_nombre',
				'cumpleanero_apellido',
				'fecha_cumpleanos',
				'sexo_cumpleanero',
				'email',
				'direccion_entrega',
				'compartido_con',
				'productos',
				'comprados',
				'post',
			],
			true
		) ) {
			// No permitir acceder a estos valores por el magic getter.
			return null;
		}

		return $this->post->{$name};
	}

	/**
	 * Obtener Id de la lista.
	 *
	 * @return int
	 */
	public function get_id() {
		return $this->idn;
	}

	/**
	 * Obtener el nombre de la lista.
	 *
	 * @return string
	 */
	public function get_nombre() {
		return $this->nombre;
	}

	/**
	 * Obtener el codigo de la lista.
	 *
	 * @return string
	 */
	public function get_codigo() {
		return $this->codigo;
	}

	/**
	 * Obtener el nombre completo del cumpleañero.
	 *
	 * @return string
	 */
	public function get_cumpleanero() {
		$nombre = $this->get_cumpleanero_nombre() . ' ' . $this->get_cumpleanero_apellido();

		return $nombre;
	}

	/**
	 * Obtener el nombre del cumpleañero.
	 *
	 * @return string
	 */
	public function get_cumpleanero_nombre() {
		return $this->cumpleanero_nombre;
	}

	/**
	 * Obtener el apellido del cumpleañero.
	 *
	 * @return string
	 */
	public function get_cumpleanero_apellido() {
		return $this->cumpleanero_apellido;
	}

	/**
	 * Obtener la fecha de cumpleaños.
	 *
	 * @return DateTime
	 */
	public function get_fecha_cumpleanos() {
		return $this->fecha_cumpleanos;
	}

	/**
	 * Obtener el sexo del cumpleañer@.
	 *
	 * @return int
	 */
	public function get_sexo_cumpleanero() {
		return $this->sexo_cumpleanero;
	}

	/**
	 * Obtener el email de la lista.
	 *
	 * @return string
	 */
	public function get_email() {
		return $this->email;
	}

	/**
	 * Obtener la direccion de entrega de la lista.
	 *
	 * @return mixed
	 */
	public function get_direccion_entrega() {
		return $this->direccion_entrega;
	}

	/**
	 * Es compartida la lista?.
	 *
	 * @return bool
	 */
	public function is_compartida() {
		return $this->compartida;
	}

	/**
	 * Listado de ids de usuarios con los que se compartio.
	 *
	 * @return int[]
	 */
	public function get_compartido_con() {
		return $this->compartido_con;
	}

	/**
	 * Agregar un usuario a la lista de compartidos.
	 *
	 * @param int $usuario_id Id del usuario a añadir.
	 */
	public function add_compartido_con( $usuario_id ) {
		if ( ! in_array( $usuario_id, $this->compartido_con, true ) ) {
			$this->compartido_con[] = $usuario_id;
		}
	}

	/**
	 * Quitar un usuario de la lista de compartidos.
	 *
	 * @param int $usuario_id Id del usuario a quitar.
	 */
	public function remove_compartido_con( $usuario_id ) {
		if ( in_array( $usuario_id, $this->compartido_con, true ) ) {
			array_splice(
				$this->compartido_con,
				array_search( $usuario_id, $this->compartido_con, true ),
				1
			);
		}
	}

	/**
	 * Obtener el listado de productos.
	 *
	 * @return WC_Product[]
	 */
	public function get_productos() {
		return $this->productos;
	}

	/**
	 * Obtener el listado de cantidades de productos.
	 *
	 * @return WC_Product[]
	 */
	public function get_cantidades_producto() {
		return $this->cantidades_producto;
	}
	
	/**
	 * Setear el listado de cantidades de productos.
	 *
	 * @return WC_Product[]
	 */
	public function set_cantidades_producto( $value ) {
		return $this->cantidades_producto = $value;
	}

	/**
	 * Obtener el listado de productos por comprar.
	 *
	 * @return WC_Product[]
	 */
	public function get_productos_por_comprar() {
		$comprados = $this->get_ids_productos( $this->comprados );
		$todos     = $this->get_ids_productos( $this->productos );

		$por_comprar = array_diff( $todos, $comprados );

		return array_map( 'wc_get_product', $por_comprar );
	}

	/**
	 * Agregar un producto a la lista.
	 *
	 * @param WC_Product $producto Producto a añadir a la lista.
	 */
	public function add_producto( WC_Product $producto, $qty = 1 ) {
		if ( ! in_array( $producto->get_id(), $this->get_ids_productos( $this->productos ), true ) ) {
			$this->productos[] = $producto;
			$this->cantidades_producto[$producto->get_id()] = $qty;
		}
	}
 
	/**
	 * Quitar un producto de la lista.
	 *
	 * @param WC_Product $producto Producto a remover de la lista.
	 */
	public function remove_producto( WC_Product $producto ) {
		$ids         = $this->get_ids_productos( $this->productos );
		$producto_id = $producto->get_id();
		if ( in_array( $producto_id, $ids, true ) ) {
			array_splice(
				$this->productos,
				array_search( $producto_id, $ids, true ),
				1
			);
		}
	}

	/**
	 * Quitar un producto de la lista por id. 
	 *
	 * @param WC_Product $producto Producto a remover de la lista.
	 */
	public function remove_producto_by_id( $producto_id ) {
		$ids = $this->get_ids_productos( $this->productos );
		if ( in_array( $producto_id, $ids, true ) ) {
			array_splice(
				$this->productos,
				array_search( $producto_id, $ids, true ),
				1
			);
		}
	}

	/**
	 * Obtener el listado de productos comprados.
	 *
	 * @return WC_Product[]
	 */
	public function get_comprados() {
		return $this->comprados;
	}

	/**
	 * Agregar un producto a la lista de comprados.
	 *
	 * @param WC_Product $producto Producto a mover de la lista.
	 */
	public function add_comprado( WC_Product $producto ) {
		if ( ! in_array( $producto->get_id(), $this->get_ids_productos( $this->comprados ), true ) ) {
			$this->comprados[] = $producto;
		}
	}

	/**
	 * Quitar un producto de la lista de comprados.
	 *
	 * @param WC_Product $producto Producto a añadir a la lista.
	 */
	public function remove_comprado( WC_Product $producto ) {
		$ids         = $this->get_ids_productos( $this->comprados );
		$producto_id = $producto->get_id();
		if ( in_array( $producto_id, $ids, true ) ) {
			array_splice(
				$this->comprados,
				array_search( $producto_id, $ids, true ),
				1
			);
		}
	}



	/**
	 * Cargar lista desde BD
	 *
	 * @param int|WP_Post $post_id Id del post u objeto post.
	 *//**
     * Guardar nueva lista o actualizar.
     *
     * @return bool|WP_Error True cuando se guarda correctamente, WP_Error si falla al guardar.
     */
    /**
     * Guardar nueva lista o actualizar.
     *
     * @return bool|WP_Error True cuando se guarda correctamente, WP_Error si falla al guardar.
     */
    public function save() {
        // Legacy fix para el nombre.
        if ( $this->get_id() && empty( trim( $this->get_cumpleanero() ) ) ) {
            $this->cumpleanero = get_post_meta( $this->get_id(), 'cumpleanero', true );
        }

        $valido = $this->validar();

        if ( $valido instanceof WP_Error ) {
            return $valido;
        }

        $attrs = [
            'post_title'     => $this->nombre,
            'post_name'      => $this->nombre,
            'post_status'    => 'publish',
            'post_type'      => Woocommerce_Gift_Ideas_Giftlist_Manager::NAME,
            'comment_status' => 'closed',
            'ping_status'    => 'closed',
            'post_category'  => [ Woocommerce_Gift_Ideas_Giftlist_Manager::NAME ],
        ];

        if ( $this->idn ) {
            // Actualizar lista.
            $attrs['ID'] = $this->idn;
        }

        $post_id = wp_insert_post( $attrs, true );

        if ( $post_id instanceof WP_Error ) {
            // Error guardando el regalo.
            return $post_id;
        } else {
            $this->post = WP_Post::get_instance( $post_id );
        }

        $this->idn    = (int) $post_id;
        $this->codigo = $this->codigo ?: $this->generar_codigo_unico();

        // Actualizar campos meta.
        update_post_meta( $this->idn, 'codigo', $this->codigo );
        update_post_meta( $this->idn, 'cumpleanero_nombre', $this->cumpleanero_nombre );
        update_post_meta( $this->idn, 'cumpleanero_apellido', $this->cumpleanero_apellido );
        update_post_meta( $this->idn, 'email', $this->email );
        update_post_meta( $this->idn, 'fecha_cumpleanos', $this->fecha_cumpleanos->format( 'Y-m-d' ) );
        update_post_meta( $this->idn, 'sexo_cumpleanero', $this->sexo_cumpleanero );
        update_post_meta( $this->idn, 'direccion_entrega', wp_json_encode( $this->direccion_entrega ) );
        update_post_meta( $this->idn, 'compartida', $this->compartida );
        update_post_meta( $this->idn, 'compartido_con', wp_json_encode( $this->compartido_con ) );
        update_post_meta(
            $this->idn,
            'productos',
            wp_json_encode( $this->get_ids_productos( $this->productos ) )
        );
        update_post_meta(
            $this->idn,
            'cantidades_producto',
            wp_json_encode( $this->cantidades_producto )
        );
        update_post_meta(
            $this->idn,
            'comprados',
            wp_json_encode( $this->get_ids_productos( $this->comprados ) )
        );

        if ( ! isset( $attrs['ID'] ) ) {
            do_action( 'wc_gift_ideas_giftlist_created', $this, wc_gift_ideas_get_giftlist_page_url( $this->get_codigo() ) );
        }

        return true;
	}
	
	public function updateCantidades($post_author)
	{
		update_post_meta(
            $this->idn,
            'productos',
            wp_json_encode( $this->get_ids_productos( $this->productos ) )
        );
        update_post_meta(
            $this->idn,
            'cantidades_producto',
            wp_json_encode( $this->cantidades_producto )
        );
        update_post_meta(
            $this->idn,
            'post_author',
            $giftlist->post_author
        );
	}

	private function cargar_desde_bd( $post_id ) {
		if ( $post_id instanceof WP_Post ) {
			$this->post = $post_id;
		} else {
			$this->post = WP_Post::get_instance( $post_id );
			if ( Woocommerce_Gift_Ideas_Giftlist_Manager::NAME !== $this->post->post_type ) {
				// No es un tipo valido.
				$this->post = null;
			}
		}

		if ( $this->post ) {
			$this->idn                  = (int) $this->post->ID;
			$this->nombre               = get_the_title( $this->idn );
			$this->codigo               = $this->post->codigo;
			$this->cumpleanero_nombre   = $this->post->cumpleanero_nombre;
			$this->cumpleanero_apellido = $this->post->cumpleanero_apellido;
			$this->fecha_cumpleanos     = date_create_from_format( 'Y-m-d', $this->post->fecha_cumpleanos );
			$this->sexo_cumpleanero     = (int) $this->post->sexo_cumpleanero;
			$this->email                = $this->post->email;
			$this->direccion_entrega    = (array) json_decode( $this->post->direccion_entrega, true );
			$this->compartida           = (bool) $this->post->compartida;
			$this->compartido_con       = array_filter(
				array_map( 'absint', (array) json_decode( $this->post->compartido_con ) ),
				function ( $user_id ) {
					return $user_id > 0;
				}
			);
			$this->comprados            = array_filter(
				array_map( 'wc_get_product', (array) json_decode( $this->post->comprados ) ),
				function ( $producto ) {
					return $producto instanceof WC_Product;
				}
			);
			$this->productos            = array_filter(
				array_map( 'wc_get_product', (array) json_decode( $this->post->productos ) ),
				function ( $producto ) {
					return $producto instanceof WC_Product;
				}
			);
			$this->cantidades_producto = (array) json_decode( $this->post->cantidades_producto );
		}
	}

	/**
	 * Validar el objeto antes de guardar.
	 *
	 * @return bool|WP_Error
	 */
	private function validar() {
		if ( empty( $this->nombre ) || empty( $this->cumpleanero_nombre ) || empty( $this->sexo_cumpleanero ) || empty( $this->email ) || ! filter_var( $this->email, FILTER_VALIDATE_EMAIL ) ) {
			return new WP_Error( '10001', __( 'Campos requeridos no suministrados', 'woocommerce-gift-ideas' ) );
		}

		if ( ! in_array( $this->sexo_cumpleanero, [ self::MASCULINO, self::FEMENINO, self::TODOS ], true ) ) {
			return new WP_Error( '10002', __( 'Seleccione el un genero válido', 'woocommerce-gift-ideas' ) );
		}

		return true;
	}

	/**
	 * Verificar si un codigo de la lista es unico.
	 *
	 * @param string $codigo Codigo a verificar.
	 *
	 * @return bool
	 */
	private function is_codigo_unico( $codigo ) {
		wp_reset_postdata();

		$query = new WP_Query( [
			'post_status'    => 'publish',
			'post_type'      => Woocommerce_Gift_Ideas_Giftlist_Manager::NAME,
			'posts_per_page' => - 1,
			'meta_key'       => 'codigo',
			'meta_value'     => $codigo,

		] );

		wp_reset_postdata();

		return 0 === $query->post_count;
	}

	/**
	 * Generar un codigo unico para la lista.
	 *
	 * @return string
	 */
	private function generar_codigo() {
		$code_length = 6;
		$codigo      = [];
		$ndig        = count( $codigo );

		while ( $code_length > $ndig ) {
			$mayus    = chr( rand( 65, 90 ) );
			$minus    = chr( rand( 97, 122 ) );
			$num      = rand( 0, 9 );
			$value    = [ $mayus, $minus, $num ];
			$value    = $value[ rand( 0, 2 ) ];
			$codigo[] = $value;
			$ndig     = count( $codigo );
		}

		return implode( '', $codigo );
	}

	/**
	 * Generar un codigo unico.
	 *
	 * @return string
	 */
	private function generar_codigo_unico() {
		do {
			$codigo = $this->generar_codigo();
		} while ( ! $this->is_codigo_unico( $codigo ) );

		return $codigo;
	}

	/**
	 * Obtener los ids de los productos.
	 *
	 * @param WC_Product[] $productos Productos a obtener los ids.
	 *
	 * @return array
	 */
	private function get_ids_productos( $productos ) {
		return array_map(
			function ( $producto ) {
				/* @var WC_Product $producto */
				return $producto->get_id();
			},
			$productos
		);
	}
}
