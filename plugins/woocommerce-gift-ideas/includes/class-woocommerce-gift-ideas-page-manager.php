<?php
/**
 * Clase para administrar las paginas publicas..
 *
 * Date: 10/21/17
 * Time: 3:19 PM
 *
 * @link       http://modobeta.pe
 * @since      1.0.0
 * @author     Modo Beta SAC <info@beta.pe>
 *
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/includes
 */

if ( ! defined( 'ABSPATH' ) ) {
	// Exit if accessed directly.
	exit;
}

/**
 * Class Woocommerce_Gift_Ideas_Page_Manager.
 *
 * @since      1.0.0
 * @author     Fernando Paz <ferpaz@beta.pe>
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/includes
 */
class Woocommerce_Gift_Ideas_Page_Manager {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $plugin_name The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $version The current version of this plugin.
	 */
	private $version;

	/**
	 * Titulos de las paginas.
	 *
	 * @var array
	 */
	private $titulos;

	/**
	 * Shorcodes de las paginas.
	 *
	 * @var array
	 */
	private $shortcodes;

	/**
	 * La(s) lista(s) actual(es).
	 *
	 * @var Woocommerce_Gift_Ideas_Giftlist|Woocommerce_Gift_Ideas_Giftlist[]|null
	 */
	private $current_giftlist;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 *
	 * @param      string $plugin_name The name of the plugin.
	 * @param      string $version     The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;
		$this->titulos     = [
			'giftlist'        => __( 'Lista de regalo', 'woocommerce-gift-ideas' ),
			'create-search'   => __( 'Crea o busca una lista de regalo', 'woocommerce-gift-ideas' ),
			'giftlist-results'=> __( 'Listas de regalo', 'woocommerce-gift-ideas' ),
			'form'            => __( 'Crear una lista de regalo', 'woocommerce-gift-ideas' ),
			'single-greeting' => __( 'Lista creada', 'woocommerce-gift-ideas' ),
		];
		$this->shortcodes  = [
			'giftlist'        => '[wc_giftideas_listsview]',
			'create-search'   => '[wc_giftideas_create_search]',
			'giftlist-results'=> '[wc_giftideas_listresults]',
			'form'            => '[wc_giftideas_page_form]',
			'single-greeting' => '[wc_giftideas_greeting]',
		];

	}

	/**
	 * Crear las paginas necesarias para la lista.
	 */
	public function create_custom_pages() {
		// Almacenar reglas de url?
		$rewrite = false;

		foreach ( $this->titulos as $key => $title ) {
			$shortcode = $this->shortcodes[ $key ];
			$page_name = $this->transform_page_name( $key );
			if ( ! $this->does_custom_page_exists( $page_name ) ) {
				$page_id = $this->save_custom_page( $page_name, $title, $key, $shortcode );
				if ( $page_id ) {
					$rewrite = true;
				}
			}
		}

		if ( $rewrite ) {
			$this->update_rewrite_rules();
		}

	}

	/**
	 * Actualizar url para listas.
	 */
	public function update_rewrite_rules() {
		set_transient( 'wc_gift_ideas_rewrite_rules', 1, 30 );
	}

	/**
	 * Aplicar actualizacion de url para listas.
	 */
	public function apply_rewrite_rules() {
		if ( ! get_transient( 'wc_gift_ideas_rewrite_rules' ) ) {
			return;
		}
		delete_transient( 'wc_gift_ideas_rewrite_rules' );
		flush_rewrite_rules();
	}

	/**
	 * Crear urls para listas.
	 */
	public function add_rewrite_rules() {
		$pages = $this->get_available_pages_id();

		$pages = array_filter( $pages );
		if ( ! empty( $pages ) ) {
			foreach ( $pages as $page ) {
				$page      = get_post( $page );
				$page_slug = $page->post_name;

				switch ( $page_slug ) {
					case 'giftlist':
						add_rewrite_rule( '(([^/]+/)*' . $page_slug . ')/([A-Za-z0-9]{6})?/page/([0-9]{1,})/{0,1}$', 'index.php?pagename=$matches[1]&wcglID=$matches[3]&paged=$matches[4]', 'top' );
						add_rewrite_rule( '(([^/]+/)*' . $page_slug . ')/([A-Za-z0-9]{6})?/{0,1}$', 'index.php?pagename=$matches[1]&wcglID=$matches[3]', 'top' );
						break;
					case 'giftlist-create-search':
						add_rewrite_rule( '(([^/]+/)*giftlist/search)/{0,1}$', 'index.php?pagename=' . $page_slug, 'top' );
						break;
					case 'giftlist-form':
						add_rewrite_rule( '(([^/]+/)*giftlist/create)/{0,1}$', 'index.php?pagename=' . $page_slug, 'top' );
						break;
					case 'giftlist-results':
						add_rewrite_rule( '(([^/]+/)*giftlist/results)/{0,1}$', 'index.php?pagename=' . $page_slug, 'top' );
						break;
					case 'giftlist-single-greeting':
						add_rewrite_rule( '(([^/]+/)*giftlist/greeting)/([A-Za-z0-9]{6})/{0,1}$', 'index.php?pagename=' . $page_slug . '&wcglID=$matches[3]', 'top' );
						break;
				}
			}
		}
	}

	/**
	 * Agregar query variable.
	 *
	 * @param array $public_var WordPress Public variable.
	 *
	 * @return array
	 */
	public function add_query_var( $public_var ) {
		$public_var[] = 'wcglID';
		$public_var[] = 'wcglis';

		return $public_var;
	}

	/**
	 * Obtener el id de las paginas disponibles.
	 *
	 * @return array
	 */
	public function get_available_pages_id() {
		$page_names = array_map( [ $this, 'transform_page_name' ], array_keys( $this->titulos ) );

		return array_filter(
			array_map( [ $this, 'get_custom_page_id' ], $page_names ),
			function ( $page_id ) {
				return ! is_null( $page_id );
			}
		);
	}

	/**
	 * Crear los shortcodes para las paginas.
	 */
	public function create_shortcodes() {
		foreach ( $this->shortcodes as $key => $shortcode ) {
			add_shortcode( trim( $shortcode, '[]' ), [ $this, 'shortcode_' . str_replace( '-', '_', $key ) ] );
		}
	}

	/**
	 * Shortcode giftlist.
	 *
	 * @param array $atts Array parameter from shortcode.
	 *
	 * @return string
	 */
	public function shortcode_giftlist( $atts = [] ) {
		$default = [
			'lists_per_page' => 10,
		];
		$atts    = shortcode_atts( $default, $atts );

		ob_start();
		$this->giftlist_page( $atts );

		return ob_get_clean();
	}

	/**
	 * Shortcode giftlist.
	 *
	 * @param array $atts Array parameter from shortcode.
	 *
	 * @return string
	 */
	public function shortcode_giftlist_results( $atts = [] ) {
		$default = [
			'lists_per_page' => 10,
		];
		$atts    = shortcode_atts( $default, $atts );

		ob_start();
		$this->giftlist_results_page( $atts );

		return ob_get_clean();
	}

	/**
	 * Shortcode buscar y crear lista de regalos.
	 *
	 * @param array $atts Shortcode atts.
	 *
	 * @return string
	 */
	public function shortcode_create_search( $atts = [] ) {
		ob_start();

		$this->create_search_page( $atts );

		return ob_get_clean();
	}

	/**
	 * Shortcode formulario de lista de regalos.
	 *
	 * @param array $atts Shortcode atts.
	 *
	 * @return string
	 */
	public function shortcode_form( $atts = [] ) {
		ob_start();

		$this->form_page( $atts );

		return ob_get_clean();
	}

	/**
	 * Shortcode para vista de creada la lista de regalos.
	 *
	 * @param array $atts Shortcode atts.
	 *
	 * @return string
	 */
	public function shortcode_single_greeting( $atts = [] ) {
		// Renderizar la vista de creada la lista.
		$giftlist = $this->get_current_giftlist();

		if ( ! ( $giftlist instanceof Woocommerce_Gift_Ideas_Giftlist ) ) {
			return '';
		}

		ob_start();

		wc_get_template(
			'createdgiftlist.php',
			[
				'giftlist' => $giftlist,
			],
			'',
			WOOCOMMERCE_GIFT_IDEAS_PATH . 'templates/'
		);

		return ob_get_clean();
	}

	/**
	 * Obtener el enlace a la pagina de listado de giftlists.
	 *
	 * @return string
	 */
	public function get_giftlist_page_url() {
		return get_page_link( $this->get_custom_page_id( $this->transform_page_name( 'giftlist' ) ) );
	}

	/**
	 * Obtener el enlace a la pagina de listado de giftlists.
	 *
	 * @return string
	 */
	public function get_giftlist_results_page_url() {
		return wc_gift_ideas_get_giftlist_page_url( 'giftlist-result' );
	}

	/**
	 * Obtener el enlace de la pagina de busqueda y creacion de una lista de regalos.
	 *
	 * @return string
	 */
	public function get_giftlist_create_search_page_url() {
		return wc_gift_ideas_get_giftlist_page_url( 'search' );
	}

	/**
	 * Obtener el enlace de la pagina de formulario de una lista de regalos.
	 *
	 * @return string
	 */
	public function get_giftlist_form_url() {
		return wc_gift_ideas_get_giftlist_page_url( 'create' );
	}

	/**
	 * Transforma un nombre de una pagina al formato de pagina esperado.
	 *
	 * @param string $key Clave de la pagina.
	 *
	 * @return string
	 */
	private function transform_page_name( $key ) {
		return 'giftlist' === $key ? $key : 'giftlist-' . $key;
	}

	/**
	 * Verificar si existe una pagina.
	 *
	 * @param string $page_name Titulo de la pagina.
	 *
	 * @return bool
	 */
	private function does_custom_page_exists( $page_name ) {
		return (bool) get_option( 'wc_gift_ideas_page_exists_' . $page_name, false );
	}

	/**
	 * Verificar si existe una pagina.
	 *
	 * @param string $page_name Titulo de la pagina.
	 *
	 * @return int|null
	 */
	private function get_custom_page_id( $page_name ) {
		$page_id = get_option( 'wc_gift_ideas_page_' . $page_name, null );

		return ! is_null( $page_id ) ? (int) $page_id : $page_id;
	}

	/**
	 * Guardar el id de una pagina.
	 *
	 * @param string $page_name Titulo de la pagina.
	 * @param int    $page_id   Id de la pagina.
	 */
	private function set_custom_page_id( $page_name, $page_id ) {
		add_option( 'wc_gift_ideas_page_exists_' . $page_name, true );
		add_option( 'wc_gift_ideas_page_' . $page_name, (int) $page_id );
	}

	/**
	 * Guardar una pagina personalizada.
	 *
	 * @param string $page_name Nombre unico de la pagina.
	 * @param string $title     Titulo de la pagina.
	 * @param string $key       Clave unica de la pagina.
	 * @param string $shortcode Shortcode a escuchar para la pagina.
	 *
	 * @return int|null
	 */
	private function save_custom_page( $page_name, $title, $key, $shortcode ) {
		$title = apply_filters( 'wc_gitf_ideas_create_new_page_post_title', $title, $key );

		$_page   = [
			'post_name'      => $page_name,
			'post_title'     => $title,
			'post_content'   => $shortcode,
			'post_status'    => 'publish',
			'post_type'      => 'page',
			'comment_status' => 'closed',
			'ping_status'    => 'closed',
			'post_category'  => [ 1 ],
		];
		$page_id = wp_insert_post( $_page );

		if ( $page_id instanceof WP_Error ) {
			// TODO: send an error.
			return null;
		}

		$this->set_custom_page_id( $page_name, $page_id );

		return (int) $page_id;
	}

	/**
	 * Obtener la(s) lista(s) actual(es).
	 *
	 * @return null|Woocommerce_Gift_Ideas_Giftlist|Woocommerce_Gift_Ideas_Giftlist[]
	 */
	private function get_current_giftlist() {
		if ( empty( $this->current_giftlist ) ) {
			$this->current_giftlist = wc_gift_ideas_get_giftlist();
		}

		return $this->current_giftlist;
	}

	/**
	 * Obtener la(s) lista(s) actual(es).
	 *
	 * @return null|Woocommerce_Gift_Ideas_Giftlist|Woocommerce_Gift_Ideas_Giftlist[]
	 */
	private function get_search_result_giftlist() {
		if ( empty( $this->current_giftlist ) ) {
			$this->current_giftlist = wc_gift_ideas_get_giftlist();
		}

		return $this->current_giftlist;
	}

	/**
	 * Tells third party services that there are tags other than pure HTML tags within the document.
	 *
	 * @param string $output Document output.
	 *
	 * @return string
	 */
	public function doctype_opengraph( $output ) {
		if ( ! is_admin() ) {
			$giftlist = $this->get_current_giftlist();
			if ( $giftlist instanceof Woocommerce_Gift_Ideas_Giftlist ) {
				$output .= "\n" . 'xmlns:og="http://opengraphprotocol.org/schema/"';
				$output .= "\n" . 'xmlns:fb="http://www.facebook.com/2008/fbml"';
			}
		}

		return $output;
	}

	/**
	 * Agregar opengraph meta tags cuando es una unica giftlist.
	 */
	public function add_opengraph_meta_tags() {
		$giftlist = $this->get_current_giftlist();
		if ( $giftlist instanceof Woocommerce_Gift_Ideas_Giftlist ) {
			$nombre      = apply_filters( 'wc_gift_ideas_opengraph_title', $giftlist->get_nombre(), $giftlist );
			$descripcion = sprintf(
				'%s %s, %s %s',
				__( 'Lista de regalos para', 'woocommerce-gift-ideas' ),
				$giftlist->get_cumpleanero(),
				__( 'celebralo con nosotros el día', 'woocommerce-gift-ideas' ),
				$giftlist->get_fecha_cumpleanos()->format( 'd/m/Y' )
			);
			$descripcion = esc_html( apply_filters( 'wc_gift_ideas_opengraph_description', $descripcion, $giftlist ) );
			$tipo        = esc_html( apply_filters( 'wc_gift_ideas_opengraph_type', 'article', $giftlist ) );
			$url         = esc_url( apply_filters( 'wc_gift_ideas_opengraph_url', wc_gift_ideas_get_giftlist_page_url( $giftlist->get_codigo() ), $giftlist ) );
			$sitio       = esc_html( apply_filters( 'wc_gift_ideas_opengraph_site', get_bloginfo(), $giftlist ) );
			if ( $giftlist->get_sexo_cumpleanero() === Woocommerce_Gift_Ideas_Giftlist::MASCULINO ) {
				$imagen = 'https://cdn2.iconfinder.com/data/icons/boy-emojis/512/boy-16-512.png';
			} else {
				$imagen = 'https://i3.silhcdn.com/3/i/shapes/lg/2/3/d49332.jpg';
			}
			$imagen = esc_url( apply_filters( 'wc_gift_ideas_opengraph_image', $imagen, $giftlist ) );

			echo <<<END
<meta property="og:title" content="$nombre"/>
<meta property="og:description" content="$descripcion"/>
<meta property="og:type" content="$tipo"/>
<meta property="og:url" content="$url"/>
<meta property="og:site_name" content="$sitio"/>
<meta property="og:image" content="$imagen"/>
END;
		}
	}

	/**
	 * Mostrar contenido de la pagina de lista de regalo.
	 *
	 * @param array $atts Atributos del shortcode a ejecutar.
	 */
	private function giftlist_page( $atts ) {
		if ( is_admin() ) {
			return;
		}
		// Ubicar la lista por el query var. Si existe la lista, mostrar la lista para dueño o compartido.
		$giftlist     = $this->get_current_giftlist();
		$pagina       = (int) get_query_var( 'paged', 1 );
		$pagina       = $pagina > 0 ? $pagina : 1;
		$n_por_pagina = $atts['lists_per_page'];

		if ( is_array( $giftlist ) ) {
			if ( ! is_user_logged_in() ) {
				// No deben ingresar a esta pagina sin iniciar sesion.
				wp_safe_redirect(
					wp_login_url(
						get_the_permalink()
					)
				);
				wp_die();
			}

			// Vista de listado de listas.
			// Reducir la lista segun la pagina actual.
			$n_total_giftlist = count( $giftlist );
			$n_paginas        = ceil( $n_total_giftlist / $n_por_pagina );
			$pagina           = $pagina > $n_paginas ? $n_paginas : $pagina;
			$giftlist         = array_slice(
				$giftlist,
				( ( $pagina - 1 ) * $n_por_pagina ),
				$n_por_pagina
			);

			// Preparar la data para la vista.
			$data = [
				'giftlists'         => $giftlist,
				'n_total_giftlists' => $n_total_giftlist,
				'pagina'            => $pagina,
				'n_paginas'         => $n_paginas,
			];

			// Renderizar la vista.
			wc_get_template(
				'giftlist-list.php',
				$data,
				'',
				WOOCOMMERCE_GIFT_IDEAS_PATH . 'templates/'
			);
		} else {
			// Vista de listado de productos en una lista.
			if ( ! $giftlist ) {
				// No hay una lista.
				if ( ! is_user_logged_in() ) {
					wc_get_template(
						'giftlist-list.php',
						[],
						'',
						WOOCOMMERCE_GIFT_IDEAS_PATH . 'templates/'
					);
				} else {
					wc_get_template(
						'giftlist-null.php',
						[ 'giftlists' => [] ],
						'',
						WOOCOMMERCE_GIFT_IDEAS_PATH . 'templates/'
					);
				}
			} else {
				// Reducir la lista segun la pagina actual.
				$productos_mostrar = ( get_current_user_id() === (int) $giftlist->post_author ? $giftlist->get_productos() : $giftlist->get_productos_por_comprar() );
				$n_total_productos = count( $productos_mostrar );
				$n_paginas         = ceil( $n_total_productos / $n_por_pagina );
				$pagina            = $pagina > $n_paginas ? $n_paginas : $pagina;
				$productos         = array_slice(
					$productos_mostrar,
					( ( $pagina - 1 ) * $n_por_pagina ),
					$n_por_pagina
				);
				$cantidades_producto = $giftlist->get_cantidades_producto();

				// Preparar la data para la vista. 
				$data = [
					'giftlist'          => $giftlist,
					'productos'         => $productos,
					'n_total_productos' => $n_total_productos,
					'pagina'            => $pagina,
					'n_paginas'         => $n_paginas,
					'cantidades_producto'         => $cantidades_producto,
				]; 

				// Renderizar la vista.
				wc_get_template(
					( $n_total_productos > 0 ? 'giftlist.php' : 'giftlist-empty.php' ),
					$data,
					'',
					WOOCOMMERCE_GIFT_IDEAS_PATH . 'templates/'
				);
			}
		}
	}

	/**
	 * Mostrar contenido de la pagina de lista de regalo.
	 *
	 * @param array $atts Atributos del shortcode a ejecutar.
	 */
	private function giftlist_results_page( $atts ) {
		if ( is_admin() ) {
			return;
		}
		// Ubicar la lista por el query var. Si existe la lista, mostrar la lista para dueño o compartido.
		$giftlist     = $this->get_current_giftlist();
		$pagina       = (int) get_query_var( 'paged', 1 );
		$pagina       = $pagina > 0 ? $pagina : 1;
		$n_por_pagina = $atts['lists_per_page'];

		if ( is_array( $giftlist ) ) {
			if ( ! is_user_logged_in() ) {
				// No deben ingresar a esta pagina sin iniciar sesion.
				wp_safe_redirect(
					wp_login_url(
						get_the_permalink()
					)
				);
				wp_die();
			}

			// Vista de listado de listas.
			// Reducir la lista segun la pagina actual.
			$n_total_giftlist = count( $giftlist );
			$n_paginas        = ceil( $n_total_giftlist / $n_por_pagina );
			$pagina           = $pagina > $n_paginas ? $n_paginas : $pagina;
			$giftlist         = array_slice(
				$giftlist,
				( ( $pagina - 1 ) * $n_por_pagina ),
				$n_por_pagina
			);

			// Preparar la data para la vista.
			$data = [
				'giftlists'         => $giftlist,
				'n_total_giftlists' => $n_total_giftlist,
				'pagina'            => $pagina,
				'n_paginas'         => $n_paginas,
			];

			// Renderizar la vista.
			wc_get_template(
				'giftlist-list.php',
				$data,
				'',
				WOOCOMMERCE_GIFT_IDEAS_PATH . 'templates/'
			);
		} else {
			// Vista de listado de productos en una lista.
			if ( ! $giftlist ) {
				// No hay una lista.
				if ( ! is_user_logged_in() ) {
					wc_get_template(
						'giftlist-list.php',
						[],
						'',
						WOOCOMMERCE_GIFT_IDEAS_PATH . 'templates/'
					);
				} else {
					wc_get_template(
						'giftlist-null.php',
						[ 'giftlists' => [] ],
						'',
						WOOCOMMERCE_GIFT_IDEAS_PATH . 'templates/'
					);
				}
			} else {
				// Reducir la lista segun la pagina actual.
				$productos_mostrar = ( get_current_user_id() === (int) $giftlist->post_author ? $giftlist->get_productos() : $giftlist->get_productos_por_comprar() );
				$n_total_productos = count( $productos_mostrar );
				$n_paginas         = ceil( $n_total_productos / $n_por_pagina );
				$pagina            = $pagina > $n_paginas ? $n_paginas : $pagina;
				$productos         = array_slice(
					$productos_mostrar,
					( ( $pagina - 1 ) * $n_por_pagina ),
					$n_por_pagina
				);

				// Preparar la data para la vista.
				$data = [
					'giftlist'          => $giftlist,
					'productos'         => $productos,
					'n_total_productos' => $n_total_productos,
					'pagina'            => $pagina,
					'n_paginas'         => $n_paginas,
				];

				// Renderizar la vista.
				wc_get_template(
					( $n_total_productos > 0 ? 'giftlist.php' : 'giftlist-empty.php' ),
					$data,
					'',
					WOOCOMMERCE_GIFT_IDEAS_PATH . 'templates/'
				);
			}
		}
	}

	/**
	 * Pagina de buscar y crear lista de regalos.
	 *
	 * @param array $atts Shortcode atts.
	 */
	private function create_search_page( $atts ) {
		// Renderizar la vista.
		wc_get_template(
			( 'giftlist-create-search.php' ),
			[
				'shortcode_atts' => $atts,
				'loged'          => is_user_logged_in(),
				'ultimo_codigo'  => wc_gift_ideas_last_giftlist_code(),
			],
			'',
			WOOCOMMERCE_GIFT_IDEAS_PATH . 'templates/'
		);
	}

	/**
	 * Pagina de formulario de lista de regalos.
	 *
	 * @param array $atts Shortcode atts.
	 */
	private function form_page( $atts ) {
		// Giftlist vacia.
		$giftlist = new Woocommerce_Gift_Ideas_Giftlist();
		if ( 1 === wp_verify_nonce( wc_gift_ideas_get_post_sanitized( 'wc-gift-ideas-nonce', '', 'string' ), 'create_giftlist' ) ) {
			// Registrar lista.
			// Obtener datos de la lista y validar datos de la lista.
			$result = Woocommerce_Gift_Ideas::get_public_instance()->create_giftlist_from_post( $giftlist );
			if ( $result instanceof WP_Error ) {
				wc_add_notice( $result->get_error_message(), 'error' );
			} else {
				// Renderizar la vista de creada la lista.
				wc_get_template(
					'createdgiftlist.php',
					[
						'giftlist' => $giftlist,
					],
					'',
					WOOCOMMERCE_GIFT_IDEAS_PATH . 'templates/'
				);

				return;
			}
		}

		// Renderizar la para el formulario.
		wc_get_template(
			'creategiftlist-form.php',
			[
				'ajax'     => false,
				'address'  => Woocommerce_Gift_Ideas_User_Address_Helper::get_address_form(),
				'giftlist' => $giftlist,
			],
			'',
			WOOCOMMERCE_GIFT_IDEAS_PATH . 'templates/'
		);
	} 
}
