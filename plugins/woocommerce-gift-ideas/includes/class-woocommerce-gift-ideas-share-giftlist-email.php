<?php
/**
 * Clase para enviar correos de listas de deseos compartidas.
 *
 * Date: 10/28/17
 * Time: 3:26 PM
 *
 * @link       http://modobeta.pe
 * @since      1.0.0
 * @author     Fernando Paz <ferpaz@beta.pe>
 *
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/includes
 */

if ( ! defined( 'ABSPATH' ) ) {
	// Exit if accessed directly.
	exit;
}

/**
 * Class Woocommerce_Gift_Ideas_Share_Giftlist_Mail.
 *
 * @since      1.0.0
 * @author     Fernando Paz <ferpaz@beta.pe>
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/includes
 */
class Woocommerce_Gift_Ideas_Share_Giftlist_Email extends WC_Email {

	/**
	 * Giftlist a enviar.
	 *
	 * @var Woocommerce_Gift_Ideas_Giftlist
	 */
	private $giftlist;

	/**
	 * Mensaje a colocar en el correo.
	 *
	 * @var string
	 */
	private $message = '';

	/**
	 * WooCommerce_Beta_Discount_Prices_Next_Order_Coupon_Mail constructor.
	 */
	public function __construct() {
		// Unique ID for custom email.
		$this->id = 'wc_gift_ideas_shared_giftlist_email';

		// Is a customer email.
		$this->customer_email = true;

		// Title field in Email settings.
		$this->title = __( 'Compartir lista de deseos', 'woocommerce-gift-ideas' );

		// Description field in email settings.
		$this->description = __( 'Es un email enviado para compartir una lista de deseos.', 'woocommerce-gift-ideas' );

		// Default heading and subject lines in WooCommerce email settings.
		$this->subject = apply_filters( 'wc_gift_ideas_shared_giftlist_email_subject', __( 'Te han compartido una lista de regalos', 'woocommerce-gift-ideas' ) );
		$this->heading = apply_filters( 'wc_gift_ideas_shared_giftlist_email_heading', __( 'Alguien cumpleaños próximamente, estas cordialmente invitado!', 'woocommerce-gift-ideas' ) );

		// Fix the template base lookup for use on admin screen template path display.
		$this->template_base  = WOOCOMMERCE_GIFT_IDEAS_PATH . 'templates/emails/';
		$this->template_html  = 'share-giftlist.php';
		$this->template_plain = 'plain/share-gitflist.php';

		// Call parent constructor to load any other defaults not explicity defined here.
		parent::__construct();
	}

	/**
	 * Prepares email content and triggers the email.
	 *
	 * @param string                          $message  Mensaje del email.
	 * @param Woocommerce_Gift_Ideas_Giftlist $giftlist Lista de regalos a compartir.
	 * @param array                           $emails   Correos a donde enviar la lista.
	 */
	public function trigger( $message, Woocommerce_Gift_Ideas_Giftlist $giftlist, $emails = [] ) {
		// Bail?
		if ( ! $giftlist->get_id() || empty( $emails ) || ! $this->is_enabled() ) {
			return;
		}

		$this->message  = $message;
		$this->giftlist = $giftlist;

		foreach ( $emails as $email ) {
			$this->recipient = $email;
			if ( ! $this->get_recipient() ) {
				continue;
			}

			// All well, send the email.
			$this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
		}
	}

	/**
	 * Obtener el contenido html del email.
	 *
	 * @return string
	 */
	public function get_content_html() {
		return wc_get_template_html(
			$this->template_html,
			[
				'email_heading' => $this->get_heading(),
				'giftlist'      => $this->giftlist,
				'message'       => $this->message,
				'sent_to_admin' => false,
				'plain_text'    => false,
				'email'         => $this,
			],
			'',
			$this->template_base
		);
	}

	/**
	 * Obtener el contenido en texto plano del email.
	 *
	 * @return string
	 */
	public function get_content_plain() {
		return wc_get_template_html(
			$this->template_plain,
			[
				'email_heading' => $this->get_heading(),
				'giftlist'      => $this->giftlist,
				'message'       => $this->message,
				'sent_to_admin' => false,
				'plain_text'    => true,
				'email'         => $this,
			],
			'',
			$this->template_base
		);
	}

	/**
	 * Initialize settings form fields.
	 */
	public function init_form_fields() {
		$this->form_fields = [
			'enabled'    => [
				'title'   => __( 'Enable/Disable', 'woocommerce' ),
				'type'    => 'checkbox',
				'label'   => 'Enable this email notification',
				'default' => 'yes',
			],
			'subject'    => [
				'title'       => __( 'Subject', 'woocommerce' ),
				'type'        => 'text',
				'description' => sprintf( 'This controls the email subject line. Leave blank to use the default subject: <code>%s</code>.', $this->subject ),
				'placeholder' => '',
				'default'     => '',
			],
			'heading'    => [
				'title'       => __( 'Email Heading', 'woocommerce' ),
				'type'        => 'text',
				'description' => sprintf( __( 'This controls the main heading contained within the email notification. Leave blank to use the default heading: <code>%s</code>.' ), $this->heading ),
				'placeholder' => '',
				'default'     => '',
			],
			'email_type' => [
				'title'       => __( 'Email type', 'woocommerce' ),
				'type'        => 'select',
				'description' => __( 'Choose which format of email to send.', 'woocommerce' ),
				'default'     => 'html',
				'class'       => 'email_type wc-enhanced-select',
				'options'     => [
					'plain'     => __( 'Plain text', 'woocommerce' ),
					'html'      => __( 'HTML', 'woocommerce' ),
					'multipart' => __( 'Multipart', 'woocommerce' ),
				],
			],
		];
	}
}
