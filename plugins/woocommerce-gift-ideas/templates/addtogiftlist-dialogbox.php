<?php
/**
 * The Template for displaying dialog to add product to a giftlist.
 *
 * @version 1.0.0
 * @package Woocommerce_Gift_Ideas/templates
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/** Listas de regalos. @var Woocommerce_Gift_Ideas_Giftlist[] $giftlists */
?>
<div class="wc-gift-ideas-add-to-wishlist wc-gift-ideas-modal modal-dialog" role="document">
	<div class="wc-gift-ideas-table modal-content">
		<div class="modal-header">
			<h5 class="wc-gift-ideas-txt"><?php esc_html_e( 'Añadir a lista de regalos', 'woocommerce-gift-ideas' ); ?></h5>
			<button type="button" class="close" data-dismiss="modal"
					aria-label="<?php esc_html_e( 'Cerrar', 'woocommerce-gift-ideas' ); ?>">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="wc-gift-ideas-cell modal-body">
			<?php if ( isset( $notice ) && ! empty( $notice ) ) : ?>
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="<?php esc_html_e( 'Cerrar', 'woocommerce-gift-ideas' ); ?>">
						<span aria-hidden="true">&times;</span>
					</button>
					<?php echo esc_html( $notice ); ?>
				</div>
			<?php endif; ?>
			<div class="wc-gift-ideas-buttons-group wc-gift-ideas-wishlist-clear">
				<a href="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>"
				   data-action="create_giftlist"
				   class="button wc-gift-ideas-button-view wc-gift-ideas-create-giftlist">
					<i class="fa fa-heart-o"></i>
					<?php esc_html_e( 'Crear una lista', 'woocommerce-gift-ideas' ); ?>
				</a>
				<?php if ( isset( $giftlists ) && ! empty( $giftlists ) ) : ?>
					<label for="wc-gift-ideas-giftlist-id"><?php esc_html_e( 'Listas disponibles', 'woocommerce-gift-ideas' ); ?></label>
					<select name="wc-gift-ideas-giftlist" id="wc-gift-ideas-giftlist-id">
						<?php foreach ( $giftlists as $giftlist ) : ?>
							<option value="<?php echo esc_attr( $giftlist->get_id() ); ?>"><?php echo esc_html( $giftlist->get_nombre() ); ?></option>
						<?php endforeach; ?>
					</select>
					<a href="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>"
					   data-action="add_to_giftlist"
					   class="button wc-gift-ideas-button-view wc-gift-ideas-addto-giftlist-final">
						<i class="fa fa-heart-o"></i>
						<?php esc_html_e( 'Añadir', 'woocommerce-gift-ideas' ); ?>
					</a>
				<?php else : ?>
					<p><?php esc_attr_e( 'No hay listas de regalos disponibles.', 'woocommerce-gift-ideas' ); ?></p>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
