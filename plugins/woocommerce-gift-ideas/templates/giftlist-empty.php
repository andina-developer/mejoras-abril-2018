<?php
/**
 * The Template for displaying an empty giftlist.
 *
 * @version 1.0.0
 * @package Woocommerce_Gift_Ideas/templates
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/* @var Woocommerce_Gift_Ideas_Giftlist $giftlist */
?>
<div class="wc-gift-ideas-giftlist woocommerce wc-gift-ideas-giftlist-clear">
	<?php do_action( 'wc_gift_ideas_before_wishlist', $giftlist ); ?>
	<?php
	if ( function_exists( 'wc_print_notices' ) ) {
		wc_print_notices();
	}
	?>
	<h5><?php echo esc_html( $giftlist->get_nombre() ); ?></h5>

	<p class="return-to-shop">
		<a class="button wc-backward"
		   href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>">
			<?php esc_html_e( 'Regresar a la tienda', 'woocommerce-gift-ideas' ); ?>
		</a>
		<?php if ( get_current_user_id() === (int) $giftlist->post_author ) : ?>
			<a class="button wc-backward"
			   href="<?php echo esc_url( wc_gift_ideas_get_giftlist_page_url() ); ?>">
				<?php esc_html_e( 'Regresar al listado', 'woocommerce-gift-ideas' ); ?>
			</a>
		<?php endif; ?>
	</p>

	<div>
		<?php echo esc_html( $giftlist->get_cumpleanero() ); ?>
		<br>
		<?php echo esc_html( $giftlist->get_fecha_cumpleanos()->format( get_option( 'date_format' ) ) ); ?>
	</div>

	<p class="cart-empty">
		<?php if ( get_current_user_id() === (int) $giftlist->post_author ) { ?>
			<?php esc_html_e( 'Tu lista de regalos está vacía.', 'woocommerce-gift-ideas' ); ?>
		<?php } else { ?>
			<?php esc_html_e( 'La lista de regalos está vacía.', 'woocommerce-gift-ideas' ); ?>
		<?php } ?>
	</p>

	<?php do_action( 'wc_gift_ideas_wishlist_is_empty' ); ?>
</div>
