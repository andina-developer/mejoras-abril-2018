<?php
/**
 * Template for the modal that holds the form to create a giftlist.
 *
 * Date: 10/25/17
 * Time: 6:23 PM
 *
 * @link       http://modobeta.pe
 * @since      1.0.0
 * @author     Fernando Paz <ferpaz@beta.pe>
 *
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/templates
 */

?>
<div class="wc-gift-ideas-create-wishlist wc-gift-ideas-modal modal-dialog modal-lg" role="document">
	<div class="wc-gift-ideas-table modal-content">
		<div class="modal-header">
			<h5 class="wc-gift-ideas-txt"><?php esc_html_e( 'Crear lista de regalos', 'woocommerce-gift-ideas' ); ?></h5>
			<button type="button" class="close" data-dismiss="modal"
					aria-label="<?php esc_html_e( 'Cerrar', 'woocommerce-gift-ideas' ); ?>">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="wc-gift-ideas-cell modal-body">
			<?php if ( isset( $notice ) && ! empty( $notice ) ) : ?>
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="<?php esc_html_e( 'Cerrar', 'woocommerce-gift-ideas' ); ?>">
						<span aria-hidden="true">&times;</span>
					</button>
					<?php echo esc_html( $notice ); ?>
				</div>
			<?php endif; ?>
			<?php
			wc_get_template(
				'creategiftlist-form.php',
				[
					'ajax'     => true,
					'address'  => $address,
					'giftlist' => $giftlist,
				],
				'',
				WOOCOMMERCE_GIFT_IDEAS_PATH . 'templates/'
			);
			?>
		</div>
	</div>
</div>
