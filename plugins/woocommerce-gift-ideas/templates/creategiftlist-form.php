<?php
/**
 * Form to create a giftlist.
 *
 * Date: 10/25/17
 * Time: 6:38 PM
 *
 * @link       http://modobeta.pe
 * @since      1.0.0
 * @author     Fernando Paz <ferpaz@beta.pe>
 *
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/templates
 */

/* @var Woocommerce_Gift_Ideas_Giftlist $giftlist */
$current_user = wp_get_current_user();
$fecha_cumpleanos = '';
if ( $giftlist->get_fecha_cumpleanos() ) {
	$fecha_cumpleanos = $giftlist->get_fecha_cumpleanos()->format( 'Y-m-d' );
}
?>
<?php if ( ! $ajax ) : ?>
	<div class="row">
		<?php
		if ( function_exists( 'wc_print_notices' ) ) {
			wc_print_notices();
		}
		?>
	</div>
<?php endif; ?>
<div class="wc-gift-ideas-form container">
	<form action="<?php echo $ajax ? esc_attr( admin_url( 'admin-ajax.php' ) ) : ''; ?>"
		  method="post"
		  class="wc-gift-ideas-form-create-giftlist">
		<?php if ( $ajax ) : ?>
			<input type="hidden" name="action" value="create_giftlist">
		<?php endif; ?>
		<?php wp_nonce_field( 'create_giftlist', 'wc-gift-ideas-nonce' ); ?>

		<div class="form-group row">
			<label class="col-sm-2 col-form-label"
				   for="nombre"><?php esc_html_e( 'Nombre de lista', 'woocommerce-gift-ideas' ); ?></label>
			<div class="col-sm-10">
				<input type="text" required class="form-control" id="nombre" name="nombre" aria-describedby="nombreHelp"
					   placeholder="<?php esc_attr_e( 'Nombre de la lista', 'woocommerce-gift-ideas' ); ?>"
					   value="<?php echo esc_attr( $giftlist->get_nombre() ); ?>">
				<small id="nombreHelp"
					   class="form-text text-muted"><?php esc_html_e( 'Nombre público de la lista', 'woocommerce-gift-ideas' ); ?></small>
			</div>
		</div>

		<div class="form-group row">
			<label class="col-sm-2 col-form-label"
				   for="cumpleanero_nombre"><?php esc_html_e( 'Nombre', 'woocommerce-gift-ideas' ); ?></label>
			<div class="col-sm-10">
				<input type="text" required class="form-control" id="cumpleanero_nombre" name="cumpleanero_nombre"
					   value="<?php if(esc_attr( $giftlist->get_cumpleanero_nombre())!=""){ echo esc_attr( $giftlist->get_cumpleanero_nombre()); }else{ echo $current_user->first_name; } ?>">
			</div>
		</div>

		<div class="form-group row">
			<label class="col-sm-2 col-form-label"
				   for="cumpleanero_apellido"><?php esc_html_e( 'Apellido', 'woocommerce-gift-ideas' ); ?></label>
			<div class="col-sm-10">
				<input type="text" required class="form-control" id="cumpleanero_apellido" name="cumpleanero_apellido"
					   value="<?php if(esc_attr( $giftlist->get_cumpleanero_apellido())!=""){ echo esc_attr( $giftlist->get_cumpleanero_apellido()); }else{ echo $current_user->last_name; } ?>">
			</div>
		</div>

		<div class="form-group row">
			<label class="col-sm-2 col-form-label"
				   for="fecha_cumpleanos"><?php esc_html_e( 'Fecha del cumpleaños', 'woocommerce-gift-ideas' ); ?></label>
			<div class="col-sm-10">
				<input type="date" required class="form-control" id="fecha_cumpleanos" name="fecha_cumpleanos"
					   min="<?php echo esc_attr( date( 'Y-m-d' ) ); ?>"
					   value="<?php echo esc_attr( $fecha_cumpleanos ); ?>">
			</div>
		</div>

		<div class="form-group row">
			<label class="col-sm-2 col-form-label"
				   for="correo_electronico"><?php esc_html_e( 'Correo electrónico', 'woocommerce-gift-ideas' ); ?></label>
			<div class="col-sm-10">
				<input type="email" required class="form-control" id="correo_electronico" name="correo_electronico"
					   value="<?php echo esc_attr( $giftlist->get_email() ); ?>">
			</div>
		</div>

		<div class="form-group row">
			<label class="col-sm-2 col-form-label"><?php esc_html_e( 'Sexo', 'woocommerce-gift-ideas' ); ?></label>
			<div class="col-sm-10">
				<div class="form-check form-check-inline">
					<label class="form-check-label">
						<input type="radio" class="form-check-input" required name="sexo_cumpleanero"
							   id="sexo_cumpleanero1"
							   value="<?php echo esc_attr( Woocommerce_Gift_Ideas_Giftlist::MASCULINO ); ?>"
						       <?php if ( Woocommerce_Gift_Ideas_Giftlist::MASCULINO === $giftlist->get_sexo_cumpleanero() ) : ?>checked<?php endif; ?>>
						<?php esc_html_e( 'Niño', 'woocommerce-gift-ideas' ); ?>
					</label>
				</div>
				<div class="form-check form-check-inline">
					<label class="form-check-label">
						<input type="radio" class="form-check-input" required name="sexo_cumpleanero"
							   id="sexo_cumpleanero2"
							   value="<?php echo esc_attr( Woocommerce_Gift_Ideas_Giftlist::FEMENINO ); ?>"
						       <?php if ( Woocommerce_Gift_Ideas_Giftlist::FEMENINO === $giftlist->get_sexo_cumpleanero() ) : ?>checked<?php endif; ?>>
						<?php esc_html_e( 'Niña', 'woocommerce-gift-ideas' ); ?>
					</label>
				</div>
				<div class="form-check form-check-inline">
					<label class="form-check-label">
						<input type="radio" class="form-check-input" required name="sexo_cumpleanero"
							   id="sexo_cumpleanero3"
							   value="<?php echo esc_attr( Woocommerce_Gift_Ideas_Giftlist::TODOS ); ?>"
						       <?php if ( Woocommerce_Gift_Ideas_Giftlist::TODOS === $giftlist->get_sexo_cumpleanero() ) : ?>checked<?php endif; ?>>
						<?php esc_html_e( 'Ver Todos', 'woocommerce-gift-ideas' ); ?>
					</label>
				</div>
			</div>
		</div>

		<div class="form-group row">
			<div class="col-sm-12">
				<strong><?php esc_html_e( 'Dirección de entrega', 'woocommerce-gift-ideas' ); ?></strong></div>
		</div>
		<?php
		foreach ( $address as $key => $field ) {
			$prop_name = str_replace( 'direccion_entrega[', '', rtrim( $key, ']' ) );
			if ( isset( $field['country_field'], $address[ $field['country_field'] ] ) ) {
				$field['country'] = ( isset( $giftlist->get_direccion_entrega()[ $prop_name ] ) ? $giftlist->get_direccion_entrega()[ $prop_name ] : $address[ $field['country_field'] ]['value'] );
			}
			wc_gift_ideas_address_form_field(
				$key,
				$field,
				( isset( $giftlist->get_direccion_entrega()[ $prop_name ] ) ? $giftlist->get_direccion_entrega()[ $prop_name ] : $field['value'] )
			);
		}
		?>

		<div class="form-group row">
			<div class="col-sm-12">
				<button type="submit"
						class="btn btn-primary pull-right"><?php esc_html_e( 'Guardar', 'woocommerce-gift-ideas' ); ?></button>
			</div>
		</div>
	</form>
</div>
