<?php
/**
 * Template when succesfully shared the giftlist.
 *
 * @version 1.0.0
 * @package Woocommerce_Gift_Ideas/templates
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/* @var Woocommerce_Gift_Ideas_Giftlist $giftlist */
?>
<div class="wc-gift-ideas-shared-wishlist wc-gift-ideas-modal modal-dialog" role="document">
	<div class="wc-gift-ideas-table modal-content">
		<div class="modal-header">
			<h5 class="wc-gift-ideas-txt"><?php esc_html_e( 'Lista de regalos enviada', 'woocommerce-gift-ideas' ); ?></h5>
			<button type="button" class="close" data-dismiss="modal"
					aria-label="<?php esc_html_e( 'Cerrar', 'woocommerce-gift-ideas' ); ?>">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="wc-gift-ideas-cell modal-body">
			<div class="alert alert-success" role="alert">
				<?php
				echo esc_html(
					sprintf(
						'%s "%s" %s "%s" %s %s',
						__( 'Tu lista', 'woocommerce-gift-ideas' ),
						$giftlist->get_nombre(),
						__( 'se ha compartido exitosamente con el código', 'woocommerce-gift-ideas' ),
						$giftlist->get_codigo(),
						__( 'a los correos', 'woocommerce-gift-ideas' ),
						implode( ', ', $correos )
					)
				);
				?>
			</div>
		</div>
	</div>
</div>
