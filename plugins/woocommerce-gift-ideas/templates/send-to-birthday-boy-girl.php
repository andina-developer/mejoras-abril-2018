<?php
/**
 * Checkbox para cargar direccion de la lista de regalos durante el checkout.
 *
 * Date: 10/29/17
 * Time: 4:51 PM
 *
 * @link       http://modobeta.pe
 * @since      1.0.0
 * @author     Fernando Paz <ferpaz@beta.pe>
 *
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/templates
 */

/* @var Woocommerce_Gift_Ideas_Giftlist $giftlist */
?>

<h3 id="ship-to-birthday-boy-girl-address">
	<label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
		<input id="ship-to-birthday-boy-girl-address"
			   class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox"
			   data-address="<?php echo esc_attr( wp_json_encode( $giftlist->get_direccion_entrega() ) ); ?>"
			   type="checkbox" name="ship_to_different_address" value="1"/>
		<span><?php esc_html_e( '¿Enviar a dirección del cumpleañer@?', 'woocommerce-gift-ideas' ); ?></span>
	</label>
</h3>
