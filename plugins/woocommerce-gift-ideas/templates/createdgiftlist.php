<?php
/**
 * Tempalte when a giftlist is created.
 *
 * Date: 11/4/17
 * Time: 11:40 AM
 *
 * @link       http://modobeta.pe
 * @since      1.0.0
 * @author     Fernando Paz <ferpaz@beta.pe>
 *
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/templates
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
/* @var Woocommerce_Gift_Ideas_Giftlist $giftlist */
?>
<div class="wc-gift-ideas-created-giftlist" role="document">
	<div class="row justify-content-center">
		<div class="col">
			<h4><?php esc_html_e( 'Bienvenid@', 'woocommerce-gift-ideas' ); ?> <?php echo esc_html( wp_get_current_user()->display_name ); ?></h4>
			<p><?php esc_html_e( 'Tu lista ya fue creada', 'woocommerce-gift-ideas' ); ?></p>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col">
			<p><?php esc_html_e( 'Comparte con tus invitados este código', 'woocommerce-gift-ideas' ); ?></p>
			<p><?php echo esc_html( $giftlist->get_codigo() ); ?></p>
			<p><?php esc_html_e( 'Para que puedan acceder a tu lista', 'woocommerce-gift-ideas' ); ?></p>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col">
			<a class="btn btn-primary"
			   href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) . '#hide-add-to-cart=1' ); ?>">
				<?php esc_html_e( 'Elegir regalos', 'woocommerce-gift-ideas' ); ?>
			</a>
		</div>
	</div>
</div>
