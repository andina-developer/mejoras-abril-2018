<?php
/**
 * Email template for a new giftlist.
 *
 * Date: 11/26/17
 * Time: 5:28 PM
 *
 * @link       http://modobeta.pe
 * @since      1.0.0
 * @author     Fernando Paz <ferpaz@beta.pe>
 *
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/templates/emails
 */

/* @var Woocommerce_Gift_Ideas_Giftlist $giftlist */
?>
<div class="wc-gift-ideas-giftlist">
	<h5>
		<a href="<?php esc_url( $url ); ?>">
			<?php echo esc_html( $giftlist->get_nombre() ); ?>
		</a>
	</h5>

	<div class="row">
		<div class="col-md-12">
			<?php echo esc_html( $giftlist->get_cumpleanero() ); ?>
		</div>
		<div class="col-md-12">
			<?php echo esc_html( $giftlist->get_fecha_cumpleanos()->format( get_option( 'date_format' ) ) ); ?>
		</div>
		<?php if ( ! empty( $direccion = $giftlist->get_direccion_entrega() ) ) : ?>
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12">
						<strong><?php esc_html_e( 'Dirección de entrega', 'woocommerce-gift-ideas' ); ?></strong></div>
					<div class="col-md-12"><?php echo json_encode( $giftlist->get_direccion_entrega(), true ); ?></div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</div>
