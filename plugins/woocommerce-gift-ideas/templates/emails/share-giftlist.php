<?php
/**
 * Email template to share a giftlist.
 *
 * Date: 10/28/17
 * Time: 3:48 PM
 *
 * @link       http://modobeta.pe
 * @since      1.0.0
 * @author     Fernando Paz <ferpaz@beta.pe>
 *
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/templates/emails
 */

/* @var Woocommerce_Gift_Ideas_Giftlist $giftlist */
?>
<div class="wc-gift-ideas-giftlist">
	<h5>
		<a href="<?php esc_url( wc_gift_ideas_get_giftlist_page_url( $giftlist->get_codigo() ) ); ?>">
			<?php echo esc_html( $giftlist->get_nombre() ); ?>
		</a>
	</h5>

	<div class="row">
		<div class="col-md-12">
			<?php echo esc_html( $giftlist->get_cumpleanero() ); ?>
		</div>
		<div class="col-md-12">
			<?php echo esc_html( $giftlist->get_fecha_cumpleanos()->format( get_option( 'date_format' ) ) ); ?>
		</div>
		<?php if ( ! empty( $direccion = $giftlist->get_direccion_entrega() ) ) : ?>
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12">
						<strong><?php esc_html_e( 'Dirección de entrega', 'woocommerce-gift-ideas' ); ?></strong></div>
					<div class="col-md-12"><?php echo WC()->countries->get_formatted_address( $direccion ); ?></div>
				</div>
			</div>
		<?php endif; ?>
	</div>

	<table class="wc-gift-ideas-table-manage-list">
		<thead>
		<tr>
			<th class="producto-minitura">&nbsp;</th>
			<th class="producto-nombre"><?php esc_html_e( 'Producto', 'woocommerce-gift-ideas' ); ?></th>
		</tr>
		</thead>
		<tbody>

		<?php
		foreach ( $giftlist->get_productos_por_comprar() as $producto ) {

			$producto_url = apply_filters( 'wc_gift_ideas_giftlist_producto_url', $producto->get_permalink(), $producto, $giftlist );
			?>
			<tr class="<?php echo esc_attr( apply_filters( 'wc_gift_ideas_giftlist_producto_class', 'giftlist-producto', $producto, $giftlist ) ); ?>">
				<td class="producto-miniatura">
					<?php
					$thumbnail = apply_filters( 'wc_gift_ideas_giftlist_producto_minitura', $producto->get_image(), $producto, $giftlist );

					if ( ! $producto->is_visible() ) {
						echo $thumbnail; // WPCS: xss ok.
					} else {
						echo sprintf( '<a href="%s">%s</a>', esc_url( $producto_url ), $thumbnail ); // WPCS: xss ok.
					}
					?>
				</td>
				<td class="producto-nombre">
					<?php
					if ( ! $producto->is_visible() ) {
						$nombre = $producto->get_title();
						$nombre .= '&nbsp;';
					} else {
						$nombre = '<a href="' . esc_url( $producto_url ) . '">' . $producto->get_title() . '</a>';
					}

					echo apply_filters( 'wc_gift_ideas_giftlist_producto_nombre', $nombre, $producto, $giftlist );
					?>
				</td>
			</tr>
			<?php

		} // End foreach().
		?>

		</tbody>
	</table>
</div>
