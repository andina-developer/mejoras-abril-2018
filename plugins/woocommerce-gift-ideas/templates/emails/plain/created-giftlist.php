<?php
/**
 * Email template for a new giftlist in plain text.
 *
 * Date: 11/26/17
 * Time: 5:27 PM
 *
 * @link       http://modobeta.pe
 * @since      1.0.0
 * @author     Fernando Paz <ferpaz@beta.pe>
 *
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/templates/emails
 */

/* @var Woocommerce_Gift_Ideas_Giftlist $giftlist */
?>
<?php esc_html_e( 'Ingresa a: ', 'woocommerce-gift-ideas' ); ?><?php esc_url( $url ); ?>
<?php esc_html_e( 'Para ver la lista de deseos creada.', 'woocommerce-gift-ideas' ); ?>
<?php echo esc_html( $giftlist->get_nombre() ); ?>
