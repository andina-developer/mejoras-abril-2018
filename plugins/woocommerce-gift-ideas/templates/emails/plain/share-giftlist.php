<?php
/**
 * Email template to share a giftlist in plain text.
 *
 * Date: 10/28/17
 * Time: 3:48 PM
 *
 * @link       http://modobeta.pe
 * @since      1.0.0
 * @author     Fernando Paz <ferpaz@beta.pe>
 *
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/templates/emails
 */

/* @var Woocommerce_Gift_Ideas_Giftlist $giftlist */
?>
<?php esc_html_e( 'Ingresa a: ', 'woocommerce-gift-ideas' ); ?><?php esc_url( wc_gift_ideas_get_giftlist_page_url( $giftlist->get_codigo() ) ); ?>
<?php esc_html_e( 'Para ver la lista de deseos que te han compartido.', 'woocommerce-gift-ideas' ); ?>
<?php echo esc_html( $giftlist->get_nombre() ); ?>
