<?php
/**
 * Template search or create a giftlist.
 *
 * Date: 04/11/17
 * Time: 10:50 AM
 *
 * @link       http://modobeta.pe
 * @since      1.0.0
 * @author     Fernando Paz <ferpaz@beta.pe>
 *
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/templates
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<div class="wc-gift-ideas-giftlist woocommerce wc-gift-ideas-giftlist-clear">
	<div class="row">
		<?php
		if ( function_exists( 'wc_print_notices' ) ) {
			wc_print_notices();
		}
		?>
	</div>

	<div class="row justify-content-center">
		<div class="col">
			<a class="button"
			   href="<?php echo esc_url( wc_gift_ideas_get_giftlist_form_page_url() ); ?>">
				<?php esc_html_e( 'Crear una lista', 'woocommerce-gift-ideas' ); ?>
			</a>
			<?php if ( $loged && $ultimo_codigo ) : ?>
				<a class="button"
				   href="<?php echo esc_url( wc_gift_ideas_get_giftlist_page_url( $ultimo_codigo ) ); ?>">
					<?php esc_html_e( 'Edita tu lista', 'woocommerce-gift-ideas' ); ?>
				</a>
			<?php endif; ?>
		</div>
		<div class="col">
			<a class="button"
			   id="wc-gift-ideas-search-giftlist"
			   data-action="search_giftlist"
			   href="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>">
				<?php esc_html_e( 'Busca una lista', 'woocommerce-gift-ideas' ); ?>
			</a>
		</div>
	</div>
</div>
