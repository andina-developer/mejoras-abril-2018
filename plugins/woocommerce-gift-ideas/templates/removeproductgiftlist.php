<?php
/**
 * The Template for displaying add to giftlist product button.
 *
 * @version 1.0.0
 * @package Woocommerce_Gift_Ideas/templates
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
global $idlistaregalos;
global $product;
/** Producto woocommerce. @var WC_Product $producto */

?>
<a href="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>"
   class="d-inline-block text-center alt wc-gift-ideas-producto-remove rojo"
   data-action="remove_from_giftlist"
   data-giftlist-id="<?php echo $idlistaregalos; ?>"
   data-producto-id="<?php echo esc_attr( $product->get_id() ); ?>"
   title="<?php esc_attr_e( 'Eliminar', 'woocommerce-gift-ideas' ); ?>">
	<i class="icon-cerrar"></i>
</a>

