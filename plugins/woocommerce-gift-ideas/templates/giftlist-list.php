<?php
/**
 * The Template for displaying giftlists.
 *
 * @version 1.0.0
 * @package Woocommerce_Gift_Ideas/templates
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<div class="wc-gift-ideas-giftlist woocommerce wc-gift-ideas-giftlist-clear">
	<?php
	if ( function_exists( 'wc_print_notices' ) ) {
		wc_print_notices();
	}
	?>
	<p class="return-to-shop">
		<a class="button wc-backward"
		   href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>">
			<?php esc_html_e( 'Regresar a la tienda', 'woocommerce-gift-ideas' ); ?>
		</a>
	</p>
	<?php if ( empty( $giftlists ) ) : ?>
		<p class="cart-empty">
			<?php esc_html_e( 'No tienes listas de regalo guardadas', 'woocommerce-gift-ideas' ); ?>
		</p>

		<?php do_action( 'wc_gift_ideas_wishlists_is_empty' ); ?>
	<?php else : ?>
		<?php do_action( 'wc_gift_ideas_before_giftlists', $giftlists, $n_total_giftlists, $pagina, $n_paginas ); ?>

		<?php do_action( 'wc_gift_ideas_before_giftlists_table', $giftlists, $n_total_giftlists, $pagina, $n_paginas ); ?>

		<table class="wc-gift-ideas-table-manage-list">

			<thead>
			<tr>
				<th class="giftlist-nombre"><?php esc_html_e( 'Nombre', 'woocommerce-gift-ideas' ); ?></th>
				<th class="giftlist-cumpleanero"><?php esc_html_e( 'Cumpleañer@', 'woocommerce-gift-ideas' ); ?></th>
				<th class="giftlist-fecha-cumpleanos"><?php esc_html_e( 'Fecha del cumpleaños', 'woocommerce-gift-ideas' ); ?></th>
				<th class="giftlist-n-regalos"><?php esc_html_e( 'Número de juguetes', 'woocommerce-gift-ideas' ); ?></th>
				<th class="giftlist-compartido"><?php esc_html_e( 'Compartida', 'woocommerce-gift-ideas' ); ?></th>
				<th class="giftlist-creado-el"><?php esc_html_e( 'Creada el', 'woocommerce-gift-ideas' ); ?></th>
				<th class="giftlist-action">&nbsp;</th>
			</tr>
			</thead>

			<tbody>

			<?php do_action( 'wc_gift_ideas_giftlists_contents_before' ); ?>

			<?php
			foreach ( $giftlists as $giftlist ) {
				/* @var Woocommerce_Gift_Ideas_Giftlist $giftlist */

				do_action( 'wc_gift_ideas_giftlist_row_before', $giftlist );

				?>
				<tr class="<?php echo esc_attr( apply_filters( 'wc_gift_ideas_giftlist_class', 'wc-gift-ideas-giftlist', $giftlist ) ); ?>">
					<td class="giftlist-nombre"><?php echo esc_html( $giftlist->get_nombre() ); ?></td>
					<td class="giftlist-cumpleanero"><?php echo esc_html( $giftlist->get_cumpleanero() ); ?></td>
					<td class="giftlist-fecha-cumpleanos">
						<time
								class="entry-date"
								datetime="<?php echo esc_attr( $giftlist->get_fecha_cumpleanos()->format( 'Y-m-d 00:00:00' ) ); ?>">
							<?php echo esc_html( mysql2date( get_option( 'date_format' ), $giftlist->get_fecha_cumpleanos()->format( 'Y-m-d 00:00:00' ) ) ); ?>
						</time>
					</td>
					<td class="giftlist-n-regalos"><?php echo esc_html( count( $giftlist->get_productos() ) ); ?></td>
					<td class="giftlist-compartido">
						<?php
						echo esc_html(
							(
							! empty( $giftlist->get_compartido_con() ) || $giftlist->is_compartida() ?
								__( 'Si', 'woocommerce-gift-ideas' )
								:
								__( 'No', 'woocommerce-gift-ideas' )
							)
						);
						?>
					</td>
					<td class="giftlist-creado-el">
						<time
								class="entry-date"
								datetime="<?php echo esc_attr( get_the_date( get_option( 'date_format' ), $giftlist->get_id() ) ); ?>">
							<?php echo esc_html( get_the_date( get_option( 'date_format' ), $giftlist->get_id() ) ); ?>
						</time>
					</td>
					<td class="giftlist-action">
						<a class="button wc-gift-ideas-giftlist-show"
						   href="<?php echo esc_url( wc_gift_ideas_get_giftlist_page_url( $giftlist->get_codigo() ) ); ?>">
							<?php esc_html_e( 'Ver', 'woocommerce-gift-ideas' ); ?>
						</a>
						<a class="button wc-gift-ideas-giftlist-share-whatsapp"
						   href="whatsapp://send?text=<?php echo rawurlencode( esc_url( wc_gift_ideas_get_giftlist_page_url( $giftlist->get_codigo() ) ) ); ?>"
						   data-giftlist-id="<?php echo esc_attr( $giftlist->get_id() ); ?>"
						   data-action="share/whatsapp/share"
						   data-href="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>"
						   data-href-action="share_giftlist_whatsapp">
							<?php esc_html_e( 'Enviar por whatsapp', 'woocommerce-gift-ideas' ); ?>
						</a>
						<a class="button wc-gift-ideas-giftlist-share-email"
						   data-giftlist-id="<?php echo esc_attr( $giftlist->get_id() ); ?>"
						   data-action="share_giftlist"
						   href="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>">
							<?php esc_html_e( 'Compartir por correo', 'woocommerce-gift-ideas' ); ?>
						</a>
						<a class="button wc-gift-ideas-giftlist-remove"
						   title="<?php esc_attr_e( 'Eliminar', 'woocommerce-gift-ideas' ); ?>"
						   data-action="remove_giftlist"
						   data-giftlist-id="<?php echo esc_attr( $giftlist->get_id() ); ?>"
						   href="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>">
							X
						</a>
					</td>
				</tr>
				<?php
				do_action( 'wc_gift_ideas_giftlist_row_after', $giftlist );
			} // End foreach().
			?>
			<?php do_action( 'wc_gift_ideas_giftlists_contents_after' ); ?>
			</tbody>
			<tfoot>
			<tr>
				<td colspan="100%">
					<?php do_action( 'wc_gift_ideas_after_giftlists_table', $giftlist ); ?>
				</td>
			</tr>
			</tfoot>
		</table>

		<?php do_action( 'wc_gift_ideas_after_giftlists', $giftlists, $n_total_giftlists, $pagina, $n_paginas ); ?>

		<div class="tinv-lists-nav wc-gift-ideas-giftlist-clear">
			<?php do_action( 'wc_gift_ideas_pagenation_giftlist', $pagina, $n_paginas, $n_total_giftlists ); ?>
		</div>
	<?php endif; ?>
</div>
