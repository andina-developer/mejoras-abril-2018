<?php
/**
 * The Template for displaying dialog for message added to giftlist product.
 *
 * @version 1.0.0
 * @package Woocommerce_Gift_Ideas/templates
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/* @var Woocommerce_Gift_Ideas_Giftlist $giftlist */

/* @var WC_Product $product */
?>
<div class="wc-gift-ideas-add-to-wishlist wc-gift-ideas-modal modal-dialog" role="document">
	<div class="wc-gift-ideas-table modal-content">
		<?php if ( ! isset( $notice ) || empty( $notice ) ) : ?>
			<div class="modal-header">
				<h5 class="wc-gift-ideas-txt"><?php esc_html_e( 'Añadido a lista de regalos', 'woocommerce-gift-ideas' ); ?></h5>
				<button type="button" class="close" data-dismiss="modal"
						aria-label="<?php esc_html_e( 'Cerrar', 'woocommerce-gift-ideas' ); ?>">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="wc-gift-ideas-cell modal-body">
				<div class="alert alert-success" role="alert">
					<?php echo esc_html( get_the_title( $product->get_id() ) ); ?>
					<?php esc_html_e( 'Añadido a lista de regalos', 'woocommerce-gift-ideas' ); ?>
					<a href="<?php echo esc_url( wc_gift_ideas_get_giftlist_page_url( $giftlist->get_codigo() ) ); ?>"
					   class="btn btn-link">
						<?php echo esc_html( $giftlist->get_nombre() ); ?>
					</a>
				</div>
			</div>
		<?php else : ?>
			<div class="wc-gift-ideas-cell modal-body">
				<div class="alert alert-error" role="alert">
					<?php echo esc_html( $notice ); ?>
				</div>
			</div>
		<?php endif; ?>
	</div>
</div>
