<?php
/**
 * The Template for displaying a giftlist.
 *
 * @version 1.0.0
 * @package Woocommerce_Gift_Ideas/templates
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/** Lista de regalos. @var Woocommerce_Gift_Ideas_Giftlist $giftlist */

/** Listado de productos. @var WC_Product[] $productos */
?>
<div class="wc-gift-ideas-giftlist woocommerce wc-gift-ideas-giftlist-clear">
	<?php do_action( 'wc_gift_ideas_before_giftlist', $giftlist ); ?>
	<?php
	if ( function_exists( 'wc_print_notices' ) ) {
		wc_print_notices();
	}
	?>
	<h5><?php echo esc_html( $giftlist->get_nombre() ); ?></h5>

	<p class="return-to-shop">
		<a class="button wc-backward"
		   href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>">
			<?php esc_html_e( 'Regresar a la tienda', 'woocommerce-gift-ideas' ); ?>
		</a>
		<?php if ( get_current_user_id() === (int) $giftlist->post_author ) : ?>
			<a class="button wc-backward"
			   href="<?php echo esc_url( wc_gift_ideas_get_giftlist_page_url() ); ?>">
				<?php esc_html_e( 'Regresar al listado', 'woocommerce-gift-ideas' ); ?>
			</a>
			<a class="button wc-gift-ideas-giftlist-share-whatsapp"
			   href="whatsapp://send?text=<?php echo rawurlencode( esc_url( wc_gift_ideas_get_giftlist_page_url( $giftlist->get_codigo() ) ) ); ?>"
			   data-giftlist-id="<?php echo esc_attr( $giftlist->get_id() ); ?>"
			   data-action="share/whatsapp/share"
			   data-href="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>"
			   data-href-action="share_giftlist_whatsapp">
				<?php esc_html_e( 'Enviar por whatsapp', 'woocommerce-gift-ideas' ); ?>
			</a>
			<a class="button wc-gift-ideas-giftlist-share-email"
			   data-giftlist-id="<?php echo esc_attr( $giftlist->get_id() ); ?>"
			   data-action="share_giftlist"
			   href="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>">
				<?php esc_html_e( 'Compartir por correo', 'woocommerce-gift-ideas' ); ?>
			</a>
		<?php endif; ?>
	</p>

	<div class="row">
		<div class="col-md-12">
			<?php echo esc_html( $giftlist->get_cumpleanero() ); ?>
		</div>
		<div class="col-md-12">
			<?php echo esc_html( $giftlist->get_fecha_cumpleanos()->format( get_option( 'date_format' ) ) ); ?>
		</div>
		<?php if ( ! empty( $direccion = $giftlist->get_direccion_entrega() ) ) : ?>
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12">
						<strong><?php esc_html_e( 'Dirección de entrega', 'woocommerce-gift-ideas' ); ?></strong></div>
					<div class="col-md-12"><?php echo WC()->countries->get_formatted_address( $direccion ); ?></div>
				</div>
			</div>
		<?php endif; ?>
	</div>


	<?php do_action( 'wc_gift_ideas_before_giftlist_table', $giftlist ); ?>
	<table class="wc-gift-ideas-table-manage-list">
		<thead>
		<tr>
			<th class="producto-minitura">&nbsp;</th>
			<th class="producto-nombre"><?php esc_html_e( 'Producto', 'woocommerce-gift-ideas' ); ?></th>
			<th class="producto-precio"><?php esc_html_e( 'Precio unitario', 'woocommerce-gift-ideas' ); ?></th>
			<th class="producto-stock"><?php esc_html_e( 'Estado del stock', 'woocommerce-gift-ideas' ); ?></th>
			<th class="producto-action">&nbsp;</th>
		</tr>
		</thead>
		<tbody>
		<?php do_action( 'wc_gift_ideas_giftlist_contents_before' ); ?>

		<?php
		foreach ( $productos as $producto ) {
			do_action( 'wc_gift_ideas_giftlist_producto_row_before', $producto, $giftlist );

			$producto_url = apply_filters( 'wc_gift_ideas_giftlist_producto_url', $producto->get_permalink(), $producto, $giftlist );
			?>
			<tr class="<?php echo esc_attr( apply_filters( 'wc_gift_ideas_giftlist_producto_class', 'giftlist-producto', $producto, $giftlist ) ); ?>">
				<td class="producto-miniatura">
					<?php
					$thumbnail = apply_filters( 'wc_gift_ideas_giftlist_producto_minitura', $producto->get_image(), $producto, $giftlist );

					if ( ! $producto->is_visible() ) {
						echo $thumbnail; // WPCS: xss ok.
					} else {
						echo sprintf( '<a href="%s">%s</a>', esc_url( $producto_url ), $thumbnail ); // WPCS: xss ok.
					}
					?>
				</td>
				<td class="producto-nombre">
					<?php
					if ( ! $producto->is_visible() ) {
						$nombre = $producto->get_title();
						$nombre .= '&nbsp;';
					} else {
						$nombre = '<a href="' . esc_url( $producto_url ) . '">' . $producto->get_title() . '</a>';
					}

					echo apply_filters( 'wc_gift_ideas_giftlist_producto_nombre', $nombre, $producto, $giftlist );
					?>
				</td>
				<td class="producto-precio">
					<?php
					echo apply_filters( 'wc_gift_ideas_giftlist_producto_precio', $producto->get_price_html(), $producto, $giftlist ); // WPCS: xss ok.
					?>
				</td>
				<td class="producto-stock">
					<?php
					$availability = (array) $producto->get_availability();
					if ( ! array_key_exists( 'availability', $availability ) ) {
						$availability['availability'] = '';
					}
					if ( ! array_key_exists( 'class', $availability ) ) {
						$availability['class'] = '';
					}
					$availability_html = empty( $availability['availability'] ) ?
						'<p class="stock ' . esc_attr( $availability['class'] ) . '"><span><i class="fa fa-check"></i></span><span class="wc_gift_ideas-txt">' . esc_html__( 'Disponible', 'woocommerce-gift-ideas' ) . '</span></p>'
						:
						'<p class="stock ' . esc_attr( $availability['class'] ) . '"><span><i class="fa fa-check"></i></span><span>' . esc_html( $availability['availability'] ) . '</span></p>';

					echo apply_filters( 'wc_gift_ideas_giftlist_item_status', $availability_html, $availability['availability'], $producto, $giftlist ); // WPCS: xss ok.
					?>
				</td>
				<td class="producto-action">
					<?php if ( get_current_user_id() !== (int) $giftlist->post_author ) : ?>
						<a href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) . '?add-to-cart=' . $producto->get_id() ) ?>"
						   class="button product_type_simple add_to_cart_button ajax_add_to_cart wc-gift-ideas-producto-add-to-cart"
						   data-product_id="<?php echo esc_attr( $producto->get_id() ); ?>"
						   data-quantity="<?php echo esc_attr( $producto->get_sku() ); ?>"
						   data-product_sku="<?php echo esc_attr( $producto->get_sku() ); ?>"
						   data-href="<?php echo esc_attr( admin_url( 'admin-ajax.php' ) ); ?>"
						   data-action="add_to_cart_from_giftlist"
						   data-giftlist-id="<?php echo esc_attr( $giftlist->get_id() ); ?>"
						   data-producto-id="<?php echo esc_attr( $producto->get_id() ); ?>">
							<i class="fa fa-shopping-cart"></i>
							<span class="wc-gift-ideas-txt">
								<?php esc_html_e( 'Agregar al carrito', 'woocommerce-gift-ideas' ); ?>
							</span>
						</a>
					<?php else : ?>
						<a href="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>"
						   class="button alt wc-gift-ideas-producto-remove"
						   data-action="remove_from_giftlist"
						   data-giftlist-id="<?php echo esc_attr( $giftlist->get_id() ); ?>"
						   data-producto-id="<?php echo esc_attr( $producto->get_id() ); ?>"
						   title="<?php esc_attr_e( 'Eliminar', 'woocommerce-gift-ideas' ); ?>">
							X
						</a>
					<?php endif; ?>
				</td>
			</tr>
			<?php

			do_action( 'wc_gift_ideas_giftlist_producto_row_after', $producto, $giftlist );
		} // End foreach().
		?>

		<?php do_action( 'wc_gift_ideas_giftlist_contents_after' ); ?>

		</tbody>
		<tfoot>
		<tr>
			<td colspan="100%">
				<?php do_action( 'wc_gift_ideas_after_giftlist_table', $giftlist ); ?>
			</td>
		</tr>
		</tfoot>
	</table>

	<?php do_action( 'wc_gift_ideas_after_giftlist', $giftlist ); ?>

	<div class="tinv-lists-nav wc-gift-ideas-giftlist-clear">
		<?php do_action( 'wc_gift_ideas_pagenation_giftlist', $pagina, $n_paginas, $n_total_productos ); ?>
	</div>
</div>
