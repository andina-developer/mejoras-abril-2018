<?php
/**
 * The Template for displaying an unexistent giftlist.
 *
 * @version 1.0.0
 * @package Woocommerce_Gift_Ideas/templates
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<div class="wc-gift-ideas-giftlist woocommerce wc-gift-ideas-giftlist-clear">
	<p class="cart-empty">
		<?php esc_html_e( 'Lista de regalos no encontrada!', 'woocommerce-gift-ideas' ); ?>
	</p>

	<?php do_action( 'wc_gift_ideas_wishlist_is_null' ); ?>

	<p class="return-to-shop">
		<a class="button wc-backward"
		   href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>">
			<?php esc_html_e( 'Regresar a la tienda', 'woocommerce-gift-ideas' ); ?>
		</a>
	</p>
</div>
