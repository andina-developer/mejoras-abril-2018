<?php
/**
 * Dialogo para enviar por email la lista de deseos.
 *
 * Date: 10/28/17
 * Time: 1:05 PM
 *
 * @link       http://modobeta.pe
 * @since      1.0.0
 * @author     Fernando Paz <ferpaz@beta.pe>
 *
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/templates
 */

/* @var Woocommerce_Gift_Ideas_Giftlist $giftlist */
?>

<div class="wc-gift-ideas-share-wishlist wc-gift-ideas-modal modal-dialog modal-lg" role="document">
	<div class="wc-gift-ideas-table modal-content">
		<div class="modal-header">
			<h5 class="wc-gift-ideas-txt"><?php esc_html_e( 'Compartir lista de regalos', 'woocommerce-gift-ideas' ); ?><?php echo esc_html( $giftlist->get_nombre() ); ?></h5>
			<button type="button" class="close" data-dismiss="modal"
					aria-label="<?php esc_html_e( 'Cerrar', 'woocommerce-gift-ideas' ); ?>">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="wc-gift-ideas-cell modal-body">
			<?php if ( isset( $notice ) && ! empty( $notice ) ) : ?>
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="<?php esc_html_e( 'Cerrar', 'woocommerce-gift-ideas' ); ?>">
						<span aria-hidden="true">&times;</span>
					</button>
					<?php echo esc_html( $notice ); ?>
				</div>
			<?php endif; ?>
			<div class="wc-gift-ideas-form container">
				<form action="<?php echo esc_attr( admin_url( 'admin-ajax.php' ) ); ?>" method="post"
					  class="wc-gift-ideas-form-share-giftlist">
					<input type="hidden" name="giftlist_id" value="<?php echo esc_attr( $giftlist->get_id() ); ?>">
					<input type="hidden" name="action" value="share_giftlist">
					<?php wp_nonce_field( 'share_giftlist', 'wc-gift-ideas-nonce' ); ?>

					<div class="form-group row">
						<label class="col-sm-3 col-form-label"
							   for="correos"><?php esc_html_e( 'Direcciones de correo', 'woocommerce-gift-ideas' ); ?></label>
						<div class="col-sm-9">
							<input type="text" required class="form-control" id="correos" name="correos"
								   aria-describedby="correosHelp"
								   placeholder="<?php esc_attr_e( 'Ej: john@gmail.com, jane@hotmail.com', 'woocommerce-gift-ideas' ); ?>"
								   value="<?php echo esc_attr( implode( ', ', $correos ) ); ?>">
							<small id="correosHelp"
								   class="form-text text-muted"><?php esc_html_e( 'Lista de correos a enviar, separados por coma (,)', 'woocommerce-gift-ideas' ); ?></small>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-3 col-form-label"
							   for="mensaje"><?php esc_html_e( 'Deja un mensaje', 'woocommerce-gift-ideas' ); ?></label>
						<div class="col-sm-9">
							<textarea name="mensaje" id="mensaje" cols="30" rows="10" class="fom-control"
									  required="required"><?php echo $mensaje; ?></textarea>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-sm-12">
							<button type="submit"
									class="btn btn-primary pull-right"><?php esc_html_e( 'Enviar', 'woocommerce-gift-ideas' ); ?></button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
