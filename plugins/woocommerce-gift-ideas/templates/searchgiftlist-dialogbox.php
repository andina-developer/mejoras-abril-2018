<?php
/**
 * The Template for displaying dialog to search a giftlist.
 *
 * @version 1.0.0
 * @package Woocommerce_Gift_Ideas/templates
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/** Lista disponibles. @var Woocommerce_Gift_Ideas_Giftlist[] $giftlists */
?>
<div class="modal-dialog" role="document">
	<div id="listas-disponibles" class="modal-content custom-modal">
		<i data-dismiss="modal" class="icon-cerrar cerrar"></i>
		<div class="modal-body">
			<?php if ( isset( $notice ) && ! empty( $notice ) ) : ?>
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="<?php esc_html_e( 'Cerrar', 'woocommerce-gift-ideas' ); ?>">
						<span aria-hidden="true">&times;</span>
					</button>
					<?php echo esc_html( $notice ); ?>
				</div>
			<?php endif; ?>
			<form id="wc-gift-ideas-search-giftlist"
				  action="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>" method="post">
				<input type="hidden" name="action" value="search_giftlist">
				<?php wp_nonce_field( 'search_giftlist', 'wc-gift-ideas-nonce' ); ?>
				<div class="form-group">
					<label for="codigo"><?php esc_html_e( 'Código de lista', 'woocommerce-gift-ideas' ); ?></label>
					<input type="text" class="form-control" id="codigo" name="codigo"
						   value="<?php echo esc_attr( $codigo ); ?>">
				</div>
				<p class="line-o text-center"><span><?php esc_html_e( 'Ó', 'woocommerce-gift-ideas' ); ?></span></p>
				<div class="form-group">
					<label for="nombre"><?php esc_html_e( 'Nombre, apellido y/o nombre de lista', 'woocommerce-gift-ideas' ); ?></label>
					<input type="text" class="form-control" id="nombre" name="nombre"
						   value="<?php echo esc_attr( $nombre ); ?>">
				</div>
				<button type="submit" id="btn-buscarlista"
						class="btn btn-lg btn-block btn-primary"><?php esc_html_e( 'Buscar', 'woocommerce-gift-ideas' ); ?></button>
			</form>
		</div>
		<?php if ( isset( $giftlists ) && ! empty( $giftlists ) ) : ?>
			<div class="row">
				<div class="col">
					<div class="giftlist-list list-group">
                        <label for="nombre">Hemos encontrado las siguientes listas</label>
						<?php foreach ( $giftlists as $giftlist ) : ?> 
							<a href="<?php echo esc_url( wc_gift_ideas_get_giftlist_page_url( $giftlist->get_codigo() ) ); ?>"
							   class="btn btn-border-primary btn-sm text-center mb-2">
								<span><?php echo esc_html( $giftlist->get_nombre() ); ?></span> |
								<span><?php echo esc_html( $giftlist->get_cumpleanero() ); ?></span> |
								<span><?php echo esc_html( mysql2date( get_option( 'date_format' ), $giftlist->get_fecha_cumpleanos()->format( 'Y-m-d 00:00:00' ) ) ); ?></span>
							</a>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</div>
