<?php
/**
 * The Template for displaying add to giftlist product button.
 *
 * @version 1.0.0
 * @package Woocommerce_Gift_Ideas/templates
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/** Producto woocommerce. @var WC_Product $producto */
$parent_id = $producto->get_parent_id();
?>
<?php if (!isset($_GET['referalid'])) { ?>
<a rel="nofollow"
   href="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>"
   class="button add-regalo woocommerce wc-gift-ideas-giftlist-addto-giftlist"
   title="<?php esc_html_e( 'Añadir a la lista de regalos', 'woocommerce-gift-ideas' ); ?>"
   data-action="add_to_giftlist"
   data-modal-action="modal"
   data-producto-id="<?php echo esc_attr( ( $parent_id ? $parent_id : $producto->get_id() ) ); ?>"
	<?php if ( $parent_id ) : ?>
		data-variation-id="<?php echo esc_attr( $producto->get_id() ); ?>"
	<?php endif; ?>>
	<i class="icon-regalo"></i>
	<?php do_action( 'wc_gift_ideas_addtogiftlist_button' ); ?>
	<!--Use data-modal-action with "direct" to add to add it to default giftlist or "dynamic-ajax" to create on the fly giftlist and select one-->
</a>
<?php } ?>
