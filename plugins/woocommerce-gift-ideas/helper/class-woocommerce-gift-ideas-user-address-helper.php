<?php
/**
 * Helper to manage user address.
 *
 * Date: 10/27/17
 * Time: 8:24 PM
 *
 * @link       http://modobeta.pe
 * @since      1.0.0
 * @author     Fernando Paz <ferpaz@beta.pe>
 *
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/
 */

if ( ! defined( 'ABSPATH' ) ) {
	// Exit if accessed directly.
	exit;
}

/**
 * Class Woocommerce_Gift_Ideas_User_Address_Helper.
 *
 * @since      1.0.0
 * @author     Fernando Paz <ferpaz@beta.pe>
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/helper
 */
class Woocommerce_Gift_Ideas_User_Address_Helper {

	/**
	 * Get address form.
	 *
	 * @return array
	 */
	public static function get_address_form() {
		$current_user = wp_get_current_user();
		$load_address = 'direccion_entrega';

		// Get address' fields.
		$address = WC()->countries->get_address_fields(
			get_user_meta(
				get_current_user_id(),
				$load_address . '_country',
				true
			),
			$load_address . '_'
		);

		$address[ $load_address . '_address_1' ]['label']       = __( 'Calle / Av. / Jr.', 'woocommerce-gift-ideas' );
		$address[ $load_address . '_address_1' ]['placeholder'] = '';
		$address[ $load_address . '_address_2' ]['label']       = __( 'Número', 'woocommerce-gift-ideas' );
		$address[ $load_address . '_address_2' ]['placeholder'] = '';

		// Load fields from checkout customizer.
		$new_address                                = apply_filters(
			'woocommerce_checkout_fields',
			[
				'shipping' => $address,
				'billing'  => $address,
			]
		);
		$address[ $load_address . '_departamento' ] = $new_address['shipping']['shipping_departamento'];
		$address[ $load_address . '_provincia' ]    = $new_address['shipping']['shipping_provincia'];
		$address[ $load_address . '_distrito' ]     = $new_address['shipping']['shipping_distrito'];
		$address[ $load_address . '_referencia' ]   = $new_address['shipping']['shipping_referencia'];

		// Avoid some fields.
		$avoid_fields = apply_filters(
			'wc_gift_ideas_address_fields',
			[
				$load_address . '_first_name',
				$load_address . '_last_name',
				$load_address . '_company',
				$load_address . '_country',
				$load_address . '_city',
				$load_address . '_state',
				$load_address . '_postcode',
			]
		);

		// Prepare values for the form.
		foreach ( array_keys( $address ) as $key ) {
			if ( in_array( $key, $avoid_fields, true ) ) {
				// Avoid some fields.
				unset( $address[ $key ] );
				continue;
			}

			$value = get_user_meta(
				get_current_user_id(),
				$key,
				true
			);

			if ( ! $value ) {
				switch ( $key ) {
					case $load_address . '_email':
						$value = $current_user->user_email;
						break;
					case $load_address . '_country':
						$value = WC()->countries->get_base_country();
						break;
					case $load_address . '_state':
						$value = WC()->countries->get_base_state();
						break;
				}
			}

			$field = $address[ $key ];
			unset( $address[ $key ] );
			$field['value'] = apply_filters( 'woocommerce_my_account_edit_address_field_value', $value, $key, $load_address );

			if ( isset( $field['country_field'] ) ) {
				$field['country_field'] = str_replace( $load_address . '_', $load_address . '[', $field['country_field'] ) . ']';
			}
			$key             = str_replace( $load_address . '_', $load_address . '[', $key ) . ']';
			$address[ $key ] = $field;
		}

		return $address;
	}
}
