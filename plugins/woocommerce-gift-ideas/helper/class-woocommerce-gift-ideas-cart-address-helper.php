<?php
/**
 * Helper to manage cart address.
 *
 * Date: 10/27/17
 * Time: 8:24 PM
 *
 * @link       http://modobeta.pe
 * @since      1.0.0
 * @author     Fernando Paz <ferpaz@beta.pe>
 *
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/
 */

if ( ! defined( 'ABSPATH' ) ) {
	// Exit if accessed directly.
	exit;
}

/**
 * Class Woocommerce_Gift_Ideas_Cart_Address_Helper.
 *
 * @since      1.0.0
 * @author     Fernando Paz <ferpaz@beta.pe>
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/helper
 */
class Woocommerce_Gift_Ideas_Cart_Address_Helper {

	/**
	 * Carrito.
	 *
	 * @var WC_Cart
	 */
	protected $cart;

	/**
	 * Adjuntar a un carrito una lista de deseos (direccion) y el producto
	 * para usarlos en el checkout solo si se encuentra el producto al finalizar el checkout.
	 *
	 * @param Woocommerce_Gift_Ideas_Giftlist $giftlist Lista de deseos a adjuntar.
	 * @param WC_Product                      $product  Producto a adjuntar.
	 */
	public function attach_giftlist_product_cart( Woocommerce_Gift_Ideas_Giftlist $giftlist, WC_Product $product ) {
		WC()->session->set( 'wc_gift_ideas_cart_attacher_giftlist', $giftlist->get_id() );
		$productos = (array) WC()->session->get( 'wc_gift_ideas_cart_attacher_products', [] );
		if ( ! in_array( $product->get_id(), $productos, true ) ) {
			$productos[] = $product->get_id();
		}
		WC()->session->set( 'wc_gift_ideas_cart_attacher_products', $productos );
	}

	/**
	 * A ejecutar cuando se realice el checkout de una orden.
	 * HOOK: woocommerce_checkout_order_processed.
	 */
	public function clean_attached() {
		WC()->session->set( 'wc_gift_ideas_cart_attacher_giftlist', null );
		WC()->session->set( 'wc_gift_ideas_cart_attacher_products', [] );
	}

	/**
	 * Adjuntar checkbox para enviar a dirección del cumpleañero.
	 *
	 * HOOK: woocommerce_before_checkout_shipping_form
	 */
	public function send_to_birthday_boy_girl() {
		$giftlist        = (int) WC()->session->get( 'wc_gift_ideas_cart_attacher_giftlist', null );
		$giftlist        = new Woocommerce_Gift_Ideas_Giftlist( $giftlist );
		$productos       = (array) WC()->session->get( 'wc_gift_ideas_cart_attacher_products', [] );
		$esta_en_carrito = false;

		foreach ( $productos as $producto ) {
			if ( $this->is_producto_en_carrito( $producto ) ) {
				$esta_en_carrito = true;
				break;
			}
		}

		if ( $giftlist->get_id() && $esta_en_carrito ) {
			wc_get_template(
				'send-to-birthday-boy-girl.php',
				[
					'giftlist' => $giftlist,
				],
				'',
				WOOCOMMERCE_GIFT_IDEAS_PATH . 'templates/'
			);
		}

	}

	/**
	 * Cargar carrito desde woocommerce.
	 */
	public function cargar_carrito() {
		if ( ! $this->cart ) {
			$this->cart = WC()->cart;
		}
	}

	/**
	 * Obtener los productos del carrito.
	 *
	 * @return array
	 */
	public function obtener_productos_carrito() {
		$this->cargar_carrito();

		return ( ! $this->cart->is_empty() && $this->cart->cart_contents ? $this->cart->cart_contents : [] );
	}

	/**
	 * Esta el producto en el carrito?.
	 * Verificar variaciones tambien.
	 *
	 * @param WC_Product|int $producto Id del producto a verificar.
	 *
	 * @return bool
	 */
	public function is_producto_en_carrito( $producto ) {
		$anadido  = false;
		$producto = is_int( $producto ) ? wc_get_product( $producto ) : $producto;
		$parent   = (int) $producto->get_parent_id();
		$idn      = (int) $producto->get_id();

		foreach ( $this->obtener_productos_carrito() as $cart_item ) {
			if (
				( ! $parent && $idn === $cart_item['product_id'] )
				||
				( $parent && $parent === $cart_item['product_id'] && $idn === $cart_item['variation_id'] )
			) {
				$anadido = true;
				break;
			};
		}

		return $anadido;
	}
}
