=== WooCommerce Gift Ideas ===
Tags: woocommerce, e-commerce, ecommerce, gift, holidays, present, giftwrap, wrapping
Requires at least: 4.8.2
Tested up to: 4.8.2
Stable tag: 1.0
License: Private

Listas de ideas de regalos, enlazadas a una fecha de cumpleaños y dirección de envío.
Con funcionalidad de compartir.

== Description ==

Permite crear y compartir una lista de ideas de regalo para un cumpleaños a través de whatsapp y correo.
*   La lista de regalos tiene la información del día del cumpleaños, genero del niñ@ y la dirección del cumpleaños.
*   La lista de regalos permanecerá activa hasta que el usuario la elimine.
*   Se carga la dirección de envío directamente en el carrito cuando se acceda al link the lista compartida.
*   Solo puede comprarse una vez un item de la lista de regalo por persona.
*   Etiquetas opengraph para la lista de regalos.
*   Compartir la lista por correo y/o whatsapp, vía link o código (La lista solo será pública para los que se les envíe)
*   El packing personalizado con 3 tipos, adicionalmente se permitir agregar una dedicatoria en texto. Esta opción tiene cargo extra por opción (Con opción de añadir de 1 o todas las opciones)


== Installation ==


1. Upload `woocommerce-gift-ideas` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Extend the templates inside `templates` folder with your own themes
