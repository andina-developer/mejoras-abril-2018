(function ($) {
    'use strict';

    var giftListBody = $('<div class="modal fade wc-gift-ideas-giftlist">'),
        formModalBody = $('<div class="modal fade wc-gift-ideas-giftlist-form" tabindex="-1" role="dialog" aria-hidden="false">'),
        shareModalBody = $('<div class="modal fade modal-center-vert wc-gift-ideas-giftlist-share">'),
        searchModal = $('<div id="resultado" class="modal fade modal-buscar-lista modal-center-vert align-items-center h-100-vh" tabindex="-1" role="dialog" aria-hidden="true">'),
        shadowObj,
        created = false;

    function shadow() {
        shadowObj = $('<div class="modal-backdrop fade show wc-gift-ideas-shadow">');
        $('body').append(shadowObj);
    }

    function unshadow() {
        shadowObj.remove();
    }

    $([]).add(giftListBody).add(formModalBody).add(shareModalBody).add(searchModal).on('shown.bs.modal', function () {
        unshadow();
    });

    /**
     * Get a parameter from the query string.
     *
     * @param name
     * @param url
     * @returns {*}
     */
    function getQueryParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    /**
     * Get a parameter from a string after the # character.
     *
     * @param name
     * @param url
     * @returns {*}
     */
    function getHashParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[#&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    // Click add to giftlist a product. Display dialog to select giftlist or directly add it
    $(document).on('click', 'a.wc-gift-ideas-giftlist-addto-giftlist', function (e) {
        e.preventDefault();
        var t = $(this),
            productoId = this.dataset.productoId,
            variationId = 'undefined' === typeof(this.dataset.variationId) ? null : this.dataset.variationId,
            urlPopup = t.attr('href'),
            action = this.dataset.action,
            stock = this.dataset.stock;

        shadow();
        $.post(
            urlPopup,
            {
                action: action,
                dialogo: true,
                direct: this.dataset.modalAction === 'direct',
                producto_id: productoId,
                variation_id: variationId,
            },
            function (data) {
                if ('undefined' !== typeof(data.redirect) && data.redirect) {
                    document.location.href = data.redirect_to;
                } else {
                    // Show added to giftlist.
                    giftListBody
                        .html(data)
                        .appendTo($('body'))
                        .data('productoId', productoId)
                        .data('variationId', variationId)
                        .data('urlPopup', urlPopup)
                        .data('action', action)
                        .modal().modal('show');
                }
            }
        );
    });

    // Click on add a product to a giftlist inside the dialog.
    $(document).on('click', 'a.wc-gift-ideas-addto-giftlist-final', function (e) {
        e.preventDefault();
        var t = $(this);

        shadow();
        $.post(
            t.attr('href'),
            {
                action: t.data('action'),
                lista_id: giftListBody.find('select').val(),
                cantidad: giftListBody.find('input[type=number]').val(),
                producto_id: giftListBody.data('productoId'),
                variation_id: giftListBody.data('variationId')
            },
            function (data) {
                unshadow(); 
                giftListBody
                    .find('div.modal-content')
                    .html($(data).find('div.modal-content').html());
                giftListBody.find('[data-dismiss="modal"]').click(function () {
                    giftListBody.modal('hide');
                });
                giftListBody.data('bs.modal').handleUpdate();
            }
        );
    });

    // Click on button to create a new giftlist. Display dialog with the form to create it.
    $(document).on('click', 'a.wc-gift-ideas-create-giftlist', function (e) {
        e.preventDefault();
        var t = $(this);

        shadow();
        $.post(
            t.attr('href'),
            {
                action: this.dataset.action,
                dialogo: true
            },
            function (data) {
                if ('object' === typeof(data) && data.redirect_to) {
                    unshadow();
                    $('div#loginModal').modal('show');
                    return;
                }

                // Show giftlist form.
                formModalBody
                    .html(data)
                    .appendTo($('body'));
                // Hide modal giftlists.
                if (t.data('not-children') !== 1) {
                    giftListBody.modal('hide');
                    formModalBody
                        .on('hidden.bs.modal', function (e) {
                            if (created) {
                                // Reload the list if the new list was created
                                $.post(
                                    giftListBody.data('urlPopup'),
                                    {
                                        action: giftListBody.data('action'),
                                        dialogo: true,
                                        producto_id: giftListBody.data('productoId'),
                                        variation_id: giftListBody.data('variationId')
                                    },
                                    function (data) {
                                        // Show modal giftlists.
                                        giftListBody
                                            .find('div.modal-content')
                                            .html($(data).find('div.modal-content').html());
                                        giftListBody.find('[data-dismiss="modal"]').click(function () {
                                            giftListBody.modal('hide');
                                        });
                                        giftListBody.modal('show');
                                    }
                                );
                                created = false;
                            } else {
                                // Display modal of list of giftlist
                                giftListBody.modal('show');
                            }
                        })
                } else {
                    unshadow();
                    formModalBody
                        .on('hidden.bs.modal', function (e) {
                            if (created) {
                                document.location.href = created.redirect_to;
                            }
                        })
                }

                formModalBody.modal().modal('show');
            }
        );
    });

    // Submit the form to create a new giftlist.
    $(document).on('submit', '.modal-content form.wc-gift-ideas-form-create-giftlist', function (e) {
        e.preventDefault();
        var t = $(this),
            button = t.find('[type="submit"]');

        button.attr('disabled', 'disabled');

        $.post(
            t.attr('action'),
            t.serialize(),
            function (data) {
                if ('object' === typeof(data)) {
                    created = data;
                    formModalBody.modal('hide');
                    shadow();
                } else {
                    // Error, display it.
                    created = false;
                    formModalBody
                        .find('div.modal-content')
                        .html($(data).find('div.modal-content').html());
                    formModalBody.find('[data-dismiss="modal"]').click(function () {
                        formModalBody.modal('hide');
                    });
                    formModalBody.data('bs.modal').handleUpdate();
                }
            }
        );
    });

    // Remove a product from the giftlist.
    $(document).on('click', 'a.wc-gift-ideas-producto-remove', function (e) {
        e.preventDefault();
        var t = $(this);

        shadow();
        $.post(
            t.attr('href'),
            {
                action: this.dataset.action,
                giftlist_id: this.dataset.giftlistId,
                producto_id: this.dataset.productoId
            },
            function (data) {
                if (!data.error) {
                    // Reload
                    document.location.reload(true);
                }
            }
        );
    });

    // Delete a giftlist.
    $(document).on('click', 'a.wc-gift-ideas-giftlist-remove', function (e) {
        e.preventDefault();
        var t = $(this);

        shadow();
        $.post(
            t.attr('href'),
            {
                action: this.dataset.action,
                giftlist_id: this.dataset.giftlistId,
                producto_id: this.dataset.productoId
            },
            function (data) {
                if (!data.error) {
                    // Reload
                    document.location.reload(true);
                }
            }
        );
    });

    // Click on share giftlist via whatsapp.
    $(document).on('click', 'a.wc-gift-ideas-giftlist-share-whatsapp', function (e) {
        // Send request to share giftlist.
        $.post(
            this.dataset.href,
            {
                action: this.dataset.hrefAction,
                giftlist_id: this.dataset.giftlistId
            }
        )
    });

    // Click on share giftlist via email.
    $(document).on('click', 'a.wc-gift-ideas-giftlist-share-email', function (e) {
        e.preventDefault();
        var t = $(this);
        shadow();
        // Get dialog to share via email.
        $.post(
            t.attr('href'),
            {
                action: this.dataset.action,
                giftlist_id: this.dataset.giftlistId,
                dialogo: true
            },
            function (data) {
                // Show modal to share giftlist via email.
                shareModalBody
                    .html(data)
                    .appendTo($('body'))
                    .modal().modal('show');
            }
        )
    });

    // Submit the form to share a giftlist via email.
    $(document).on('submit', 'form.wc-gift-ideas-form-share-giftlist', function (e) {
        e.preventDefault();
        var t = $(this),
            button = t.find('[type="submit"]');

        button.attr('disabled', 'disabled');

        $.post(
            t.attr('action'),
            t.serialize(),
            function (data) {
                shareModalBody
                    .find('div.modal-content')
                    .html($(data).find('div.modal-content').html());
                shareModalBody.find('[data-dismiss="modal"]').click(function () {
                    shareModalBody.modal('hide');
                });
                shareModalBody.data('bs.modal').handleUpdate();
            }
        );
    });

    // Click on add to cart when sharing a giftlist.
    $(document).on('click', 'a.wc-gift-ideas-producto-add-to-cart', function (e) {
        e.preventDefault();
        // Send request to share giftlist.
        $.post(
            this.dataset.href,
            {
                action: this.dataset.action,
                giftlist_id: this.dataset.giftlistId,
                producto_id: this.dataset.productoId
            }
        )
    });

    $(document).on('wc.gift-ideas.loaded.provincias', '#shipping_provincia', function (e) {
        // Choose the select of provincia.
        var address;

        try {
            address = JSON.parse(this.dataset.address);
        } catch (e) {
            address = {};
        }

        if ('undefined' !== typeof(address.provincia)) {
            $('#shipping_provincia').val(address.provincia).trigger('change');
        }
    });

    $(document).on('wc.gift-ideas.loaded.distritos', '#shipping_distrito', function (e) {
        // Choose the select of distritos.
        var address;

        try {
            address = JSON.parse(this.dataset.address);
        } catch (e) {
            address = {};
        }

        if ('undefined' !== typeof(address.distrito)) {
            $('#shipping_distrito').val(address.distrito).trigger('change');
        }
    });

    // Click to use giftlist address as shipping address.
    $(document).on('click', 'input#ship-to-birthday-boy-girl-address', function (e) {
        var t = $(this), address = JSON.parse(this.dataset.address);

        var addressLine1 = $('input#shipping_address_1'),
            addressLine2 = $('input#shipping_address_2'),
            departamento = $('select#shipping_departamento'),
            referencia = $('input#shipping_referencia');

        if (t.is(':checked')) {
            addressLine1.val(address.address_1 + ' ' + address.address_2);
            addressLine2.val('');
            if ('undefined' !== typeof(address.departamento)) {
                departamento.val(address.country);
                departamento.trigger('change');
            }
            if ('undefined' !== typeof(address.referencia)) {
                referencia.val(address.referencia);
            }
        } else {
            addressLine1.val('');
            addressLine2.val('');
            departamento.val('');
            departamento.trigger('change');
            referencia.val('');
        }
    });

    // Click on buscar una lista. Desplegar modal.
    $(document).on('click', 'a#wc-gift-ideas-search-giftlist', function (e) {
        e.preventDefault();

        shadow();
        $.post(
            $(this).attr('href'),
            {
                action: this.dataset.action
            },
            function (data) {
                searchModal
                    .html(data)
                    .appendTo($('body'))
                    .modal().modal('show');
            }
        );
    });

    // Submit search giftlist form.
    $(document).on('submit', 'form#wc-gift-ideas-search-giftlist', function (e) {
        e.preventDefault();
        var t = $(this),
            button = t.find('[type="submit"]');

        button.attr('disabled', 'disabled');

        $.post(
            t.attr('action'),
            t.serialize(),
            function (data) {
                if ('object' === typeof(data) && 'undefined' !== typeof(data.redirect) && data.redirect) {
                    // Redirigir a la lista conseguida.
                    document.location.href = data.redirect_to;
                } else {
                    searchModal
                        .find('div.modal-content')
                        .html($(data).find('div.modal-content').html());
                    searchModal.find('[data-dismiss="modal"]').click(function () {
                        searchModal.modal('hide');
                    });
                    searchModal.data('bs.modal').handleUpdate();
                }
            }
        );
    });

    // Hide add to cart button when #hide-add-to-cart=1 is on the url. Add it to every link.
    var hide = getHashParameterByName('hide-add-to-cart');
    if (hide) {
        console.log('Hiding all add to cart buttons.');
        $('body').addClass('wc-gift-ideas-force-hide-add-to-cart');
        $('a').each(function (index, elem) {
            elem = $(elem);
            var href = elem.attr('href');

            // avoid new implementations links. tel:...
            var newReg = new RegExp('^(https?:|/).+');
            if (elem.hasClass('wc-gitf-ideas-dont-hide') || !href || !newReg.test(href)) {
                return;
            }

            if (href.length && href !== '#') {
                var hasCharExp = new RegExp('#');
                var hasHideCartExp = new RegExp('#.*&?hide-add-to-cart');
                href = hasCharExp.test(href) ?
                    (
                        hasHideCartExp.test(href) ?
                            href
                            :
                            href + 'hide-add-to-cart=1'
                    )
                    :
                    href + '#hide-add-to-cart=1';
                elem.attr('href', href)
            }
        });
    }

})(jQuery);
