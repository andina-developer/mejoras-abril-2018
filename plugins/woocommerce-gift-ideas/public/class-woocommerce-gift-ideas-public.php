<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://modobeta.pe
 * @since      1.0.0
 *
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/public
 */

if ( ! defined( 'ABSPATH' ) ) {
	// Exit if accessed directly.
	exit;
}

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/public
 * @author     Fernando Paz <info@beta.pe>
 */
class Woocommerce_Gift_Ideas_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $plugin_name The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $version The current version of this plugin.
	 */
	private $version;

	/**
	 * Administrador de listas.
	 *
	 * @var Woocommerce_Gift_Ideas_Giftlist_Manager
	 */
	private $giftlist_manager;

	/**
	 * Administrador de pagina de las listas.
	 *
	 * @var Woocommerce_Gift_Ideas_Page_Manager
	 */
	private $page_manager;

	/**
	 * Clase para envio de correos de lista compartida.
	 *
	 * @var Woocommerce_Gift_Ideas_Share_Giftlist_Email
	 */
	private $share_giftlist_email;

	/**
	 * Clase para envio de correos de lista creada.
	 *
	 * @var Woocommerce_Gift_Ideas_Created_Giftlist_Email
	 */
	private $created_giftlist_email;

	/**
	 * Cart helper para adjuntar lista de regalo.
	 *
	 * @var Woocommerce_Gift_Ideas_Cart_Address_Helper
	 */
	private $cart_helper;

	/**
	 * Emails a encolar.
	 *
	 * @var array
	 */
	private $email_queue;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 *
	 * @param      string $plugin_name The name of the plugin.
	 * @param      string $version     The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {
		$this->plugin_name      = $plugin_name;
		$this->version          = $version;
		$this->giftlist_manager = new Woocommerce_Gift_Ideas_Giftlist_Manager( $this->plugin_name, $this->version );
		$this->page_manager     = new Woocommerce_Gift_Ideas_Page_Manager( $this->plugin_name, $this->version );
		$this->cart_helper      = new Woocommerce_Gift_Ideas_Cart_Address_Helper();
		$this->email_queue      = [];
	}

	/**
	 * Obtener el administrador de listas.
	 *
	 * @return Woocommerce_Gift_Ideas_Giftlist_Manager
	 */
	public function get_giftlist_manager() {
		return $this->giftlist_manager;
	}

	/**
	 * Obtener el administrador de paginas de las listas.
	 *
	 * @return Woocommerce_Gift_Ideas_Page_Manager
	 */
	public function get_page_manager() {
		return $this->page_manager;
	}

	/**
	 * Get cart helper.
	 *
	 * @return Woocommerce_Gift_Ideas_Cart_Address_Helper
	 */
	public function get_cart_helper() {
		return $this->cart_helper;
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Woocommerce_Gift_Ideas_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Woocommerce_Gift_Ideas_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/woocommerce-gift-ideas-public.css', [], $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		if ( ! is_admin() ) {
			// Uso general.
			wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/woocommerce-gift-ideas-public.js', [ 'jquery' ], $this->version, false );
		}

	}

	/**
	 * Hook para boton de añadir a lista de regalos.
	 */
	public function add_remove_product_giftlist() {
		if ( is_page( 'giftlist' ) ) {
			$producto_id = get_the_ID();
			if ( $producto_id ) {
				$producto = wc_get_product( $producto_id );
			} else {
				global $product;
				$producto = $product;
			}

			wc_get_template(
				'removeproductgiftlist.php',
				[
					'producto' => $producto,
				],
				'',
				WOOCOMMERCE_GIFT_IDEAS_PATH . 'templates/'
			);
		}
		//wc_get_template_part('removeproductgiftlist.php',WOOCOMMERCE_GIFT_IDEAS_PATH . 'templates/');
	}

	/**
	 * Boton agregar a lista de regalos.
	 */
	public function add_to_giftlist_button() {
		if ( ! is_page( 'giftlist' ) ) {
			if ( is_user_logged_in() ) {
				$producto_id = get_the_ID();

				if ( $producto_id ) {
					$producto = wc_get_product( $producto_id );
				} else {
					global $product;
					$producto = $product;
				}

				wc_get_template(
					'addtogiftlist.php',
					[
						'producto' => $producto,
					],
					'',
					WOOCOMMERCE_GIFT_IDEAS_PATH . 'templates/'
				);
			}
		}
	}

	/**
	 * Dialogo para añadir un producto a una lista de regalos.
	 */
	public function add_to_giftlist_dialog() {
		if ( is_user_logged_in() ) {
	 		$mostrar_dialogo = wc_gift_ideas_get_post_sanitized( 'dialogo', false, 'bool' );
			$direct          = wc_gift_ideas_get_post_sanitized( 'direct', false, 'bool' );
			$lista_id        = wc_gift_ideas_get_post_sanitized( 'lista_id', null, 'int' );
			$producto_id     = wc_gift_ideas_get_post_sanitized( 'producto_id', null, 'int' );
			$variation_id    = wc_gift_ideas_get_post_sanitized( 'variation_id', null, 'int' );
			$cantidad    	 = wc_gift_ideas_get_post_sanitized( 'cantidad', null, 'int' );
			$data            = [];

			if ( $direct ) {
				// Directly added to giftlist. If there is one.
				$codigo           = wc_gift_ideas_last_giftlist_code();
				$data['redirect'] = false;
				if ( ! $codigo ) {
					// Bail, redirect to create one.
					$data['redirect']    = true;
					$data['redirect_to'] = wc_gift_ideas_get_giftlist_form_page_url();
					wp_send_json( $data );
					wp_die();
				} else {
					// Attach to giftlist.
					$lista    = $this->get_giftlist_manager()->obtener_giftlist_by_codigo( $codigo );
					$producto = wc_get_product( $producto_id );
					$lista->add_producto( $producto, 1);
					$result = $lista->save();

					$template = 'addedtogiftlist-dialogbox.php';
					if ( $result instanceof WP_Error ) {
						$data['notice'] = $result->get_error_message();
					} else {
						$data['giftlist'] = $lista;
						$data['product']  = $producto;
					}
				}
			} else {
				$giftlists = $this->get_giftlist_manager()->obtener_giftlist_usuario( get_current_user_id() );
				$producto = wc_get_product( $producto_id );
				$data      = [
					'giftlists' => $giftlists,
					'stock'  	=> $producto->get_stock_quantity()
				];
				$template  = 'addtogiftlist-dialogbox.php';

				if ( ! $mostrar_dialogo ) {
					// Añadir producto a la lista.
					$lista    = new Woocommerce_Gift_Ideas_Giftlist( $lista_id );
					$lista->add_producto( $producto, $cantidad );
					$result = $lista->save();

					if ( $result instanceof WP_Error ) {
						$data['notice'] = $result->get_error_message();
					} else {
						$data['giftlist'] = $lista;
						$data['product']  = $producto;
						$template         = 'addedtogiftlist-dialogbox.php';
					}
				}
			}

			wc_get_template(
				$template,
				$data,
				'',
				WOOCOMMERCE_GIFT_IDEAS_PATH . 'templates/'
			);
		}

		wp_die();
	}

	/**
	 * Dialogo para crear una lista de regalos.
	 */
	public function create_giftlist_dialog() {
		if ( is_user_logged_in() ) {
			// Mensaje de alerta.
			$notice = null;
			// Mostrar dialogo?
			$mostrar_dialogo = wc_gift_ideas_get_post_sanitized( 'dialogo', false, 'bool' );
			// Giftlist vacia.
			$giftlist = new Woocommerce_Gift_Ideas_Giftlist();
			if ( ! $mostrar_dialogo && 1 === wp_verify_nonce( wc_gift_ideas_get_post_sanitized( 'wc-gift-ideas-nonce', '', 'string' ), 'create_giftlist' ) ) {
				// Registrar lista.
				// Obtener datos de la lista y validar datos de la lista.
				$result = $this->create_giftlist_from_post( $giftlist );
				if ( $result instanceof WP_Error ) {
					$notice = $result->get_error_message();
				} else {
					// enviar respuesta json con el id y nombre de la lista.
					wp_send_json(
						[
							'redirect_to' => wc_gift_ideas_get_giftlist_page_url( 'greeting' ) . '/' . $giftlist->get_codigo(),
							'nombre'      => $giftlist->get_nombre(),
							'id'          => $giftlist->get_id(),
						]
					);
				}
			}

			wc_get_template(
				'creategiftlist-dialogbox.php',
				[
					'notice'   => $notice,
					'giftlist' => $giftlist,
					'address'  => Woocommerce_Gift_Ideas_User_Address_Helper::get_address_form(),
				],
				'',
				WOOCOMMERCE_GIFT_IDEAS_PATH . 'templates/'
			);
		} else {
			wp_send_json( [
				'redirect_to' => wp_get_referer(),
			] );
		}

		wp_die();
	}

	/**
	 * Enviar email de lista de regalos creada.
	 *
	 * @param Woocommerce_Gift_Ideas_Giftlist $giftlist Lista de regalos.
	 * @param string                          $url      Url de la lista de regalos.
	 */
	public function send_email_giftlist_created( Woocommerce_Gift_Ideas_Giftlist $giftlist, $url ) {
		// Send email.
		$user = wp_get_current_user();
		$this->enqueue_email( [
			'prop' => 'created_giftlist',
			'func' => 'trigger',
			'args' => [ $giftlist, $url, [ $user->user_email ] ],
		] );
	}

	/**
	 * Eliminar un producto de una giftlist.
	 */
	public function remove_from_giftlist() {
		if ( is_user_logged_in() ) {
			$giftlist_id = wc_gift_ideas_get_post_sanitized( 'giftlist_id', null, 'int' );
			$producto_id = wc_gift_ideas_get_post_sanitized( 'producto_id', null, 'int' );
			$response    = [
				'error'   => true,
				'message' => __( 'Datos incorrectos', 'woocommerce-gift-ideas' ),
			];

			if ( $giftlist_id && $producto_id ) {
				// Load the giftlist.
				$giftlist = new Woocommerce_Gift_Ideas_Giftlist( $giftlist_id );

				if ( $giftlist->get_id() && get_current_user_id() === (int) $giftlist->post_author ) {
					// Remove it if its the owner.
					$giftlist->remove_producto( wc_get_product( $producto_id ) );
					$result   = $giftlist->save();
					$response = [
						'error'   => ( $result instanceof WP_Error ),
						'message' => ( ( $result instanceof WP_Error ) ? $result->get_error_message() : '' ),
					];
				}
			}

			wp_send_json( $response );
		}

		wp_die();
	}

	/**
	 * Eliminar una lista de regalos.
	 */
	public function remove_giftlist() {
		if ( is_user_logged_in() ) {
			$giftlist_id = wc_gift_ideas_get_post_sanitized( 'giftlist_id', null, 'int' );
			$giftlist    = new Woocommerce_Gift_Ideas_Giftlist( $giftlist_id );

			if ( $giftlist->get_id() && get_current_user_id() === (int) $giftlist->post_author ) {
				// Solo permitir acceso al creador.
				$this->giftlist_manager->delete_giftlist( $giftlist );
				wp_send_json(
					[
						'error' => false,
					]
				);
			}
		}

		wp_die();
	}

	/**
	 * Proceso de compartir lista de regalos por whatsapp.
	 */
	public function share_giftlist_whatsapp() {
		if ( is_user_logged_in() ) {
			$giftlist_id = wc_gift_ideas_get_post_sanitized( 'giftlist_id', null, 'int' );
			$giftlist    = new Woocommerce_Gift_Ideas_Giftlist( $giftlist_id );

			if ( $giftlist->get_id() && get_current_user_id() === (int) $giftlist->post_author ) {
				// Solo permitir acceso al creador.
				if ( ! $giftlist->is_compartida() ) {
					$giftlist->compartida = true;
					$result               = $giftlist->save();
				} else {
					$result = true;
				}
				$response = [
					'error'   => ( $result instanceof WP_Error ),
					'mensaje' => ( $result instanceof WP_Error ? $result->get_error_message() : '' ),
				];

				wp_send_json( $response );
			}
		}

		wp_die();
	}

	/**
	 * Dialogo y proceso de compartir lista de regalos por correo.
	 */
	public function share_giftlist_email() {
		if ( is_user_logged_in() ) {
			$giftlist_id = wc_gift_ideas_get_post_sanitized( 'giftlist_id', null, 'int' );
			$giftlist    = new Woocommerce_Gift_Ideas_Giftlist( $giftlist_id );

			if ( $giftlist->get_id() && get_current_user_id() === (int) $giftlist->post_author ) {
				// Solo permitir acceso al creador.
				$mostrar_dialogo = wc_gift_ideas_get_post_sanitized( 'dialogo', false, 'bool' );
				$template        = 'sharegiftlist-dialogbox.php';
				$notice          = null;
				$correos         = [];
				$mensaje         = null;
				if ( ! $mostrar_dialogo && 1 === wp_verify_nonce( wc_gift_ideas_get_post_sanitized( 'wc-gift-ideas-nonce', '', 'string' ), 'share_giftlist' ) ) {
					// Procesar data.
					$correos = wc_gift_ideas_get_post_sanitized( 'correos', '', 'string' );
					$mensaje = wc_gift_ideas_get_post_sanitized( 'mensaje', '', 'string' );
					$correos = explode( ',', str_replace( ' ', '', $correos ) );
					$correos = array_map( 'is_email', array_map( 'trim', $correos ) );
					$correos = array_values( array_filter( $correos, 'is_string' ) );
					if ( empty( $correos ) ) {
						$notice = __( 'Ingrese al menos un correo válido', 'woocommerce-gift-ideas' );
					} else {
						// Send email.
						$this->enqueue_email( [
							'prop' => 'share_giftlist',
							'func' => 'trigger',
							'args' => [ $mensaje, $giftlist, $correos ],
						] );
						$template = 'sharedgiftlist-dialogbox.php';
					}
				}

				wc_get_template(
					$template,
					[
						'giftlist' => $giftlist,
						'notice'   => $notice,
						'correos'  => $correos,
						'mensaje'  => $mensaje,
					],
					'',
					WOOCOMMERCE_GIFT_IDEAS_PATH . 'templates/'
				);
			}
		}

		wp_die();
	}

	/**
	 * Cargar de sesion la cola.
	 */
	public function init_emails() {
		if ( ! is_admin() ) {
			$this->email_queue = WC()->session->get( 'wc_gift_ideas_queued_emails', [] );
		}
	}

	/**
	 * Inicializar clase de envio de correos por lista compartida.
	 *
	 * @return Woocommerce_Gift_Ideas_Share_Giftlist_Email
	 */
	public function init_share_giftlist_email() {
		if ( ! $this->share_giftlist_email ) {
			$this->share_giftlist_email = new Woocommerce_Gift_Ideas_Share_Giftlist_Email();
		}

		return $this->share_giftlist_email;
	}

	/**
	 * Inicializar clase de envio de correos por lista creada.
	 *
	 * @return Woocommerce_Gift_Ideas_Created_Giftlist_Email
	 */
	public function init_created_giftlist_email() {
		if ( ! $this->created_giftlist_email ) {
			$this->created_giftlist_email = new Woocommerce_Gift_Ideas_Created_Giftlist_Email();
		}

		return $this->created_giftlist_email;
	}

	/**
	 * Enviar correos encolados.
	 */
	public function send_queued_emails() {
		if ( ! is_admin() || isset( WC()->session ) ) {
			foreach ( $this->email_queue as $email ) {
				call_user_func_array( [ $this->{$email['prop'] . '_email'}, $email['func'] ], $email{'args'} );
			}

			WC()->session->set( 'wc_gift_ideas_queued_emails', [] );
		}
	}

	/**
	 * Agregar un producto al carrito desde una giftlist.
	 */
	public function add_to_cart_from_giftlist() {
		// RENATO
		global $woocommerce;
		global $wpdb;
		if ( is_user_logged_in() ) {
			$list_id 	 = wc_gift_ideas_get_post_sanitized( 'giftlist_id', null, 'int' );
			$product_id = wc_gift_ideas_get_post_sanitized( 'producto_id', null, 'int' );
			$producto    = wc_get_product( $product_id );

			$productos = (array) WC()->session->get( 'wc_gift_ideas_cart_attacher_products', [] );
			if ( ! in_array( $producto->get_id(), $productos, true ) ) {
				$productos[] = $producto->get_id();
			}
			WC()->session->set( 'wc_gift_ideas_cart_attacher_products', $productos );
			
			if ($list_id) {
				$quantity = 1;
				$row = $wpdb->get_row( "SELECT * FROM spanktg_parallel_cart WHERE user_id = ".get_current_user_id()." and product_id =".$product_id );
				if ( empty($row) ) {
					$wpdb->insert( 'spanktg_parallel_cart', 
						[
							'product_id' =>  $product_id,
							'quantity' =>  $quantity,
							'list_id' =>  $list_id,
							'user_id' =>  get_current_user_id()
						], 
						['%d', '%d', '%d', '%d']   
					);
				} else {
					$wpdb->update( 
						'spanktg_parallel_cart', 
						array( 
							'quantity' => $row->quantity + $quantity,
						), 
						array( 'id' => $row->id ), 
						array( 
							'%d'
						), 
						array( '%d' ) 
					);
				}
			}
		}




		// if ( is_user_logged_in() ) {
		// 	$giftlist_id = wc_gift_ideas_get_post_sanitized( 'giftlist_id', null, 'int' );
		// 	$producto_id = wc_gift_ideas_get_post_sanitized( 'producto_id', null, 'int' );
		// 	$giftlist    = new Woocommerce_Gift_Ideas_Giftlist( $giftlist_id );
		// 	$producto    = wc_get_product( $producto_id );
		// 	if (
		// 		$giftlist->get_id()
		// 		&&
		// 		( ! empty( $giftlist->get_compartido_con() ) || $giftlist->is_compartida() )
		// 		&&
		// 		get_current_user_id() !== (int) $giftlist->post_author
		// 		&&
		// 		$producto
		// 	) {
		// 		// Permitir acceso a los que no crearon la lista.
		// 		$giftlist->add_comprado( $producto );
		// 		$result = $giftlist->save();

		// 		if ( ! $result instanceof WP_Error ) {
		// 			// Guardar data de la direccion de envio.
		// 			$this->cart_helper->attach_giftlist_product_cart( $giftlist, $producto );
		// 		}
		// 	}
		// }

		wp_die();
	}

	/**
	 * Crear una giftlist por los datos enviados por $_POST.
	 *
	 * @param Woocommerce_Gift_Ideas_Giftlist $giftlist Giftlist a crear.
	 *
	 * @return WP_Error|true
	 */
	public function create_giftlist_from_post( Woocommerce_Gift_Ideas_Giftlist $giftlist ) {
		$giftlist->nombre               = wc_gift_ideas_get_post_sanitized( 'nombre', '', 'string' );
		$giftlist->cumpleanero_nombre   = wc_gift_ideas_get_post_sanitized( 'cumpleanero_nombre', '', 'string' );
		$giftlist->cumpleanero_apellido = wc_gift_ideas_get_post_sanitized( 'cumpleanero_apellido', '', 'string' );

		$cumpleanero_nombre   = $giftlist->get_cumpleanero_nombre();
		$cumpleanero_apellido = $giftlist->get_cumpleanero_apellido();
		if ( empty( $cumpleanero_nombre ) || empty( $cumpleanero_apellido ) ) {
			// Usar legacy form.
			$giftlist->cumpleanero = wc_gift_ideas_get_post_sanitized( 'cumpleanero', '', 'string' );
		}

		$giftlist->fecha_cumpleanos  = wc_gift_ideas_get_post_sanitized( 'fecha_cumpleanos', ' ', 'string' );
		$giftlist->sexo_cumpleanero  = wc_gift_ideas_get_post_sanitized( 'sexo_cumpleanero', '', 'string' );
		$giftlist->email             = wc_gift_ideas_get_post_sanitized( 'correo_electronico', '', 'string' );
		$giftlist->direccion_entrega = (array) wc_gift_ideas_get_post( 'direccion_entrega', [] );

		return $giftlist->save();
	}

	/**
	 * Ubicar una giftlist por codigo o por nombre cumpleañero.
	 */
	public function search_giftlist() {
		$mostrar_dialogo = wc_gift_ideas_get_post_sanitized( 'dialogo', false, 'bool' );
		$data            = [
			'notice' => null,
			'codigo' => null,
			'nombre' => null,
		];
		$template        = 'searchgiftlist-dialogbox.php';

		if ( ! $mostrar_dialogo && 1 === wp_verify_nonce( wc_gift_ideas_get_post_sanitized( 'wc-gift-ideas-nonce', '', 'string' ), 'search_giftlist' ) ) {
			$data['nombre'] = wc_gift_ideas_get_post_sanitized( 'nombre', '', 'string' );
			$data['codigo'] = wc_gift_ideas_get_post_sanitized( 'codigo', '', 'string' );
			// Ubicar giftlists.
			$giftlist_cod = $this->get_giftlist_manager()->obtener_giftlist_by_codigo( $data['codigo'] );
			if ( ! $giftlist_cod ) {
				// Ubicar por nombre. 
				$giftlists = $this->get_giftlist_manager()->buscar_giftlist_cumpleanero( $data['nombre'] );
				if ( empty( $giftlists ) ) {
					$data['notice'] = __( 'No hay listas que coincidan con la búsqueda', 'woocommerce-gift-ideas' );
				} elseif ( 1 === count( $giftlists ) ) {
					// Redirect to giftlist page.
					wp_send_json( [
						'redirect'    => true,
						'redirect_to' => wc_gift_ideas_get_giftlist_page_url( $giftlists[0]->get_codigo() ), 
					] );
					wp_die();
					//Aqui debo mostrar el layer
				} else {
					// Mostrar listado para elegir.
					$data['giftlists'] = $giftlists;
				}
			} else {
				// Redirect to giftlist page.
				wp_send_json( [
					'redirect'    => true,
					'redirect_to' => wc_gift_ideas_get_giftlist_page_url( $data['codigo'] ),
				] );
				wp_die();
			}
		}

		wc_get_template(
			$template,
			$data,
			'',
			WOOCOMMERCE_GIFT_IDEAS_PATH . 'templates/'
		);

		wp_die();
	}

	/**
	 * Encolar un correo.
	 *
	 * @param array $email Email data a encolar.
	 */
	private function enqueue_email( $email ) {
		$this->email_queue[] = $email;

		if ( ! empty( $this->email_queue ) ) {
			// Force load the mailer to send the email.
			WC()->mailer();
		}
	}
}
