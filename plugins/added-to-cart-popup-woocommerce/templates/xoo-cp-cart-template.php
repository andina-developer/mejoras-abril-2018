<?php

//Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	return;
}

global $xoo_cp_gl_qtyen_value;
?>

<div class="cnt-table">
    <table class="xoo-cp-pdetails clearfix" data-cp="<?php echo htmlentities( json_encode( array(
		'key'   => $cart_item_key,
		'pname' => $product_name
	) ) ); ?>">
        <tr>
            <td class="xoo-cp-remove d-none"><i class="xcp-icon-cancel-circle xcp-icon"></i></td>

            <td class="xoo-cp-pimg align-middle text-left">
                <a href="<?php echo $product_permalink; ?>">
                    <img src="<?php echo imagen_personalizada( $product_id, 'wp-thumb-pupup-productos' ); ?>"
                         alt="<?php $product_name; ?>">
                </a>
            </td>

            <td class="xoo-cp-ptitle align-middle">
                <a href="<?php echo $product_permalink; ?>">
                    <h2><?php echo $product_name; ?></h2>
                </a>

                <p><span><?php _e( 'SKU: ', 'b4st' ); ?></span><?php echo $_product->sku; ?></p>

                <p class="mx-0"><span><?php _e( 'Cantidad: ', 'b4st' ); ?></span><?php echo $cart_item['quantity']; ?></p>

				<?php if ( $attributes ): ?>
                    <div class="xoo-cp-variations d-none"><?php echo $attributes; ?></div>
				<?php endif; ?>
            </td>

            <td class="xoo-cp-pprice text-left text-md-right">
                <p><span><?php _e( 'Precio unitario:', 'b4st' ); ?></span><br>
                    <span class="precio"><?php echo $product_price; ?></span>
                </p>
            </td>

            <td class="xoo-cp-pqty d-none">
				<?php if ( $_product->is_sold_individually() || ! $xoo_cp_gl_qtyen_value ): ?>
                    <span><?php echo $cart_item['quantity']; ?></span>
				<?php else: ?>
                    <div class="xoo-cp-qtybox">
                        <span class="xcp-minus xcp-chng">-</span>
						<?php echo xoo_cp_qty_input( $cart_item['quantity'], $_product ); ?>
                        <span class="xcp-plus xcp-chng">+</span>
                    </div>
				<?php endif; ?>
            </td>
        </tr>
    </table>
</div>

<div class="xoo-cp-ptotal clearfix">
    <span class="xcp-totxt float-md-left"><?php _e( 'Sub-total', 'added-to-cart-popup-woocommerce' ); ?>: </span>
    <span class="xcp-ptotal float-md-right"><?php echo $product_subtotal; ?></span>
</div>

