jQuery(function($){
    // Show the login dialog box on click
    $('a#show_login').on('click', function(e){
        $('body').prepend('<div class="login_overlay"></div>');
        $('form#login').fadeIn(500);
        $('div.login_overlay, form#login a.close').on('click', function(){
            $('div.login_overlay').remove();
            $('form#login').hide();
        });
        e.preventDefault();
    });

    // Perform AJAX login on form submit
    $('form#login').on('submit', function(e){
        $('form#login p.status').show().text(ajax_login_object.loadingmessage);
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_login_object.ajaxurl,
            data: {
                'action': 'ajaxlogin', //calls wp_ajax_nopriv_ajaxlogin
                'username': $('form#login #username').val(),
                'password': $('form#login #password').val(),
                'security': $('form#login #security').val() },
            success: function(data){
                $('form#login p.status').text(data.message);
                $('form#login p.status').addClass(data.aviso);
                if (data.loggedin === true){
                    document.location.href = ajax_login_object.redirecturl;
                }
            }
        });
        e.preventDefault();
    });

    $(document).on("click", "#btn-login", function(e) {
        e.preventDefault();
        $('#cerrarPopup').fadeOut();
        $('#cerrarLogin').fadeIn();

        var altura = $("#no-logged").height() + 30;

        $(this).closest(".modal-body").css('min-height',altura);
        $(".cargando").fadeIn();

        setTimeout(function(){
            $("#logged").hide();
            $(".cargando").fadeOut();
            //$("#username").focus();
        }, 500);
    });

    $(document).on("click", "#cerrarLogin", function (e) {
        e.preventDefault();
        var altura = $("#no-logged").height();

        $(".cargando").fadeIn();
        $(this).closest(".modal-body").css('min-height',altura);

        setTimeout(function(){
            $(".cargando").fadeOut();
            $("#logged").show();
        }, 500);

        $(this).fadeOut();
        $('#cerrarPopup').fadeIn();
    });

    $('#loginModal').on('shown.bs.modal', function (e) {
        $('html, body').addClass('bug-input');
    });

    $('#loginModal').on('hidden.bs.modal', function (e) {
        var altura = $("#no-logged").height();
        $("#loginModal .modal-body").css('min-height',altura);
        $("#logged").show();
        $('#cerrarLogin').hide();
        $('#cerrarPopup').show();

        $('html, body').removeClass('bug-input');
    });
});