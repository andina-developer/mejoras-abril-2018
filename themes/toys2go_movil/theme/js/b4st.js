(function ($) {

	'use strict';

	$(document).ready(function() {

		// Comments
		$('.commentlist li').addClass('card');
		$('.comment-reply-link').addClass('btn btn-secondary');

		// Forms
        $('select, input[type=text], input[type=email], input[type=password], input[type=tel], input[type=date], textarea').addClass('form-control');
		$('input[type=submit]').addClass('btn btn-primary');

		$('select').addClass('custom-select');

    // Pagination fix for ellipsis
    $('.pagination .dots').addClass('page-link').parent().addClass('disabled');

		// You can put your own code in here

	});

}(jQuery));
