<?php get_header(); ?>

<?php
/*
 * Slider principal
 */
if ( wpmd_is_device() ):
    echo do_shortcode( '[sliderPrincipalMovil]' );
else:
    echo do_shortcode( '[sliderPrincipalDesktop]' );
endif;

/*
 * Bloque "Comprar por"
 */
if ( wpmd_is_device() ):
    echo do_shortcode( '[comprarPorMovil]' );
else:
    echo do_shortcode( '[filtroHome]' );
endif;

/**
 * Bloque "Productos Recomendados"
 */
echo do_shortcode( '[productosRecomenados]' );

/**
 * Bloque "Crear lista" Desktop
 */
if ( wpmd_is_notdevice() ):
    echo do_shortcode( '[crearListaDesktop]' );
endif;

/**
 * Bloque "Crear lista" Móvil
 */
if ( wpmd_is_device() ):
	echo do_shortcode( '[crearListaMovil]' );
endif;

/**
 * bloque "Categorías"
 */
echo do_shortcode( '[carruselCategorias]' );

/**
 * bloque "Novedades"
 */
echo do_shortcode( '[productosNovedades]' );

/**
 * Bloque "Marcas"
 */
if ( !wpmd_is_device() ):
    echo do_shortcode( '[carruselMarcas]' );
endif;

/**
 * Bloque "Suscripción" en el archivo footer.php
 */

?>

<?php get_footer(); ?>