<div class="modal modal-login fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog centrar-v" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button id="cerrarPopup" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="icon-cerrar xoo-cp-close"></i>
                </button>

                <button id="cerrarLogin" type="button" class="close" style="display: none;">
                    <i class="icon-cerrar xoo-cp-close"></i>
                </button>

				<?php if ( ! is_user_logged_in() ): ?>
                    <div id="no-logged" class="absoluto no-logged">
                        <form id="login" action="login" method="post" class="w-100" autocomplete="off">
                            <h1 class="d-none"><?php _e( 'Iniciar sesión', 'b4st' ); ?></h1>
                            <p class="status"></p>

                            <div class="form-group w-100">
                                <label for="username"><?php _e( 'Correo electrónico', 'b4st' ); ?></label>
                                <input id="username" type="text" name="username" class="form-control" autocomplete="nope">
                            </div>

                            <div class="form-group w-100">
                                <label for="password"><?php _e( 'Contraseña', 'b4st' ); ?></label>
                                <input id="password" type="password" name="password" class="form-control" autocomplete="nope">
                            </div>

                            <div class="form-group w-100">
                                <input class="submit_button btn-lg btn btn-primary btn-block" type="submit"
                                       value="<?php _e( 'Ingresar', 'b4st' ); ?>" name="submit">
                            </div>

                            <div class="text-center w-100">
                                <a class="lost link" href="<?php echo get_site_url(); ?>/mi-cuenta/lost-password/">
									<?php _e( '¿Olvidaste tu contraseña?', 'b4st' ); ?></a>
                            </div>

							<?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
                        </form>
                    </div>

                    <div id="logged" class="absoluto logged">
                        <div class="w-100 aviso">
                            <p class="text-center"><?php _e( 'Para poder crear una lista debe estar registrado.', 'b4st' ); ?></p>
                        </div>

                        <div class="form-group w-100">
                            <a href="#" id="btn-login"
                               class="btn btn-block btn-lg btn-primary"><?php _e( 'Iniciar sesión', 'b4st' ); ?></a>
                        </div>

                        <div class="text-center w-100">
                            <a href="<?php echo get_bloginfo( 'url' ); ?>/registro/"
                               id="btn-registro" class="btn btn-block btn-lg btn-primary">
                                <?php _e( 'Registrarme', 'b4st' ); ?></a>
                        </div>

                        <div class="text-center w-100 invitado d-none">
                            <span>ó</span>
                        </div>

                        <div class="text-center w-100 invitado d-none">
                            <a href="#" class="link" data-dismiss="modal"
                               aria-label="Close"><?php _e( 'Ingresar como invitado', 'b4st' ); ?></a>
                        </div>
                    </div>
				<?php else: ?>
                    <div id="no-logged" class="absoluto">
                        <div class="form-group w-100">
                            <a href="<?php echo get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ); ?>"
                               id="btn-login" title="<?php _e( 'Ver mi cuenta', 'b4st' ); ?>"
                               class="btn btn-block btn-lg btn-primary"><?php _e( 'Ver mi cuenta', 'b4st' ); ?></a>
                        </div>

                        <div class="text-center w-100">
                            <a href="<?php echo wp_logout_url( $_SERVER['REQUEST_URI'] ); ?>" id="btn-registro"
                               title="<?php _e( 'Cerrar Sesión', 'b4st' ); ?>"
                               class="btn btn-block btn-lg btn-primary"><?php _e( 'Cerrar Sesión', 'b4st' ); ?></a>
                        </div>
                    </div>
				<?php endif; ?>

                <div id="cargando" class="cargando absoluto" style="display: none;">
                    <i class="icon-spin4"></i>
                </div>
            </div>
        </div>
    </div>
</div>