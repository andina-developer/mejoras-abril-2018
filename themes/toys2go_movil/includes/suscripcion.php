<section class="section suscripcion">
    <div class="container">
        <div class="row justify-content-lg-center">
            <div class="col-12 col-lg-11 content text-center">
                <div class="row">
                    <div class="col-12 col-lg-6">
                        <div class="row h-100 mx-1">
                            <div class="col-12 col-lg-10 mx-auto contenido">
                                <h3 class="h2"><?php _e( 'Suscríbete y obtén 10% de descuento', 'b4st' ); ?></h3>

                                <?php echo do_shortcode( '[contact-form-7 id="193" title="Suscripción"]' ); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-lg-6 d-none d-lg-block">
                        <div class="row fila-4px justify-content-lg-center h-100">
                            <div class="col-12">
                                <h3 class="h2"><?php _e( 'Crea o encuentra una', 'b4st' ); ?></h3>
                            </div>

                            <div class="col-12 col-lg-6 contenido">
                                <h3 class="celeste dos-icon tit-regalos"><span><?php _e( 'LISTA DE REGALOS', 'b4st' ); ?></span></h3>
                            </div>

                            <div class="col-12 col-lg-4 contenido">
                                <a href="<?php echo esc_url( wc_gift_ideas_get_giftlist_page_url() ); ?>" class="btn btn-sm btn-primary" title="<?php _e( 'Crea o encuentra una lista de regalos', 'b4st' ); ?>">
                                    <?php _e( 'DESCUBRIR MÁS', 'b4st' ); ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>