<div class="section cnt-slick woocommerce">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="titulo-productos <?php echo ( isset( $clase ) ) ? implode( " ", $clase ) : ''; ?>">
					<?php echo ( isset( $titulo) ) ? $titulo : ''; ?>
				</h2>
			</div>
		</div>

		<div class="row carrusel-home">
			<div class="col-12">
				<div class="products slider marcas">
					<?php
                        $nummarcas = 0;
					foreach( $terms as $term ) {
                        $term_id    = $term->term_id;
                        $logo       = get_field( 'logo', 'marca_' . $term_id );
                        $miniatura  = $logo['sizes']['wp-carousel-marcas'];
                        $nombre     = $term->name;
                        if(!empty($logo)){
                            if($nummarcas < 21){
                                ?>
                                <div class="text-center">
                                    <a href="<?php echo get_term_link( $term_id ); ?>" title="<?php $nombre; ?>">
                                        <img src="<?php echo $miniatura; ?>" alt="<?php $nombre; ?>" class="img-fluid">
                                    </a>
                                </div>
                                <?php
                            }
                            $nummarcas++;
                        }
					}
					?>
				</div>
			</div>
		</div>
	</div>
</div>