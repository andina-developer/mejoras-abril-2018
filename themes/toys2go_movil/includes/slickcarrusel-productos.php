<div class="section cnt-slick woocommerce">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="titulo-productos <?php echo ( isset($clase) ) ? implode( " ",$clase ) : ''; ?>"><?php echo ( isset($titulo) ) ? $titulo : ''; ?></h2>
            </div>
        </div>

        <div class="row carrusel-home">
            <div class="col-12">
                <div class="products slider productos">
                    <?php
                    while ( $productos->have_posts() ) : $productos->the_post();
                        echo wc_get_template_part( 'content', 'product' );
                    endwhile;
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>