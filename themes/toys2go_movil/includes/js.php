<script>
    //$('').on('hidden.bs.modal', function (e) {
    jQuery(document).on("shown.bs.modal", ".wc-gift-ideas-giftlist-form", function () {
        $('body').addClass('modal-open');
    });

    jQuery(document).on("hidden.bs.modal", ".wc-gift-ideas-giftlist-form", function () {
        //$('body').removeClass('modal-open');
    });

    jQuery(function ($) {
        $('.loading').removeClass('show');
        setTimeout(function(){ $('.loading').remove(); }, 500);

        <?php /*if( isset( $_GET['view'] ) ) { */?>/*
        $('.loading').removeClass('show');
        */<?php /*} */?>

        // Override woof range slider.
        $('.woof_range_slider').each(function (index, input) {
            try {
                var slider = $(input).data('ionRangeSlider');
                if (slider) {
                    slider.update({
                        max: 300,
                        max_postfix: '+'
                    });
                }
            } catch (e) {

            }
        });

        window['woof_get_submit_link'] = function () {
            //filter woof_current_values values
            if (woof_is_ajax) {
                woof_current_values.page = woof_ajax_page_num;
            }
            //+++
            if (Object.keys(woof_current_values).length > 0) {
                jQuery.each(woof_current_values, function (index, value) {
                    if (index == swoof_search_slug) {
                        delete woof_current_values[index];
                    }
                    /*if (index == 's') {
                        delete woof_current_values[index];
                    }*/
                    if (index == 'product') {
                        //for single product page (when no permalinks)
                        delete woof_current_values[index];
                    }
                    if (index == 'really_curr_tax') {
                        delete woof_current_values[index];
                    }
                });
            }


            //***
            if (Object.keys(woof_current_values).length === 2) {
                if (('min_price' in woof_current_values) && ('max_price' in woof_current_values)) {
                    var l = woof_current_page_link + '?min_price=' + woof_current_values.min_price;
                    if (woof_current_values.max_price < 300) {
                        l += '&max_price=' + woof_current_values.max_price;
                    }

                    if (woof_is_ajax) {
                        history.pushState({}, "", l);
                    }
                    return l;
                }
            }


            //***

            if (Object.keys(woof_current_values).length === 0) {
                if (woof_is_ajax) {
                    history.pushState({}, "", woof_current_page_link);
                }
                return woof_current_page_link;
            }
            //+++
            if (Object.keys(woof_really_curr_tax).length > 0) {
                woof_current_values['really_curr_tax'] = woof_really_curr_tax.term_id + '-' + woof_really_curr_tax.taxonomy;
            }
            //+++
            var link = woof_current_page_link + "?" + swoof_search_slug + "=1";
            //console.log(woof_current_page_link);
            //just for the case when no permalinks enabled
            if (!woof_is_permalink) {

                if (woof_redirect.length > 0) {
                    link = woof_redirect + "?" + swoof_search_slug + "=1";
                    if (woof_current_values.hasOwnProperty('page_id')) {
                        delete woof_current_values.page_id;
                    }
                } else {
                    link = location.protocol + '//' + location.host + "?" + swoof_search_slug + "=1";
                    /*
					 if (!woof_is_ajax) {
					 link = location.protocol + '//' + location.host + "?" + swoof_search_slug + "=1";
					 }

					 if (woof_current_values.hasOwnProperty('page_id')) {
					 link = location.protocol + '//' + location.host + "?" + swoof_search_slug + "=1";
					 }
					 */
                }
            }
            //console.log(link);
            //throw('STOP!');

            //any trash for different sites, useful for quick support
            var woof_exclude_accept_array = ['path'];

            if (Object.keys(woof_current_values).length > 0) {
                jQuery.each(woof_current_values, function (index, value) {
                    if (index == 'page' && woof_is_ajax) {
                        index = 'paged';//for right pagination if copy/paste this link and send somebody another by email for example
                    }

                    if ('max_price' === index && value >= 300) {
                        return;
                    }

                    //http://www.dev.woocommerce-filter.com/?swoof=1&woof_author=3&woof_sku&woof_text=single
                    //avoid links where values is empty
                    if (typeof value !== 'undefined') {
                        if ((typeof value && value.length > 0) || typeof value == 'number') {
                            if (jQuery.inArray(index, woof_exclude_accept_array) == -1) {
                                link = link + "&" + index + "=" + value;
                            }
                        }
                    }

                });
            }

            //+++
            //remove wp pagination like 'page/2'
            link = link.replace(new RegExp(/page\/(\d+)\//), "");
            if (woof_is_ajax) {
                history.pushState({}, "", link);

            }

            //throw ("STOP!");
            return link;
        };

        /**
         * Carrusel principal
         */
        $('#carousel-principal').carousel({
            interval: 2000
        });

		<?php if ( wpmd_is_device() ): ?>
        /**
         * Menú offside
         */
        $('#left').offcanvas({
            modifiers: 'left'
        });
        $(document).trigger("enhance");
        $("#cerrar").trigger("offcanvas.close");
		<?php endif; ?>

        /*
         * Slick Carrusel Productos
         */
        $('.productos').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            centerPadding: '30px',
            responsive: [
                {
                    breakpoint: 3000,
                    settings: {
                        prevArrow: '<button type="button" class="slick-prev circ"><i class="icon-angulo-izquierda"></i></button>',
                        nextArrow: '<button type="button" class="slick-next circ"><i class="icon-angulo-derecha"></i></button>',
                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        arrows: false,
                        variableWidth: true
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        arrows: false,
                        variableWidth: true
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        arrows: false,
                        variableWidth: true
                    }
                }
            ]
        });

        /*
         * Slick Carrusel Productos
         */
        $('.categorias').slick({
            touchMove: true,
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 5,
            slidesToScroll: 5,
            centerPadding: '30px',
            responsive: [
                {
                    breakpoint: 3000,
                    settings: {
                        slidesToShow: 5,
                        prevArrow: '<button type="button" class="slick-prev circ"><i class="icon-angulo-izquierda"></i></button>',
                        nextArrow: '<button type="button" class="slick-next circ"><i class="icon-angulo-derecha"></i></button>'
                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        infinite: true,
                        arrows: false,
                        variableWidth: true
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        arrows: false,
                        variableWidth: true
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        arrows: false,
                        variableWidth: true
                    }
                }
            ]
        });

        /*
         * Back to top
         */
        var offset = 300,
            offset_opacity = 400,
            scroll_top_duration = 700,
            $back_to_top = $('.cd-top');

        var body_h = $(window).height(),
            altura_m = body_h;

        var s = $back_to_top;
        var pos = s.offset().top + s.height(); //offset that you need is actually the div's top offset + it's height
        $(window).scroll(function () {
            var windowpos = $(window).scrollTop();
            var windowheight = $(window).height();
            if (windowpos + windowheight > pos - 100) s.addClass('stick');
        });

        var s = $back_to_top;
        var pos = s.offset().top + s.height(); //offset that you need is actually the div's top offset + it's height

        $(window).scroll(function () {
            var windowpos = $(window).scrollTop();
            var windowheight = $(window).height();
            if (windowpos + windowheight > pos - 100) s.addClass('stick');

            else s.removeClass('stick');
        });

        $(window).scroll(function () {
            ( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
            if ($(this).scrollTop() > offset_opacity) {
                $back_to_top.addClass('cd-fade-out');
            }

            if ($(this).scrollTop() > altura_m) {
                $back_to_top.addClass('fijo');
            }

            if ($(this).scrollTop() < altura_m) {
                $back_to_top.removeClass('fijo');
            }
        });

        $back_to_top.on('click', function (event) {
            event.preventDefault();
            $('body,html').animate({
                    scrollTop: 0,
                }, scroll_top_duration
            );
        });
    });
</script>

<script>
    jQuery(function ($) {
		<?php if ( wpmd_is_notdevice() ) : ?>

		<?php if ( is_home() ): ?>

        /**
         * Redimenciona el tamaño
         */
        var resizeText = function () {
            var currentSize = $(".cnt > .icono").width() * $(".cnt > .icono").height();
            var ancho = $(".cnt > .icono").width();
            var tamano = ancho * 0.48;

            console.log(ancho);
            console.log(tamano);

            $(".cnt > .icono i").css("font-size", tamano + 'px');
        };

        $(window).bind('resize', function () {
            resizeText();
        }).trigger('resize');

        /**
         * Efecto icono/imagen Categoría
         */
        $('.categorias .slick-slide>div').hover(
            function () {
                $(this).find('.icono').stop().animate({'opacity': '0'}, 200);
                $(this).find('.imagen').stop().animate({'opacity': '1'}, 200);
            },
            function () {
                $(this).find('.icono').stop().animate({'opacity': '1'}, 200);
                $(this).find('.imagen').stop().animate({'opacity': '0'}, 200);
            }
        );

        /**
         * Slick Carrusel Marcas
         */
        $('.marcas').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 7,
            slidesToScroll: 1,
            centerPadding: '30px',
            prevArrow: '<button type="button" class="slick-prev circ"><i class="icon-angulo-izquierda"></i></button>',
            nextArrow: '<button type="button" class="slick-next circ"><i class="icon-angulo-derecha"></i></button>',
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        infinite: true,
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                }
            ]
        });
		<?php endif; ?>

		<?php
		if ( is_home() || is_shop() || is_product_category() ):
		$width = ( is_home() ) ? 350 : 195;
		?>
        $('.slider-input').jRange({
            from: 0,
            to: 300,
            step: 1,
            scale: [0, 25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300],
            format: '%s',
            width: <?php echo $width; ?>,
            showLabels: true,
            isRange: true,
            showScale: false
        });

        $(document).on('change', '.slider-input', function () {
            var valores = JSON.parse("[" + this.value + "]");
            if (valores[1] == 300) {
                $(".pointer-label.high").addClass("plus");
            } else {
                $(".pointer-label.high").removeClass("plus");
            }
        });

		<?php endif;// End is_home() ?>

		<?php endif; ?>

		<?php
		// Solo para el detalle del producto
		if ( is_product() ):
		?>
        // Popup para la galeria
        $('.popup-gallery').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Cargando imagen #%curr%...',
            mainClass: 'mfp-img-mobile',
            callbacks: {
                elementParse: function (item) {
                    // Function will fire for each target element
                    // "item.el" is a target DOM element (if present)
                    // "item.src" is a source that you may modify
                    console.log(item.el.context.className);
                    if (item.el.context.className == 'video') {
                        item.type = 'iframe',
                            item.iframe = {
                                patterns: {
                                    youtube: {
                                        index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).

                                        id: 'v=', // String that splits URL in a two parts, second part should be %id%
                                        // Or null - full URL will be returned
                                        // Or a function that should return %id%, for example:
                                        // id: function(url) { return 'parsed id'; }

                                        src: '//www.youtube.com/embed/%id%?autoplay=1' // URL that will be set as a source for iframe.
                                    },
                                    vimeo: {
                                        index: 'vimeo.com/',
                                        id: '/',
                                        src: '//player.vimeo.com/video/%id%?autoplay=1'
                                    },
                                    gmaps: {
                                        index: '//maps.google.',
                                        src: '%id%&output=embed'
                                    }
                                }
                            }
                    } else {
                        item.type = 'image',
                            item.tLoading = 'Cargando imagen #%curr%...',
                            item.mainClass = 'mfp-img-mobile',
                            item.image = {
                                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
                            }
                    }

                }
            },
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                titleSrc: function (item) {
                    return item.el.attr('title');
                }
            }
        });

		<?php
		// Solo para desktop
		if ( wpmd_is_notdevice() ):
		?>
        $('.slider-for').slick({
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slider-nav'
        });

        $('.slider-nav').slick({
            arrows: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            dots: false,
            centerMode: false,
            focusOnSelect: true,
            vertical: true,
            centerPadding: '0px'
        });
		<?php
		// Solo para móviles
		else:
		?>
        $('.slider-movil').slick({
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            vertical: false,
            dots: true
        });

		<?php
		endif;
		?>

		<?php
		endif;
		?>

		<?php if ( is_shop() ): ?>
        $(document).on('click', '.woof_container_inner h4 a', function (event) {
            $(this).parent().toggleClass("opened");
        });
		<?php endif; ?>

		<?php if ( is_cart() ): ?>
        $(document).on('click', '.product-remove .remove', function () {
            $("[name='update_cart']").trigger("click");
        });
        $(document).on('change', '.qty', function () {
            $("[name='update_cart']").trigger("click");
        });
		<?php endif; ?>

		<?php
		if ( is_page_template( 'paginas/page-preguntas-frecuentes.php' ) ):
		$altura = ( wpmd_is_device() ) ? 85 : 175;
		?>
        $('.collapse').on('hide.bs.collapse', function (e) {
            var $panel = $(this).closest('.item-scroll');
            $($panel).removeClass('activo');
        });

        $('.collapse').on('shown.bs.collapse', function (e) {
            var $panel = $(this).closest('.item-scroll');

            $('.item-scroll').removeClass('activo');
            $($panel).addClass('activo');

            $('html,body').animate({
                scrollTop: $panel.offset().top -<?php echo $altura; ?>px
            }, 500);
        });
		<?php endif; ?>

		<?php if ( wpmd_is_device() ) : ?>
        // Submenú en versión móvil abierto por defecto

        var tipogrid = '<?php echo $_POST["modoview"]; ?>';
        $("input[name='modoview']").val(tipogrid);

        $("#formpasogenero #woo_view_nav_ul_mobile a").click(function () {
            var view = $(this).attr("id");
            $("input[name='modoview']").val(view);
            var form = $('form#formpasogenero');
            form.submit();
        });
        $("#filtro-movil-home #woo_view_nav_ul_mobile a").click(function () {
            var view = $(this).attr("id");
            $("input[name='modoview']").val(view);
            var form = $('form#filtro-movil-home');
            form.submit();
        });

        if(tipogrid == 'list'){
            $(".products.tienda.clearfix").removeClass("grid");
            $(".products.tienda.clearfix").addClass("list");
        }else{
            $(".products.tienda.clearfix").addClass("grid");
            $(".products.tienda.clearfix").removeClass("list");
        }



        $('.collapse').collapse('show');

        $(':radio[name="genero"]').change(function() {
            var form = $('form#formpasogenero');
            form.find('input[name="genero"]').val();
            form.find('input[name="preetapa"]').val(1);
            form.submit();
        });

        $(':radio[name="edad"]').change(function() {
            var form = $('form#filtro-movil-home');
            form.find('input[name="edad"]').val();
            form.submit();
        });

        $(':radio[name="precio"]').change(function() {
            var form = $('form#filtro-movil-home');
            form.find('input[name="precio"]').val();
            form.submit();
        });

        $('a.boton-filtro-movil').click(function (e) {
            e.preventDefault();
            var term_id = this.dataset.term;
            var form = $(this).siblings('form#filtro-movil-home');
            form.find('input[name="genero"]').val(term_id);
            form.submit();
        });

        $('form#filtro-movil-home select#select-genero').change(function () {
            var form = $('form#filtro-movil-home');
            //var etapa = form.find('input[name="etapa"]');
           // etapa.val(etapa.val() - 1);
            form.submit();
        });
        $('form#filtro-movil-home select#select-precios').change(function () {
            $("#width_sizeprecios").html($('#select-precios option:selected').text());
            $("#select-precios").width($("#sizeprecios").width());
            var form = $('form#filtro-movil-home');
            //var etapa = form.find('input[name="etapa"]');
            // etapa.val(etapa.val() - 1);
            form.submit();
        });
        $('form#filtro-movil-home select#select-edad').change(function () {
            $("#width_sizeedad").html($('#select-edad option:selected').text());
            $(this).width($("#sizeedad").width());
            var form = $('form#filtro-movil-home');
            var etapa = form.find('input[name="etapa"]');
            //etapa.val(etapa.val() - 1);
            form.submit();
        });
        document.onreadystatechange = function () {
            if (document.readyState == "complete") {
                $("#width_sizeprecios").html($('#select-precios option:selected').text());
                $("#select-precios").width($("#sizeprecios").width());

                $("#width_sizeedad").html($('#select-edad option:selected').text());
                $("#select-edad").width($("#sizeedad").width());
            }
        }
        <?php if ( is_checkout() ) : ?>

        jQuery(document).ready(function () {
            $(".actions a[href='#previous']").hide();

            $('#customheightstep').on("resize", function(){
                console.log("ha cambiadoc el alto");
            });

            $("#billing_ckusarmismadireccion").attr("checked","checked");

        });

            $("#order_review_derecha > .shop_table > tfoot > .shipping").remove();
            $("#order_review_derecha > #payment").remove();
            
            var form = $("#checkout").show();
            // Regla de validación con jQuery validation - https://jqueryvalidation.org/
            form.validate({
                rules: {
                    billing_first_name: {
                        required: true
                    }
                }
            });

            /**
             * Botones "Anterior", "Siguiente" en formulario por pasos del Checkout solo en móviles
             */
            $('.btnNext').click(function(){
                var siguiente = $('a.active').parent('li').next('li').find('a');

                $(siguiente).removeClass('disabled');
                $(siguiente).trigger('click');
            });

            // Actualizar alto de contenedor de steps
            function update_content_height() {
                var content = $('form section.content-step.current .heightstep');
                content.css('height', 'auto');
                content.parent().css('height', content.outerHeight() + 'px');
            }

            $(document).on("change", ":checkbox[name='billing_ckusarmismadireccion']", function () {
                setTimeout(function(){
                    update_content_height();
                },300);
            });
            $(document).on("change", ":radio[name='payment_method']", function () {
                setTimeout(function(){
                    update_content_height();
                },300);
            });
        $(document).ready(function () {
            $('.shipping_method_m_collapse').slideUp(200);
            setTimeout(function(){
                $('.shipping_method_m_collapse.activo').slideDown(200);
            },200);
            setTimeout(function(){
                update_content_height();
            },500);
        })
        $(document).on("change", ":radio.shipping_method", function () {
            $('.shipping_method_m_collapse').slideUp(200);
            var hermanos = $(this).siblings('div');
            hermanos.slideDown(200);

            setTimeout(function(){
                update_content_height();
            },300);
        });
            form.steps({
                headerTag: "h3",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                titleTemplate: '<span class="number">#index#</span> <span class="texto">#title#</span>',
                labels: {
                    current: "current step:",
                    pagination: "Pagination",
                    finish: "Comprar ahora<i class='icon-derecha' aria-hidden='true'></i>",
                    next: "Siguiente <i class='icon-derecha' aria-hidden='true'></i>",
                    previous: "Anterior <i class='icon-izquierda' aria-hidden='true'></i>",
                    loading: "Cargando ..."
                },
                onStepChanging: function (event, currentIndex, newIndex) {
                    form.validate().settings.ignore = "";
                    if (newIndex < currentIndex) {
                        return true;
                    }
                    var others = form
                        .find('section.content-step')
                        .not(':eq('+currentIndex+')')
                        .find('select,input,textarea')
                        .map(function(){
                            var id = $(this).attr('id');
                            if (id != undefined) {
                                return '#' + id;
                            }

                            return '[name="'+$(this).attr('name')+'"]';
                        })
                        .get()
                        .join(',');
                    switch (currentIndex) {
                        case 0:
                            $( "#field_documento_usuario" ).rules( "add", {required: true});
                            $( "#billing_address_1" ).rules( "add", {required: true});
                            $( "#billing_phone" ).rules( "add", {required: true});
                            $( "#billing_email" ).rules( "add", {required: true});
                            $( "#billing_provincia" ).rules( "add", {required: true});
                            $( "#billing_departamento" ).rules( "add", {required: true});
                            $( "#billing_distrito" ).rules( "add", {required: true});
                            $( "#billing_referencia" ).rules( "add", {required: true});
                            if ($('#billing_referencia').is(':checked')) {
                                $( "#billing_nomdireccion" ).rules( "add", {required: true});
                            }

                            $( "input[name=comprobante_boleta]" ).rules( "add", {required: true});
                            $( "#billing_referencia" ).rules( "add", {required: true});
                            break;
                        case 1:
                            console.log("paso 1");
                            form.validate().settings.ignore = others;
                            break;
                        case 2:
                            console.log("paso 2");
                            break;
                        case 3:
                            form.validate().settings.ignore = form
                                    .find('section.content-step')
                                    .eq(currentIndex)
                                    .find('select,input,textarea')
                                    .map(function(){
                                        var id = $(this).attr('id');
                                        if (id != undefined) {
                                            return '#' + id;
                                        }

                                        return '[name="'+$(this).attr('name')+'"]';
                                    })
                                    .get()
                                    .join(',') +
                                "," + others;
                            break;
                    }
                    //form.valid();
                    //update_content_height();
                    setTimeout(function(){
                        update_content_height();
                    },300);
                    return form.valid();

                },
                onInit: function (event, currentIndex) {
                    setTimeout(function(){
                        update_content_height();
                    },100);
                },
                onStepChanged: function (event, currentIndex, priorIndex) {
                    update_content_height();
                },
                onFinishing: function (event, currentIndex) {
                    form.validate().settings.ignore = "select,input,textarea";
                    return form.valid();
                },
                onFinished: function (event, currentIndex) {
                    form.submit();
                }
            });

            // Añade un cuarto campo vacío al formulario Checkout en móviles
            var felicitaciones = '<li role="tab" class="" aria-disabled="true" aria-selected="false"><a href="#"><span class="number">4</span> <span class="texto">¡Listo!</span></a></li>';
            $(".steps.clearfix ul[role=tablist]").append(felicitaciones);

            // Oculta los pasos
            //$(document).find('.mobile-checkout-form .steps').css('display', 'none');

            $(document).on('click', '#ship-to-different-address-checkbox', function () {
                //
            });
            <?php endif; ?>
		<?php else:  ?>

        $(document).ajaxComplete(function() {

                //$('.shipping_method_m_collapse').slideUp(200);
               // $('.shipping_method_m_collapse.activo').slideDown(200);

        });

        $(document).on("change", ":radio.shipping_method", function () {
            $('.shipping_method_m_collapse').slideUp(200);
            var hermanos = $(this).siblings('div');
            hermanos.slideDown(200);
        });
        <?php endif; ?>

        <?php if( is_page( 'giftlist' ) ) : ?>
        //Solo para la página giftlist. Coloca el asterisco (*) en los campos de Dirección de envío.
        $(document).on("shown.bs.modal", ".wc-gift-ideas-giftlist-form", function () {
            //$("#direccion_entrega_departamento_field label, #direccion_entrega_provincia_field label, #direccion_entrega_distrito_field label, #direccion_entrega_distrito label, #direccion_entrega_referencia_field label").append(' <abbr class="required" title="obligatorio">*</abbr>');
        });
        <?php endif; ?>
    });
</script>

<?php if ( wpmd_is_notdevice() && ! is_shop() ) : ?>
	<script type="text/javascript">
        jQuery(function ($) {
            var main_url = '<?php echo esc_url( home_url( '/' ) ); ?>';
            $('form#filtro').submit(function (e) {
                e.preventDefault();
                var query = ['swoof=1'];

                var genero = $(this).find('[name="genero"]:checked').val();
                var edad = $(this).find('[name="edad"]:checked').val();
                var precio = $(this).find('[name="precio"]').val();

                if (genero !== "") {
                    query.push('genero=' + genero);
                }
                if (edad !== "") {
                    query.push('edad=' + edad);
                }
                if (precio !== "") {
                    precio = precio.split(',');
                    query.push('min_price=' + precio[0]);
                    if (precio[1] < 300) {
                        query.push('max_price=' + precio[1]);
                    }
                }

                document.location.href = main_url + '?' + query.join('&');
            });
        });
	</script>
<?php endif; ?>

<?php
if( is_page(  'registro' )): ?>
<script>
    $(document).ready(function() {
        var timestamp = (new Date).getTime();
        $('form #user_username').val(timestamp);
        $('form #user_username_field').css('display', 'none');
    });
</script>
?>
<?php endif; ?>
<?php if ( is_checkout() ) : ?>
    <script>
        $( document ).ajaxComplete(function() {
            if($('.woocommerce-invalid').length > 0){
                $('.woocommerce-invalid').each(function(){
                    $(this).find('input').focus();
                    return false;
                });
            }
        });
    </script>
<?php endif; ?>

