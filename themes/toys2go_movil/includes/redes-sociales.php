<?php
$idPage = getidbyslug( 'contactanos' );
$args = array(
    'post_type'         => 'page',
    'p'                 => $idPage,
    'posts_per_page'    => 1
);

$query = new WP_Query( $args );

if ( $query->have_posts() ) :
    while ( $query->have_posts() ) : $query->the_post();
        ?>
        <div class="redes-sociales clearfix text-center">
            <?php
            $id_contacto = get_the_ID();

            if( have_rows( 'redes_sociales', $id_contacto ) ):
                echo '<ul class="redes menu-list list-unstyled">';

                while ( have_rows( 'redes_sociales', $id_contacto ) ) : the_row();
                    $str     = get_sub_field('nombre');
                    $pattern = '/(\D+)(\|)(\D+)/';
                    $res     = preg_match_all($pattern, $str, $matches);

                    echo '<li class="' . $matches[1][0]. '"><a href="' . get_sub_field( 'enlace_red_social' ) . '" title="' . $matches[1][0] . '" target="_blank"><i class="fa ' . $matches[3][0] . '"></i></a></li>';
                endwhile;

                echo '</ul>';
            endif;
            ?>
        </div>
        <?php
    endwhile;
endif;
wp_reset_query();
?>