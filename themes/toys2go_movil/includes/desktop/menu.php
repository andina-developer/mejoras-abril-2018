<div class="fixed-top bg-inverse">
    <?php if ( ! is_checkout() ) : ?>
    <div class="sup">
        <div class="container">
            <div class="row">
                <div class="col-12-pc">

                </div>

                <div class="col-88-pc">
                    <ul class="buscador list-unstyled">
                        <li class="formulario">
							<?php echo get_search_form(); ?>
                        </li>

                        <li class="icon-text">
                            <div>
	                            <?php $texto = ( is_user_logged_in() ) ? km_get_users_name() : 'Iniciar Sesión'; ?>

                                <a data-toggle="modal" data-target="#loginModal"
                                   title="<?php echo $texto; ?>">
                                    <i class="icon-avatar-linea"></i>
                                    <span><?php echo $texto; ?></span>
                                </a>
                            </div>
                        </li>

                        <?php
                        if ( is_user_logged_in() ) : // Sí el usuario está loggeado
                            $usuario_id = get_current_user_id();
                            $n_listas = Woocommerce_Gift_Ideas::get_public_instance()->get_giftlist_manager()->obtener_giftlist_usuario($usuario_id);
                            $tiene_listas = count($n_listas) > 0;

                            if ( $tiene_listas ) :
                        ?>

                        <li class="icon-text">
                            <div>
                                <a href="<?php echo esc_url( wc_gift_ideas_get_giftlist_page_url() ); ?>"
                                   title="<?php _e( 'Acceso a mi lista de regalos', 'b4st' ); ?>">
                                    <span class="icon-grupo">
                                        <i class="icon-regalo"></i>
                                        <span><?php echo count($n_listas); ?></span>
                                    </span>
                                    <span><?php _e( 'Lista de Regalos', 'b4st' ); ?></span>
                                </a>
                            </div>
                        </li>

                        <?php
                            endif;
                        endif;
                        ?>

                        <li class="carrito">
                            <a class="cart-contents" href="<?php echo esc_url( wc_get_cart_url() ); ?>"
                               title="<?php _e( 'Ver carrito de compras' ); ?>">
                                <i class="icon-carrito" aria-hidden="true"></i>
                                <span class="header-cart-count"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="sub">
        <div class="container">
            <div class="row fila-4px">
                <div class="col-12-pc logo">
                    <a class="navbar-brand p-absolute" href="<?php echo home_url( '/' ); ?>" title="<?php bloginfo( 'name' ); ?>">
                        <img src="<?php echo get_template_directory_uri(); ?>/theme/img/logo-desktop.png"
                             class="img-fluid" alt="<?php echo get_bloginfo( 'description' ); ?>" width="140"
                             height="38">
                    </a>
                </div>

                <div class="col-88-pc">
                    <nav class="navbar navbar-expand-xl navbar-inverse menu-categorias">
                        <button class="navbar-toggler mr-0 ml-auto" type="button" data-toggle="collapse"
                                data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
                                aria-label="Toggle navigation">
                            <i class="icon-menu-hamburger" aria-hidden="true"></i>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarNavDropdown">
							<?php wp_menu_categorias(); ?>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <?php else: ?>
        <div class="container tienda-segura">
            <div class="row fila-4px d-flex">
                <div class="col-12-pc logo d-flex">
                    <a class="navbar-brand my-2" href="<?php echo home_url( '/' ); ?>" title="<?php bloginfo( 'name' ); ?>">
                        <img src="<?php echo get_template_directory_uri(); ?>/theme/img/logo-desktop.png"
                             class="img-fluid" alt="<?php echo get_bloginfo( 'description' ); ?>" width="140"
                             height="38">
                    </a>
                </div>

                <div class="col-88-pc d-flex flex-column justify-content-center align-items-end">
                    <ul class="list-unstyled mb-0 datos-contacto mb-2">
                        <li class="list-inline-item"><?php _e('Llámanos: ', 'b4st'); ?></li>
                        <?php
                        global $numero, $whatsapp;
                        if ( $numero )
                            echo '<li class="list-inline-item"><a href="tel:' . $numero . '" class="gris">' . $numero . '</a></li>';
                        if ( $whatsapp )
                            echo '<li class="list-inline-item"><i class="icon-whatsapp"></i> <a href="https://api.whatsapp.com/send?phone=+51'. $whatsapp .'"  class="gris"> ' . $whatsapp . '</a></li>';
                        ?>
                    </ul>

                    <h1 class="rojo text-uppercase mb-0"><span class="d-inline-block"><?php _e( 'Tienda segura' , 'b4st' ); ?></span>
                        <i class="icon-porcentaje-escudo d-inline-block"></i></h1>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>