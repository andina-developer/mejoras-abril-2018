<?php if ( ! is_checkout() ) : ?>
<header class="main fixed-top text-center bg-gris2">
    <div class="container">
        <div class="row justify-content-end">
            <div class="col-4">
                <a href="<?php echo esc_url( wc_gift_ideas_get_giftlist_page_url() ); ?>"
                   class="d-inline-block mr-auto ml-auto text-uppercase blanco"
                    title="<?php _e('¡Crea una lista de regalos!', 'b4st'); ?>">
                    <h2><?php _e('¡Crea una lista de regalos!', 'b4st'); ?></h2>
                </a>
            </div>

            <div class="col-4">
                <ul class="list-unstyled float-right mb-0 datos-contacto">
                    <li class="list-inline-item"><?php _e('Llámanos: ', 'b4st'); ?></li>
                    <?php
                    global $numero, $whatsapp;
                    if ( $numero )
                        echo '<li class="list-inline-item"><a href="tel:' . $numero . '" class="blanco">' . $numero . '</a></li>';
                    if ( $whatsapp )
                        echo '<li class="list-inline-item"><i class="icon-whatsapp"></i> <a href="https://api.whatsapp.com/send?phone=+51'. $whatsapp .'"  class="blanco"> ' . $whatsapp . '</a></li>';
                    ?>
                </ul>
            </div>
        </div>
    </div>
</header>
<?php endif; ?>