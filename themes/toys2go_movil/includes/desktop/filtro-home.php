<?php
$args = [
	'taxonomy'   => 'genero',
	'hide_empty' => false,
	// Esta opción muestra solo las categorías superiores.
	'parent'     => 0,
];

$args_edad = [
	'taxonomy'   => 'edad',
	'hide_empty' => false,
	// Esta opción muestra solo las categorías superiores.
	'parent'     => 0,
];
?>
<section class="filtro-home main bg-celeste" id="filtro-home">
	<div class="container">
		<form role="search" method="post" id="filtro" action="<?php echo esc_url( home_url( '/' ) ); ?>">
			<div class="row">
				<div class="col-12">
					<h2><i class="icon-lupa-sello"></i> <?php esc_html_e( 'Busca tu regalo', 'b4st' ); ?></h2>
				</div>

				<div class="col-md-4 genero">
					<h3 class="blanco"><span class="numero bg-amarillo">1</span> Elige el Género:</h3>

					<div class="text-center cont">
						<input type="hidden" value="" name="s">
						<input type="hidden" name="post_type" value="product">
						<?php
						$generos = get_terms( $args );
						if ( ! empty( $generos ) ) {
							?>
							<ul class="list-unstyled mb-0 radio-button">
								<?php
								$idambos = [];

								foreach ( $generos as $genero ) {
									$id        = $genero->term_id;
									$nombre    = $genero->name;
									$slug      = $genero->slug;
									$icono     = get_field( 'icono', 'genero_' . $id );
									$idambos[] = $slug;
									?>
									<li class="value-<?php echo esc_attr( $slug ); ?>">
										<input id="<?php echo esc_attr( $slug ); ?>" type="radio" name="genero"
											   value="<?php echo esc_attr( $slug ); ?>">
										<label for="<?php echo esc_attr( $slug ); ?>" class="label">
											<i class="<?php echo esc_attr( $icono ); ?>"></i>
											<?php echo esc_html( $nombre ); ?>
										</label>
									</li>
									<?php
								}
								?>

								<li class="d-none">
									<input id="ambos" type="radio" name="genero"
										   value="<?php echo esc_attr( implode( ',', $idambos ) ); ?>">
									<label for="ambos" class="label"><i class="icon-nino-nina"></i> Ambos</label>
								</li>
							</ul>
							<?php
						}
						?>
					</div>
				</div>

				<div class="col-md-4 edad">
					<h3 class="blanco"><span class="numero bg-amarillo">2</span> Elige la Edad:</h3>

					<div class="text-center cont">
						<?php
						$edades = get_terms( $args_edad );
						if ( ! empty( $edades ) ) {
							?>
							<ul class="list-unstyled mb-0 radio-button">
								<?php
								foreach ( $edades as $edad ) {
									$id     = $edad->term_id;
									$nombre = $edad->name;
									$slug   = $edad->slug;
									$icono  = get_field( 'icono', 'genero_' . $id );
									?>
									<li class="value-<?php echo esc_attr( $slug ); ?>">
										<input id="<?php echo esc_attr( $slug ); ?>" type="radio" name="edad"
											   value="<?php echo esc_attr( $slug ); ?>">
										<label for="<?php echo esc_attr( $slug ); ?>" class="label">
											<?php echo esc_html( $edad->name ); ?>
										</label>
									</li>
									<?php
								}
								?>
							</ul>
							<?php
						}
						?>
					</div>
				</div>

				<div class="col-md-4 precio">
					<h3 class="blanco">
                        <span class="numero bg-amarillo">3</span> <?php esc_html_e( 'Elige el Precio:', 'b4st' ); ?>
					</h3>

					<div class="text-center cont">
						<input type="hidden" name="precio" class="slider-input" value="50,250"/>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-12 text-right">
					<button type="submit" name="buscar"
							class="btn btn-danger"><?php esc_html_e( 'BUSCAR', 'b4st' ); ?></button>
				</div>
			</div>
		</form>
	</div>
</section>
