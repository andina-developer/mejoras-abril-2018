<?php
$numero = get_option('numero_slider');

$args = array(
    'posts_per_page' => $numero,
    'post_type' => 'sliders'
);

query_posts($args);

if (have_posts()):
    $num_slider = count(query_posts($args))
    ?>

    <div data-ride="carousel" class="carousel carousel-fade carousel-principal carousel-desktop" id="carousel-principal">
        <ol class="carousel-indicators">
            <?php
            $contador = 0;
            while (have_posts()) : the_post();
                ?>
                <li class="<?php echo ($contador == 0) ? 'active' : ''; ?>" data-slide-to="<?php echo $contador; ?>" data-target="#carousel-principal"></li>
                <?php
                $contador++;
            endwhile;
            ?>
        </ol>
        <div role="listbox" class="carousel-inner">
            <?php
            $contador = 0;
            while (have_posts()) : the_post();
                //COLOR DE FONDO
                $hex = get_field('color_de_fondo');
                list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");

                //COLOR DE TEXTO
                if ($hex != "") :
                    $blanco = '#ffffff';
                else:
                    $blanco = '#1d1d1d';
                endif;

                //IMAGEN DE FONDO
                $imgbg  = get_field( 'imagen_de_fondo' );
                $thumbg = $imgbg['sizes']['fondo-carousel'];

                //COLOR DE FONDO
                $hex = get_field('color_de_fondo');
                list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");

                //SUBTÍTULO
                $subtitulo = get_field('subtitulo');

                //IMAGEN PRODUCTO
                if ( get_field( 'imagen_desktop' ) ) {
                    $imagen = get_field( 'imagen_desktop' );
                } else {
                    if ( get_field( 'imagen_moviles' ) ) {
                        $imagen = get_field( 'imagen_moviles' );

                    }
                }

                if ( wpmd_is_device() ) {
                    $miniatura  = $imagen['sizes']['wp-carousel-principal-sm'];
                } else {
                    $miniatura  = $imagen['sizes']['wp-carousel-principal-lg'];
                }

                //ENLACE
                $enlace = get_field('enlace');
                ?>
                <div class="carousel-item <?php echo ( $contador == 0 ) ? 'active' : ''; ?>" style="background-color: <?php echo $hex; ?>">
                    <div class="container">
                        <div class="row justify-content-md-center">
                            <div class="col-4">
                                <h3 class="text-uppercase" data-animation="animated bounceInRight" style="color:<?php echo $blanco; ?>;"><?php the_title(); ?></h3>
                                <p data-animation="animated bounceInRight" style="color:<?php echo $blanco; ?>;"><?php //echo strip_tags( limite( get_the_content(), 80, ' ', '' ) ); ?> <?php echo $subtitulo; ?> </p>
                                <a data-animation="animated bounceInRight" href="<?php echo $enlace; ?>" class="btn btn-lg" title="Leer más - <?php the_title(); ?>" style="background-color:<?php echo $blanco; ?>; border-color:<?php echo $blanco; ?>; color: <?php echo $hex; ?>;"><span>COMPRAR</span></a>
                            </div>

                            <div class="col-4">
                                <img src="<?php echo $miniatura; ?>" alt="<?php the_title(); ?>" class="img-fluid center-block" data-animation="animated fadeInRight">
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                $contador++;
            endwhile;
            ?>
        </div>
        <a data-slide="prev" role="button" href="#carousel-example-captions" class="left carousel-control">
            <span aria-hidden="true" class="icon-prev"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a data-slide="next" role="button" href="#carousel-example-captions" class="right carousel-control">
            <span aria-hidden="true" class="icon-next"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <?php
endif;
wp_reset_query();
?>