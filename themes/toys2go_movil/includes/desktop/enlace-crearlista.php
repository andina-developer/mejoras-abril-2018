<section class="crear-lista-desktop bg-gris5 main" id="crear-lista-desktop">
    <div class="container text-center">
        <div class="row">
            <div class="col-12">
                <h2 class="ribbon bg-amarillo font-weight-bold"><?php _e( '¡Crea una lista de regalos!', 'b4st' ); ?></h2>
            </div>
        </div>

        <div class="row justify-content-md-center fila-4px">
            <div class="col-3 col-md-3">
                <img src="<?php echo get_template_directory_uri(); ?>/theme/img/pinguino2.svg" height="90" width="191"
                     alt="Crear una lista de regalos" class="img-fluid">
            </div>

            <div class="col-6 col-md-5">
                <p class="lead celeste m-0">
                    <strong>
                        Así podrás recibir los regalos que más te<br class="d-none d-xl-block"> gusten.
                        Además, encuentra la lista de<br class="d-none d-xl-block"> regalos de tus amigos aquí.
                    </strong>
                </p>
            </div>

            <div class="col-3 col-md-3 align-bottom">
                <a href="<?php echo esc_url( wc_gift_ideas_get_giftlist_page_url() ); ?>" class="btn btn-lg btn-primary btn-sm btn-icon btn-icon-amarillo btn-icon-regalo">
                    <span>DESCUBRE<br> MÁS</span>
                </a>
            </div>
        </div>
    </div>
</section>