<div class="text-center">
    <div class="rounded-circle cnt" style="background-color: <?php echo $color; ?>">
        <a href="<?php echo get_term_link( $term_id ); ?>"></a>

	    <?php if ( $imagen ) : ?>
        <div class="imagen">
	        <?php echo $imagen; ?>
        </div>
	    <?php endif; ?>

	    <?php if ( $icono ) : ?>
        <div class="icono">
            <i class="<?php echo $icono; ?>"></i>
        </div>
        <?php endif; ?>
    </div>

    <h3><?php echo $nombre; ?></h3>
</div>