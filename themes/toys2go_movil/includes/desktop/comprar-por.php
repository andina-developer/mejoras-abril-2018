<?php

$terms = get_terms( array(
    'taxonomy'      => 'genero',
    'parent'        => 0,
    'hide_empty'    => 0,
    'number'        => 2
) );
?>
<section class="genero-link main" id="genero-link">
    <div class="container">
        <div class="row fila-4px">
            <div class="col-12">
                <h2 class="h1"><?php _e( 'Compra por:', 'b4st' ); ?></h2>
            </div>
            <?php
            foreach ( $terms as  $term):
                $icono  = get_field( 'icono', 'genero_' . $term->term_id );
                $enlace = get_term_link( $term->term_id );
                $nombre = $term->name;
                $color  = get_field( 'color', 'genero_' . $term->term_id );
                ?>
                <div class="col-6">
                    <a href="<?php echo $enlace; ?>" style="background-color: <?php echo $color; ?>;">
                        <i class="<?php echo $icono; ?>"></i>
                        <?php echo $nombre; ?>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>