<div class="modal modal-login fade" id="suscripcioModal" tabindex="-1" role="dialog" aria-labelledby="suscripcioModal"
     aria-hidden="true">
    <div class="modal-dialog centrar-v" role="document">
        <div class="modal-content">
            <div class="modal-body suscrito">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="icon-cerrar xoo-cp-close"></i>
                </button>
                <div id="no-logged" class="absoluto">
                    <div class="text-center w-100">
                        <?php _e( 'Tu suscripción se ha registrado satisfactoriamente', 'b4st' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>