<div class="section cnt-slick woocommerce">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="titulo-productos sin-icon <?php echo ( isset( $clase ) ) ? implode( " ", $clase ) : ''; ?>">
                    <?php echo ( isset( $titulo) ) ? $titulo : ''; ?>
                </h2>
            </div>
        </div>

        <div class="row carrusel-home">
            <div class="col-12">
                <div class="products slider categorias">
                <?php
                foreach( $terms as $term ) {
                    $term_id    = $term->term_id;
                    $icono      = get_field( 'icono', 'product_cat_' . $term_id );
                    $color      = get_field( 'color_de_fondo', 'product_cat_' . $term_id );
                    $nombre     = $term->name;

                    $meta       = get_term_meta( $term_id );
                    $thumb_id   = $meta['thumbnail_id'][0];
                    $imagen     = wp_get_attachment_image( $thumb_id, 'wp-carousel-slick' );

                    if ( wpmd_is_device() ) {
                        require( locate_template( 'includes/movil/categoria.php' ) );
                    } else {
                        require( locate_template( 'includes/desktop/categoria.php' ) );
                    }
                }
                ?>
                </div>
            </div>
        </div>
    </div>
</div>