<header class="main fixed-top text-center bg-gris2">
    <a href="<?php echo esc_url( wc_gift_ideas_get_giftlist_page_url() ); ?>" title="<?php _e('¡Crea una lista de regalos!', 'b4st'); ?>">
        <h2><?php _e('¡Crea una lista de regalos!', 'b4st'); ?></h2>
    </a>
</header>