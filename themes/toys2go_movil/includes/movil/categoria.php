<div class="text-center">
    <div class="rounded cnt" style="background-color: <?php echo $color; ?>">
        <a href="<?php echo get_term_link( $term_id ); ?>"></a>
		<?php
        if ( $icono ):
            ?>
            <i class="<?php echo $icono; ?>"></i>
            <?php
        endif;
		?>
        <h3><?php echo $nombre; ?></h3>
    </div>
</div>