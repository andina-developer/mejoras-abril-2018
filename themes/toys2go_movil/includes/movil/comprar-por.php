<?php
$terms = get_terms( [
	'taxonomy'      => 'genero',
	'parent'        => 0,
	'hide_empty'    => 0,
	'number'        => 2,
    'exclude'       => array(3278),
] );
?>
<section class="genero-link main" id="genero-link">
	<div class="container">
		<div class="row fila-4px">
			<div class="col-12">
				<h2 class="h1"><?php esc_html_e( 'Compra por:', 'b4st' ); ?></h2>
			</div>
			<?php
			foreach ( $terms as $term ) :
				$icono = get_field( 'icono', 'genero_' . $term->term_id );
				$enlace = get_term_link( $term->term_id );
				$nombre = $term->name;
				$color = get_field( 'color', 'genero_' . $term->term_id );
				?>
				<div class="col-6">
					<form role="search" method="post" action="<?php home_url( '/' ); ?>" id="filtro-movil-home">
						<input type="hidden" name="s" value="">
						<input type="hidden" name="post_type" value="product">
						<input type="hidden" name="movil" value="1">
						<input type="hidden" name="etapa" value="1">
						<input type="hidden" name="genero" value="">
					</form>
					<a class="boton-filtro-movil" data-term="<?php echo esc_attr( $term->term_id ); ?>"
					   href="<?php echo esc_url( $enlace ); ?>"
					   style="background-color: <?php echo esc_attr( $color ); ?>;">
						<i class="<?php echo esc_attr( $icono ); ?>"></i>
						<?php echo esc_html( $nombre ); ?>
					</a>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>
