<section class="crear-lista bg-gris5 main" id="crear-lista">
    <div class="container">
        <div class="row text-center">
            <div class="col-12">
                <h2 class="text-celeste"><?php _e( '¡Crea una lista de regalos!', 'b4st' ); ?></h2>
                <p><?php _e( 'Así podrás recibir los regalos que más te <br>
                    gusten. Además, encuentra la lista de<br> regalos de tus amigos aquí.', 'b4st' ); ?></p>
                <a href="<?php echo esc_url( wc_gift_ideas_get_giftlist_page_url() ); ?>" class="btn btn-primary btn-sm btn-icon btn-icon-regalo">
                    <span><?php _e( 'Descubre más', 'b4st' ); ?></span>
                </a>
            </div>
        </div>
    </div>
</section>