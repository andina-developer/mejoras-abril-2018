<?php
$texto = ( is_user_logged_in() ) ? 'Mi Cuenta' : 'Iniciar Sesión';
?>

<aside class="js-offcanvas" id="left" role="complementary">
    <header class="text-center">
        <a class="cerrar js-offcanvas-close" id="cerrar">
            <i class="icon-cerrar"></i>
        </a>

        <a href="<?php echo get_site_url(); ?>">
            <img src="<?php echo get_template_directory_uri(); ?>/theme/img/logo-blanco.svg" class="img-fluid"
                 alt="<?php echo get_bloginfo( 'description' ); ?>" width="140" height="38">
        </a>
    </header>

    <section>
        <div class="link-accesos">
			<?php if ( is_user_logged_in() ) : ?>
                <a href="<?php echo get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ); ?>"
                   class="btn btn-primary btn-block" title="<?php _e( 'Mi Cuenta', 'b4st' ); ?>">
					<?php _e( 'Mi Cuenta', 'b4st' ); ?></a>
			<?php else: ?>
                <a href="<?php echo get_bloginfo( 'url' ); ?>/registro/"
                   class="btn btn-primary btn-block" title="<?php _e( 'Crear Cuenta', 'b4st' ); ?>">
                    <?php _e( 'Crear Cuenta', 'b4st' ); ?></a>
                <a data-toggle="modal" data-target="#loginModal" class="btn btn-light btn-block"
                   title="<?php _e( 'Iniciar Sesión', 'b4st' ) ?>"><?php _e( 'Iniciar Sesión', 'b4st' ) ?></a>
			<?php endif; ?>
        </div>
    </section>

	<?php wp_menu_superior_nav(); ?>
</aside>


<nav class="navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse">
    <?php if ( is_checkout() ) : ?>
    <ul class="cabecera text-center tienda-segura">
        <li class="logo text-center mr-2">
            <a class="navbar-brand" href="<?php echo home_url( '/' ); ?>" title="<?php bloginfo( 'name' ); ?>">
                <img src="<?php echo get_template_directory_uri(); ?>/theme/img/logo.svg" class="img-fluid"
                     alt="<?php echo get_bloginfo( 'description' ); ?>" width="140" height="38">
            </a>
        </li>

        <li>
            <h1 class="rojo text-uppercase mb-0"><span class="d-inline-block"><?php _e( 'Tienda segura' , 'b4st' ); ?></span>
                <i class="icon-porcentaje-escudo d-inline-block"></i></h1>
        </li>
    </ul>
    <?php else: ?>
    <ul class="cabecera text-center">
        <li>
            <a class="js-offcanvas-trigger" data-offcanvas-trigger="left" href="#left"
               title="<?php echo _e( 'Menú', 'b4st' ); ?>">
                <i class="icon-menu-hamburger"></i>
            </a>
        </li>
        <li class="buscar-movil">
            <a href="" title="<?php echo _e( 'Buscar', 'b4st' ); ?>" data-toggle="dropdown" aria-haspopup="true"
               aria-expanded="false">
                <i class="icon-lupa"></i>
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
				<?php echo get_search_form(); ?>
            </div>
        </li>
        <li class="logo text-center">
            <a class="navbar-brand" href="<?php echo home_url( '/' ); ?>" title="<?php bloginfo( 'name' ); ?>">
                <img src="<?php echo get_template_directory_uri(); ?>/theme/img/logo.svg" class="img-fluid"
                     alt="<?php echo get_bloginfo( 'description' ); ?>" width="140" height="38">
            </a>
        </li>
        <li>
            <a data-toggle="modal" data-target="#loginModal" title="<?php echo $texto; ?>">
                <i class="icon-avatar-linea"></i>
            </a>
        </li>
        <li>
            <a class="cart-contents" href="<?php echo wc_get_cart_url(); ?>"
               title="<?php echo _e( 'Ver carrito de compras', 'b4st' ); ?>">
                <i class="icon-carrito" aria-hidden="true"></i>
                <span class="header-cart-count"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
            </a>
        </li>
    </ul>
    <?php endif; ?>
</nav>