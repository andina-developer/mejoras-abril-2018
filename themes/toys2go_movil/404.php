<?php get_header(); ?>

    <section class="pagina-404 pagina-contenido">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12" role="main">
                    <div class="page-header text-center">
						<h1 class="celeste">¡Oops!</h1>
                        <p><?php _e( 'Parece que ha habido un error...', 'b4st' ); ?></p>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <a href="<?php echo get_bloginfo( 'url' ); ?>" class="btn btn-xl btn-primary mx-auto d-block">
                        <?php _e( 'Volver', 'b4st' ); ?></a>
                </div>
            </div>
        </div>

	    <?php if ( wpmd_is_device() ): ?>
            <div class="container">
                <img src="<?php echo get_template_directory_uri(); ?>/theme/img/pinguino404-movil.svg" alt="Error 404" width="66"
                     height="90" class="img-fluid mx-auto d-block">
            </div>
	    <?php else: ?>
            <aside class="text-center img-404">
                <img src="<?php echo get_template_directory_uri(); ?>/theme/img/pinguino404.svg" alt="Error 404" width="445"
                     height="381" class="img-fluid">
            </aside>
	    <?php endif; ?>

    </section>

<?php get_footer(); ?>