<?php
/**
 * Form to create a giftlist.
 *
 * Date: 10/25/17
 * Time: 6:38 PM
 *
 * @link       http://modobeta.pe
 * @since      1.0.0
 * @author     Fernando Paz <ferpaz@beta.pe>
 *
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/templates
 */

/* @var Woocommerce_Gift_Ideas_Giftlist $giftlist */
$current_user = wp_get_current_user();
$fecha_cumpleanos = '';
if ( $giftlist->get_fecha_cumpleanos() ) {
	$fecha_cumpleanos = $giftlist->get_fecha_cumpleanos()->format( 'Y-m-d' );
}
?>
<?php if ( ! $ajax ) : ?>
	<div class="row">
		<?php
		if ( function_exists( 'wc_print_notices' ) ) {
			wc_print_notices();
		}
		?>
	</div>
<?php endif; ?>
<div class="wc-gift-ideas-form container">
	<form action="<?php echo esc_attr( admin_url( 'admin-ajax.php' ) ); ?>" method="post"
		  class="wc-gift-ideas-form-create-giftlist">
		<div class="pad-vert-20">
			<div class="row justify-content-md-center">
				<div class="col-md-10">
					<input type="hidden" name="action" value="create_giftlist">
					<?php wp_nonce_field( 'create_giftlist', 'wc-gift-ideas-nonce' ); ?>

					<div class="form-group">
						<label
								for="nombre"><?php esc_html_e( 'Nombre de lista (*)', 'woocommerce-gift-ideas' ); ?></label>
						<div class="">
							<input type="text" required class="form-control" id="nombre" name="nombre"
								   aria-describedby="nombreHelp"
								   value="<?php echo esc_attr( $giftlist->get_nombre() ); ?>">
							<small id="nombreHelp"
								   class="form-text text-muted d-none"><?php esc_html_e( 'Nombre público de la lista', 'woocommerce-gift-ideas' ); ?></small>
						</div>
					</div>

					<div class="form-group">
						<label for="fecha_cumpleanos"><?php esc_html_e( 'Fecha del evento', 'woocommerce-gift-ideas' ); ?></label>
						<input type="date" class="form-control" name="fecha_cumpleanos"
							   min="<?php echo esc_attr( date( 'Y-m-d' ) ); ?>"
							   value="<?php echo esc_attr( $fecha_cumpleanos ); ?>">
					</div>

					<div class="form-group">
						<label for="cumpleanero_nombre"><?php esc_html_e( 'Nombre (*)', 'woocommerce-gift-ideas' ); ?></label>
						<div class="">
							<input type="text" required class="form-control" id="cumpleanero_nombre"
								   name="cumpleanero_nombre"
								   value="<?php if(esc_attr( $giftlist->get_cumpleanero_nombre())!=""){ echo esc_attr( $giftlist->get_cumpleanero_nombre()); }else{ echo $current_user->first_name; } ?>">
						</div>
					</div>

					<div class="form-group">
						<label for="cumpleanero_apellido"><?php esc_html_e( 'Apellido (*)', 'woocommerce-gift-ideas' ); ?></label>
						<div class="">
							<input type="text" required class="form-control" id="cumpleanero_apellido"
								   name="cumpleanero_apellido"
								   value="<?php if(esc_attr( $giftlist->get_cumpleanero_apellido())!=""){ echo esc_attr( $giftlist->get_cumpleanero_apellido()); }else{ echo $current_user->last_name; } ?>">
						</div>
					</div>

					<div class="form-group">
						<label for="correo_electronico"><?php esc_html_e( 'Correo electrónico (*)', 'woocommerce-gift-ideas' ); ?></label>
						<div class="">
							<input type="email" required class="form-control" id="correo_electronico"
								   name="correo_electronico"
								   value="<?php if(esc_attr( $giftlist->get_email())!=""){ echo esc_attr( $giftlist->get_email()); }else{ echo $current_user->user_email; } ?>">
						</div>
					</div>

					<div class="form-group">
						<div class="">
							<ul class="list-unstyled custom-radios-regalos mb-0 radio-button">
								<li>
									<input type="radio" class="form-check-input" required checked name="sexo_cumpleanero"
										   id="sexo_cumpleanero1"
										   value="<?php echo esc_attr( Woocommerce_Gift_Ideas_Giftlist::MASCULINO ); ?>"
									       <?php if ( Woocommerce_Gift_Ideas_Giftlist::MASCULINO === $giftlist->get_sexo_cumpleanero() ) : ?>checked<?php endif; ?>>
									<label for="sexo_cumpleanero1" class="label text-celeste">
										<?php esc_html_e( 'niño', 'woocommerce-gift-ideas' ); ?>
									</label>
								</li>
								<li>
									<input type="radio" required name="sexo_cumpleanero"
										   id="sexo_cumpleanero2"
										   value="<?php echo esc_attr( Woocommerce_Gift_Ideas_Giftlist::FEMENINO ); ?>"
									       <?php if ( Woocommerce_Gift_Ideas_Giftlist::FEMENINO === $giftlist->get_sexo_cumpleanero() ) : ?>checked<?php endif; ?>>
									<label for="sexo_cumpleanero2" class="label text-rojo">
										<?php esc_html_e( 'niña', 'woocommerce-gift-ideas' ); ?>
									</label>
								</li>
								<li>
									<input type="radio" required name="sexo_cumpleanero"
										   id="sexo_cumpleanero3"
										   value="<?php echo esc_attr( Woocommerce_Gift_Ideas_Giftlist::TODOS ); ?>"
									       <?php if ( Woocommerce_Gift_Ideas_Giftlist::TODOS === $giftlist->get_sexo_cumpleanero() ) : ?>checked<?php endif; ?>>
									<label for="sexo_cumpleanero3" class="label text-amarillo">
										<?php esc_html_e( 'ver Todos', 'woocommerce-gift-ideas' ); ?>
									</label>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>


		<div class="row">
			<div class="title-add-regalos">
				<div class="col-12">
					<h2 class="titulo-productos regalo"><?php _e( 'Ingresa tu dirección de envío:', 'b4st' ); ?></h2>
				</div>
			</div>
		</div>
		<div class="pad-vert-20">
			<div class="row justify-content-md-center">
				<div class="col-md-10">
					<?php
					foreach ( $address as $key => $field ) {
						$prop_name = str_replace( 'direccion_entrega[', '', rtrim( $key, ']' ) );
						if ( isset( $field['country_field'], $address[ $field['country_field'] ] ) ) {
							$field['country'] = ( isset( $giftlist->get_direccion_entrega()[ $prop_name ] ) ? $giftlist->get_direccion_entrega()[ $prop_name ] : $address[ $field['country_field'] ]['value'] );
						}

						wc_gift_ideas_address_form_field(
							$key,
							$field,
							( isset( $giftlist->get_direccion_entrega()[ $prop_name ] ) ? $giftlist->get_direccion_entrega()[ $prop_name ] : $field['value'] )
						);
					}
					?>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row justify-content-center">
				<div class="col-10 col-sm-6 pad-vert-20">
					<button type="submit"
							class="btn btn-primary btn-lg btn-block"><?php esc_html_e( 'Crear lista', 'woocommerce-gift-ideas' ); ?></button>
				</div>
			</div>
		</div>
	</form>
</div>
