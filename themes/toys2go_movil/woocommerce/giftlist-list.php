<?php
/**
 * The Template for displaying giftlists.
 *
 * @version 1.0.0
 * @package Woocommerce_Gift_Ideas/templates
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<div class="wc-gift-ideas-giftlist woocommerce wc-gift-ideas-giftlist-clear">
	<?php
	if ( function_exists( 'wc_print_notices' ) ) {
		wc_print_notices();
	}
	?>
	<div class="row justify-content-md-center pad-vert-100" style="padding-top: 50px;padding-bottom: 50px;">
		<div class="col-md-8">
			<div class="row">
				<div class="col-md-6 text-center">
					<a href="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ) ?>"
					   data-action="create_giftlist"
					   data-not-children="1"
					   class="wc-gift-ideas-create-giftlist btn-crea-lista btn btn-lg btn-block btn-custom-red">
						Crea una lista
					</a>
					<a class="edit-list-gifts d-inline-block invisible" href="">Edita tu lista</a>
				</div>
				<div class="col-md-6">
					<a href="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>" id="wc-gift-ideas-search-giftlist"
					   data-action="search_giftlist"
					   class="btn-encuentra-lista btn btn-lg btn-block btn-primary">Encuentra una lista</a>
				</div>
			</div>
		</div>
	</div>

	<?php if ( empty( $giftlists ) ) : ?>
		<p class="cart-empty text-center">
			<?php esc_html_e( 'No tienes listas de regalo guardadas', 'woocommerce-gift-ideas' ); ?>
		</p>

		<?php do_action( 'wc_gift_ideas_wishlists_is_empty' ); ?>
	<?php else : ?>
		<?php do_action( 'wc_gift_ideas_before_giftlists', $giftlists, $n_total_giftlists, $pagina, $n_paginas ); ?>

		<?php do_action( 'wc_gift_ideas_before_giftlists_table', $giftlists, $n_total_giftlists, $pagina, $n_paginas ); ?>
		<h2><strong>MIS LISTAS</strong></h2>
		<table class="wc-gift-ideas-table-manage-list w-100 tabla-personalizada table-responsive">
			<thead>
			<tr>
				<th class="giftlist-nombre"><?php esc_html_e( 'Nombre', 'woocommerce-gift-ideas' ); ?></th>
				<th class="giftlist-cumpleanero"><?php esc_html_e( 'Cumpleañer@', 'woocommerce-gift-ideas' ); ?></th>
				<th class="giftlist-fecha-cumpleanos"><?php esc_html_e( 'Fecha del evento', 'woocommerce-gift-ideas' ); ?></th>
				<th class="giftlist-n-regalos"><?php esc_html_e( 'Número de juguetes', 'woocommerce-gift-ideas' ); ?></th>
				<th class="giftlist-compartido d-none"><?php esc_html_e( 'Compartida', 'woocommerce-gift-ideas' ); ?></th>
				<th class="giftlist-creado-el"><?php esc_html_e( 'Creada el', 'woocommerce-gift-ideas' ); ?></th>
				<th class="giftlist-action">&nbsp;</th>
			</tr>
			</thead>

			<tbody>

			<?php do_action( 'wc_gift_ideas_giftlists_contents_before' ); ?>

			<?php
			foreach ( $giftlists as $giftlist ) {
				/* @var Woocommerce_Gift_Ideas_Giftlist $giftlist */

				do_action( 'wc_gift_ideas_giftlist_row_before', $giftlist );

				?>
				<tr class="<?php echo esc_attr( apply_filters( 'wc_gift_ideas_giftlist_class', 'wc-gift-ideas-giftlist', $giftlist ) ); ?>">
					<td class="giftlist-nombre"><?php echo esc_html( $giftlist->get_nombre() ); ?></td>
					<td class="giftlist-cumpleanero"><?php echo esc_html( $giftlist->get_cumpleanero() ); ?></td>
					<td class="giftlist-fecha-cumpleanos">
						<?php if (!empty($giftlist->get_fecha_cumpleanos())) { ?>
						<time class="entry-date"
								datetime="<?php echo esc_attr( $giftlist->get_fecha_cumpleanos()->format( 'Y-m-d 00:00:00' ) ); ?>">
							<?php echo esc_html( mysql2date( get_option( 'date_format' ), $giftlist->get_fecha_cumpleanos()->format( 'Y-m-d 00:00:00' ) ) ); ?>
						</time>
						<?php } else {
							echo '-';
							}?>
					</td>
					<td class="giftlist-n-regalos"><?php echo esc_html( count( $giftlist->get_productos() ) ); ?></td>
					<td class="giftlist-compartido d-none">
						<?php
						echo esc_html(
							(
							! empty( $giftlist->get_compartido_con() ) || $giftlist->is_compartida() ?
								__( 'Si', 'woocommerce-gift-ideas' )
								:
								__( 'No', 'woocommerce-gift-ideas' )
							)
						);
						?>
					</td>
					<td class="giftlist-creado-el">
						<time class="entry-date"
								datetime="<?php echo esc_attr( get_the_date( get_option( 'date_format' ), $giftlist->get_id() ) ); ?>">
							<?php echo esc_html( get_the_date( get_option( 'date_format' ), $giftlist->get_id() ) ); ?>
						</time>
						
					</td>
					<?php 
						$expired = false;
						if (!empty($giftlist->get_fecha_cumpleanos())) {
							$today = (new DateTime())->format('Y-m-d'); //use format whatever you are using
							$toBeComparedDate = $giftlist->get_fecha_cumpleanos()->format( 'Y-m-d' );
							$expiry = (new DateTime($toBeComparedDate))->format('Y-m-d');
							if (strtotime($today) > strtotime($expiry)){
								$expired = true;
							}
						}
					?>
					<td class="giftlist-action text-right" >
						<?php if (!$expired) { ?> 
							<a class="wc-gift-ideas-giftlist-show text-center d-inline-block celeste"
							href="<?php echo esc_url( wc_gift_ideas_get_giftlist_page_url( $giftlist->get_codigo() ) ); ?>"
							title="<?php _e( 'Ver Lista de Regalos', 'b4st' ); ?>">
								<i class="icon-eye" aria-hidden="true"></i>
							</a>
							<a class="wc-gift-ideas-giftlist-share-whatsapp text-center d-inline-block celeste"
							href="whatsapp://send?text=<?php echo rawurlencode( esc_url( wc_gift_ideas_get_giftlist_page_url( $giftlist->get_codigo() ) ) ); ?>"
							data-giftlist-id="<?php echo esc_attr( $giftlist->get_id() ); ?>"
							data-action="share/whatsapp/share"
							data-href="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>"
							data-href-action="share_giftlist_whatsapp"
							title="<?php _e( 'Compartir lista por WhatsApp', 'b4st' ); ?>">
								<i class="icon-whatsapp" aria-hidden="true"></i>
							</a>
							<a class="text-center celeste d-inline-block wc-gift-ideas-giftlist-share-email"
							data-giftlist-id="<?php echo esc_attr( $giftlist->get_id() ); ?>"
							data-action="share_giftlist"
							href="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>"
							title="<?php esc_html_e( 'Compartir por correo', 'woocommerce-gift-ideas' ); ?>">
								<i class="icon-mail"></i>
							</a>
						<?php	} ?>

						<a class="wc-gift-ideas-giftlist-remove text-center d-inline-block rojo"
						   title="<?php esc_attr_e( 'Eliminar', 'woocommerce-gift-ideas' ); ?>"
						   data-action="remove_giftlist"
						   data-giftlist-id="<?php echo esc_attr( $giftlist->get_id() ); ?>"
						   href="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>">
							<i class="icon-cerrar"></i>
						</a>
					</td>
				</tr>
				<?php
				do_action( 'wc_gift_ideas_giftlist_row_after', $giftlist );
			} // End foreach().
			?>
			<?php do_action( 'wc_gift_ideas_giftlists_contents_after' ); ?>
			</tbody>
			<tfoot class="invisible">
			<tr>
				<td colspan="100%">
					<?php do_action( 'wc_gift_ideas_after_giftlists_table', $giftlist ); ?>
				</td>
			</tr>
			</tfoot>
		</table>

		<?php do_action( 'wc_gift_ideas_after_giftlists', $giftlists, $n_total_giftlists, $pagina, $n_paginas ); ?>

		<div class="tinv-lists-nav wc-gift-ideas-giftlist-clear">
			<?php do_action( 'wc_gift_ideas_pagenation_giftlist', $pagina, $n_paginas, $n_total_giftlists ); ?>
		</div>
	<?php endif; ?>
</div>
