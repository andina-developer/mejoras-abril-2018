<?php
	$producto_id = $_POST["producto_id"];
	$variation_id = $_POST["variation_id"];
	$_product = wc_get_product( $producto_id );
?>
<table>
	<tr>
		<td class="td-image-product"><img src="<?php echo imagen_personalizada( $producto_id, 'wp-thumb-pupup-productos' ); ?>"
		         alt="<?php echo $_product->name; ?>"></td>
		<td class="td-name-product"><h2><?php echo $_product->name; ?></h2></td>
	</tr>
</table>
