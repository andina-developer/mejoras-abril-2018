<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $post, $product;

/* @var WC_Product $product */

if ( has_post_thumbnail() ) {
    $id_thumb           = get_post_thumbnail_id( get_the_ID() );
	$alt_postthumbnail  = get_post_meta( $id_thumb, '_wp_attachment_image_alt', true);
} else {
	$alt_postthumbnail  = get_the_title();
}
?>
<div class="col-md-6 <?php echo ( wpmd_is_phone() ) ? 'img-movil' : 'img-desktop'; ?>">
	<figure class="woocommerce-galeria">
        <div class="row justify-content-center">
		<?php
        // Obtiene todas las imagenes de la galería
		$attachment_ids = $product->get_gallery_image_ids();

		/**
		 * Vista para Móviles
		 */
		if ( wpmd_is_device() ):
            ?>
            <div class="col-9 popup-gallery">
			    <?php if ( empty( $attachment_ids ) ): ?>
				    <?php if ( has_post_thumbnail() ): ?>
                        <div class="popup-gallery">
                            <a href="<?php imagen_personalizada( $post->ID, 'full' ); ?>" title="<?php echo $alt_postthumbnail; ?>">
                                <img src="<?php imagen_personalizada( $post->ID, 'wp-galeria-productos' ); ?>" alt="<?php echo $alt_postthumbnail; ?>">
                            </a>
                        </div>
				    <?php else: ?>
                        <div class="woocommerce-product-gallery__image--placeholder">
                            <img src="<?php echo esc_url( wc_placeholder_img_src() ); ?>" alt="<?php echo $alt_postthumbnail; ?>" class="wp-post-image img-fluid">
                        </div>
				    <?php endif; ?>
			    <?php else: ?>
                <div class="slider-movil">
                    <?php
                    if ( has_post_thumbnail() ):
                        ?>
                        <div>
                            <a href="<?php imagen_personalizada( $post->ID, 'full' ); ?>" title="<?php echo $alt_postthumbnail; ?>">
                                <img src="<?php imagen_personalizada( $post->ID, 'wp-galeria-productos' ); ?>" alt="">
                            </a>
                        </div>
                        <?php
                    endif;
                    ?>

                    <?php
                    foreach( $attachment_ids as $attachment_id ) {
                        $thumb = wp_get_attachment_image_src( $attachment_id, 'wp-galeria-productos' );
	                    $completa   = wp_get_attachment_image_src( $attachment_id, 'full' );
                        ?>
                        <div>
                            <a href="<?php echo $completa[0]; ?>" title="<?php getaltimage( $attachment_id ); ?>">
                                <img src="<?php echo $thumb[0]; ?>" alt="<?php getaltimage( $attachment_id ); ?>">
                            </a>
                        </div>
                        <?php
                    }
                    ?>

                    <?php
                    while ( have_rows( 'video' ) ) : the_row();
                        $video      = get_sub_field( 'url' );
                        $titulo     = ( get_sub_field( 'titulo' ) ) ? get_sub_field( 'titulo' ) : $video->title;
                        $miniatura  = $video->thumbnail_url;
                        $proveedor  = $video->provider_name;
                        ?>
                        <div class="video">
                            <?php
                            if ( $proveedor == 'YouTube' ):
                                $html = $video->html;
                                $video_idn = get_string_between( $html, 'embed/', '?' ); //Id del video de Youtube
                                ?>
                                <a href="https://www.youtube.com/watch?v=<?php echo $video_idn ?>" class="video" title="<?php echo $titulo; ?>">
                                    <img src="<?php echo $miniatura; ?>" alt="<?php echo $titulo; ?>" class="img-fluid">
                                </a>
                                <?php
                            endif;
                            ?>
                        </div>
                        <?php
                    endwhile;
                    ?>
                </div>
	            <?php endif; ?>
            </div>
        <?php
		/**
		 * Vista para Desktops
		 */
        else:
        ?>
            <?php if ( empty( $attachment_ids ) ): ?>
                <div class="col-12">
                    <?php
                    if ( has_post_thumbnail() ):
                        ?>
                        <div class="popup-gallery">
                            <a href="<?php imagen_personalizada( $post->ID, 'full' ); ?>" title="<?php echo $alt_postthumbnail; ?>">
                                <img src="<?php imagen_personalizada( $post->ID, 'wp-galeria-productos' ); ?>" alt="<?php echo $alt_postthumbnail; ?>">
                            </a>
                        </div>
                    <?php
                    else:
                        ?>
                        <div>
                            <img src="<?php echo esc_url( wc_placeholder_img_src() ); ?>" alt="<?php echo $alt_postthumbnail; ?>" class="wp-post-image img-fluid">
                        </div>
                        <?php
                    endif;
                    ?>
                </div>
            <?php else: ?>
                <div class="col-3">
                    <div class="slider slider-nav">
                        <?php
                        if ( has_post_thumbnail() ):
                            ?>
                            <div>
                                <img src="<?php imagen_personalizada( $post->ID, 'wp-thumb-galeria-productos' ); ?>" alt="<?php echo $alt_postthumbnail; ?>">
                            </div>
                        <?php
                        endif;
                        ?>

                        <?php
                        foreach( $attachment_ids as $attachment_id ) {
                            $thumb = wp_get_attachment_image_src( $attachment_id, 'wp-thumb-galeria-productos' );
                            ?>
                            <div>
                                <img src="<?php echo $thumb[0]; ?>" alt="<?php getaltimage( $attachment_id ); ?>">
                            </div>
                            <?php
                        }
                        ?>

                        <?php
                        while ( have_rows( 'video' ) ) : the_row();
                            $video      = get_sub_field( 'url' );
                            $titulo     = ( get_sub_field( 'titulo' ) ) ? get_sub_field( 'titulo' ) : $video->title;
                            $miniatura  = $video->thumbnail_url;
                            ?>
                            <div class="video">
                                <img src="<?php echo $miniatura; ?>" alt="<?php echo $titulo; ?>" class="img-fluid">
                            </div>
                            <?php
                        endwhile;
                        ?>

                    </div>
                </div>

                <div class="col-9">
                    <div class="slider slider-for popup-gallery">
                        <?php
                        if ( has_post_thumbnail() ):
                            ?>
                            <div>
                                <a href="<?php imagen_personalizada( $post->ID, 'full' ); ?>" title="<?php echo $alt_postthumbnail; ?>">
                                    <img src="<?php imagen_personalizada( $post->ID, 'wp-galeria-productos' ); ?>" class="img-fluid" alt="<?php echo $alt_postthumbnail; ?>">
                                </a>
                            </div>
                            <?php
                        endif;
                        ?>

                        <?php
                        foreach( $attachment_ids as $attachment_id ) {
                            $miniatura  = wp_get_attachment_image_src( $attachment_id, 'wp-galeria-productos' );
                            $completa   = wp_get_attachment_image_src( $attachment_id, 'full' );
                            ?>
                            <div>
                                <a href="<?php echo $completa[0]; ?>" title="<?php getaltimage( $attachment_id ); ?>">
                                    <img src="<?php echo $miniatura[0]; ?>" alt="<?php getaltimage( $attachment_id ); ?>" class="img-fluid">
                                </a>
                            </div>
                            <?php
                        }
                        ?>

                        <?php
                        while ( have_rows( 'video' ) ) : the_row();
                            $video      = get_sub_field( 'url' );
                            $titulo     = ( get_sub_field( 'titulo' ) ) ? get_sub_field( 'titulo' ) : $video->title;
                            $miniatura  = $video->thumbnail_url;
                            $proveedor  = $video->provider_name;
                            ?>
                            <div class="video">
                            <?php
                            if ( $proveedor == 'YouTube' ):
                                $html = $video->html;
                                $video_idn = get_string_between( $html, 'embed/', '?' ); //Id del video de Youtube
                                ?>
                                <a href="https://www.youtube.com/watch?v=<?php echo $video_idn ?>" class="video" title="<?php echo $titulo; ?>">
                                    <img src="<?php echo $miniatura; ?>" alt="<?php echo $titulo; ?>" class="img-fluid">
                                </a>
                                <?php
                                endif;
                            ?>
                            </div>
                            <?php
                        endwhile;
                        ?>
                    </div>
                </div>
            <?php endif; ?>
		<?php
        endif;
        ?>
        </div>
	</figure>
</div>

<?php
/*if ( get_field( 'video' ) ) :
    $video = get_field( 'video' );
    var_dump( $video );
endif;*/
?>