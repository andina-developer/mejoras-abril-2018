<?php
/**
 * The Template for displaying an unexistent giftlist.
 *
 * @version 1.0.0
 * @package Woocommerce_Gift_Ideas/templates
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<div class="row justify-content-md-center pad-vert-100">
    <div class="col-md-8">
        <div class="row">
            <div class="col-md-6 text-center">
                <a data-toggle="modal"
                   data-target="#loginModal"
                   title="Iniciar Sesión"
                   class="btn-crea-lista btn btn-lg btn-block btn-custom-red blanco">
                    Crea una lista
                </a>
                <a class="edit-list-gifts d-inline-block invisible" href=""><?php _e( 'Edita tu lista', 'b4st' ); ?></a>
            </div>

            <div class="col-md-6">
                <a href="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>" id="wc-gift-ideas-search-giftlist"
                   data-action="search_giftlist"
                   class="btn-encuentra-lista btn btn-lg btn-block btn-primary">
                    <?php _e( 'Encuentra una lista', 'b4st' ); ?></a>
            </div>
        </div>
    </div>
</div>
