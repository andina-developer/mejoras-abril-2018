<?php
global $woocommerce;
$items      = $woocommerce->cart->get_cart();
$total_cart = $woocommerce->cart->get_cart_total();

//var_dump($woocommerce->cart);
?>
<ul class="list-unstyled carrito-checout">
	<?php
	foreach ( $items as $item => $values ) {
		$_product = wc_get_product( $values['data']->get_id() );
		$price    = get_post_meta( $values['product_id'], '_regular_price', true );
		$quantity = $values['quantity'];
		$precio   = $price * $quantity;
		?>

        <li class="clearfix">
            <div class="float-left">
				<?php echo $_product->get_title() . ' x ' . $quantity; ?>
            </div>

            <div class="float-right">
                <strong class="superstrong"><?php echo wc_price($precio); ?></strong>
            </div>
        </li>

		<?php
	}
	?>

    <li class="delivery clearfix">
        <div class="float-left">
		    <?php _e( 'Delivery', 'b4st' ); ?>
        </div>

        <div class="float-right">
            <strong class="superstrong">S/15.00</strong>
        </div>
    </li>
</ul>

<div class="total_cart clearfix carrito-checout">
    <div class="float-left">
		<strong class="superstrong"><?php _e( 'Sub-Total:', 'b4st' ); ?></strong>
    </div>
    <div class="float-right">
        <strong class="rojo superstrong"><?php echo $total_cart; ?></strong>
    </div>
</div>
