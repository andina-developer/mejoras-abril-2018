<?php
/**
 * Empty cart page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-empty.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

wc_print_notices();

/**
 * @hooked wc_empty_cart_message - 10
 */
do_action( 'woocommerce_cart_is_empty' );

if ( wc_get_page_id( 'shop' ) > 0 ) : ?>
    <section class="pagina-404 pagina-contenido">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12" role="main">
                    <div class="page-header text-center">
                        <h1 class="celeste">¡Oops!</h1>
                        <p><?php _e( 'Tu carrito está vacío', 'b4st' ); ?></p>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <a href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>" class="btn btn-xl btn-primary mx-auto d-block">
                        <?php _e( 'Return to shop', 'woocommerce' ) ?></a>
                </div>
            </div>
        </div>

        <?php if ( wpmd_is_device() ): ?>
            <div class="container">
                <img src="<?php echo get_template_directory_uri(); ?>/theme/img/pinguino404-movil.svg" alt="Error 404" width="66"
                     height="90" class="img-fluid mx-auto d-block">
            </div>
        <?php else: ?>
            <aside class="text-center img-404">
                <img src="<?php echo get_template_directory_uri(); ?>/theme/img/pinguino404.svg" alt="Error 404" width="445"
                     height="381" class="img-fluid">
            </aside>
        <?php endif; ?>

    </section>
<?php endif; ?>
