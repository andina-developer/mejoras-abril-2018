<?php
/**
 * Email template to share a giftlist.
 *
 * Date: 10/28/17
 * Time: 3:48 PM
 *
 * @link       http://modobeta.pe
 * @since      1.0.0
 * @author     Fernando Paz <ferpaz@beta.pe>
 *
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/templates/emails
 */

/* @var Woocommerce_Gift_Ideas_Giftlist $giftlist */
?>
<?php
/**
 * Email template to share a giftlist.
 *
 * Date: 10/28/17
 * Time: 3:48 PM
 *
 * @link       http://modobeta.pe
 * @since      1.0.0
 * @author     Fernando Paz <ferpaz@beta.pe>
 *
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/templates/emails
 */

/* @var Woocommerce_Gift_Ideas_Giftlist $giftlist */
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="Content-Security-Policy" content="default-src gap://ready file://* *; style-src 'self' http://* https://* 'unsafe-inline'; script-src 'self' http://* https://* 'unsafe-inline' 'unsafe-eval'">
    <title><?php echo esc_html( $giftlist->get_cumpleanero() ); ?> te ha compartido su lista de regalos</title></head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<div id="wrapper">
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
        <tbody>
        <tr>
            <td align="center" valign="top">
                <div></div>
                <table border="0" cellpadding="0" cellspacing="0" width="600"
                       style="background-color:#fdfdfd;border:1px solid #dcdcdc;border-radius:3px!important">
                    <tbody>
                    <tr>
                        <td align="center" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" width="600"
                                   style="background-color:#00abf1;border-radius:3px 3px 0 0!important;color:#ffffff;border-bottom:0;font-weight:bold;line-height:100%;vertical-align:middle;font-family:Helvetica,Roboto,Arial,sans-serif">
                                <tbody>
                                <tr>
                                    <td align="left" style="padding:20px 40px;display:block">
                                        <a href="http://www.toys2go.pe/" title="Toys2Go">
                                            <img alt="Toys2Go" src="http://modobeta.biz/dev/toys2go/wp-content/themes/toys2go_movil/theme/img/logo-desktop.png" height="60" width="60"
                                            alt="toy2go" style="display: block;">
                                        </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" width="600">
                                <tbody>
                                <tr>
                                    <td valign="top" style="background-color:#fdfdfd">
                                        <table border="0" cellpadding="20" cellspacing="0" width="100%"
                                               style="color:#6a6a6a;font-family:Helvetica,Roboto,Arial,sans-serif;font-size:14px;">
                                            <tbody>
                                            <tr>
                                                <td valign="top" align="center">
                                                    <h2 style="color:#6a6a6a;font-family:Helvetica,Roboto,Arial,sans-serif;font-size:18px;line-height:130%;text-align:center;margin:10px 0px;">
                                                        <?php echo esc_html( $giftlist->get_cumpleanero() ); ?> te ha compartido su lista de regalos</h2>
                                                    <p><?php echo $_POST["mensaje"]; ?></p>

                                                    <table width="100%">
                                                        <tr>
                                                            <td align="center"><h2 style="color:#bb2936;font-family:Helvetica,Roboto,Arial,sans-serif;font-size:18px;line-height:130%;text-align:center;margin:10px 0px;">
                                                                    Lista: <a href="<?php echo esc_url( wc_gift_ideas_get_giftlist_page_url( $giftlist->get_codigo() ) ); ?>"
                                                                              style="color:#bb2936;font-family:Helvetica,Roboto,Arial,sans-serif;font-size:18px;line-height:130%;text-decoration:none!important;"><?php echo esc_html( $giftlist->get_nombre() ); ?></a>
                                                                </h2></td>
                                                        </tr>
                                                    </table>
                                                    <table width="100%" border="1" style="border-collapse: collapse; border:1px solid #e1e1e1;color:#6a6a6a;font-family:Helvetica,Roboto,Arial,sans-serif;font-size:14px;">
                                                        <?php
                                                        foreach ( $giftlist->get_productos_por_comprar() as $producto ) {
                                                            $producto_url = apply_filters( 'wc_gift_ideas_giftlist_producto_url', $producto->get_permalink(), $producto, $giftlist );
                                                            ?>
                                                            <tr class="<?php echo esc_attr( apply_filters( 'wc_gift_ideas_giftlist_producto_class', 'giftlist-producto', $producto, $giftlist ) ); ?>">
                                                                <td align="left" style="font-weight: bold; padding: 7px; width: 145px;" width="145">
                                                                    <?php
                                                                    $thumbnail = apply_filters( 'wc_gift_ideas_giftlist_producto_minitura', $producto->get_image(), $producto, $giftlist );
                                                                    preg_match( '/src\="(?<imagen>[^"]+)"/', $thumbnail, $match);
                                                                    ?>
                                                                    <a href="<?php echo esc_url( $producto_url ); ?>">
                                                                        <img src="http:<?php echo $match['imagen']; ?>" alt="<?php echo $producto->get_title(); ?>"
                                                                             style="display: block; max-width: 100%; height: auto">
                                                                    </a>
                                                                </td>
                                                                <td align="left" style="padding: 7px;">
                                                                    <?php
                                                                    if ( ! $producto->is_visible() ) {
                                                                        $nombre = $producto->get_title();
                                                                        $nombre .= '&nbsp;';
                                                                    } else {
                                                                        $nombre = '<a href="' . esc_url( $producto_url ) . '">' . $producto->get_title() . '</a>';
                                                                    }

                                                                    echo apply_filters( 'wc_gift_ideas_giftlist_producto_nombre', $nombre, $producto, $giftlist );
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        } //End foreach()
                                                        ?>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>