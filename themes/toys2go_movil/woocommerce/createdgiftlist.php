<?php
/**
 * Tempalte when a giftlist is created.
 *
 * Date: 11/4/17
 * Time: 11:40 AM
 *
 * @link       http://modobeta.pe
 * @since      1.0.0
 * @author     Fernando Paz <ferpaz@beta.pe>
 *
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/templates
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
/* @var Woocommerce_Gift_Ideas_Giftlist $giftlist */
?>
<div class="container lista-creada pad-vert-35">
    <div class="justify-content-md-center">
        <div class=" text-center">
            <h2 class="celeste">¡<?php esc_html_e( 'Bienvenid@', 'woocommerce-gift-ideas' ); ?> <?php echo esc_html( wp_get_current_user()->user_firstname ); ?>!</h2>
            <p>Tu lista ya fue creada</p>
            <div class="box-listacreada d-none">
                <small>Comparte con tus invitados este código:</small>
                <h2 class="celeste"><?php echo esc_html( $giftlist->get_codigo() ); ?></h2>
                <small>para que puedan acceder a tu lista.</small>
            </div>
            <p>Navega y haz click en este icono</p>
            <span><i class="icon-regalo"></i></span>
            <p>Para elegir los regalos que más te gusten.</p>
            <div class="row justify-content-center">
                <div class="col-11 pad-vert-20">
                    <a class="btn btn-primary"
                       href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) . '#hide-add-to-cart=1' ); ?>">
                        <?php esc_html_e( 'Elegir regalos', 'woocommerce-gift-ideas' ); ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>