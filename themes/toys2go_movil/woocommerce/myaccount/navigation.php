<?php
/**
 * My Account navigation
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/navigation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_account_navigation' );
?>

<nav class="custom-nav-account pad-vert-100">
	<ul class="list-unstyled">
		<?php foreach ( wc_get_account_menu_items() as $endpoint => $label ) : ?>

			<li class="d-inline-block text-center item-menu-cuenta caja <?php echo $endpoint; ?>">
				<a class="d-flex caja" href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoint ) ); ?>"><?php echo esc_html( $label ); ?></a>
			</li>
		<?php endforeach; ?>
		<li class="d-inline-block text-center item-menu-cuenta caja giftlist">
			<a href="/giftlist/" title="Acceso a mi lista de regalos">
				<span class="icon-grupo">
					<i class="icon-regalo"></i>
				</span>
				<span>Lista de Regalos</span>
			</a>
		</li>
	</ul>
</nav>

<?php do_action( 'woocommerce_after_account_navigation' ); ?>
