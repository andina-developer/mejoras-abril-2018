<?php
/**
 * My Addresses
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-address.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

$customer_id = get_current_user_id();

if (!wc_ship_to_billing_address_only() && wc_shipping_enabled()) {
    $get_addresses = apply_filters('woocommerce_my_account_get_addresses', array(
        'billing' => __('Billing address', 'woocommerce'),
        'shipping' => __('Shipping address', 'woocommerce'),
    ), $customer_id);
} else {
    $get_addresses = apply_filters('woocommerce_my_account_get_addresses', array(
        'billing' => __('Billing address', 'woocommerce'),
    ), $customer_id);
}

$oldcol = 1;
$col = 1;
?>

    <h2 class="celeste pad-vert-20">TUS DIRECCIONES</h2>

<?php if (!wc_ship_to_billing_address_only() && wc_shipping_enabled()) : ?>
    <div class="u-columns woocommerce-Addresses addresses row">
<?php endif; ?>

<?php foreach ($get_addresses as $name => $title) : ?>

    <div class="col-lg-6 edit-direcciones">
        <header class="woocommerce-Address-title title">
            <h3><?php echo $title; ?></h3>
            <a href="<?php echo esc_url(wc_get_endpoint_url('edit-address', $name)); ?>"
               class="edit btn btn-primary btn-sm"><?php _e('Edit', 'woocommerce'); ?></a>
        </header>
        <address>
            <?php
            $address = apply_filters('woocommerce_my_account_my_address_formatted_address', array(
                'first_name' => get_user_meta($customer_id, $name . '_first_name', true),
                'last_name' => get_user_meta($customer_id, $name . '_last_name', true),
                'company' => get_user_meta($customer_id, $name . '_company', true),
                'address_1' => get_user_meta($customer_id, $name . '_address_1', true),
                'address_2' => get_user_meta($customer_id, $name . '_address_2', true),
                'city' => get_user_meta($customer_id, $name . '_city', true),
                'state' => get_user_meta($customer_id, $name . '_state', true),
                'postcode' => get_user_meta($customer_id, $name . '_postcode', true),
                'country' => get_user_meta($customer_id, $name . '_country', true),
            ), $customer_id, $name);

            $formatted_address = WC()->countries->get_formatted_address($address);

            if (!$formatted_address) {
                _e('You have not set up this type of address yet.', 'woocommerce');
            } else {
                echo $formatted_address;
            }
            ?>
        </address>
    </div>

<?php endforeach; ?>

<?php
global $wpdb;
$current_id = get_current_user_id();
$query = $wpdb->prepare("select * from " . $wpdb->prefix . "customer_address where user_id = " . $current_id, '');
$resultado = $wpdb->get_results($query);
if (!empty($resultado)) {
    foreach ($resultado as $itemDir) {
        $customer_id = $itemDir->user_id;
        ?>
        <div class="col-lg-6 edit-direcciones">
            <header class="woocommerce-Address-title title">
                <h3><?php echo $itemDir->nombre; ?></h3>
                <a href="<?php echo esc_url(wc_get_endpoint_url('edit-address', $itemDir->ID)); ?>"
                   class="edit btn btn-primary btn-sm"><?php _e('Edit', 'woocommerce'); ?></a>
            </header>
            <address>
                <?php

                $query = $wpdb->prepare("SELECT dr.idDist as ID, dr.distrito, pr.provincia as provincia, " .
                    "de.departamento FROM " . $wpdb->prefix . "ubigeo_distrito dr " .
                    "inner join " . $wpdb->prefix . "ubigeo_provincia pr on dr.idProv = pr.idProv " .
                    "inner join " . $wpdb->prefix . "ubigeo_departamento de on de.idDepa = pr.idDepa " .
                    "where dr.idDist=" . $itemDir->distrito_id, '');
                $ubigeo = $wpdb->get_row($query);

                echo $itemDir->direccion . " - " . $itemDir->numero;
                echo "<br>";
                echo $ubigeo->departamento;
                echo "<br>";
                echo $ubigeo->provincia;
                echo "<br>";
                echo $ubigeo->distrito;
                echo "<br>";
                echo $itemDir->referencia;
                ?>
            </address>
        </div>
    <?php }
} ?>

<?php if (!wc_ship_to_billing_address_only() && wc_shipping_enabled()) : ?>
    </div>
<?php endif;
