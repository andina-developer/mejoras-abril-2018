<?php
/**
 * Template for the modal that holds the form to create a giftlist.
 *
 * Date: 10/25/17
 * Time: 6:23 PM
 *
 * @link       http://modobeta.pe
 * @since      1.0.0
 * @author     Fernando Paz <ferpaz@beta.pe>
 *
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/templates
 */

?>
<div class="wc-gift-ideas-create-wishlist wc-gift-ideas-modal modal-dialog modal-lg custom-modal" role="document">
	<div class="wc-gift-ideas-table modal-content">
        <i data-dismiss="modal" class="icon-cerrar cerrar"></i>
        <div class="title-add-regalos col-12 no-border-top"><h2 class="titulo-productos regalo">Crea tu lista:</h2></div>
		<div class="wc-gift-ideas-cell form-add-regalos">
			<?php if ( isset( $notice ) && ! empty( $notice ) ) : ?>
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="<?php esc_html_e( 'Cerrar', 'woocommerce-gift-ideas' ); ?>">
						<span aria-hidden="true">&times;</span>
					</button>
					<?php echo esc_html( $notice ); ?>
				</div>
			<?php endif; ?>
			<?php
			wc_get_template(
				'creategiftlist-form.php',
				[
					'address'  => $address,
					'giftlist' => $giftlist,
				],
				'',
				WOOCOMMERCE_GIFT_IDEAS_PATH . 'templates/'
			);
			?>
		</div>
	</div>
</div>
