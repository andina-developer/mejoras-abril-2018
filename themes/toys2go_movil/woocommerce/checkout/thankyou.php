<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<div class="woocommerce-order">

	<?php if ( $order ) : ?>

		<?php if ( $order->has_status( 'failed' ) ) : ?>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php _e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
				<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php _e( 'Pay', 'woocommerce' ) ?></a>
				<?php if ( is_user_logged_in() ) : ?>
					<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php _e( 'My account', 'woocommerce' ); ?></a>
				<?php endif; ?>
			</p>

		<?php else : ?>
            <div class="row justify-content-center">
                    <div class="col-12" role="main">
                        <div class="page-header text-center">
                            <div class="min-width-280 bg-blanco">
                                <h1 class="celeste"><strong><?php _e( '¡Felicitaciones!', 'b4st' ); ?></strong></h1>
                                <p class="lead">
                                    <strong class="d-block"><?php _e( 'Tu pedido ha sido completado.', 'b4st' ); ?></strong>
                                </p>
                                <p>
                                    <strong><?php _e( 'En los próximos minutos te llegará un correo con el <br class="d-none d-md-block d-lg-block"> detalle de tu orden.', 'b4st' ); ?></strong>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            <div class="row justify-content-center">
                    <div class="col-12 col-lg-7">
                        <a href="<?php echo get_bloginfo('url'); ?>"
                           class="btn btn-xl d-block btn-primary mx-auto"><?php _e( 'Seguir comprando', 'b4st' ); ?></a>

                        <a href="#"></a>

                        <figure>
                            <img src="<?php echo get_template_directory_uri(); ?>/theme/img/pinguino-cuenta-creada.svg"
                                 alt="<?php echo get_the_title(); ?>" class="img-fluid mx-auto d-block" width="211" height="248">
                        </figure>
                    </div>
                </div>

            <div class="row justify-content-md-center d-none">
                <div class="col-md-10">
                    <div class="contenido">
                        <div class="detalle">
                            <ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">
                                <li class="woocommerce-order-overview__order order">
                                    <?php _e( 'Order number:', 'woocommerce' ); ?>
                                    <strong><?php echo $order->get_order_number(); ?></strong>
                                </li>

                                <li class="woocommerce-order-overview__date date">
                                    <?php _e( 'Date:', 'woocommerce' ); ?>
                                    <strong><?php echo wc_format_datetime( $order->get_date_created() ); ?></strong>
                                </li>

                                <li class="woocommerce-order-overview__total total">
                                    <?php _e( 'Total:', 'woocommerce' ); ?>
                                    <strong><?php echo $order->get_formatted_order_total(); ?></strong>
                                </li>

                                <?php if ( $order->get_payment_method_title() ) : ?>

                                    <li class="woocommerce-order-overview__payment-method method">
                                        <?php _e( 'Payment method:', 'woocommerce' ); ?>
                                        <strong><?php echo wp_kses_post( $order->get_payment_method_title() ); ?></strong>
                                    </li>

                                <?php endif; ?>

                            </ul>

                            <?php do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>
                            <?php do_action( 'woocommerce_thankyou', $order->get_id() ); ?>
                        </div>
                    </div>
                </div>
            </div>

		<?php endif; ?>


	<?php else : ?>

		<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), null ); ?></p>

	<?php endif; ?>

</div>
