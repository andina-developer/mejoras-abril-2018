<form name="checkout" id="checkout" method="post" class="checkout woocommerce-checkout desktop-checkout-form"
      action="<?php echo esc_url(wc_get_checkout_url()); ?>" enctype="multipart/form-data">
    <div class="row mt-3 mb-4">
        <div class="col-12 col-md-4 col-left">
            <div class="borde-all borde-radius-10">
                <?php if ($checkout->get_checkout_fields()) : ?>

                    <?php do_action('woocommerce_checkout_before_customer_details'); ?>

                    <div class="col2-set" id="customer_details">
                        <div class="clearfix">
                            <?php do_action('woocommerce_checkout_billing'); ?>
                        </div>

                        <div class="clearfix">
                            <?php do_action('woocommerce_checkout_shipping'); ?>
                        </div>
                    </div>

                    <?php do_action('woocommerce_checkout_before_order_review'); ?>

                    <div id="order_review" class="woocommerce-checkout-review-order">
                        <?php do_action('woocommerce_checkout_order_review'); ?>
                    </div>

                    <?php do_action('woocommerce_checkout_after_order_review'); ?>

                    <?php do_action('woocommerce_checkout_after_customer_details'); ?>

                <?php endif; ?>
            </div>
        </div>

        <div class="col-12 col-md-4 col-center">
            <div class="borde-all borde-radius-10">
                <?php mostrar_opciones_com_pago( $checkout ); ?>

                <?php do_action( 'metodos_de_pago' ); ?>

                <?php do_action( 'form_add_cupon' ); ?>
            </div>

            <?php $order_button_text = apply_filters( 'woocommerce_order_button_text', __( 'Comprar ahora', 'woocommerce' ) ); ?>
            <div class="text-center mt-3">
                <?php echo apply_filters( 'woocommerce_order_button_html', '<input type="submit" class="btn btn-block btn-primary btn-xl d-block text-capitalize" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr( $order_button_text ) . '" data-value="' . esc_attr( $order_button_text ) . '" />' ); ?>
            </div>
            
            <div class="text-center mt-2">
                <a href="<?php echo esc_url(wc_get_cart_url()); ?>">
                    <strong>< <?php _e("volver al carrito", "b4st"); ?></strong>
                </a>
            </div>
        </div>

        <div class="col-12 col-md-4 col-right">
            <div id="order_review_derecha" class="woocommerce-checkout-review-order consombra">
                <?php do_action( 'woocommerce_checkout_order_review' ); ?>
            </div>
        </div>
    </div>
</form>
