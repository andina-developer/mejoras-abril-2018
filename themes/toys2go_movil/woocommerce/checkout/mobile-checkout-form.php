<!-- Comienzo Móvil-->
<?php $estaLogueado = true; ?>

<form name="checkout" id="checkout" method="post" class="checkout woocommerce-checkout mobile-checkout-form"
      action="<?php echo esc_url(wc_get_checkout_url()); ?>" enctype="multipart/form-data">

    <h3><?php _e('Envío', 'b4st'); ?></h3>
    <section class="content-step">
        <div id="customheightstep" class="heightstep">
            <?php if ($checkout->get_checkout_fields()) : ?>
                <?php do_action('woocommerce_checkout_before_customer_details'); ?>
                <div class="row" id="customer_details">
                    <div class="col-12">
                        <?php do_action('woocommerce_checkout_billing'); ?>

                        <?php do_action('woocommerce_checkout_shipping'); ?>
                    </div>
                </div>
                <?php do_action('woocommerce_checkout_after_customer_details'); ?>
            <?php endif; ?>

            <!-- Lista de metodos de envio -->
            <?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>

                <?php do_action('woocommerce_review_order_before_shipping'); ?>

                <?php wc_cart_totals_shipping_html(); ?>

                <?php do_action('woocommerce_review_order_after_shipping'); ?>

            <?php endif; ?>
        </div>
    </section>

    <h3><?php _e('Packing', 'b4st'); ?></h3>
    <section class="content-step">
        <div class="heightstep">

            <?php do_action('seccion_packing'); ?>
        </div>
    </section>

    <h3><?php _e('Pago', 'b4st'); ?></h3>
    <section class="content-step">
        <div class="heightstep">
            <?php mostrar_opciones_com_pago( $checkout ); ?>
            <?php //do_action('seccion_comprobante_pago'); ?>
            <h3 class="titulo-productos dinero bordes bordes-b titulo_pago_cab">Elige la forma de pago:</h3>
            <?php do_action('metodos_pago'); ?>
            <?php do_action( 'form_add_cupon' ); ?>
        </div>
    </section>
</form>

<div id="order_review_derecha" class="woocommerce-checkout-review-order p-relative mt-3 mb-3">
    <a class="btn-open-detalle p-absolute d-inline-block" data-toggle="collapse" href="#collapsedetalle" role="button" aria-expanded="false" aria-controls="collapsedetalle">
        <i class="icon-angulo-arriba"></i>
    </a>

    <h2 class="bordes bordes-tb semibold"><?php _e('Resumen de compra', 'woocommerce'); ?></h2>

    <div id="collapsedetalle" class="collapse show">
        <?php do_action('woocommerce_checkout_order_review'); ?>
    </div>
</div>