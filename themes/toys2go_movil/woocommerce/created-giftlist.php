<?php
/**
 * Email template for a new giftlist.
 *
 * Date: 11/26/17
 * Time: 5:28 PM
 *
 * @link       http://modobeta.pe
 * @since      1.0.0
 * @author     Fernando Paz <ferpaz@beta.pe>
 *
 * @package    Woocommerce_Gift_Ideas
 * @subpackage Woocommerce_Gift_Ideas/templates/emails
 */

/* @var Woocommerce_Gift_Ideas_Giftlist $giftlist */
?>
<?php
    global $wpdb;
    $direccioncompleta = json_encode( $giftlist->get_direccion_entrega(), true );
    $obj = json_decode($direccioncompleta);
?>
<div id="wrapper">
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
        <tbody>
        <tr>
            <td align="center" valign="top">
                <div></div>
                <table border="0" cellpadding="0" cellspacing="0" width="600"
                       style="background-color:#fdfdfd;border:1px solid #dcdcdc;border-radius:5px!important">
                    <tbody>
                    <tr>
                        <td align="center" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" width="600"
                                   style="background-color:#00abf1;border-radius:5px 5px 0 0!important;color:#ffffff;border-bottom:0;font-weight:bold;line-height:100%;vertical-align:middle;font-family:Helvetica,Roboto,Arial,sans-serif">
                                <tbody>
                                <tr>
                                    <td align="center" style="padding:25px 0;display:block">
                                        <h2 style="color:#fff;font-family:Helvetica,Roboto,Arial,sans-serif;font-size:25px;text-align:center;">
                                            LISTA DE REGALOS CREADA</h2>
                                        <!--<img width="90px; display: none;" alt="Toys2Go - Tienda de regalos para niños por internet"
                                            src="http://35.224.195.178/toys2go.pe/wp-content/themes/toys2go_movil/theme/img/logo-desktop.png">-->
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" width="600">
                                <tbody>
                                <tr>
                                    <td valign="top" style="background-color:#fdfdfd">
                                        <table border="0" cellpadding="20" cellspacing="0" width="100%"
                                               style="color:#6a6a6a;font-family:Helvetica,Roboto,Arial,sans-serif;font-size:14px;">
                                            <tbody>
                                            <tr>
                                                <td valign="top" align="center">


                                                    <table width="100%" border="1" bordercolor="#e1e1e1"
                                                           style="border-collapse: collapse; border:1px solid #e1e1e1;color:#6a6a6a;font-family:Helvetica,Roboto,Arial,sans-serif;font-size:14px;">
                                                        <tr>
                                                            <td align="left" style="font-weight: bold; padding: 7px;">
                                                                Nombre de la lista:
                                                            </td>
                                                            <td align="left" style="padding: 7px;">
                                                                <a style="color: #00abf1; font-weight: 900;" target="_blank" title="<?php echo esc_html( $giftlist->get_nombre() ); ?>" href="<?php echo esc_url( $url ); ?>">
                                                                    <?php echo esc_html( $giftlist->get_nombre() ); ?>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="font-weight: bold; padding: 7px;">
                                                                Cumpleañero:
                                                            </td>
                                                            <td align="left" style="padding: 7px;">
                                                                <?php echo esc_html( $giftlist->get_cumpleanero() ); ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="font-weight: bold; padding: 7px;">
                                                                Fecha de cumpleaños:
                                                            </td>
                                                            <td align="left" style="padding: 7px;">
                                                                <?php echo esc_html( $giftlist->get_fecha_cumpleanos()->format( get_option( 'date_format' ) ) ); ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" align="left" style="font-weight: bold; padding: 7px; color: #00abf1;">
                                                                <?php esc_html_e( 'Dirección de entrega', 'woocommerce-gift-ideas' ); ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="190px" align="left"
                                                                style="font-weight: bold; padding: 7px;">Calle / Av. / Jr.
                                                            </td>
                                                            <td align="left" style="padding: 7px;">
                                                                <?php echo $obj->{'address_1'} . " - " . $obj->{'address_1'}; ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="font-weight: bold; padding: 7px;">
                                                                Departamento:
                                                            </td>
                                                            <td align="left" style="padding: 7px;">
                                                                <?php
                                                                $namesdepartamentos = $wpdb->get_results( 'SELECT departamento FROM `spanktg_ubigeo_departamento` WHERE `idDepa` = '.$obj->{'departamento'}, OBJECT );
                                                                foreach ($namesdepartamentos as $namedepartamento){
                                                                    $departamentoencontrado = $namedepartamento->departamento;
                                                                    break;
                                                                }
                                                                ?>
                                                                <?php echo $departamentoencontrado; ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="font-weight: bold; padding: 7px;">
                                                                Provincia:
                                                            </td>
                                                            <td align="left" style="padding: 7px;">
                                                                <?php
                                                                $namesprovincias = $wpdb->get_results( 'SELECT provincia FROM `spanktg_ubigeo_provincia` WHERE `idProv` = '.$obj->{'provincia'}, OBJECT );
                                                                foreach ($namesprovincias as $nameprovincia){
                                                                    $provinciaencontrado = $nameprovincia->provincia;
                                                                    break;
                                                                }
                                                                ?>
                                                                <?php echo $provinciaencontrado; ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="font-weight: bold; padding: 7px;">
                                                                Distrito:
                                                            </td>
                                                            <td align="left" style="padding: 7px;">
                                                                <?php
                                                                $namesdistritos = $wpdb->get_results( 'SELECT distrito FROM `spanktg_ubigeo_distrito` WHERE `idDist` = '.$obj->{'distrito'}, OBJECT );
                                                                foreach ($namesdistritos as $namedistrito){
                                                                    $distritoencontrado = $namedistrito->distrito;
                                                                    break;
                                                                }
                                                                ?>
                                                                <?php echo $distritoencontrado; ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="font-weight: bold; padding: 7px;">
                                                                Referencia:
                                                            </td>
                                                            <td align="left" style="padding: 7px;">
                                                                <?php echo $obj->{'referencia'}; ?>
                                                            </td>
                                                        </tr>

                                                    </table>

                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>