<?php
/**
 * The Template for displaying a giftlist.
 *
 * @version 1.0.0
 * @package Woocommerce_Gift_Ideas/templates
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/** Lista de regalos. @var Woocommerce_Gift_Ideas_Giftlist $giftlist */

/** Listado de productos. @var WC_Product[] $productos */
?>
<div class="wc-gift-ideas-giftlist woocommerce wc-gift-ideas-giftlist-clear">
	<?php do_action( 'wc_gift_ideas_before_giftlist', $giftlist ); ?>
	<?php
	if ( function_exists( 'wc_print_notices' ) ) {
		wc_print_notices();
	}
	?>
    <div class="pad-vert-100 text-center" style="padding-top: 50px;padding-bottom: 50px;">
        <?php if ( get_current_user_id() === (int) $giftlist->post_author ) : ?>
            <h2 class="celeste">Tu lista de regalo: <?php echo esc_html( $giftlist->get_nombre() ); ?></h2>
            <p>Elimina o agrega regalos a tu lista <br> ó <br>Comparte esta lista con tus invitados</p>

            <div class="box-listacreada d-inline-block mt-0 mb-3">
                <small>Comparte con tus invitados este código:</small>
                <h2 class="celeste"><?php echo esc_html( $giftlist->get_codigo() ); ?></h2>
                <small>para que puedan acceder a tu lista.</small>
            </div>

            <p class="return-to-shop tabla-personalizada">
                <a class="btn d-block d-sm-inline-block btn-danger wc-backward"
                href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>">
                    <?php esc_html_e( 'Regresar a la tienda', 'woocommerce-gift-ideas' ); ?>
                </a>
                    <a class="btn d-block d-sm-inline-block btn-primary wc-backward"
                    href="<?php echo esc_url( wc_gift_ideas_get_giftlist_page_url() ); ?>">
                        <?php esc_html_e( 'Regresar al listado', 'woocommerce-gift-ideas' ); ?>
                    </a>
                    <a class="btn btn-primary wc-gift-ideas-giftlist-share-whatsapp"
                    href="whatsapp://send?text=<?php echo rawurlencode( esc_url( wc_gift_ideas_get_giftlist_page_url( $giftlist->get_codigo() ) ) ); ?>"
                    data-giftlist-id="<?php echo esc_attr( $giftlist->get_id() ); ?>"
                    data-action="share/whatsapp/share"
                    data-href="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>"
                    data-href-action="share_giftlist_whatsapp">
                        <i class="icon-whatsapp"></i>
                    </a>
                    <a class="btn btn-primary wc-gift-ideas-giftlist-share-email"
                    data-giftlist-id="<?php echo esc_attr( $giftlist->get_id() ); ?>"
                    data-action="share_giftlist"
                    href="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>">
                        <i class="icon-mail"></i>
                    </a>
            </p>
        <?php else: ?>
            <h1>Bienvenido a la lista de regalos:</h1>
            <h2 class="celeste"> <?php echo esc_html( $giftlist->get_nombre() ); ?></h2>
            <h2>Creada por:</h2>
            <h2 class="celeste"> <?php echo esc_html( $giftlist->get_cumpleanero() ); ?></h2>
            <?php if (!empty($giftlist->get_fecha_cumpleanos())) { ?>
                <h2>Fecha:</h2>
                <h2> <?php echo $giftlist->get_fecha_cumpleanos()->format( get_option( 'date_format' ) ) ?></h2>
            <?php } ?>

            <!-- <div class="box-listacreada d-inline-block mt-0 mb-3">
                <small>Comparte con tus amigos este código:</small>
                <h2 class="celeste"><?php echo esc_html( $giftlist->get_codigo() ); ?></h2>
                <small>para que puedan acceder a esta lista.</small>
            </div> -->

        <?php endif; ?>
    </div>

	<!--<div class="row invisible">
		<div class="col-md-12">
			<?php echo esc_html( $giftlist->get_cumpleanero() ); ?>
		</div>
		<div class="col-md-12 ">
			<?php echo esc_html( $giftlist->get_fecha_cumpleanos()->format( get_option( 'date_format' ) ) ); ?>
		</div>
		<?php if ( ! empty( $direccion = $giftlist->get_direccion_entrega() ) ) : ?>
			<div class="col-md-12 ">
				<div class="row">
					<div class="col-md-12">
						<strong><?php esc_html_e( 'Dirección de entrega', 'woocommerce-gift-ideas' ); ?></strong></div>
					<div class="col-md-12"><?php echo WC()->countries->get_formatted_address( $direccion ); ?></div>
				</div>
			</div>
		<?php endif; ?>
	</div>-->


	<?php do_action( 'wc_gift_ideas_before_giftlist_table', $giftlist ); ?>
	
    <div class="row">
        <div class="col-12">
            <div class="products tienda">
                <?php
                // Logica para traer los datos de los productos y almacenar sus cantidades en la lista de regalos
                global $product;
                global $post;
                global $idlistaregalos;
                global $cantidadproducto;
                global $cantidad;
                global $gl;
                $cantidadproducto = 0;
                $gl = $giftlist;
                $idlistaregalos = $giftlist->get_id();
                $post_stored = $post;
                
                
                $cantidades_producto = $gl->get_cantidades_producto();
                
                // POST para actualizar la cantidad de productos a regalar en la lista de regalos
                if (isset($_POST['qty'])) {
                    foreach ( $cantidades_producto as $key => $value ) {
                        if ( $key == $_POST['product_id'] ) {
                            $cantidades_producto[$key] =  $_POST['qty'];
                        }
                    }
                    $gl->set_cantidades_producto($cantidades_producto);
                    $gl->updateCantidades(get_current_user_id());
                    
                }
                
                foreach ($productos as $producto) {
                    $cantidad = $producto->get_stock_quantity();
                    $post = get_post( $producto->get_id() );
                    foreach ($cantidades_producto as $key => $value) {
                        if ($key == $producto->get_id()) {
                            if ($cantidad < $value) {
                                $cantidades_producto[$key] =  $cantidad;
                                $gl->set_cantidades_producto($cantidades_producto);
                                
                                $cantidadproducto = $cantidad;
                            } else {
                                $cantidadproducto = $value;
                            }
                        }
                    }

                    $product = $producto; 
                    
                    echo wc_get_template_part( 'content', 'product' );
                }
                $gl->updateCantidades(get_current_user_id());

                $post = $post_stored;
                ?>
            </div>
        </div>
    </div>

	<?php do_action( 'wc_gift_ideas_after_giftlist', $giftlist ); ?>

	<div class="tinv-lists-nav wc-gift-ideas-giftlist-clear">
		<?php do_action( 'wc_gift_ideas_pagenation_giftlist', $pagina, $n_paginas, $n_total_productos ); ?>
	</div>
    <?php if ( get_current_user_id() !== (int) $giftlist->post_author ) : ?>
    <div class="row">
        <div class="col-12 text-center">
            <a class="btn d-block d-sm-inline-block btn-primary wc-backward" href="/tienda">
                <?php esc_html_e( 'Buscar más regalos', 'woocommerce-gift-ideas' ); ?>
            </a>
        </div>
    </div>
    <?php endif; ?>
    <br>
    <br>
    <br>
    <br>
</div>
