<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see           https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package       WooCommerce/Templates
 * @version       2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
get_header( 'shop' );
?>
    <aside class="">
        <?php
        if ( wpmd_is_device() ) :
        $etapa      = WC()->session->get( 'custom_filter_etapa', 2 );
        $genero     = WC()->session->get( 'custom_filter_genero', null );
        $edad       = WC()->session->get( 'custom_filter_edad', null );
        //$edadaux    = WC()->session->get( 'custom_filter_edad', null );
        $precio     = WC()->session->get( 'custom_filter_precio', null );
        //$etapaview  = WC()->session->get( 'custom_filter_precio', null );

        global $wp_query;

        //var_dump($wp_query);
        $golbalgenero = $wp_query->query['genero'];
        ?>
        <?php if(empty($golbalgenero)){ ?>
            <form role="search" method="post" action="<?php home_url( '/' ); ?>" id="formpasogenero">
            <div class="full-borde">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <input type="hidden" name="s" value="">
                            <input type="hidden" name="post_type" value="product">
                            <input type="hidden" name="movil" value="1">
                            <input type="hidden" name="etapa" value="1">
                            <input type="hidden" name="modoview" value="">
                            <?php do_action( 'filtro_genero' ); ?>
                        </div>
                    </div>
                </div>
                <div class="bordetop">
                    <div class="container">
                        <div class="row fila-4px">
                            <div class="col-5 form-inline herramientas-desktop">
                                <?php if ( is_search() ): ?>
                                    <small>
                                        <?php esc_html_e( 'Buscar por > ', 'b4st' ); ?>
                                        <span class="celeste"><?php the_search_query(); ?></span>
                                    </small>
                                <?php endif; ?>
                            </div>

                            <div class="col-7 justify-content-end form-inline herramientas-desktop">
                                <?php
                                do_action( 'woocommerce_before_shop_loop' );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        <?php }else{ ?>
        <form role="search" method="post" action="<?php home_url( '/' ); ?>" id="filtro-movil-home">
            <input type="hidden" name="s" value="">
            <input type="hidden" name="post_type" value="product">
            <input type="hidden" name="movil" value="1">
            <input type="hidden" name="modoview" value="">
            <input type="hidden" name="etapa" value="<?php echo esc_attr( $etapa ); ?>">

            <div class="full-borde filtro-movil">
                <div class="container">
                    <div class="row">
                        <div class="col-12 form-inline herramientas-desktop">
                            <?php do_action( 'select_genero', $edad, $precio ); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="full-borde">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <?php if ( 2 === ( (int) $etapa ) ) : ?>
                                <?php do_action( 'filtro_edad' ); ?>
                            <?php elseif(3 === ( (int) $etapa )): ?>
                                <?php do_action( 'filtro_precios', $precio ); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="full-borde">
                <div class="container">
                    <div class="row fila-4px">
                        <div class="col-5 form-inline herramientas-desktop">
                            <?php if ( is_search() ): ?>
                                <small>
                                    <?php esc_html_e( 'Buscar por > ', 'b4st' ); ?>
                                    <span class="celeste"><?php the_search_query(); ?></span>
                                </small>
                            <?php endif; ?>
                        </div>

                        <div class="col-7 justify-content-end form-inline herramientas-desktop">
                            <?php
                            do_action( 'woocommerce_before_shop_loop' );
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>



            <?php else: ?>
                <div class="miga full-borde">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <?php do_action( 'woocommerce_avisos' ); ?>
                            </div>

                            <div class="col-lg-4">
                                <?php
                                if ( wpmd_is_notdevice() ) {
                                    if ( function_exists( 'yoast_breadcrumb' ) ) {
                                        yoast_breadcrumb( '<span id="breadcrumbs">', '</span>' );
                                    }
                                }
                                ?>
                            </div>

                            <div class="col-lg-8 form-inline align-items-center justify-content-end herramientas-desktop">
                                <?php
                                /**
                                 * woocommerce_before_shop_loop hook.
                                 *
                                 * @hooked wc_print_notices - 10
                                 * @hooked woocommerce_result_count - 20
                                 * @hooked woocommerce_catalog_ordering - 30
                                 */
                                do_action( 'woocommerce_before_shop_loop' );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
    </aside>

    <div id="container" class="container">

        <?php
        /**
         * woocommerce_before_main_content hook.
         *
         * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
         * @hooked woocommerce_breadcrumb - 20
         * @hooked WC_Structured_Data::generate_website_data() - 30
         */
        do_action( 'woocommerce_before_main_content' );
        ?>

        <header class="woocommerce-products-header">
            <?php /*if ( apply_filters( 'woocommerce_show_page_title', true ) ) : */ ?><!--
            <h1 class="woocommerce-products-header__title page-title"><?php /*woocommerce_page_title(); */ ?></h1>
		--><?php /*endif; */ ?>

            <?php
            /**
             * woocommerce_archive_description hook.
             *
             * @hooked woocommerce_taxonomy_archive_description - 10
             * @hooked woocommerce_product_archive_description - 10
             */
            do_action( 'woocommerce_archive_description' );
            ?>
        </header>


        <div class="row">
            <?php if ( wpmd_is_notdevice() ): ?>
                <div class="col-12 col-1-5">
                    <div class="widget">
                        <h4><?php _e( 'FILTRAR POR:', 'b4st' ); ?></h4>
                    </div>

                    <?php echo do_shortcode( '[woof]' ); ?>
                </div>
            <?php endif; ?>

            <div class="col-12 <?php echo ( wpmd_is_notdevice() ) ? 'col-4-5' : ''; ?>">
                <?php if ( have_posts() ) : ?>

                    <?php woocommerce_product_loop_start(); ?>

                    <?php woocommerce_product_subcategories(); ?>

                    <?php while ( have_posts() ) : the_post(); ?>

                        <?php
                        /**
                         * woocommerce_shop_loop hook.
                         *
                         * @hooked WC_Structured_Data::generate_product_data() - 10
                         */
                        do_action( 'woocommerce_shop_loop' );
                        ?>

                        <?php wc_get_template_part( 'content', 'product' ); ?>

                    <?php endwhile; // end of the loop. ?>

                    <?php woocommerce_product_loop_end(); ?>

                    <?php
                    /**
                     * woocommerce_after_shop_loop hook.
                     *
                     * @hooked woocommerce_pagination - 10
                     */
                    do_action( 'woocommerce_after_shop_loop' );
                    ?>

                <?php elseif ( ! woocommerce_product_subcategories( [
                    'before' => woocommerce_product_loop_start( false ),
                    'after'  => woocommerce_product_loop_end( false ),
                ] )
                ) : ?>

                    <?php
                    /**
                     * woocommerce_no_products_found hook.
                     *
                     * @hooked wc_no_products_found - 10
                     */
                    do_action( 'woocommerce_no_products_found' );
                    ?>

                <?php endif; ?>
            </div>
        </div>

        <?php
        /**
         * woocommerce_after_main_content hook.
         *
         * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
         */
        do_action( 'woocommerce_after_main_content' );
        ?>

    </div>


<?php get_footer( 'shop' ); ?>