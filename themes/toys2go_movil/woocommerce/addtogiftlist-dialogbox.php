<?php
/**
 * The Template for displaying dialog to add product to a giftlist.
 *
 * @version 1.0.0
 * @package Woocommerce_Gift_Ideas/templates
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/** Listas de regalos. @var Woocommerce_Gift_Ideas_Giftlist[] $giftlists */
?>
<div class="wc-gift-ideas-add-to-wishlist modal-center-vert wc-gift-ideas-modal modal-dialog" role="document">
	<div class="wc-gift-ideas-table modal-content custom-modal">
		<div class="modal-header">
            <h4 class="celeste"><?php esc_html_e( 'Añadir a lista de regalos', 'woocommerce-gift-ideas' ); ?> <i class="icon-regalo"></i></h4>
            <i data-dismiss="modal" class="icon-cerrar cerrar"></i>
		</div>
		<div class="wc-gift-ideas-cell modal-body">
			<?php if ( isset( $notice ) && ! empty( $notice ) ) : ?>
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="<?php esc_html_e( 'Cerrar', 'woocommerce-gift-ideas' ); ?>">
						<span aria-hidden="true">&times;</span>
					</button>
					<?php echo esc_html( $notice ); ?>
				</div>
			<?php endif; ?>
			<?php echo wc_get_template_part( 'content', 'popupaddproduct' ); ?>
		</div>
        <div class="modal-footer">
            <div class="wc-gift-ideas-buttons-group w-100 wc-gift-ideas-wishlist-clear text-center">
                <a href="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>"
                   data-action="create_giftlist"
                   class="wc-gift-ideas-button-view wc-gift-ideas-create-giftlist">
                    <i class="fa fa-heart-o"></i>
			        <?php esc_html_e( 'Crear una lista', 'woocommerce-gift-ideas' ); ?>
                </a>
		        <?php if ( isset( $giftlists ) && ! empty( $giftlists ) ) : ?>
                    <label for="wc-gift-ideas-giftlist-id"><?php esc_html_e( 'Listas disponibles', 'woocommerce-gift-ideas' ); ?></label>
                    <select class="form-control-sm d-block d-sm-inline mb-2" name="wc-gift-ideas-giftlist" id="wc-gift-ideas-giftlist-id">
				        <?php foreach ( $giftlists as $giftlist ) : ?>
                            <option value="<?php echo esc_attr( $giftlist->get_id() ); ?>"><?php echo esc_html( $giftlist->get_nombre() ); ?></option>
				        <?php endforeach; ?>
                    </select>
					<style>
						#wc-gift-ideas-giftlist-product-quantity-id {
							width: 50px;
							color: #00abf1;
							font-size: 16px;
							position: relative;
							float: none;
							height: 100%;
							border-color: #ccc !important;
    						border-style: solid;
							padding: 0;
							margin: 0;
							text-align: center;
							border: 1px solid #bbb3b9;
							border-right: 0;
							border-radius: 2px 0 0 2px;
							border-width: 1px 0;
							font-weight: 300;
						}
						.minus {
							position: relative;
							float: none;
							height: 100%;
							border-radius: 5px 0 0 5px;
							background-clip: padding-box;
							width: 35px;
							font-size: 30px;
							line-height: 1;
							border-color: #ccc !important;
							background-color: transparent;
							border-width: 1px;
							border-style: solid;
							cursor: pointer;
							font-weight: 300;
							color: #515151;
						}
						.plus {
							color: #515151;
							position: relative;
							float: none;
							height: 100%;
							border-radius: 0 5px 5px 0;
							background-clip: padding-box;
							width: 35px;
							font-size: 30px;
							line-height: 1;
							border-color: #ccc !important;
							border-width: 1px;
							border-style: solid;
							background-color: transparent;
							cursor: pointer;
							font-weight: 300;
						}
					</style>  
					<label for="wc-gift-ideas-giftlist-product-quantity-id"><?php esc_html_e( 'Cantidad', 'woocommerce-gift-ideas' ); ?></label>
					<div class="quantity buttons_added" style="height: 48px;display: inline-flex; margin-bottom: 20px">
						<input type="button" value="-" class="minus">
						<input type="number" class="input-text qty text" step="1" max="<?php echo $stock; ?>" min="1" value="1" title="Cantidad" pattern="[0-9]*" inputmode="numeric" name="wc-gift-ideas-giftlist-product-quantity" id="wc-gift-ideas-giftlist-product-quantity-id"> 
						<input type="button" value="+" class="plus">
					</div>
					
                    <a href="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>"
                       data-action="add_to_giftlist"
                       class="btn btn-lg btn-block btn-danger wc-gift-ideas-button-view wc-gift-ideas-addto-giftlist-final">
                        <i class="fa fa-heart-o"></i>
				        <?php esc_html_e( 'Añadir', 'woocommerce-gift-ideas' ); ?>
                    </a>

		        <?php else : ?>
                    <p><?php esc_attr_e( 'No hay listas de regalos disponibles.', 'woocommerce-gift-ideas' ); ?></p>
		        <?php endif; ?>

            </div>
        </div>
	</div>
</div>
