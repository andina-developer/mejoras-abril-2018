<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;
global $cantidadproducto;
global $cantidad;
global $gl;
global $idlistaregalos;
global $wpdb;

if (!is_page( 'giftlist' )) {
    // Ensure visibility
    if ( empty( $product ) || ! $product->is_visible() ) {
        return;
    }
}


$ultimos    = '';
$umbral     = get_option( 'woocommerce_notify_low_stock_amount' );

if ( $cantidad <= $umbral )
    $ultimos = 'ultimos';

?>
<div <?php post_class( $ultimos ); ?>>

    <div class="ctn-imagen">
        <?php
        /**
         * woocommerce_before_shop_loop_item hook.
         *
         * @hooked woocommerce_template_loop_product_link_open - 10
         */
        if ( !is_page( 'giftlist' ) ) {
            do_action( 'woocommerce_before_shop_loop_item' );
        } 

        /**
         * woocommerce_before_shop_loop_item_title hook.
         *
         * @hooked woocommerce_show_product_loop_sale_flash - 10
         * @hooked woocommerce_template_loop_product_thumbnail - 10
         */
        do_action( 'woocommerce_before_shop_loop_item_title' );

        /**
         * woocommerce_after_shop_loop_item hook.
         *
         * @hooked woocommerce_template_loop_product_link_close - 5
         */
        if ( is_page( 'giftlist' ) ) { 
            if (get_current_user_id() == (int) $gl->post_author ) {
                do_action( 'woocommerce_after_shop_loop_item' );
            }    
        } else {
            do_action( 'woocommerce_after_shop_loop_item' );
        }

        /**
         * @hooked woocommerce_template_loop_add_to_cart - 10
         */
        do_action( 'post_imagen_producto' );
        ?>
    </div>

    <?php
    /**
     * woocommerce_shop_loop_item_title hook.
     *
     * @hooked woocommerce_template_loop_product_title - 10
     */
    do_action( 'woocommerce_shop_loop_item_title' );
    
    /**
     * woocommerce_after_shop_loop_item_title hook.
     *
     * @hooked woocommerce_template_loop_rating - 5
     * @hooked woocommerce_template_loop_price - 10 
     */
    do_action( 'woocommerce_after_shop_loop_item_title' );

    // Cantidades de producto por lista de regalo y boton de actualizacion de cantidades y para invitado, cantidad y boton de carrito de compras.
    
    if ( is_page( 'giftlist' ) && $cantidadproducto > 0) : 
        if ( is_page( 'giftlist' ) && get_current_user_id() == (int) $gl->post_author ) { ?>
        
            <form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST">
                <input type="hidden" name="product_id" value="<?php echo $product->get_id() ?>">
                Se desea: <div class="quantity buttons_added" style="height: 35px;display: inline-flex; margin-bottom: 20px; margin-right: 5px">
                            <input type="button" value="-" class="minus">
                            <input type="number" readonly data-product_id="<?php echo $product->get_id() ?>" class="input-text qty text" step="1" name="qty"
                            value="<?php echo $cantidadproducto ?>" min="1" max="<?php echo $cantidad ?>" value="1" title="Cantidad" pattern="[0-9]*" inputmode="numeric"> 
                            <input type="button" value="+" class="plus">
                        </div><br>
                        <input type="submit" value="Actualizar Cantidad">
            </form>
        <?php } else {
            echo 'Se desea: '.$cantidadproducto.'<br>';
        }
        ?>
        <?php if ( get_current_user_id() !== (int) $gl->post_author ): ?>
        
            <style>
                .onlysee-guest .wc-gift-ideas-producto-remove, .ctn-imagen .add-cart-qty {
                    display: none !important; 
                }
            </style>
            <div class="onlysee-guest">
                <?php 
                    $class = 'button product_type_simple add_to_cart_button ajax_add_to_cart btn btn-primary';
                    echo '<div class="quantity buttons_added" style="height: 35px;display: inline-flex; margin-bottom: 20px; margin-right: 5px">
                            <input type="button" value="-" class="minus">
                            <input type="number" readonly data-product_id="'.$product->get_id().'" class="input-text qty text" step="1" max="'.$cantidad.'" min="1" value="1" title="Cantidad" pattern="[0-9]*" inputmode="numeric"> 
                            <input type="button" value="+" class="plus">
                        </div>';

                    echo apply_filters( 'woocommerce_loop_add_to_cart_link',
                        sprintf( '<a rel="nofollow" id="add_cart_'.$product->get_id().'" href="%s" data-baselink="%s" data-referalid="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s">%s</a>',
                            esc_url( $product->add_to_cart_url().'&referalid='.$idlistaregalos ),
                            esc_attr( $product->add_to_cart_url().'&referalid='.$idlistaregalos ),
                            esc_attr( $idlistaregalos ),
                            esc_attr( 1 ),
                            esc_attr( $product->get_id() ),
                            esc_attr( $product->get_sku() ),
                            esc_attr( isset( $class ) ? $class : 'button' ),
                            esc_html( $product->add_to_cart_text() )
                        ),
                    $product );
                ?>
            </div>
            <script>
                $('.qty').change(function (e) { 
                    $('#add_cart_'+$(this).data('product_id')).removeData( "quantity" );
                    document.getElementById('add_cart_'+$(this).data('product_id')).setAttribute('data-quantity', $(this).val());
                    // $('#add_cart_'+$(this).data('product_id')).data('quantity', $(this).val());
                    console.log($('#add_cart_'+$(this).data('product_id')).data('quantity'));
                });
            </script>
        <?php endif; ?>
    <?php else: ?>
        <?php if ( is_page( 'giftlist' ) ) :  ?>
            <h3 class="text-center verde xoo-cp-added">TODOS COMPRADOS</h3>
        <?php endif; ?>
    <?php endif; ?>
</div>

