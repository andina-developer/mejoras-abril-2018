<?php
/**
 * Bloque "Suscripcion"
 */
if ( ! is_page_template( 'paginas/page-preguntas-frecuentes.php' )
        && ! is_page_template( 'paginas/page-quienes-somos.php' )
        && ! is_page_template( 'paginas/page-contactanos.php' )
        && ! is_page_template( 'paginas/page-cuenta-creada.php' )
        && ! is_page( 'carrito' )
        && ! is_checkout()
        // && ! is_page_template( 'paginas/page-registro.php' )
        && ! is_404() ) :
	echo do_shortcode( '[suscripcion]' );
endif;
?>

<footer class="main bg-celeste">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <h5 class="blanco"><?php _e( 'Llámanos: ', 'b4st' ); ?></h5>
                <ul class="llamanos px-0 mb-3 mb-lg-5">
					<?php
					global $numero, $whatsapp;
					if ( $numero ) :
					    if ( wpmd_is_phone() ) :
						    echo '<li class="list-inline-item blanco"><a href="tel:' . $numero . '" class="blanco text-underline">' . $numero . '</a></li>';
					    else:
                            echo '<li class="list-inline-item blanco text-underline">' . $numero . '</li>';
                        endif;
					endif;
					if ( $whatsapp ) {
						echo '<li class="list-inline-item blanco"><i class="icon-whatsapp"></i> <a href="https://api.whatsapp.com/send?phone=+51' . $whatsapp . '"  class="blanco"> ' . $whatsapp . '</a></li>';
					}
					?>
                </ul>
            </div>

            <div class="col-6 col-lg-4 col-xl-3 text-right text-md-center">
                <h4 class="blanco mb-1 mb-lg-3"><?php _e( 'Conócenos', 'b4st' ); ?></h4>
				<?php wp_menu_footer_1(); ?>
            </div>

            <div class="col-6 col-lg-4 col-xl-3 text-left text-md-center">
                <h4 class="blanco mb-1 mb-lg-3"><?php _e( 'Servicio al Cliente', 'b4st' ); ?></h4>
				<?php wp_menu_footer_2(); ?>
            </div>

            <div class="col-12 col-lg-4 col-xl-3 text-center">
                <h4 class="blanco mb-2 mb-lg-3"><?php _e( 'Redes Sociales', 'b4st' ); ?></h4>
				<?php get_template_part( 'includes/redes', 'sociales' ); ?>
            </div>
        </div>

        <div class="row">
            <div class="sub col text-center">
                <span class="blanco"><small>&copy; <?php echo date( 'Y' ); ?> <a
                    href="<?php echo home_url( '/' ); ?>"><?php bloginfo( 'name' ); ?></a></small></span>
            </div>
        </div>
    </div>
</footer>



<a href="#0" class="cd-top">
    <i class="icon-flecha-arribla"></i>
    <span>Subir</span>
</a>

<?php echo do_shortcode( '[login_modal]' ); ?>

<?php echo do_shortcode( '[suscripcion_modal]' ); ?>

<!--<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js'></script>-->

<?php wp_footer(); ?>

<?php get_template_part( 'includes/js' ); ?>
<input type="hidden" id="base_url" value="<?php echo get_site_url(); ?>">
</body>
</html>
