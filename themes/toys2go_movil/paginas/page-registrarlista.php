<?php
/**
 * Template Name: Registrar lista de regalos
 */
?>
<?php get_header(); ?>
<div class="title-add-regalos no-border-top">
	<div class="container">
		<div class="row justify-content-md-center">
			<div class="col-12 col-md-9">
				<h2><i class="icon-camion-sello rojo"></i> Crea tu lista:</h2>
			</div>
		</div>
	</div>
</div>
<div class="container form-add-regalos pad-vert-20">
	<div class="row justify-content-md-center">
		<div class="col-md-8">
			<div class="form-group">
				<label for="">Nombre de la lista (*)</label>
				<input type="text">
			</div>
			<div class="form-group">
				<label for="">Nombre (*)</label>
				<input type="text">
			</div>
			<div class="form-group">
				<label for="">Apellido (*)</label>
				<input type="text">
			</div>
			<div class="form-group">
				<label for="">Correo electrónico (*)</label>
				<input type="text">
			</div>
			<div class="form-group">
                <ul class="list-unstyled custom-radios-regalos mb-0 radio-button">
                    <li class="value-nina">
                        <input id="nina" type="radio" name="genero" value="19">
                        <label for="nina" class="label rojo">
                            Niña
                        </label>
                    </li>
                    <li class="value-nino">
                        <input id="nino" type="radio" name="genero" value="18">
                        <label for="nino" class="label text-celeste">Niño</label>
                    </li>
                    <li>
                        <input id="ambos" type="radio" name="genero" value="19,18">
                        <label for="ambos" class="label text-amarillo">Ver todos</label>
                    </li>
                </ul>
			</div>

		</div>
	</div>
</div>
<div class="title-add-regalos">
	<div class="container">
		<div class="row justify-content-md-center">
			<div class="col-12 col-md-9">
				<h2><i class="icon-camion-sello rojo"></i> <?php _e( 'Ingresa tu dirección de envío:', 'b4st' ); ?></h2>
			</div>
		</div>
	</div>
</div>

<div class="container form-add-regalos pad-vert-20">
    <div class="row justify-content-md-center">
        <div class="col-md-8">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-8 col-7">
                        <label for="">Calle / Av. / Jr. (*)</label>
                        <input type="text">
                    </div>
                    <div class="col-sm-4 col-5">
                        <label for="">Número (*)</label>
                        <input type="text">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="">Distrito (*)</label>
                <select name="" id="">
                    <option value=""></option>
                </select>
            </div>
            <div class="form-group">
                <label for="">Ciudad (*)</label>
                <select name="" id="">
                    <option value=""></option>
                </select>
            </div>
            <div class="form-group">
                <label for="">Referencia (*)</label>
                <input type="text">
            </div>
            <div class="form-group">
                <div class="row justify-content-center">
                    <div class="col-10 col-sm-6 pad-vert-20">
                        <button type="button" class="btn btn-lg btn-block btn-primary">Crear lista</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
