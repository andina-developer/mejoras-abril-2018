<?php
/**
 * Template Name: Duplicados
 */
?>

<?php
global $wpdb;
$tablename = $wpdb->prefix."posts";



    //Checking to see if the user email already exists
    $datum = $wpdb->get_results("SELECT * FROM $tablename WHERE students_email = '".$email."'");

    //Print the $datum object to see how the count is stored in it.
    //I'm assuming the key is 'count'

    if($wpdb->num_rows > 0) {
        //Display duplicate entry error message and exit
        ?>
        <div class="wrap">
            <div class="error"><p>Student exsist!</p></div> <!-- wp class error for error notices --->
        </div>
        <?php
        //return or exit
    }

    //Now since the email is unique and valid, lets go ahead and enter it
    //assigning the new data to the table rows
    $newdata = array(
        'students_name'=>$name,
        'students_lastname'=>$surname,
        'students_email'=>$email,
        'students_date'=>current_time( 'mysql' ),
    );
    //inserting a record to the database
    $wpdb->insert(
        $tablename,
        $newdata
    );
    //displaying the success message when student is added
    ?>
    <div class="wrap">
        <div class="updated"><p>Student added!</p></div>
    </div>
<?php
?>