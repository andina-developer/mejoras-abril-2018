<?php
/**
 * Template Name: Quiénes Somos
 */
?>

<?php get_header(); ?>

	<section class="quienes-somos pagina-contenido">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12" role="main">
					<div class="page-header text-center">
						<?php the_title( '<h1 class="celeste">', '</h1>' ); ?>
					</div>
				</div>
			</div>

			<div class="row justify-content-center">
				<div class="col-12 col-lg-7">
					<?php the_content(); ?>

                    <figure>
                        <img src="<?php echo get_template_directory_uri(); ?>/theme/img/pinguino-quienes-somos.svg"
                             alt="<?php echo get_the_title(); ?>" class="img-fluid mx-auto d-block" width="211" height="248">
                    </figure>
				</div>
			</div>
		</div>
	</section>

<?php get_footer(); ?>