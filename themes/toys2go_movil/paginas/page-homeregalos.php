<?php
/**
 * Template Name: Home regalos
 */
?>
<?php get_header(); ?>
<div class="container pad-vert-100">
    <div class="row justify-content-md-center">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-6 text-center">
                    <a href="" class="btn-crea-lista btn btn-lg btn-block btn-custom-red">Crea una lista</a>
                    <a class="edit-list-gifts d-inline-block" href="">Edita tu lista</a>
                </div>
                <div class="col-md-6">
                    <a href="#" data-toggle="modal" data-target="#modalbuscarlista" class="btn-encuentra-lista btn btn-lg btn-block btn-primary">Encuentra una lista</a>
                    <div class="modal fade modal-buscar-lista" id="modalbuscarlista" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="">Código de lista</label>
                                        <input type="text" >
                                    </div>
                                    <p class="line-o text-center"><span>ó</span></p>
                                    <div class="form-group">
                                        <label for="">Nombre, apellido o nombre de lista</label>
                                        <input type="text">
                                    </div>
                                    <button type="button" class="btn btn-lg btn-block btn-primary">Buscar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
