<?php
/**
 * Template Name: Account - Panel de control
 */
?>

<?php get_header(); ?>
    <section class="pagina-contenido pad-vert-20">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-10">
                    <?php
                        if ( is_user_logged_in() ) {
                            do_action('action_myacoount_breadcrumb');
                        }
                    ?>
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>