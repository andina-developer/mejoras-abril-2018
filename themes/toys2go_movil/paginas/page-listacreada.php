<?php
/**
 * Template Name: Lista creada
 */
?>
<?php get_header(); ?>
<div class="container lista-creada pad-vert-35">
	<div class="row justify-content-md-center">
		<div class="col-12 col-md-6 col-lg-4 text-center">
            <?php the_content(); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
