<?php
/**
 * Template Name: Registro
 */
?>

<?php get_header(); ?>

    <aside class="">
        <div class="miga full-borde">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-lg-10" role="main">
                        <div class="page-header">
                            <h3 class="titulo-productos usuario mb-3">
                                <?php
                                if ( is_user_logged_in() ) :
                                    _e( 'Usted ya está registrado', 'b4st' );
                                else:
                                    _e( 'Ingresa tus datos para registrarte:', 'b4st' );
                                endif;
                                ?>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </aside>

    <section class="registro pagina-contenido">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-8">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>