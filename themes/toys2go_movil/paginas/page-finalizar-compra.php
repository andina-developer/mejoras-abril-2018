<?php
/**
 * Template Name: Finalizar compra
 */
?>

<?php get_header(); ?>

<?php
$classes = get_body_class();

if ( in_array( 'woocommerce-order-received', $classes ) ) :
    ?>
    <section class="cuenta-creada gracias-compra pagina-contenido relativo <?php echo ( wpmd_is_device() ) ? 'v-movil' : 'v-desktop'; ?>">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-md-12">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </section>
    <?php
else:
    ?>
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-12 mt-0 mt-md-3">
                <div id="" role="main">
                    <?php the_content(); ?>
                </div>
            </div>

        </div>
    </div>
<?php endif; ?>

<?php get_footer(); ?>