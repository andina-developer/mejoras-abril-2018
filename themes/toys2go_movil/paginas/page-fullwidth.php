<?php
/**
 * Template Name: Ancho completo
 */
?>

<?php get_header(); ?>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <?php the_content(); ?>
            </div>
        </div>
    </div>

<?php get_footer(); ?>