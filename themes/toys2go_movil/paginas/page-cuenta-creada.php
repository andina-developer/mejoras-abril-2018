<?php
/**
 * Template Name: Cuenta creada
 */
?>

<?php get_header(); ?>

    <section class="cuenta-creada pagina-contenido relativo <?php echo ( wpmd_is_device() ) ? 'v-movil' : 'v-desktop'; ?>">
        <div class="container">

            <div class="row justify-content-center">
                <div class="col-12" role="main">
                    <div class="page-header text-center">
                        <div class="min-width-280 bg-blanco d-inline-block">
                            <h1 class="celeste">¡Listo!</h1>
                            <p>Cuenta creada</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center">
                <div class="col-12 col-lg-7">
                    <?php the_content(); ?>

                    <a href="<?php echo get_permalink( wc_get_page_id( 'shop' ) ); ?>"
                    class="btn btn-xl d-block btn-primary mx-auto"><?php _e( 'Seguir comprando', 'b4st' ); ?></a>

                    <figure>
                        <img src="<?php echo get_template_directory_uri(); ?>/theme/img/pinguino-cuenta-creada.svg"
                             alt="<?php echo get_the_title(); ?>" class="img-fluid mx-auto d-block" width="211" height="248">
                    </figure>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>