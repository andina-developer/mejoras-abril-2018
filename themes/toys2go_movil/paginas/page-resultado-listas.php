<?php
/**
 * Template Name: Resultados lista de regalos
 */
?>
<?php get_header(); ?>
<div class="container pad-vert-100">
    <div class="row justify-content-md-center">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">                    
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
