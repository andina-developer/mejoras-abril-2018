<?php
/**
 * Template Name: Contáctanos
 */
?>

<?php get_header(); ?>

	<section class="contactanos pagina-contenido">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12" role="main">
					<div class="page-header text-center">
						<?php the_title( '<h1 class="celeste">', '</h1>' ); ?>
					</div>
				</div>
			</div>

			<div class="row justify-content-center">
				<div class="col-12 col-lg-5">
					<?php the_content(); ?>

					<?php if( get_field( 'numero_telefonico' ) || get_field( 'whatsapp' ) ): ?>
						<p>
							<span><?php _e( 'Call center:', 'b4st' ) ?></span>
							<?php the_field( 'numero_telefonico'  ); ?> /
							<?php the_field( 'whatsapp'  ); ?>
						</p>
					<?php endif; ?>

					<?php if( get_field( 'correo_de_contacto' ) ): ?>
						<p>
							<span><?php _e( 'Mail:', 'b4st' ) ?></span>
							<a href="mailto:<?php the_field( 'correo_de_contacto' ); ?>">
								<?php the_field( 'correo_de_contacto'  ); ?>
							</a>
						</p>
					<?php endif; ?>

					<?php if( get_field( 'direccion_principal' ) ): ?>
						<p>
							<span><?php _e( 'Dirección:', 'b4st' ) ?></span> <?php the_field( 'direccion_principal' ); ?>
						</p>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>

<?php get_footer(); ?>