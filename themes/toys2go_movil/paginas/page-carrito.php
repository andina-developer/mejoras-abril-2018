<?php
/**
 * Template Name: Carrito
 */
?>

<?php get_header(); ?>

	<div class="container">
		<div class="row">
			<div class="col-12">
				<?php the_content(); ?>
			</div>
		</div><!-- /.row -->
	</div><!-- /.container -->

<?php get_footer(); ?>