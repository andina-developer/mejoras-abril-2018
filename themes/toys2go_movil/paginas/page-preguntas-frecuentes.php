<?php
/**
 * Template Name: Preguntas Frecuentes
 */
?>

<?php get_header(); ?>

<section class="preguntas-frecuentes pagina-contenido">
	<div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-10" role="main">
				<div class="page-header text-center">
					<?php the_title( '<h1 class="celeste">', '</h1>' ); ?>
				</div>
			</div>
		</div>

		<div class="row justify-content-center">
			<div class="col-12 col-lg-10">
                <?php the_content(); ?>
				<?php if ( have_rows( 'preguntas_y_respuestas' ) ): ?>
					<div id="preguntasFrecuentes" class="p-frecuentes" data-children=".item-scroll">

						<?php
						$cnt = 0;
						while ( have_rows( 'preguntas_y_respuestas' ) ):
							the_row();
							?>
							<div class="item-scroll <?php echo ( $cnt == 0 ) ? 'activo' : '' ; ?>">
								<a data-toggle="collapse" data-parent="#preguntasFrecuentes" href="#exampleAccordion<?php echo $cnt; ?>"
								   aria-expanded="<?php echo ( $cnt == 0 ) ? 'true' : '' ; ?>"
								   aria-controls="exampleAccordion<?php echo $cnt; ?>">
									<?php the_sub_field( 'pregunta' ); ?>
								</a>

								<div id="exampleAccordion<?php echo $cnt; ?>" class="collapse <?php echo ( $cnt == 0 ) ? 'show' : '' ; ?>"
								     role="tabpanel">
                                    <?php the_sub_field( 'respuesta' ); ?>
								</div>
							</div>

							<?php
							$cnt++;
						endwhile;
						?>

					</div>
				<?php endif; ?>

			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>