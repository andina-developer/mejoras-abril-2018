<?php
/**
 * Permite la carga de imágenes en formato SVG por administrador
 *
 * @param $mimes
 *
 * @return mixed
 */
function cc_mime_types($mimes) {
	if ( is_admin() ) {
		$mimes['svg'] = 'image/svg+xml';
		return $mimes;
	}
}
/*add_filter('upload_mimes', 'cc_mime_types');

add_filter('wpcf7_form_elements', function($content) {
    $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);

    return $content;
});*/

/**
 * Alterna entre dos Walkers el menú principal, para móviles y desktop respectivamente
 *
 * @param $args
 *
 * @return array
 */
function modify_nav_menu_args( $args ) {
	if ( 'navbar' == $args['theme_location'] ) {
		if ( wpmd_is_device() ) {
			$args['walker'] = new bs4Navwalker();
		} else {
			$args['walker'] = new b4st_walker_nav_menu();
		}
	}

	return $args;
}

add_filter( 'wp_nav_menu_args', 'modify_nav_menu_args' );

/**
 * Agrega una clase al body cuando está activa el anuncio superior
 *
 * @param $classes
 *
 * @return array
 */
function my_body_classes( $classes ) {
	$classes[] = '';

	if ( strpos( $_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0' ) !== false ) {
		$classes[] = 'ie11';
	}

    if ( preg_match( '/(?i)msie [10]/', $_SERVER['HTTP_USER_AGENT'] ) ) {
        $classes[] = 'ie10';
    }

	if ( wpmd_is_safari_browser() ) {
		$classes[] = 'safari';
	}

	switch ( true ) {
		case wpmd_is_phone():
			$classes[] = 'phone';
		case wpmd_is_tablet():
			$classes[] = 'tablet';
		case wpmd_is_notdevice():
			$classes[] = 'desktop';
	}

	return $classes;
}

add_filter( 'body_class', 'my_body_classes' );

/**
 * Cambia en número de productos por página
 */
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );
function new_loop_shop_per_page( $cols ) {
	$cols = 16;

	return $cols;
}

/**
 * Cambia el número de columnas de acuerdo a cada dispositivo
 */
add_filter( 'loop_shop_columns', 'wc_product_columns_frontend' );
function wc_product_columns_frontend() {
	global $woocommerce;

	$columns = 4;

	if ( is_shop() ) :
		if ( wpmd_is_tablet() ) {
			$columns = 3;
		}
		if ( wpmd_is_phone() ) {
			$columns = 2;
		}
	endif;

	return $columns;
}

/**
 * Cambia el máximo de productos relacionados
 *
 * @param $args
 *
 * @return mixed
 */
function bbloomer_change_number_related_products( $args ) {
	$args['posts_per_page'] = 20;

	return $args;
}

add_filter( 'woocommerce_output_related_products_args', 'bbloomer_change_number_related_products' );

/**
 * Oculta el formulario de cupón en la vista de checkout.
 *
 * @param $enabled
 *
 * @return bool
 */
function hide_coupon_field_on_checkout( $enabled ) {
	if ( is_checkout() ) {
		$enabled = false;
	}

	return $enabled;
}
//add_filter( 'woocommerce_coupons_enabled', 'hide_coupon_field_on_checkout' );

/**
 * Remove the invalid part 0=1 of a query.
 *
 * @param string $where Where clause of current query.
 * @param WP_Query $query Current query.
 *
 * @return string
 */
function remove_0_1_where_clause( $where, WP_Query $query ) {
	if ( is_admin() ) {
		return $where;
	}

	$remove = (bool) WC()->session->get( 'remove_0_1_where_clause', false );
	if ( $remove ) {
		WC()->session->set( 'remove_0_1_where_clause', false );
		$where = preg_replace( '/AND\s+((0\s*=\s*1)|(1\s*=\s*0))/', '', $where );
	}

	return $where;
}

add_filter( 'posts_where_request', 'remove_0_1_where_clause', 10, 2 );

/**
 * Menu del usuario personalizado
 *
 * @param $menu_links
 * @return mixed
 */

function misha_remove_my_account_links( $menu_links ){
    unset( $menu_links['dashboard'] ); // Dashboard
    unset( $menu_links['payment-methods'] ); // Payment Methods
    unset( $menu_links['downloads'] ); // Downloads
    unset( $menu_links['customer-logout'] ); // Logout
    return $menu_links;
}
add_filter ( 'woocommerce_account_menu_items', 'misha_remove_my_account_links' );

/**
 * @param $orderby
 * @return mixed
 */
function my_woocommerce_catalog_orderby( $orderby ) {
    unset($orderby["rating"]);
    unset($orderby["popularity"]);
    unset($orderby["date"]);
    return $orderby;
}
add_filter( "woocommerce_catalog_orderby", "my_woocommerce_catalog_orderby", 20 );
