<?php
/**
 * Muestra el slider principal de la web. Versión móviles
 *
 * @param $atts
 */
function sliderPrincipalMovil( $atts ) {
	get_template_part( 'includes/movil/slider', 'principal' );
}

add_shortcode( 'sliderPrincipalMovil', 'sliderPrincipalMovil' );

/**
 * Muestra el slider principal de la web. Versión desktops
 *
 * @param $atts
 */
function sliderPrincipalDesktop( $atts ) {
	get_template_part( 'includes/desktop/slider', 'principal' );
}

add_shortcode( 'sliderPrincipalDesktop', 'sliderPrincipalDesktop' );


/**
 * Muestra el bloque "Comprar por" . Versión movil
 *
 * @param $atts
 */
function comprarPorMovil( $atts ) {
	get_template_part( 'includes/movil/comprar', 'por' );
}

add_shortcode( 'comprarPorMovil', 'comprarPorMovil' );

/**
 * Muestra el bloque "Comprar por" . Versión desktop
 *
 * @param $atts
 */
function comprarPorDesktop( $atts ) {
	get_template_part( 'includes/desktop/comprar', 'por' );
}

add_shortcode( 'comprarPorDesktop', 'comprarPorDesktop' );

/**
 * Muestra el bloque "Busca tu regalo" . Versión desktop
 *
 * @param $atts
 */
function filtroHome( $atts ) {
	get_template_part( 'includes/desktop/filtro', 'home' );
}

add_shortcode( 'filtroHome', 'filtroHome' );

/**
 * Muestra el enlace de para "Crear lista" versión móvil
 *
 * @param $atts
 */
function crearListaMovil( $atts ) {
	get_template_part( 'includes/movil/enlace', 'crearlista' );
}

add_shortcode( 'crearListaMovil', 'crearListaMovil' );

/**
 * Muestra el enlace de para "Crear lista" versión desktop
 *
 * @param $atts
 */
function crearListaDesktop( $atts ) {
	get_template_part( 'includes/desktop/enlace', 'crearlista' );
}

add_shortcode( 'crearListaDesktop', 'crearListaDesktop' );

/**
 * Productos Recomendados
 *
 * @param $atts
 *
 * @return string
 */
function productosRecomenados( $atts ) {
	extract(
		shortcode_atts( array(
			'cat_novedades' => 1,
		), $atts ) );
	global $post, $more;

	$args = array(
		'post_type'           => 'product',
		'post_status'         => 'publish',
		'orderby'             => 'rand',
		'ignore_sticky_posts' => 1,
		'posts_per_page'      => 12,
		'meta_key'            => 'recomendado',
		'meta_value'          => true

		/*'tax_query'             => array(
			array(
				'taxonomy'      => 'product_cat',
				'field'         => 'id', //This is optional, as it defaults to 'term_id'
				'terms'         => $cat_novedades,
				//'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
			)
		)*/
	);

	$productos = new WP_Query( $args );

	if ( $productos->have_posts() ) {
		$titulo = 'Recomendado';
		$clase  = array( 'like' );
		require( locate_template( 'includes/slickcarrusel-productos.php' ) );
	} else {
		$novedad = 'No hay productos en esta categoría';
	}

	return $novedad;

	wp_reset_postdata();
}

add_shortcode( 'productosRecomenados', 'productosRecomenados' );

/**
 * Productos Novedades
 *
 * @param $atts
 *
 * @return string
 */
function productosNovedades( $atts ) {
	global $post, $more;

	$args = array(
		'post_type'           => 'product',
		'post_status'         => 'publish',
		'orderby'             => 'rand',
		'ignore_sticky_posts' => 1,
		'posts_per_page'      => 12,
		'order'               => 'DESC',
		'orderby'             => 'ID'
	);

	$productos = new WP_Query( $args );

	if ( $productos->have_posts() ) {
		$titulo = 'Novedades Toys2Go';
		$clase  = array( 'estrella' );
		require( locate_template( 'includes/slickcarrusel-productos.php' ) );
	} else {
		$novedad = 'No hay productos en esta categoría';

		return $novedad;
	}

	return $novedad;

	wp_reset_postdata();
}

add_shortcode( 'productosNovedades', 'productosNovedades' );


/**
 * Carrusel de categorías de productos
 *
 * @param $atts
 */
function carruselCategorias( $atts ) {
	$args = array(
		'taxonomy'   => 'product_cat',
		'hide_empty' => false,
		'parent'     => 0    // Esta opción muestra solo las categorías superiores
	);

	$terms = get_terms( $args );

	if ( ! empty( $terms ) ) {
		$titulo = 'Categorías';
		require( locate_template( 'includes/slickcarrusel-categorias.php' ) );
	}
}

add_shortcode( 'carruselCategorias', 'carruselCategorias' );

/**
 * Carrusel de marcas
 *
 * @param $atts
 */
function carruselMarcas( $atts ) {
	$args = array(
		'taxonomy'   => 'marca',
		'hide_empty' => false,
		'parent'     => 0,    // Esta opción muestra solo las categorías superiores
	);

	$terms = get_terms( $args );

	if ( ! empty( $terms ) ) {
		require( locate_template( 'includes/slickcarrusel-marcas.php' ) );
	}
}

add_shortcode( 'carruselMarcas', 'carruselMarcas' );

/**
 * Bloque suscripcion
 *
 * @param $atts
 */
function suscripcion( $atts ) {
	require( locate_template( 'includes/suscripcion.php' ) );
}
add_shortcode( 'suscripcion', 'suscripcion' );

/**
 * Login de usuarios
 */
function login_modal() {
	require( locate_template( 'includes/popup-login.php' ) );
}
add_shortcode( 'login_modal', 'login_modal' );

/**
 * Login de usuarios
 */
function suscripcion_modal() {
    require( locate_template( 'includes/suscripcion-modal.php' ) );
}
add_shortcode( 'suscripcion_modal', 'suscripcion_modal' );

