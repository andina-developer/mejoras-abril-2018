<?php
// Favicon al header
function head_favicon() {
    ?>
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri() ?>/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri() ?>/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri() ?>/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_template_directory_uri() ?>/manifest.json">
    <link rel="mask-icon" href="<?php echo get_template_directory_uri() ?>/safari-pinned-tab.svg" color="#00abf1">
    <meta name="theme-color" content="#ffffff">
    <?php
}
add_action( 'wp_head', 'head_favicon' );

/**
 * Posts types personalizados
 */
function postsTypesPropios() {
    register_post_type( 'sliders', [
            'labels'      => [
                'name'          => _( 'Sliders' ),
                'singular_name' => _( 'Slider' ),
                'add_new_item'  => _( 'Añadir nueva Slider' ),
                'edit_item'     => _( 'Editar Slider' ),
                'add_new'       => _( 'Añadir nuevo Slider' ),
            ],
            'show_ui'     => true,
            'public'      => true,
            'has_archive' => true,
            'menu_icon'   => 'dashicons-images-alt2',
            'supports'    => [ 'title', 'editor', 'page-attributes', 'thumbnail' ],
        ]
    );

    register_post_type( 'descargas', [
            'labels'      => [
                'name'          => _( 'Descargas' ),
                'singular_name' => _( 'descarga' ),
                'add_new_item'  => _( 'Añadir nueva descarga' ),
                'edit_item'     => _( 'Editar descarga' ),
                'add_new'       => _( 'Añadir nueva descarga' ),
            ],
            'taxonomies'  => [ 'tema_descarga' ],
            'show_ui'     => true,
            'public'      => true,
            'has_archive' => true,
            'menu_icon'   => 'dashicons-download',
            'supports'    => [ 'title', 'editor', 'page-attributes', 'thumbnail', 'tipo', 'ceca' ],
        ]
    );
}
add_action( 'init', 'postsTypesPropios' );

/**
 * Taxonomías personalizadas
 */
function taxonomiasPropias() {
    register_taxonomy( 'genero', 'product', [
            'hierarchical' => true,
            'labels'       => [
                'name'          => _( 'Género' ),
                'singular_name' => _( 'Género' ),
                'edit_item'     => _( 'Editar Género' ),
                'update_item'   => _( 'Actualizar Género' ),
                'add_new_item'  => _( 'Agregar nuevo género' ),
            ],
            'show_ui'      => true,
            'rewrite'      => [ 'slug' => 'genero' ],
        ]
    );

    register_taxonomy( 'edad', 'product', [
            'hierarchical' => true,
            'labels'       => [
                'name'          => _( 'Edad' ),
                'singular_name' => _( 'Edad' ),
                'edit_item'     => _( 'Editar Edad' ),
                'update_item'   => _( 'Actualizar Edad' ),
                'add_new_item'  => _( 'Agregar nuevo Edad' ),
            ],
            'show_ui'      => true,
            'rewrite'      => [ 'slug' => 'edad' ],
        ]
    );

    register_taxonomy( 'marca', 'product', [
            'hierarchical' => true,
            'labels'       => [
                'name'          => _( 'Marcas' ),
                'singular_name' => _( 'Marca' ),
                'edit_item'     => _( 'Editar Marca' ),
                'update_item'   => _( 'Actualizar Marca' ),
                'add_new_item'  => _( 'Agregar nueva Marca' ),
            ],
            'show_ui'      => true,
            'rewrite'      => [ 'slug' => 'marca' ],
        ]
    );
}
add_action( 'init', 'taxonomiasPropias', 0 );

/**
 * Fuentes
 */
function fuentes() {
    ?>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
    <?php
}
add_action( 'wp_head', 'fuentes' );

/**
 *
 */
function wpdocs_after_setup_theme() {
    add_theme_support( 'html5', [ 'search-form' ] );
}

add_action( 'after_setup_theme', 'wpdocs_after_setup_theme' );

/**
 * Añade los menús disponible en la plantilla
 */
if ( ! function_exists( "wp_bootstrap_theme_support" ) ) {
    function wp_bootstrap_theme_support() {
        add_theme_support( 'menus' );

        register_nav_menus(
            [
                'navbar'     => 'Menú Superior',
                'categorias' => 'Menú Categorías',
                'footer_1'   => 'Menú Footer 1',
                'footer_2'   => 'Menú Footer 2',
                'footer_3'   => 'Menú Footer 3',
            ]
        );
    }
}
add_action( 'after_setup_theme', 'wp_bootstrap_theme_support' );

function wp_menu_superior_nav() {
    wp_nav_menu( [
        'theme_location' => 'navbar',
        'container'      => false,
        'menu_class'     => 'menuSuperior ml-auto ml-lg-0 mr-lg-0 w-100',
        'fallback_cb'    => '__return_false',
        'items_wrap'     => '<ul id="%1$s" class="navbar-nav mr-auto mt-2 mt-lg-0 %2$s">%3$s</ul>',
        'depth'          => 2,
    ] );
}

;

function wp_menu_categorias() {
    wp_nav_menu( [
        'theme_location' => 'categorias',
        'container'      => false,
        'menu_class'     => 'menuSuperior',
        'fallback_cb'    => '__return_false',
        'items_wrap'     => '<ul id="%1$s" class="navbar-nav bd-navbar-nav flex-row %2$s">%3$s</ul>',
        'depth'          => 2,
        'walker'         => new b4st_walker_nav_menu(),
    ] );
}

;

function wp_menu_footer_1() {
    wp_nav_menu( [
        'theme_location' => 'footer_1',
        'container'      => false,
        'menu_class'     => 'menuFooter',
        'fallback_cb'    => '__return_false',
        'depth'          => 1,
    ] );
}

;

function wp_menu_footer_2() {
    wp_nav_menu( [
        'theme_location' => 'footer_2',
        'container'      => false,
        'menu_class'     => 'menuFooter',
        'fallback_cb'    => '__return_false',
        'depth'          => 1,
    ] );
}

;

function wp_menu_footer_3() {
    wp_nav_menu( [
        'theme_location' => 'footer_3',
        'container'      => false,
        'menu_class'     => 'menuFooter',
        'fallback_cb'    => '__return_false',
        'depth'          => 1,
    ] );
}

;


/**
 * Ajustes slider principal
 *
 */
class Sub_menu {
    /**
     * Autoload method
     *
     * @return void
     */
    public function __construct() {
        add_action( 'admin_menu', [ &$this, 'register_sub_menu' ] );
    }

    /**
     * Register submenu
     *
     * @return void
     */
    public function register_sub_menu() {
        add_submenu_page(
            'edit.php?post_type=sliders', 'Ajustes', 'Ajustes', 'manage_options', 'ajustes-sliders', [
                &$this,
                'submenu_page_callback',
            ]
        );
    }

    /**
     * Render submenu
     *
     * @return void
     */
    public function submenu_page_callback() {
        //must check that the user has the required capability
        if ( ! current_user_can( 'manage_options' ) ) {
            wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
        }

        // variables for the field and option names
        $opt_name          = 'numero_slider';
        $hidden_field_name = 'mt_submit_hidden';
        $data_field_name   = 'numero_slider';

        // Read in existing option value from database
        $opt_val = get_option( $opt_name );

        // See if the user has posted us some information
        // If they did, this hidden field will be set to 'Y'
        if ( isset( $_POST[ $hidden_field_name ] ) && $_POST[ $hidden_field_name ] == 'Y' ) {
            // Read their posted value
            $opt_val = $_POST[ $data_field_name ];

            // Guardar el valor publicado en la base de datos
            update_option( $opt_name, $opt_val );

            // Mensaje de actualización
            ?>
            <div class="updated"><p><strong><?php _e( 'Ajustes guardados.', 'b4st' ); ?></strong></p></div>
            <?php
        }
        echo '<div class="wrap">';

        // Cabecera
        echo "<h2>" . __( 'Ajustes Slider', 'b4st' ) . "</h2>";

        ?>
        <div>
            <form name="form1" method="post" action="">
                <input type="hidden" name="<?php echo $hidden_field_name; ?>" value="Y">

                <p><?php _e( "Número de sliders:", 'b4st' ); ?>
                    <input type="number" name="<?php echo $data_field_name; ?>" value="<?php echo $opt_val; ?>"
                           size="20">
                </p>
                <hr/>

                <p class="submit">
                    <input type="submit" name="Submit" class="button-primary"
                           value="<?php esc_attr_e( 'Save Changes' ) ?>"/>
                </p>
            </form>
        </div>

        <?php
    }

}

new Sub_menu();

/**
 * Display a custom taxonomy dropdown in admin
 *
 * @author Mike Hemberger
 * @link   http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
 */
add_action( 'restrict_manage_posts', 'tsm_filter_post_type_by_taxonomy' );
function tsm_filter_post_type_by_taxonomy() {
    global $typenow;
    $post_type = 'product'; // change to your post type
    $taxonomy  = 'genero'; // change to your taxonomy
    if ( $typenow == $post_type ) {
        $selected      = isset( $_GET[ $taxonomy ] ) ? $_GET[ $taxonomy ] : '';
        $info_taxonomy = get_taxonomy( $taxonomy );
        wp_dropdown_categories( [
            'show_option_all' => __( "Todos {$info_taxonomy->label}" ),
            'taxonomy'        => $taxonomy,
            'name'            => $taxonomy,
            'orderby'         => 'name',
            'selected'        => $selected,
            'show_count'      => true,
            'hide_empty'      => true,
        ] );
    };
}

/**
 * Filter posts by taxonomy in admin
 *
 * @author  Mike Hemberger
 * @link    http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
 */
add_filter( 'parse_query', 'tsm_convert_id_to_term_in_query' );
function tsm_convert_id_to_term_in_query( $query ) {
    global $pagenow;
    $post_type = 'product';
    $taxonomy  = 'genero';
    $q_vars    = &$query->query_vars;
    if ( $pagenow == 'edit.php' && isset( $q_vars['post_type'] ) && $q_vars['post_type'] == $post_type && isset( $q_vars[ $taxonomy ] ) && is_numeric( $q_vars[ $taxonomy ] ) && $q_vars[ $taxonomy ] != 0 ) {
        $term                = get_term_by( 'id', $q_vars[ $taxonomy ], $taxonomy );
        $q_vars[ $taxonomy ] = $term->slug;
    }
}

/**
 * Display a custom taxonomy dropdown in admin
 *
 * @author Mike Hemberger
 * @link   http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
 */
add_action( 'restrict_manage_posts', 'tsm_filter_post_type_by_edad' );
function tsm_filter_post_type_by_edad() {
    global $typenow;
    $post_type = 'product'; // change to your post type
    $taxonomy  = 'edad'; // change to your taxonomy
    if ( $typenow == $post_type ) {
        $selected      = isset( $_GET[ $taxonomy ] ) ? $_GET[ $taxonomy ] : '';
        $info_taxonomy = get_taxonomy( $taxonomy );
        wp_dropdown_categories( [
            'show_option_all' => __( "Todos {$info_taxonomy->label}" ),
            'taxonomy'        => $taxonomy,
            'name'            => $taxonomy,
            'orderby'         => 'name',
            'selected'        => $selected,
            'show_count'      => true,
            'hide_empty'      => true,
        ] );
    };
}

add_filter( 'parse_query', 'tsm_convert_id_to_term_in_query_edad' );
function tsm_convert_id_to_term_in_query_edad( $query ) {
    global $pagenow;
    $post_type = 'product';
    $taxonomy  = 'edad';
    $q_vars    = &$query->query_vars;
    if ( $pagenow == 'edit.php' && isset( $q_vars['post_type'] ) && $q_vars['post_type'] == $post_type && isset( $q_vars[ $taxonomy ] ) && is_numeric( $q_vars[ $taxonomy ] ) && $q_vars[ $taxonomy ] != 0 ) {
        $term                = get_term_by( 'id', $q_vars[ $taxonomy ], $taxonomy );
        $q_vars[ $taxonomy ] = $term->slug;
    }
}

/**
 * Display a custom taxonomy dropdown in admin
 *
 * @author Mike Hemberger
 * @link   http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
 */
add_action( 'restrict_manage_posts', 'tsm_filter_post_type_by_categoria' );
function tsm_filter_post_type_by_categoria() {
    global $typenow;
    $post_type = 'product'; // change to your post type
    $taxonomy  = 'product_cat'; // change to your taxonomy
    if ( $typenow == $post_type ) {
        $selected      = isset( $_GET[ $taxonomy ] ) ? $_GET[ $taxonomy ] : '';
        $info_taxonomy = get_taxonomy( $taxonomy );
        wp_dropdown_categories( [
            'show_option_all' => __( "Todos {$info_taxonomy->label}" ),
            'taxonomy'        => $taxonomy,
            'name'            => $taxonomy,
            'orderby'         => 'name',
            'selected'        => $selected,
            'show_count'      => true,
            'hide_empty'      => true,
        ] );
    };
}

add_filter( 'parse_query', 'tsm_convert_id_to_term_in_query_categoria' );
function tsm_convert_id_to_term_in_query_categoria( $query ) {
    global $pagenow;
    $post_type = 'product';
    $taxonomy  = 'product_cat';
    $q_vars    = &$query->query_vars;
    if ( $pagenow == 'edit.php' && isset( $q_vars['post_type'] ) && $q_vars['post_type'] == $post_type && isset( $q_vars[ $taxonomy ] ) && is_numeric( $q_vars[ $taxonomy ] ) && $q_vars[ $taxonomy ] != 0 ) {
        $term                = get_term_by( 'id', $q_vars[ $taxonomy ], $taxonomy );
        $q_vars[ $taxonomy ] = $term->slug;
    }
}

function cargaretapa( $genero_key, $meta_query, $tax_query, $genero, $edad, $precio, $edad_key, $price_key ) {
    if ( ! $genero_key ) {
        $tax_query[] = [];
        $tax_keys    = array_keys( $tax_query );
        $genero_key  = end( $tax_keys );
    }
    $tax_query[ $genero_key ] = [
        'taxonomy'         => 'genero',
        'field'            => 'id',
        'terms'            => $genero,
        'include_children' => true,
    ];

    /*if ( isset( $tax_query[ $edad_key ] ) ) {
        unset( $tax_query[ $edad_key ] );
    }
    if ( isset( $meta_query[ $price_key ] ) ) {
        unset( $meta_query[ $price_key ] );
    }*/

    if ( ! $edad_key ) {
        $tax_query[] = [];
        $tax_keys    = array_keys( $tax_query );
        $edad_key    = end( $tax_keys );
    }
    $tax_query[ $edad_key ] = [
        'taxonomy'         => 'edad',
        'field'            => 'id',
        'terms'            => [ $edad ],
        'include_children' => true,
    ];

    if ( isset( $meta_query[ $price_key ] ) ) {
        unset( $meta_query[ $price_key ] );
    }

    if ( ! $price_key ) {
        $meta_query[] = [];
        $meta_keys    = array_keys( $meta_query );
        $price_key    = end( $meta_keys );
    }
        $tax_query['relation'] = 'AND';

        $meta_query[ $price_key ] = [
            'key'     => '_price',
            'value'   => $precio,
            'compare' => 'BETWEEN',
            'type'    => 'NUMERIC',
        ];

        $available_meta_keys      = array_filter( array_keys( $meta_query ), function ( $key ) {
            return 'relation' !== $key;
        } );
        if ( 2 <= count( $available_meta_keys ) ) {
            // Agregar relation key.
            $meta_query['relation'] = 'AND';
        }


    WC()->session->set( 'custom_filter_genero', $genero[0] );
    WC()->session->set( 'custom_filter_edad', $edad );
    if ( $precio ) {
        WC()->session->set( 'custom_filter_precio', implode( ',', $precio ) );
    } else {
        WC()->session->set( 'custom_filter_precio', null );
    }

    return array($tax_query, $meta_query);
}

/**
 * Filtrar el query actual para insertar filtro personalizado.
 *
 * @param WP_Query $query Query siendo ejecutada.
 *
 * @return WP_Query
 */
add_action( 'pre_get_posts', 'filter_search_toys' );
function filter_search_toys( WP_Query $query ) {

    // Obtener parametros post,
    $movil  = wc_gift_ideas_get_post_sanitized( 'movil', false, 'bool' );
    $etapa  = wc_gift_ideas_get_post_sanitized( 'etapa', null, 'int' );
    $genero = wc_gift_ideas_get_post_sanitized( 'genero', '', 'string' );
    $edad   = wc_gift_ideas_get_post_sanitized( 'edad', null, 'int' );
    $precio = wc_gift_ideas_get_post_sanitized( 'precio', '', 'string' );
    $selectedad   = wc_gift_ideas_get_post_sanitized( 'select-edad', null, 'int' );



    if(!empty($selectedad)){
        $edad = wc_gift_ideas_get_post_sanitized( 'select-edad', null, 'int' );
    }


    // Procesar parametros validos de post.
    $genero = array_values( array_filter( explode( ',', $genero ), function ( $val ) {
        return ! empty( $val );
    } ) );

    $precio = array_values( array_filter( explode( ',', $precio ), function ( $val ) {
        return ! is_string( $val ) || 0 !== strlen( $val );
    } ) );

    if ( $query->is_main_query() && ! is_admin() && is_post_type_archive( 'product' ) ) {
        $tax_query  = (array) $query->get( 'tax_query' );
        $meta_query = (array) $query->get( 'meta_query' );
        $genero_key = null;
        $edad_key   = null;
        $price_key  = null;
        // Search Tax Query passed keys.
        foreach ( $tax_query as $key => $value ) {
            if ( 'relation' === $key || ! is_array( $value ) ) {
                continue;
            }

            if ( array_key_exists( 'taxonomy', $value ) && 'genero' === $value['taxonomy'] ) {
                $genero_key = $key;
            }

            if ( array_key_exists( 'taxonomy', $value ) && 'edad' === $value['taxonomy'] ) {
                $edad_key = $key;
            }
        }

        // Search Meta Query passed keys.
        foreach ( $meta_query as $key => $value ) {
            if ( ! is_array( $value ) ) {
                continue;
            }

            if ( array_key_exists( 'key', $value ) && '_price' === $value['key'] ) {
                $price_key = $key;
            }
        }

        if ( ! $movil && ! empty( $genero ) && $edad && ! empty( $precio ) ) {
            $tax_query['relation'] = 'AND';
            if ( ! $genero_key ) {
                $tax_query[] = [];
                $tax_keys    = array_keys( $tax_query );
                $genero_key  = end( $tax_keys );
            }
            $tax_query[ $genero_key ] = [
                'taxonomy'         => 'genero',
                'field'            => 'id',
                'terms'            => $genero,
                'include_children' => true,
            ];
            if ( ! $edad_key ) {
                $tax_query[] = [];
                $tax_keys    = array_keys( $tax_query );
                $edad_key    = end( $tax_keys );
            }
            $tax_query[ $edad_key ] = [
                'taxonomy'         => 'edad',
                'field'            => 'id',
                'terms'            => [ $edad ],
                'include_children' => true,
            ];

            if ( ! $price_key ) {
                $meta_query[] = [];
                $meta_keys    = array_keys( $meta_query );
                $price_key    = end( $meta_keys );
            }
            $meta_query[ $price_key ] = [
                'key'     => '_price',
                'value'   => $precio,
                'compare' => 'BETWEEN',
                'type'    => 'NUMERIC',
            ];
            $available_meta_keys      = array_filter( array_keys( $meta_query ), function ( $key ) {
                return 'relation' !== $key;
            } );
            if ( 2 <= count( $available_meta_keys ) ) {
                // Agregar relation key.
                $meta_query['relation'] = 'AND';
            }
        } else if ( $movil && $etapa ) {
            switch ( $etapa ) {
                case 1:
                    $etapa ++;
                case 2:
                    if ( ! empty( $edad ) ) {
                        $etapa = 3;
                    }
                case 3:
                    if ( ! empty( $precio ) ) {
                        $etapa = 4;
                    }
            }

            $array_query = cargaretapa( $genero_key, $meta_query, $tax_query, $genero, $edad, $precio, $edad_key, $price_key );

            $etapa = $etapa > 4 ? 4 : $etapa;
            WC()->session->set( 'custom_filter_etapa', $etapa );

            /*switch ( $etapa ) {
                case 2:
                    // Tax de genero.
                    if ( ! $genero_key ) {
                        $tax_query[] = [];
                        $tax_keys    = array_keys( $tax_query );
                        $genero_key  = end( $tax_keys );
                    }
                    $tax_query[ $genero_key ] = [
                        'taxonomy'         => 'genero',
                        'field'            => 'id',
                        'terms'            => $genero,
                        'include_children' => true,
                    ];
                    if ( isset( $tax_query[ $edad_key ] ) ) {
                        unset( $tax_query[ $edad_key ] );
                    }
                    if ( isset( $meta_query[ $price_key ] ) ) {
                        unset( $meta_query[ $price_key ] );
                    }

                    WC()->session->set( 'custom_filter_genero', $genero[0] );
                    WC()->session->set( 'custom_filter_edad', null );
                    WC()->session->set( 'custom_filter_precio', null );
                    break;
                case 3:
                    // Tax de genero y edad.
                    if ( ! $genero_key ) {
                        $tax_query[] = [];
                        $tax_keys    = array_keys( $tax_query );
                        $genero_key  = end( $tax_keys );
                    }
                    $tax_query[ $genero_key ] = [
                        'taxonomy'         => 'genero',
                        'field'            => 'id',
                        'terms'            => $genero,
                        'include_children' => true,
                    ];
                    if ( ! $edad_key ) {
                        $tax_query[] = [];
                        $tax_keys    = array_keys( $tax_query );
                        $edad_key    = end( $tax_keys );
                    }
                    $tax_query[ $edad_key ] = [
                        'taxonomy'         => 'edad',
                        'field'            => 'id',
                        'terms'            => [ $edad ],
                        'include_children' => true,
                    ];

                    if ( isset( $meta_query[ $price_key ] ) ) {
                        unset( $meta_query[ $price_key ] );
                    }

                    WC()->session->set( 'custom_filter_edad', $edad );
                    WC()->session->set( 'custom_filter_precio', null );
                    break;
                case 4:
                    // Tax de genero, edad mas meta de precio.
                    $tax_query['relation'] = 'AND';
                    if ( ! $genero_key ) {
                        $tax_query[] = [];
                        $tax_keys    = array_keys( $tax_query );
                        $genero_key  = end( $tax_keys );
                    }
                    $tax_query[ $genero_key ] = [
                        'taxonomy'         => 'genero',
                        'field'            => 'id',
                        'terms'            => $genero,
                        'include_children' => true,
                    ];
                    if ( ! $edad_key ) {
                        $tax_query[] = [];
                        $tax_keys    = array_keys( $tax_query );
                        $edad_key    = end( $tax_keys );
                    }
                    $tax_query[ $edad_key ] = [
                        'taxonomy'         => 'edad',
                        'field'            => 'id',
                        'terms'            => [ $edad ],
                        'include_children' => true,
                    ];

                    if ( ! $price_key ) {
                        $meta_query[] = [];
                        $meta_keys    = array_keys( $meta_query );
                        $price_key    = end( $meta_keys );
                    }
                    $meta_query[ $price_key ] = [
                        'key'     => '_price',
                        'value'   => $precio,
                        'compare' => 'BETWEEN',
                        'type'    => 'NUMERIC',
                    ];
                    $available_meta_keys      = array_filter( array_keys( $meta_query ), function ( $key ) {
                        return 'relation' !== $key;
                    } );
                    if ( 2 <= count( $available_meta_keys ) ) {
                        // Agregar relation key.
                        $meta_query['relation'] = 'AND';
                    }
                    WC()->session->set( 'custom_filter_precio', implode( ',', $precio ) );
                    break;
            }*/

            $tax_query  = $array_query[0];
            $meta_query = $array_query[1];

            $available_tax_keys = array_filter( array_keys( $tax_query ), function ( $key ) {
                return 'relation' !== $key;
            } );

            if ( 1 == count( $available_tax_keys ) && isset( $tax_query['relation'] ) ) {
                // Quitar relation key.
                unset( $tax_query['relation'] );
            }

            $available_meta_keys = array_filter( array_keys( $meta_query ), function ( $key ) {
                return 'relation' !== $key;
            } );
            if ( 1 == count( $available_meta_keys ) && isset( $meta_query['relation'] ) ) {
                // Quitar relation key.
                unset( $meta_query['relation'] );
            }
        }

        // Set changes on tax and meta queries.
        $query->set( 'tax_query', $tax_query );
        $query->set( 'meta_query', ( empty( $meta_query ) ) ? false : $meta_query );

        // To remove 0=1 in where clause of query. (Don't know why is happening)
        WC()->session->set( 'remove_0_1_where_clause', true );
    }

    return $query;
}

/**
 * Abre modal cuando se ha completado una suscipción
 */
function popup_confirmacion_suscripcion() {
    ?>
    <script type="text/javascript">
        document.addEventListener( 'wpcf7mailsent', function( event ) {
            if ( '193' == event.detail.contactFormId ) {
                $('#suscripcioModal').modal('show');
            }
        }, false );
    </script>
    <?php
}
add_action( 'wp_footer', 'popup_confirmacion_suscripcion' );

/**
 * Hoja de estilos para el administrador
 *
 * @param $hook
 */
function add_custom_css_file( $hook ) {
    wp_enqueue_style(
        'css_admin',
        get_template_directory_uri() . '/theme/css/editor-style.css');
}
add_action('admin_enqueue_scripts', 'add_custom_css_file', 999);


function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/theme/img/logo-desktop.png);
            height:135px;
            width:135px;
            background-size: cover;
            background-repeat: no-repeat;
            padding-bottom: 0;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );