<?php
/**
 * Remueve Hooks por defecto de Woocommerce
 */

/**
 * Actualiza el precio al aumentar cantidad de productos.
 */
add_filter( 'woocommerce_add_to_cart_fragments', 'iconic_cart_count_fragments', 10, 1 );
function iconic_cart_count_fragments( $fragments ) {
	$fragments['span.header-cart-count'] = '<span class="header-cart-count">' . WC()->cart->get_cart_contents_count() . '</span>';

	return $fragments;
}

/**
 * Show the product title in the product loop. By default this is an H2.
 */
function woocommerce_template_loop_product_title_h3() {
	echo '<h3 class="woocommerce-loop-product__title">' . get_the_title() . '</h3>';
}

/**
 * Agregando parametro de referencia a la lista de regalos
 */
add_action( 'woocommerce_before_shop_loop_item', 'customizing_loop_product_link_open', 9 );
function customizing_loop_product_link_open() {
    global $product;

	// if ( is_page( 'giftlist' ) ) {
		remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
        // remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_link_open', 10 );
        add_action( 'woocommerce_before_shop_loop_item', 'custom_link_for_product_referal', 10 );
        // add_action( 'woocommerce_shop_loop_item_title', 'custom_link_for_product_category', 10 );
    // }
}

function custom_link_for_product_referal() {
	global $product;
	global $idlistaregalos;

	if ($idlistaregalos) {
		$link = $product->get_permalink().'?referalid='.$idlistaregalos;
	} else {
		$link = $product->get_permalink();
	}
    // HERE BELOW, Define the Link to be replaced

    echo '<a href="' . $link . '" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">';
}


/**
 * Agregar carrito agrega tambien a la lista paralela que nos ayudar a llevar control de los productos de lista de regalos
 */
function custome_add_to_cart() {
	global $woocommerce;
	global $wpdb;

	if (isset($_POST['referalid'])) {
		$list_id = $_POST['referalid'];
		$product_id = $_POST['add-to-cart'];
		$quantity = $_POST['quantity'];
	}

	$row = $wpdb->get_row( "SELECT * FROM spanktg_parallel_cart WHERE user_id = ".get_current_user_id()." and product_id =".$product_id );
	if ( empty($row) ) {
		$wpdb->insert( 'spanktg_parallel_cart', 
			[
				'product_id' =>  $product_id,
				'quantity' =>  $quantity,
				'list_id' =>  $list_id,
				'user_id' =>  get_current_user_id()
			], 
			['%d', '%d', '%d', '%d']   
		);
	} else {
		$wpdb->update( 
			'spanktg_parallel_cart', 
			array( 
				'quantity' => $row->quantity + $quantity,
			), 
			array( 'id' => $row->id ), 
			array( 
				'%d'
			), 
			array( '%d' ) 
		);
	}

}
add_action('woocommerce_add_to_cart', 'custome_add_to_cart');
/**
 * Cuando el pago sea efectuado se descontara de la lista de regalos los productos o cantidades de productos comprados
 */
// add_action( 'woocommerce_payment_complete', 'so_payment_complete' );
function so_payment_complete( $order_id ){
	global $wpdb;
	$order = wc_get_order( $order_id );
    $user = $order->get_user();
	$paralel_products = $wpdb->get_results("SELECT * FROM spanktg_parallel_cart WHERE user_id = ".$user->id);
	
	foreach ( $paralel_products as $pp ) {
		$giftlist = new Woocommerce_Gift_Ideas_Giftlist( $pp->list_id );
		$cantidades_producto = $giftlist->get_cantidades_producto();
		foreach ( $cantidades_producto as $key => $value ) {
			if ( $key == $pp->product_id ) {
				if ( $value <= $pp->quantity ) {
					// $giftlist->remove_producto( wc_get_product( $pp->product_id ) );
					unset($cantidades_producto[$key]);
				} else {
					$cantidades_producto[$key] = $value - $pp->quantity;
				}
			}
		}
		$giftlist->set_cantidades_producto($cantidades_producto);
		$giftlist->updateCantidades($user->id);
		// Despues de esto los paralel desaparecen

		$wpdb->delete( 'spanktg_parallel_cart', array( 'id' => $pp->id ), array( '%d' ) );
	}
	
}
add_action( 'woocommerce_order_status_completed', 'so_payment_complete', 1);

/**
 * Cuando remueves un item del carrito la tabla paralela dbe actualizarse tambien
 */

function ss_cart_updated( $cart_item_key, $cart ) {
	global $wpdb;
    $product_id = $cart->cart_contents[ $cart_item_key ]['product_id']; 
	$wpdb->delete( 'spanktg_parallel_cart', array( 'product_id' => $product_id, 'user_id' => get_current_user_id() ), array( '%d' ) );
};
add_action( 'woocommerce_remove_cart_item', 'ss_cart_updated', 10, 2 );

/**
 * Cuando actualizas la cantidad dde un producto la tabla paralela debe actualizarse tambien
 */
function paralel_cart_item_quantity( $cart_item_key, $quantity, $old_quantity, $cart ){
	global $wpdb;
	
	$product_id = $cart->cart_contents[ $cart_item_key ]['product_id']; 
	$row = $wpdb->get_row( "SELECT * FROM spanktg_parallel_cart WHERE user_id = ".get_current_user_id()." and product_id =".$product_id );
	$wpdb->update( 
		'spanktg_parallel_cart', 
		array( 
			'quantity' => $quantity,
		), 
		array( 'id' => $row->id ), 
		array( 
			'%d'
		), 
		array( '%d' ) 
	);
}
add_action( 'woocommerce_after_cart_item_quantity_update', 'paralel_cart_item_quantity', 20, 4 );


/* 2 - Add New Anchor Link Or Button with your Text */
// add_action('woocommerce_after_shop_loop_item','replace_add_to_cart_button');
// function replace_add_to_cart_button() {
// global $product;
// $link = $product->get_permalink();
// echo '<a href="' . esc_attr($link) . '">View Listing</a>';
// // if you want to show here Button than replace above 1 line code by below 
//  echo '<form action="' . esc_url($link) . '" method="get">
//        <button type="submit" class="single_add_to_cart_button button alt">' . __('View More', 'woocommerce') . '</button>
//       </form>';
// }











/**
 * Contenido completo detalle del producto
 */
function contenido_productos() {
	?>
	<div class="col-12 descripcion-producto">
		<div class="clearfix">
			<h2 class="h3 semibold"><?php _e( 'CARACTERÍSTICAS DEL PRODUCTO', 'b4st' ) ?></h2>
		</div>

		<div class="clearfix">
			<?php the_content(); ?>
		</div>
	</div>
	<?php
}

/**
 * Sidebar Tienda
 */
function sidebar_tienda() {
	if ( wpmd_is_notdevice() ) {
		get_sidebar();
	}
}

/**
 * Muestra el selector para cambiar la cantidad de productos
 */
function ps_selectbox() {
	global $wp_query;

	if ( wpmd_is_notdevice() ) {
		$per_page = filter_input( INPUT_GET, 'perpage', FILTER_SANITIZE_NUMBER_INT );
		echo '<div class="woocommerce-perpage br-1 d-none">';
		echo '<label class="mr-sm-2" for="productperpage">' . __( 'Mostrar # productos', 'b4st' ) . ': </label>';
		echo '<select onchange="if (this.value) window.location.href=this.value" class="custom-select borde-gris-claro form-control form-control-sm" id="productperpage">';
		$orderby_options = [
			'16' => '16',
			'24' => '24',
			'32' => '32',
			'64' => '64',
		];
		$query_string = $_SERVER["QUERY_STRING"];
		$arrParams = explode("&",$query_string);
		foreach ( $orderby_options as $value => $label ) {
			//echo "<option " . selected( $per_page, $value ) . " value='?perpage=$value'>$label</option>";
			$queryParams = "";
			for($i=0;$i<count($arrParams);$i++){
				$itemParam = explode("=",$arrParams[$i]);
				if($itemParam[0]!="perpage"){
					$queryParams .= $itemParam[0]."=".$itemParam[1]."&";
				}
			}
			$queryParams .= "perpage=$value";
			echo "<option " . selected( $per_page, $value ) . " value='?".$queryParams."'>$label</option>";
		}
		echo '</select>';
		echo '&nbsp;<span>de ' . $wp_query->found_posts . '</span></div>';
	}
}

/**
 * Cambia las cantidad de productos que se muestran en la tienda
 */
add_action( 'pre_get_posts', 'ps_pre_get_products_query' );
function ps_pre_get_products_query( $query ) {
	$per_page = filter_input( INPUT_GET, 'perpage', FILTER_SANITIZE_NUMBER_INT );
	if ( $query->is_main_query() && ! is_admin() && is_post_type_archive( 'product' ) ) {
		$query->set( 'posts_per_page', $per_page );
	}
}

/**
 * Select de filtro para genero de productos en movil.
 * HOOK: select_genero.
 *
 * @see wp-content/themes/toys2go_movil/woocommerce/archive-product.php.
 *
 * @param int    $edad   Id del termino seleccionado.
 * @param string $precio Unido por coma "0,50".
 */
function function_select_genero( $edad = null, $precio = null ) {
	$current_genero      = get_term_by( 'id', get_query_var( 'genero' ), 'genero' );
	$id_current_genero   = $current_genero->term_id;
	$name_current_genero = $current_genero->name;
	$edad                = $edad ? get_term_by( 'id', $edad, 'edad' ) : null;


	//die();

	$terms = get_terms( [
		'taxonomy'   => 'genero',
		'parent'     => 0,
		'hide_empty' => 0,
		'number'     => 2,
		'exclude'    => $id_current_genero,
	] );
	?>
	<label for="select-genero"><?php esc_html_e( 'Elegiste ver productos de', 'b4st' ); ?>&nbsp;</label>
	<select class="select-noarrow select-genero borde-gris-claro form-control form-control-sm" id="select-genero" name="genero">
		<option value="<?php echo esc_attr( $id_current_genero ); ?>"
				selected><?php echo esc_html( $name_current_genero ); ?></option>
		<?php
		foreach ( $terms as $term ) {
			$id_genero     = $term->term_id;
			$nombre_genero = $term->name;
			?>
			<option value="<?php echo esc_attr( $id_genero ); ?>"><?php echo esc_html( $nombre_genero ); ?></option>
			<?php
		}
		?>
	</select>
	<?php
	if ( $edad ) {
        $edades = get_terms( [
            'taxonomy'   => 'edad',
            'parent'     => 0,
            'hide_empty' => 0,
            'exclude'    => $edad->term_id
        ] );

        //echo "<span>/</span>" . $edad->name;
        ?>
        <select id="sizeedad" class="selectoculto">
            <option id="width_sizeedad"></option>
        </select>
        /<select class="ml-2 select-noarrow borde-gris-claro form-control form-control-sm" id="select-edad" name="edad">
            <option value="<?php echo $edad->term_id; ?>" selected><?php echo $edad->name; ?></option>
        <?php
        foreach ( $edades as $ed ) {
            $id_edad     = $ed->term_id;
            $nombre_edad = $ed->name;
            ?>
            <option value="<?php echo esc_attr( $id_edad ); ?>"><?php echo esc_html( $nombre_edad ); ?></option>
            <?php
        }
        ?>
        </select>
        <?php
        //var_dump($edad->term_id);
	}
	?>
    <?php
	if ( $precio ) {
		$rangos = [
			'0,50'     => 'S/ 0 - S/ 50',
			'51,100'   => 'S/ 51 - S/ 100',
			'101,150'  => 'S/ 101 - 150',
			'151,2500' => 'S/ 151 a +',
		];
        ?>
        <select id="sizeprecios" class="selectoculto">
            <option id="width_sizeprecios"></option>
        </select>
        /<select class="ml-2 select-noarrow borde-gris-claro form-control form-control-sm" id="select-precios" name="precio">
            <?php
            foreach ($rangos as $rango => $key){
                if ($rango == $precio){ ?>
                    <option value="<?php echo $rango; ?>" selected><?php echo $key; ?></option>
                <?php }else{ ?>
                    <option value="<?php echo $rango; ?>"><?php echo $key; ?></option>
                <?php
                }
            }
            ?>
        </select>
        <?php
	}
}

/**
 * Select de filtro para edad de productos en movil.
 * HOOK: filtro_edad.
 *
 * @see wp-content/themes/toys2go_movil/woocommerce/archive-product.php.
 */


function filtro_genero_movil(){
    $args_genero = [
        'taxonomy'   => 'genero',
        'hide_empty' => false,
        // Esta opción muestra solo las categorías superiores.
        'parent'     => 0,
    ];
    $generos    = get_terms( $args_genero );
    if ( ! empty( $generos ) ) {
        ?>
        <div id="genero" role="tablist" class="collapse-filtros filtro-genero w-100">
            <div>
                <div role="tab" id="headinggenero">
                    <?php $valueexpand = ( wpmd_is_device()) ? 'true' : 'false'; ?>
                    <a data-toggle="collapse" href="#collapsegenero" aria-expanded="<?php echo $valueexpand; ?>" aria-controls="collapsegenero"
                       class="clearfix">
                    </a>
                </div>
                <?php $expandtrue = ( wpmd_is_device()) ? 'show' : ''; ?>
                <div id="collapsegenero" class="collapse <?php echo $expandtrue ?>" role="tabpanel" aria-labelledby="headingOne"
                     data-parent="#genero">
                        <ul class="list-unstyled mb-0 radio-button estilo text-right">
                            <?php
                            foreach ( $generos as $genero ) {
                                $id   = $genero->term_id;
                                $slug = $genero->slug;
                                ?>
                                <li class="value-<?php echo esc_attr( $slug ); ?>">
                                    <input id="<?php echo esc_attr( $slug ); ?>" type="radio" name="genero"
                                           value="<?php echo esc_attr( $id ); ?>">
                                    <label for="<?php echo esc_attr( $slug ); ?>" class="label">
                                        <?php echo esc_html( $genero->name ); ?>
                                    </label>
                                </li>
                                <?php
                            }
                            ?>

                        </ul>
                </div>
            </div>
        </div>
        <?php
    }
}
function filtro_edad_movil() {
	$args_edad = [
		'taxonomy'   => 'edad',
		'hide_empty' => false,
		// Esta opción muestra solo las categorías superiores.
		'parent'     => 0,
	];
	$edades    = get_terms( $args_edad );

	if ( ! empty( $edades ) ) {
		?>
		<div id="edad" role="tablist" class="collapse-filtros filtro-edad w-100">
			<div>
				<div role="tab" id="headingedad">
                    <?php $valueexpand = ( wpmd_is_device()) ? 'true' : 'false'; ?>
					<a data-toggle="collapse" href="#collapseedad" aria-expanded="<?php echo $valueexpand; ?>" aria-controls="collapseedad"
					   class="clearfix">
					</a>
				</div>
                <?php $expandtrue = ( wpmd_is_device()) ? 'show' : ''; ?>
				<div id="collapseedad" class="collapse <?php echo $expandtrue ?>" role="tabpanel" aria-labelledby="headingOne"
					 data-parent="#edad">
						<ul class="list-unstyled mb-0 radio-button estilo text-right">
							<?php
							foreach ( $edades as $edad ) {
								$id   = $edad->term_id;
								$slug = $edad->slug;
								?>
								<li class="value-<?php echo esc_attr( $slug ); ?>">
									<input id="<?php echo esc_attr( $slug ); ?>" type="radio" name="edad"
										   value="<?php echo esc_attr( $id ); ?>">
									<label for="<?php echo esc_attr( $slug ); ?>" class="label">
										<?php echo esc_html( $edad->name ); ?>
									</label>
								</li>
								<?php
							}
							?>
						</ul>
				</div>
			</div>
		</div>
		<?php
	}
}

/**
 * Select de filtro para precio de productos en movil.
 * HOOK: filtro_precios.
 *
 * @see wp-content/themes/toys2go_movil/woocommerce/archive-product.php.
 *
 * @param string $precio Unido por coma "0,50".
 */
function filtro_precio_movil( $precio = null ) {
	$rangos = [
		[
			'id'  => 1,
			'tag' => 'S/ 0 - S/ 50',
			'min' => 0,
			'max' => 50,
		],
		[
			'id'  => 2,
			'tag' => 'S/ 51 - S/ 100',
			'min' => 51,
			'max' => 100,
		],
		[
			'id'  => 3,
			'tag' => 'S/ 101 - 150',
			'min' => 101,
			'max' => 150,
		],
		[
			'id'  => 4,
			'tag' => 'S/ 151 a +',
			'min' => 151,
			'max' => 5000,
		],
	];
	?>
	<div id="precio" role="tablist" class="collapse-filtros filtro-precio w-100">
		<div>
			<div role="tab" id="headingprecio">
                <?php $valueexpand = ( wpmd_is_device()) ? 'true' : 'false'; ?>
				<a data-toggle="collapse" href="#collapseprecio" aria-expanded="<?php echo $valueexpand ?>" aria-controls="collapseprecio"
				   class="clearfix"></a>
			</div>
            <?php $expandtrue = ( wpmd_is_device()) ? 'show' : ''; ?>
			<div id="collapseprecio" class="collapse <?php echo $expandtrue ?>" role="tabpanel" aria-labelledby="headingOne"
				 data-parent="#precio">
					<ul class="list-unstyled mb-0 radio-button estilo text-right">
						<?php
						foreach ( $rangos as $rango ) {
							?>
							<li class="value-<?php echo esc_attr( $rango['id'] ); ?>">
								<input id="rango-<?php echo esc_attr( $rango['id'] ); ?>" type="radio" name="precio"
									   value="<?php echo esc_attr( $rango['min'] . ',' . $rango['max'] ); ?>"
									<?php echo $precio && ( $rango['min'] . ',' . $rango['max'] ) === $precio ? esc_attr( 'checked' ) : ''; ?>>
								<label for="rango-<?php echo esc_attr( $rango['id'] ); ?>" class="label">
									<?php echo esc_html( $rango['tag'] ); ?>
								</label>
							</li>
							<?php
						}
						?>

					</ul>
			</div>
		</div>
	</div>
	<?php
}

/**
 * Botón seguir comprando
 */
function seguir_comprando() {
	global $woocommerce;
	?>
	<a href="<?php echo wc_get_page_permalink( 'shop' ); ?>"
	   class="btn btn-primary text-uppercase"><?php _e( 'Seguir comprando', 'b4st' ); ?></a>
	<?php
}

function etiqueta_agotado() {
    ?>
    <span class="agotado absolute"><?php _e( '¡fuera de stock!', 'b4st' ); ?></span>
    <span class="ultimas absolute"><?php _e( '¡últimas unidades!', 'b4st' ); ?></span>
    <?php
}

/*---AGREGAR BREADCRUMB ACCOUNT - WOOCOMMERCE---*/
function myacoount_breadcrumb(){
    $args = array(
        'delimiter' => ' / ',
        'home' => ''
    );
    woocommerce_breadcrumb( $args );
}

function titulo_facturacion() {
    ?>
    <div class="clearfix bordes bordes-tb">
        <h2 class="h3 titulo-productos estrella"><?php _e( 'Ingrese sus datos de Facturación', 'b4st' ); ?></h2>
    </div>
    <?php
}

function titulo_envio() {
    ?>
    <div class="clearfix bordes bordes-tb">
        <h2 class="h3 titulo-productos estrella"><?php _e( 'Ingresa tu dirección de envío:', 'b4st' ); ?></h2>
    </div>

    <p class="mt-3 form-row"><?php _e ( 'Enviar a la dirección de facturación o a una dirección diferente.', 'b4st' ); ?></p>
    <?php
}

function titulo_pago() {
    ?>
    <h3 class="titulo-productos dinero bordes bordes-tb titulo_pago_cab"><?php _e( 'Elige la forma de pago:', 'b4st' ); ?></h3>
    <?php
}

function titulo_carro() {
    ?>
    <div class="clearfix bordes bordes-tb titulo-resumen">
        <h2 class="h3 titulo-productos estrella"><?php _e( 'Resumen de compra:', 'b4st' ); ?></h2>
    </div>
    <?php
}
//add_action( 'woocommerce_review_order_before_cart_contents', 'titulo_carro', 2 );

function get_title_shipping_method_from_method_id( $method_rate_id = '' ){
	if( ! empty( $method_rate_id ) ){
		$method_key_id = str_replace( ':', '_', $method_rate_id ); // Formating
		$option_name = 'woocommerce_'.$method_key_id.'_settings'; // Get the complete option slug
		$objeto=get_option( $option_name, true );
		$cost=(isset($objeto["cost"]))?$objeto["cost"]:0;
		$titulo=$objeto['title'];
		return array("titulo"=>$titulo,"costo"=>$cost); // Get the title and return it
	} else {
		return false;
	}
}

function campo_cupon_checkout() {
    ?>
    <div class="bordes bordes-t3 bordes-alt clearfix">
        <p class="form-row validate-required woocommerce-validated" id="billing_cupon_field" data-priority="10">
            <label for="billing_cupon" class=""><?php _e( 'Código de descuento', 'b4st' ); ?> <abbr class="required" title="obligatorio">*</abbr></label>
            <input type="text" class="form-control" name="billing_cupon" id="billing_cupon"
                   placeholder="" autocomplete="given-name">
        </p>
    </div>
    <?php
}

/**
 *
 */
$active_plugins = apply_filters( 'active_plugins', get_option( 'active_plugins' ) );
if ( in_array( '../woocommerce/woocommerce.php', $active_plugins) ){
    class WC_NEW_SHIPPING_METHOD {
        function __construct() {
            add_filter( 'woocommerce_shipping_methods',array($this,'add_wc_new_shipping_method'));
            add_action( 'woocommerce_shipping_init',array($this,'wc_new_shipping_method_init') );
        }

        function add_wc_new_shipping_method( $methods ) {
            $methods[] = 'WC_NEW_Shipping_Method';
            return $methods;
        }

        function wc_new_shipping_method_init(){
            require_once 'class-new-shipping.php';
        }
    }

    new WC_NEW_SHIPPING_METHOD();
}

/**
 * Añade los campos: Nombre, Apellido, Correo electrónico, Tipo de documento y Teléfono
 * al formulario del checkout
 *
 */


/**
 * Remueve acciones
 */
// remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_shop_loop', 'wc_print_notices', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );
remove_action( 'woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20 );
//remove_action( 'woocommerce_cart_is_empty', 'wc_empty_cart_message', 10 );
remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );

/**
 * Añade acciones
 */
// add_action( 'post_imagen_producto', 'woocommerce_template_loop_add_to_cart', 10 );
add_action( 'woocommerce_shop_loop_item_title', 'custom_link_for_product_referal', 1 );
add_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_link_close', 11 );
add_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title_h3', 10 );
add_action( 'woocommerce_sidebar', 'sidebar_tienda', 10 );
add_action( 'woocommerce_before_shop_loop', 'ps_selectbox', 25 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 1 );
add_action( 'woocommerce_after_single_product_summary', 'contenido_productos', 10 );
add_action( 'woocommerce_producto_relacionados', 'woocommerce_output_related_products', 20 );
add_action( 'select_genero', 'function_select_genero', 20, 2 );
add_action( 'filtro_genero', 'filtro_genero_movil' );
add_action( 'filtro_edad', 'filtro_edad_movil' );
add_action( 'filtro_precios', 'filtro_precio_movil' );
add_action( 'woocommerce_proceed_to_checkout', 'seguir_comprando', 19 );
add_action( 'metodos_pago', 'woocommerce_checkout_payment', 20 );
add_action( 'action_myacoount_breadcrumb', 'myacoount_breadcrumb', 20 );
// Descomentar la línea siguiente para habilitar la etiqueta "Producto agotado"
// add_action( 'woocommerce_before_shop_loop_item_title', 'etiqueta_agotado', 11 );
add_action( 'woocommerce_avisos', 'wc_print_notices', 10 );
add_action( 'metodos_de_pago', 'titulo_pago', 19 );
add_action( 'metodos_de_pago', 'woocommerce_checkout_payment', 20 );
//add_action( 'form_add_cupon', 'woocommerce_checkout_coupon_form', 10 );
add_action( 'form_add_cupon', 'campo_cupon_checkout', 10 );

