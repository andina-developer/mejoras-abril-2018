<?php
function b4st_search_form( $form ) {
    $form = '<form class="form-inline w-100 form-s" role="search" method="get" id="searchform" action="' . home_url('/') . '" >
        <div class="input-group input-group-sm w-100">
            <input type="text" class="form-control" aria-label="' . esc_attr__( 'Ingresa tu búsqueda', 'b4st' ) . '" 
                value="' . get_search_query() . '" placeholder="' . esc_attr__( 'Ingresa tu búsqueda', 'b4st' ) . '" 
                name="s" id="s">
            <button class="input-group-addon" type="submit" id="searchsubmit" value="'. esc_attr__('Buscar', 'b4st') .'">
                <i class="icon-lupa"></i>
            </button>
            
            <!--<button type="reset" class="input-group-addon" id="clearinput" value="'. esc_attr__('Borrar búqueda', 'b4st') .'">
                <i class="icon-cerrar"></i>
            </button>-->
        </div>
		<input type="hidden" name="post_type" value="product">
    </form>';
    return $form;
}
add_filter( 'get_search_form', 'b4st_search_form' );