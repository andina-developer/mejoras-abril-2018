<?php

function b4st_enqueues() {

	/**
     * Styles
     */

	wp_register_style('bootstrap-css', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css', false, '4.0.0-beta', null);
	wp_enqueue_style('bootstrap-css');

	/*wp_register_style('font-awesome-css', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css', false, '4.7.0', null);
	wp_enqueue_style('font-awesome-css');*/

  	wp_register_style('style', get_template_directory_uri() . '/theme/css/style.css', array(), '3.2.2', 'all');
	wp_enqueue_style('style');

    wp_register_style( 'slick-carousel', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css', array(), '2.0.0', 'all' );
    wp_enqueue_style( 'slick-carousel' );

    wp_register_style( 'slick-carousel-theme', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css', array(), '2.0.0', 'all' );
    wp_enqueue_style( 'slick-carousel-theme' );

    wp_register_style( 'magnific-popup', 'https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css', array(), '2.0.0', 'all' );
    wp_enqueue_style( 'magnific-popup' );

	/**
     * Scripts
     */

	/* Note: this above uses WordPress's onboard jQuery. You can enqueue other pre-registered scripts from WordPress too. See:
	https://developer.wordpress.org/reference/functions/wp_enqueue_script/#Default_Scripts_Included_and_Registered_by_WordPress */

  	wp_register_script('modernizr', 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', false, '2.8.3', true);
	wp_enqueue_script('modernizr');

	wp_register_script('tether', 'https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js', 'bootstrap', '1.4.0', true);
	wp_enqueue_script('tether');

    wp_register_script('popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js', 'bootstrap', '1.11.0', true);
    wp_enqueue_script('popper');

  	wp_register_script('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js', false, '4.0.0-alpha.6', true);
	wp_enqueue_script('bootstrap');

	wp_register_script('b4st-js', get_template_directory_uri() . '/theme/js/b4st.js', false, null, true);
	wp_enqueue_script('b4st-js');

	wp_register_script('jrange', get_template_directory_uri() . '/theme/jRange-master/jquery.range-min.js', 'jquery', '0.1', true);
	wp_enqueue_script('jrange');

    wp_register_script('slick-carousel', 'https://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js', 'jquery', '2.1.1', true);
    wp_enqueue_script('slick-carousel');

    wp_register_script('magnific-popup', 'https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js', 'jquery', '1.0.0', true);
    wp_enqueue_script('magnific-popup');

    wp_register_script('magnific-popup', 'https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js', 'jquery', '1.0.0', true);
    wp_enqueue_script('magnific-popup');

    if ( wpmd_is_device() ) {
        wp_register_script('js-offcanvas', 'https://npmcdn.com/js-offcanvas@1.0/dist/_js/js-offcanvas.pkgd.min.js', 'jquery', '1.4.0', true);
        wp_enqueue_script('js-offcanvas');
    }

    if ( is_checkout() ) {
        wp_register_script('jquery-validation', 'https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js', 'jquery', '1.17.0', true);
        wp_enqueue_script('jquery-validation');

        wp_register_script('additional-methods', 'https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/additional-methods.min.js', 'jquery', '1.17.0', true);
        wp_enqueue_script('additional-methods');

        wp_register_script('jquery-steps', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.min.js', 'jquery', '1.17.0', true);
        wp_enqueue_script('jquery-steps');
    }


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script('comment-reply');
	}

    if( is_page( 'registro' ) && ! is_user_logged_in() ) {
        // Descomentar para habilitar el medidor de fuerza de la contraseña de Registro
        //wp_register_script('pippin-form-js', get_template_directory_uri() . '/theme/js/forms.js', false, '1.0.0', true);
        //wp_enqueue_script('pippin-form-js');
    }

    /*if ( is_admin() ) {
        wp_register_script('custom-adminjs', get_template_directory_uri() . '/theme/js/custom-adminjs.js', false, null, true);
        wp_enqueue_script('custom-adminjs');
    }*/
}
add_action('wp_enqueue_scripts', 'b4st_enqueues', 100);

function wpdocs_selectively_enqueue_admin_script() {
    if ( class_exists( 'WC_Settings_Payment_Gateways', true ) ) {
        wp_register_script('summernote', 'https://cdn.jsdelivr.net/npm/tinymce@4.7.9/tinymce.min.js', 'jquery', '1.4.0');
        wp_enqueue_script('summernote');

        wp_register_script('custom-adminjs', get_template_directory_uri() . '/theme/js/custom-adminjs.js', 'jquery', '1.4.0');
        wp_enqueue_script('custom-adminjs');
    }
}
add_action( 'admin_enqueue_scripts', 'wpdocs_selectively_enqueue_admin_script' );



