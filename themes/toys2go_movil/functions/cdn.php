<?php
/**
 * Created by Paolo Pacheco <paolopacheco23@gmail.com>
 * User: Paolo Pacheco
 * Date: 27/04/2017
 * Time: 12:33 AM
 */

/**
 * Carga JQuery por CDN y envía el script al footer
 */
function replace_jquery() {
    if ( !is_admin() ) {
        wp_deregister_script('jquery');
        wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js', false, '1.11.3', true);
        wp_enqueue_script('jquery');

        wp_deregister_script('jquery-migrate');
        wp_register_script('jquery-migrate', 'https://cdn.jsdelivr.net/jquery.migrate/1.4.1/jquery-migrate.min.js', false, '1.4.1', true);
        wp_enqueue_script('jquery-migrate');
    }
}
add_action('init', 'replace_jquery');

/**
 * Envía todos los js al footer
 */
function remove_head_scripts() {
    remove_action('wp_head', 'wp_print_scripts');
    remove_action('wp_head', 'wp_print_head_scripts', 9);
    remove_action('wp_head', 'wp_enqueue_scripts', 1);

    add_action('wp_footer', 'wp_print_scripts', 5);
    add_action('wp_footer', 'wp_enqueue_scripts', 5);
    add_action('wp_footer', 'wp_print_head_scripts', 5);
}
add_action( 'wp_enqueue_scripts', 'remove_head_scripts' );

function bootstrap() {

}
//add_action( 'wp_enqueue_scripts', 'bootstrap' );