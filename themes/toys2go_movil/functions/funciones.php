<?php
/**
 * Tamaños personalizados para las imágenes
 */
add_image_size('wp-carousel-principal-sm', 350, 280, true);
add_image_size('wp-carousel-principal-lg', 600, 480, true);
add_image_size('wp-carousel-slick', 245, 245, true);
add_image_size('wp-carousel-marcas', 150, 150, true);
add_image_size('wp-galeria-productos', 450, 450, true);
add_image_size('wp-thumb-galeria-productos', 88, 88, true);
add_image_size('wp-thumb-pupup-productos', 90, 90, true);
add_image_size('wp-thumb-carrito-productos', 60, 60, true);
add_image_size('wp-thumb-packing', 600, 600, true);

/**
 * Comprueba la existencia de un librería JS en un CDN remoto y la imprime,
 * de lo contrario, imprime la libería local.
 *
 * @param $remote_url
 * @param $local_path
 * @param $resource_name
 * @param array $dependencies
 * @param bool $version
 * @param bool $footer
 */
function load_from_cdn_or_local($remote_url, $local_path, $resource_name, $dependencies = array(), $version = false, $footer = false) {
    $is_cdn_up = get_transient($resource_name);
    if ($is_cdn_up) {
        $load_source = $remote_url;
    } else {
        $cdn_response = wp_remote_get($remote_url);

        if (is_wp_error($cdn_response) || !in_array(wp_remote_retrieve_response_code($cdn_response), array(
                '200',
                200
            ), true)
        ) {
            $load_source = $local_path;
        } else {
            $is_cdn_up = set_transient($resource_name, true, MINUTE_IN_SECONDS * 20);
            $load_source = $remote_url;
        }
    }

    wp_register_script(
        $resource_name,
        $load_source,
        $dependencies,
        $version,
        $footer
    );
    wp_enqueue_script($resource_name);
}

/**
 * Agregar opciones de generales al plugin ACF
 */
if (function_exists('acf_add_options_page')) {
    acf_add_options_page();
}

/**
 * Genera URL personalizdas de las imágenes
 *
 * @param $idImagen
 * @param $tamano
 */
function imagen_personalizada($idImagen, $tamano)
{
    $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($idImagen), $tamano);
    echo $thumb[0];
}

/**
 * Limita y escapa cadenas de texto
 *
 * @param $string
 * @param $limit
 * @param string $break
 * @param string $pad
 *
 * @return string
 */
function limite($string, $limit, $break = ".", $pad = "…")
{
    if (strlen($string) <= $limit) {
        return $string;
    }
    if (false !== ($breakpoint = strpos($string, $break, $limit))) {
        if ($breakpoint < strlen($string) - 1) {
            $string = substr($string, 0, $breakpoint) . $pad;
        }
    }

    return $string;
}

/**
 * Muestra la fecha de hoy en formato español
 */
function fechahoy()
{
    $dias = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
    $meses = array(
        "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre"
    );
    //echo $dias[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;
    echo date('d') . " de " . $meses[date('n') - 1] . " del " . date('Y');
}

/**
 * Obtiene el id desde el Slug
 *
 * @param $slug
 *
 * @return int
 */
function getidbyslug($slug)
{
    $idbyslug = get_page_by_path($slug);

    return $idbyslug->ID;
}

/**
 * Extrae una cadena dentro de otra cadena
 *
 * @param $string
 * @param $start
 * @param $end
 *
 * @return string
 */
function get_string_between($string, $start, $end)
{
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}

/**
 * Despliega los datos de contacto
 */
$idPage = getidbyslug('contactanos');

$args = array(
    'post_type' => 'page',
    'p' => $idPage,
    'posts_per_page' => 1
);

$query = new WP_Query($args);
if ($query->have_posts()) :
    while ($query->have_posts()) : $query->the_post();
        $numero = get_field('numero_telefonico', get_the_ID());
        $whatsapp = get_field('whatsapp', get_the_ID());
    endwhile;

    wp_reset_query();
endif;

set_query_var('numero', $numero);
set_query_var('whatsapp', $whatsapp);

/**
 * Obtiene el atributo "alt" de las imágenes,
 * de no existir, devuelve el título del post.
 *
 * @param string $ideimagen
 */
function getaltimage($ideimagen = '')
{
    $image_alt = get_post_meta($ideimagen, '_wp_attachment_image_alt', true);

    if (empty($image_alt)) {
        $image_alt = get_the_title();
    }

    echo $image_alt;
}

/**
 * Login de usuarios con ajax
 */
function ajax_login_init()
{
    wp_register_script('ajax-login-script', get_template_directory_uri() . '/theme/js/ajax-login-script.js', array('jquery'));
    wp_enqueue_script('ajax-login-script');

    wp_localize_script('ajax-login-script', 'ajax_login_object', array(
        'ajaxurl' => admin_url('admin-ajax.php'),
        //'redirecturl' => $_SERVER['REQUEST_URI'],
        'redirecturl' => get_bloginfo( 'url' ),
        'loadingmessage' => __('Enviando, espera por favor...')
    ));

    // Enable the user with no privileges to run ajax_login() in AJAX
    add_action('wp_ajax_nopriv_ajaxlogin', 'ajax_login');
}

// Execute the action only if the user isn't logged in
if (!is_user_logged_in()) {
    add_action('init', 'ajax_login_init');
}

function ajax_login()
{
    // First check the nonce, if it fails the function will break
    check_ajax_referer('ajax-login-nonce', 'security');

    // Nonce is checked, get the POST data and sign user on
    $info = array();
    $info['user_login'] = $_POST['username'];
    $info['user_password'] = $_POST['password'];
    $info['remember'] = true;

    $user_signon = wp_signon($info, false);
    if (is_wp_error($user_signon)) {
        echo json_encode(array('loggedin' => false, 'message' => __('Correo o contraseña incorrecta.'), 'aviso' => 'rojo'));
    } else {
        echo json_encode(array('loggedin' => true, 'message' => __('Acceso correcto'), 'aviso' => 'verde'));
    }

    die();
}

// Register Custom Packing
function custom_post_packing()
{

    $labels = array(
        'name' => _x('Packings', 'Packing General Name', 'text_domain'),
        'singular_name' => _x('Packing', 'Packing Singular Name', 'text_domain'),
        'menu_name' => __('Packings', 'text_domain'),
        'name_admin_bar' => __('Packing', 'text_domain'),
        'archives' => __('Packing Archives', 'text_domain'),
        'attributes' => __('Packing Attributes', 'text_domain'),
        'parent_item_colon' => __('Packing padre:', 'text_domain'),
        'all_items' => __('Todos los Packings', 'text_domain'),
        'add_new_item' => __('Añadir nuevo Packing', 'text_domain'),
        'add_new' => __('Añadir nuevo', 'text_domain'),
        'new_item' => __('Nuevo Packing', 'text_domain'),
        'edit_item' => __('Editar Packing', 'text_domain'),
        'update_item' => __('Actualizar Packing', 'text_domain'),
        'view_item' => __('Ver Packing', 'text_domain'),
        'view_items' => __('Ver Packings', 'text_domain'),
        'search_items' => __('Buscar Packings', 'text_domain'),
        'not_found' => __('No encontrado', 'text_domain'),
        'not_found_in_trash' => __('No encontrado en papelera', 'text_domain'),
        'featured_image' => __('Imagen destacada', 'text_domain'),
        'set_featured_image' => __('Modificar imagen destacada', 'text_domain'),
        'remove_featured_image' => __('Eliminar imagen destacada', 'text_domain'),
        'use_featured_image' => __('Usar imagen destacada', 'text_domain'),
        'insert_into_item' => __('Insert into item', 'text_domain'),
        'uploaded_to_this_item' => __('Uploaded to this item', 'text_domain'),
        'items_list' => __('Items list', 'text_domain'),
        'items_list_navigation' => __('Items list navigation', 'text_domain'),
        'filter_items_list' => __('Filter items list', 'text_domain'),
    );
    $args = array(
        'label' => __('Packing', 'text_domain'),
        'description' => __('Descripción del Packing', 'text_domain'),
        'labels' => $labels,
        'supports' => array(),
        'taxonomies' => array('category', 'post_tag'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );
    register_post_type('post_packing', $args);

}

add_action('init', 'custom_post_packing', 0);
// Si es mobile
if (wpmd_is_device()) {
    add_action('seccion_packing', 'mostrar_opciones_packing');
    add_action('seccion_comprobante_pago', 'mostrar_opciones_com_pago');
} else {
    add_action('woocommerce_review_order_after_shipping', 'woocommerce_option_packing');
}

function woocommerce_option_packing() {

    mostrar_opciones_packing();
    //mostrar_opciones_com_pago();
    ?>

    <?php
}

function mostrar_opciones_com_pago( $checkout ) {
    $oData = wp_parse_args($_POST["post_data"]);
    $checkComprobante = (isset($oData["billing_comprobante"])) ? $oData["billing_comprobante"] : "";
    ?>
    <?php if ( ! wpmd_is_device() ) { ?>
    <tr class="titulo_pago_cab">
        <td colspan="2"><h2 class="titulo_paso_desktop tres"><?php _e('Pago:', 'b4st'); ?></h2></td>
    </tr>

    <tr class="shipping packing">
        <td colspan="2">
            <h2 class="h3 titulo-productos regalo bordes bordes-b"><?php _e( 'Elije el documento que prefieras:', 'b4st' ); ?></h2>
        </td>
    </tr>

    <tr class="billing_doc_venta">
        <td colspan="2">
            <div class="bordes bordes-b bordes-alt clearfix">
                <p class="form-row form-row form-row-first float-none">
                    <label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
                        <input class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox"
                               id="comprobante_boleta"
                               type="radio" <?php echo ($checkComprobante == "Boleta") ? "checked" : "" ?>
                               name="billing_comprobante" value="Boleta"
                               checked />
                        <span><?php _e( 'Boleta', 'woocommerce' ); ?></span>
                    </label>
                </p>
            </div>

            <div class="bordes bordes-b bordes-alt clearfix">
                <p class="form-row form-row-last float-none">
                    <label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
                        <input class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox"
                               id="comprobante_factura"
                               type="radio" <?php echo ($checkComprobante == "Factura") ? "checked" : "" ?>
                               name="billing_comprobante" value="Factura"/>
                        <span><?php _e('Factura', 'woocommerce'); ?></span>
                    </label>
                </p>
            </div>

            <div id="campos_factura" class="bordes bordes-b bordes-alt clearfix">
                <p class="form-row form-row-last validate-required">
                    <label for="factura_ruc" class="">RUC <abbr class="required" title="obligatorio">*</abbr></label>
                    <input type="text" class="input-text form-control" name="factura_ruc" id="factura_ruc" value="">
                </p> 
            </div>
        </td>
    </tr>

    <tr>
        <td>
            <div class="woocommerce-additional-fields">
                <p class="form-row pt-3"><?php _e( 'Dirección de facturación', 'b4st' ) ?>:</p>
                <?php
                $fields = $checkout->get_checkout_fields( 'billing1' );

                foreach ( $fields as $key => $field ) {
                    if ( isset( $field['country_field'], $fields[ $field['country_field'] ] ) ) {
                        $field['country'] = $checkout->get_value( $field['country_field'] );
                    }
                    woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
                }
                ?>
            </div>
        </td>

        <td>
            <div class="woocommerce-shipping-fields">
                <?php if ( true === WC()->cart->needs_shipping_address() ) : ?>

                    <h3 id="ship-to-different-address" class="d-none">
                        <label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
                            <input id="ship-to-different-address-checkbox" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" <?php checked( apply_filters( 'woocommerce_ship_to_different_address_checked', 'shipping' === get_option( 'woocommerce_ship_to_destination' ) ? 1 : 0 ), 1 ); ?> type="checkbox" name="ship_to_different_address" value="1" /> <span><?php _e( 'Ship to a different address?', 'woocommerce' ); ?></span>
                        </label>
                    </h3>

                    <div class="shipping_address">

                        <?php do_action( 'woocommerce_before_checkout_shipping_form', $checkout ); ?>

                        <div class="woocommerce-shipping-fields__field-wrapper">
                            <?php
                            $fields = $checkout->get_checkout_fields( 'shipping' );

                            foreach ( $fields as $key => $field ) {
                                if ( isset( $field['country_field'], $fields[ $field['country_field'] ] ) ) {
                                    $field['country'] = $checkout->get_value( $field['country_field'] );
                                }
                                woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
                            }
                            ?>
                        </div>

                        <?php do_action( 'woocommerce_after_checkout_shipping_form', $checkout ); ?>

                    </div>

                <?php endif; ?>
            </div>
        </td>
    </tr>
    <?php } else { ?>
    <div class="clearfix">
        <h2 class="h3 titulo-productos regalo bordes bordes-b"><?php _e( 'Elije el documento que prefieras:', 'b4st' ); ?></h2>
    </div>
    <div class="bordes bordes-b bordes-alt clearfix">
        <p class="form-row form-row form-row-first float-none">
            <label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
                <input class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox"
                       id="comprobante_boleta"
                       type="radio" <?php echo ($checkComprobante == "Boleta") ? "checked" : ""; ?>
                       name="billing_comprobante" value="Boleta"
                       checked />
                <span><?php _e( 'Boleta', 'woocommerce' ); ?></span>
            </label>
        </p>
    </div>
    <div class="bordes bordes-b bordes-alt clearfix">
        <p class="form-row form-row-last float-none">
            <label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
                <input class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox"
                       id="comprobante_factura"
                       type="radio" <?php echo ($checkComprobante == "Factura") ? "checked" : "" ?>
                       name="billing_comprobante" value="Factura"/>
                <span><?php _e('Factura', 'woocommerce'); ?></span>
            </label>
        </p>
    </div>

    <div id="campos_factura" class="bordes bordes-b bordes-alt clearfix">
        <p class="form-row form-row-last validate-required">
            <label for="factura_ruc" class="">RUC <abbr class="required" title="obligatorio">*</abbr></label>
            <input type="text" class="input-text form-control" name="factura_ruc" id="factura_ruc" value="">
        </p>
    </div>
<div class="bordes bordes-b bordes-alt">
    <div class="woocommerce-additional-fields">
        <p class="form-row pt-3 semibold"><?php _e( 'Dirección de facturación', 'b4st' ) ?>:</p>
        <?php
        $fields = $checkout->get_checkout_fields( 'billing1' );
        foreach ( $fields as $key => $field ) {
            if ( isset( $field['country_field'], $fields[ $field['country_field'] ] ) ) {
                $field['country'] = $checkout->get_value( $field['country_field'] );
            }
            woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
        }
        ?>
    </div>
    <div class="woocommerce-shipping-fields mt-3">
            <?php if ( true === WC()->cart->needs_shipping_address() ) : ?>

                <h3 id="ship-to-different-address" class="d-none">
                    <label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
                        <input id="ship-to-different-address-checkbox" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" <?php checked( apply_filters( 'woocommerce_ship_to_different_address_checked', 'shipping' === get_option( 'woocommerce_ship_to_destination' ) ? 1 : 0 ), 1 ); ?> type="checkbox" name="ship_to_different_address" value="1" /> <span><?php _e( 'Ship to a different address?', 'woocommerce' ); ?></span>
                    </label>
                </h3>

                <div class="shipping_address">

                    <?php do_action( 'woocommerce_before_checkout_shipping_form', $checkout ); ?>

                    <div class="woocommerce-shipping-fields__field-wrapper">
                        <?php
                        $fields = $checkout->get_checkout_fields( 'shipping' );

                        foreach ( $fields as $key => $field ) {
                            if ( isset( $field['country_field'], $fields[ $field['country_field'] ] ) ) {
                                $field['country'] = $checkout->get_value( $field['country_field'] );
                            }
                            woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
                        }
                        ?>
                    </div>

                    <?php do_action( 'woocommerce_after_checkout_shipping_form', $checkout ); ?>

                </div>

            <?php endif; ?>
        </div>
</div>
    <?php } ?>
    <script>
        jQuery(function ($) {
            $('.img-packing').magnificPopup({
                type: 'image',
                tLoading: 'Cargando imagen #%curr%...',
                mainClass: 'mfp-no-margins mfp-with-zoom',
                image: {
                    tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                    titleSrc: function (item) {
                        return item.el.attr('title');
                    }
                },
                zoom: {
                    enabled: true,
                    duration: 300 // don't foget to change the duration also in CSS
                }
            });
        });
    </script>
    <?php
}

function mostrar_opciones_packing() {
    $htmlPacking = "";
    global $wpdb;
    $ruta_imagen = get_template_directory_uri() . '/theme/img/logo-small.jpg';
    $oData = wp_parse_args($_POST["post_data"]);
    $mensaje = (isset($oData["shipping_mensaje"])) ? $oData["shipping_mensaje"] : "";
    $rutaImagenPackingActual = (isset($oData["packing_img"])) ? $oData["packing_img"] : $ruta_imagen;
    $aPackingNivel2 = (isset($oData["ckpacking"])) ? $oData["ckpacking"] : array();
    //echo "<pre>";var_dump($aPackingNivel2);exit;
    $oPacking = (isset($oData["rbpacking"])) ? explode("|", $oData["rbpacking"]) : explode("|", __("Packing Toys2Go", "b4st") . "|0|0");
    $ID_actual_nivel2 = $oPacking[0] . "|" . $oPacking[1];
    if (!empty($aPackingNivel2)) {
        $oBanPackN2 = explode("|", $aPackingNivel2[0]);
        $ID_actual_nivel1 = $oBanPackN2[2];
    } else {
        $ID_actual_nivel1 = $oPacking[2];
    }

    $htmlPacking .= "<div class='metodo-packing'>";
    $htmlPacking .= "<figure><img src='" . $ruta_imagen . "' class='img-fluid'></figure>";
    $htmlPacking .= "<div class='contenido'>";
    $htmlPacking .= "<label class='validate-required'>";
    //$htmlPacking .= '<input type="radio" name="rbpacking" ' . (($ID_actual_nivel1 == 0) ? "checked=true" : "") . ' value="0">';
    $htmlPacking .= '<input type="radio" name="rbpacking" class="radio_packing" ' . (($ID_actual_nivel1 == 0) ? "checked=true" : "") . ' value="' . __("Packing Toys2Go", "b4st") . "|0|0|" . $ruta_imagen . '">';
    $htmlPacking .= "<span>" . __("Packing Toys2Go", "b4st") . "<span>";
    $htmlPacking .= "</label>";
    $htmlPacking .= "<p>" . __('Papel y/o bolsa Toys2Go', 'b4st') . "</p>";
    $htmlPacking .= "<strong>" . __('Gratis', 'b4st') . "</strong>";
    $htmlPacking .= "</div>";
    $htmlPacking .= "</div>";

    $sqlRegalo = "SELECT * FROM " . $wpdb->prefix . "posts where post_type = 'post_packing'";
    $objectos = $wpdb->get_results($wpdb->prepare($sqlRegalo, ""));
    if (!empty($objectos)) {
        // Todos los packings nivel 1
        foreach ($objectos as $item) {
            $checked_nivel1 = ($ID_actual_nivel1 == $item->ID) ? "checked='true' " : "";
            if (has_post_thumbnail()) {
                $imagen = wp_get_attachment_image_src(get_post_thumbnail_id($item->ID), 'full');
                $ruta_imagen = $imagen[0];
            } else {
                $ruta_imagen = get_template_directory_uri() . '/theme/img/logo-small.jpg';
            }

            $monto = get_post_meta($item->ID, 'packing_monto', true);
            $rows = get_field('listpacking', $item->ID); // get all the rows

            $htmlPacking .= "<div class='metodo-packing'>";
            $htmlPacking .= "<figure><img src='" . $ruta_imagen . "' class='img-fluid'></figure>";
            $htmlPacking .= "<div class='contenido'>";
            $htmlPacking .= "<label " .
                ' ' . ((empty($checked_nivel1)) ? 'aria-expanded="false"' : 'aria-expanded="true"') .
                ' ' . ((empty($rows)) ? "" : 'data-toggle="collapse" data-target="#nivel2"') .
                ' ' . ((empty($rows)) ? "class='validate-required'" : '') .
                ">";
            $htmlPacking .= '<input type="radio" id="packing_' . $item->ID . '" name="' .
                ((empty($rows)) ? "rbpacking" : "rbpacking_padre") . '" ' . $checked_nivel1 .
                ' class= "radio_packing ' .
                ((empty($rows)) ? "" : "clpacking_padre") . '" ' .
                ' value="' . $item->post_title . "|" . $monto . "|" . $item->ID . "|" . $ruta_imagen . '">';
            //$htmlPacking .= '<input type="radio" name="' . ( (empty($rows) ) ? "rbpacking" : "rbpacking_padre") . '" value="' . $item->post_title . "|" . $monto."|".$item->ID . '">'; // Paolin
            $htmlPacking .= "<span>" . $item->post_title . "</span>";
            $htmlPacking .= "</label>";
            $htmlPacking .= "<p>" . $item->post_content . "</p>";
            $htmlPacking .= (empty($rows)) ? "<strong>" . $monto . "</strong>" : " ";
            if (!empty($rows)) {
                $htmlNivel2 = "";
                $escolapse = false;
                // Todos los packings nivel 2
                foreach ($rows as $row) {

                    $checked_nivel2 = "";
                    if (!empty($aPackingNivel2)) {
                        foreach ($aPackingNivel2 as $itemPagN2) {
                            $objetoPackn2 = explode("|", $itemPagN2);
                            $ID_actual_nivel2 = trim($objetoPackn2[0]) . "|" . trim($objetoPackn2[1]);
                            if ($ID_actual_nivel2 == ($row["nombre"] . "|" . $row["costo"])) {
                                $checked_nivel2 = "checked='true' ";
                                break;
                            }
                        }
                    }

                    if ($escolapse == false && !empty($checked_nivel2)) {
                        $escolapse = true;
                    }
                    $imagen = $row["imagen"];
                    $imgthumb = (isset($imagen['sizes']['wp-thumb-pupup-productos'])) ? $imagen['sizes']['wp-thumb-pupup-productos'] : "";
                    $imgfull = (isset($imagen['sizes']['wp-thumb-packing'])) ? $imagen['sizes']['wp-thumb-packing'] : "";
                    $htmlNivel2 .= "<li>";
                    $htmlNivel2 .= '<figure title="' . $checked_nivel2 . '">';
                    $htmlNivel2 .= '<a href="' . $imgfull . '" title="' . $row["nombre"] . '" class="img-packing"><img src="' . $imgthumb . '" class="img-fluid" alt="' . $row["nombre"] . '"></a>';
                    $htmlNivel2 .= '</figure>';
                    $htmlNivel2 .= '<div>';
                    //$htmlNivel2 .= "<h2>" . $ID_actual_nivel2 . "<-->" . ( $row["nombre"] . "|" . $row["costo"] ) . "</h2>";
                    $htmlNivel2 .= "<label>";
                    //$checked_nivel2 ="checked='true' ";
                    $htmlNivel2 .= '<input type="checkbox" class="ckpacking_nivel2" data-padre="' . $item->ID . '" name="ckpacking[]" ' . $checked_nivel2 . ' value="' .
                        ((empty($row["nombre"])) ? $item->post_title : $row["nombre"]) . "|" . $row["costo"] . "|" . $item->ID . "|" . $imgthumb . '">';
                    $htmlNivel2 .= "<span>" . get_woocommerce_currency_symbol() . " " . $row["costo"] . "</span>";
                    $htmlNivel2 .= "</label>";
                    $htmlNivel2 .= '</div>';
                    $htmlNivel2 .= "</li>";
                }
                $htmlPacking .= "<div id='nivel2' class='collapse niveles " . (($escolapse == true) ? "show" : "") . "'><ul>";
                $htmlPacking .= $htmlNivel2;
                $htmlPacking .= "</ul></div>";
            }
            $htmlPacking .= "</div>";
            $htmlPacking .= "</div>";
        }

    }
    ?>
    <?php if (!wpmd_is_device()) { ?>
    <!-- Mostrar version desktop-->
    <tr class="shipping titulo_pago_cab">
        <th colspan="2">
            <h2 class="titulo_paso_desktop dos mt-4"><?php _e("Packing:", "b4st"); ?></h2>
        </th>
    </tr>
    <tr class="shipping packing">
        <th colspan="2">
            <h2 class="h3 titulo-productos regalo bordes bordes-b"><?php _e('Elige el tipo de packing:', 'b4st'); ?></h2>
        </th>
    </tr>
    <tr class="shipping packing">
        <td colspan="2">
            <div class="p-18-10 metodos-packing">
                <input type="hidden" name="packing_img" id="packing_img"
                       value="<?php echo $rutaImagenPackingActual; ?>">
                <?php echo $htmlPacking; ?>
            </div>
        </td>
    </tr>
    <tr class="shipping packing">
        <td colspan="2">
            <div class="mb-2 bordes bordes-t bordes-alt pb-3">
                <textarea name="shipping_mensaje" placeholder="Deja una dedicatoria"
                          class="form-control"><?php echo $mensaje; ?></textarea>
            </div>
        </td>
    </tr>
<?php } else { ?>
    <!-- Mostrar version mobile -->
    <div class="row">
        <div class="col-md-12">
            <h2 class="h3 titulo-productos regalo bordes bordes-b"><?php _e('Elige el tipo de packing:', 'b4st'); ?></h2>
            <input type="hidden" name="packing_img" id="packing_img" value="<?php echo $rutaImagenPackingActual; ?>">
            <?php echo $htmlPacking; ?>
            <div class="mt-3 mb-3 bordes bordes-b bordes-alt pb-3">
                <textarea name="shipping_mensaje" placeholder="Deja una dedicatoria"
                          class="form-control"><?php echo $mensaje; ?></textarea>
            </div>
        </div>
    </div>
    <?php
} ?>

    <?php
}

add_action('woocommerce_cart_calculate_fees', 'woocommerce_custom_surcharge');
function woocommerce_custom_surcharge()
{
    global $woocommerce;
    global $wpdb;
    if (is_admin() && !defined('DOING_AJAX'))
        return;

    $monto = 0;
    $nombre = "Packing Toys2Go (Gratis)";
    $oData = (isset($_POST["post_data"])) ? wp_parse_args($_POST["post_data"]) : null;

    if (isset($_POST["ckpacking"])) {
        $aPackingNivel2 = $_POST["ckpacking"];
    } else {
        $aPackingNivel2 = (isset($oData["ckpacking"])) ? $oData["ckpacking"] : array();
    }

    $Packings = array();
    if (!empty($aPackingNivel2)) {
        foreach ($aPackingNivel2 as $itemPagN2) {
            $objetoPackn2 = explode("|", $itemPagN2);
            $Packings[] = array("nombre" => trim($objetoPackn2[0]), "precio" => trim($objetoPackn2[1]));
        }
    }

    if (!empty($Packings)) {
        $montoTotal = 0;
        $nombre = "";
        $ultimoNombre = "";
        foreach ($Packings as $fee) {
            $montoTotal += $fee["precio"];

            if ($ultimoNombre != $fee["nombre"] && $fee["nombre"] != "") {
                $nombre .= $fee["nombre"] . ", ";
            }
            $ultimoNombre = $fee["nombre"];
        }
        $nombre = (!empty($nombre)) ? trim($nombre, ", ") : "Personalizado";
        $woocommerce->cart->add_fee($nombre, $montoTotal, true, 'standard');

    } else {
        if (!isset($_POST["post_data"])) {
            $oPacking = explode("|", $_POST["rbpacking"]);
        } else {
            $oPacking = (isset($oData["rbpacking"])) ? explode("|", $oData["rbpacking"]) : array("Packing Toys2Go (Gratis)", 0);
        }
        $nombre = $oPacking[0];
        $monto = $oPacking[1];
        $imagen = $oPacking[3];
        //." <img src='".$imagen."' class='img-fluid'>"
        $woocommerce->cart->add_fee($nombre, $monto, true, 'standard');
    }


}


//
/**
 * @description Hook que añade una metabox
 */
add_action("add_meta_boxes", "udp_metabox");


/**
 * @description Añade una caja a un post
 */
function udp_metabox()
{
    add_meta_box(
        "udp_caja", //identificador
        "Informaci&oacute;n del packing", //título de la caja
        "show_info_packing", //función que pinta el contenido
        "post_packing", //donde queremos que sea visible, post, page o nombre del post type
        "advanced", //donde queremos que aparezca, advanced por defecto o side (en el sidebar)
        "high" //prioridad a la hora de mostrarse, high será la primera
    );
}

//creamos un listado de los campos para proyectos
$udp_meta = array(
    array(
        "lbl" => "Monto",
        "id" => "packing_monto",
        "type" => "number"
    )
);

/**
 * @description Pinta el html de la metabox
 */
function show_info_packing()
{
    global $udp_meta, $post;

    //campo nonce (campo oculto) por seguridad, evitamos que el form sea procesado desde otro dominio
    wp_nonce_field("udp_metabox_nonce", "udp_box_nonce");

    foreach ($udp_meta as $field) {
        //obtenemos el valor del campo guardado anteriormente
        $meta = get_post_meta($post->ID, $field["id"], true);

        //también podemos obtenerlo con get_post_custom pasando el $post->ID
        //$meta = get_post_custom($post->ID);

        //comprobamos el tipo de campo
        switch ($field["type"]) {
            case 'number':
                ?>
                <p style="width: 30%">
                    <label for="<?php echo $field['id'] ?>"><?php echo $field["lbl"] ?></label><br>
                    <input value="<?php echo $meta ?>" type="number" id="<?php echo $field['id'] ?>"
                           name="<?php echo $field['id'] ?>" class="widefat">
                </p>
                <?php
                break;
            case 'text':
                ?>
                <p>
                    <label for="<?php echo $field['id'] ?>"><?php echo $field["lbl"] ?></label><br>
                    <input value="<?php echo $meta ?>" type="text" id="<?php echo $field['id'] ?>"
                           name="<?php echo $field['id'] ?>" class="widefat">
                </p>
                <?php
                break;
            case 'checkbox':
                ?>
                <p>
                    <input <?php echo $meta === "on" ? "checked" : "" ?> type="checkbox" id="<?php echo $field['id'] ?>"
                                                                         name="<?php echo $field['id'] ?>">
                    <?php echo $field["lbl"] ?>
                </p>
                <?php
                break;
            case 'wysiwyg':
                ?>
                <p>
                    <label for="<?php echo $field['id'] ?>"><?php echo $field["lbl"] ?></label><br>
                    <?php wp_editor(
                        $meta, //valor de editor
                        $field['id'] //id del campo
                    ) ?>
                </p>
                <?php
                break;
        }
    }
}

add_action("save_post", "udp_save_metabox");

/**
 * @description Salva el contenido de la metabox
 * @param $post_id Id del post
 */
function udp_save_metabox($post_id)
{
    //si no lleva la variable post meta_box_nonce o no concuerda con udp_metabox_nonce salimos
    if (!isset($_POST["udp_box_nonce"]) || !wp_verify_nonce($_POST["udp_box_nonce"], "udp_metabox_nonce")) {
        return;
    }
    //si es un autoguardado salimos
    if (defined("DOING_AUTOSAVE") && DOING_AUTOSAVE) {
        return;
    }
    //si el usuario no tiene privilegios salimos
    if (!current_user_can("edit_post")) {
        return;
    }

    //en otro caso podemos guardar la meta
    global $udp_meta;

    foreach ($udp_meta as $field) {
        //obtenemos los nuevos datos
        $udpPostData = $_POST[$field["id"]];

        //actualiza el valor de la tabla wp_postmeta
        update_post_meta(
            $post_id, //id del post
            $field["id"], //id del campo
            $field["type"] === "text" ? esc_attr($udpPostData) : $udpPostData //nuevo valor
        );
    }
}


/**
 * Get user's first and last name, else just their first name, else their
 * display name. Defalts to the current user if $user_id is not provided.
 *
 * @param  mixed $user_id The user ID or object. Default is current user.
 * @return string          The user's name.
 */
function km_get_users_name($user_id = null)
{
    $user_info = $user_id ? new WP_User($user_id) : wp_get_current_user();
    if ($user_info->first_name) {
        /*if ( $user_info->last_name ) {
            return $user_info->first_name . ' ' . $user_info->last_name;
        }*/
        return $user_info->first_name;
    }
    return $user_info->display_name;
}

add_action('woocommerce_checkout_process','verificar_form_checkout');
function verificar_form_checkout(){
    if(isset($_POST["billing_comprobante"])){
        if(empty(trim($_POST["billing_comprobante"]))){
            wc_add_notice( sprintf( __( '%s is a required field.', 'woocommerce' ), '<strong>'.__('Documento de pago').'</strong>' ), 'error' );
        }
    }

    if(isset($_POST["billing_tipodoc"]) && isset($_POST["billing_razon_social"])){
        if(trim($_POST["billing_tipodoc"])=="ruc" && empty(trim($_POST["billing_razon_social"]))){
            wc_add_notice( sprintf( __( '%s is a required field.', 'woocommerce' ), '<strong>'.__('Raz&oacute;n social:').'</strong>' ), 'error' );
        }
    }
}