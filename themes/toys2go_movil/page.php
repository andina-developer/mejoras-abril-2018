<?php get_header(); ?>

    <section class="pagina pagina-contenido">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12" role="main">
                    <div class="page-header text-center">
						<?php the_title( '<h1 class="celeste">', '</h1>' ); ?>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center">
                <div class="col-12 col-lg-7">
					<?php the_content(); ?>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>