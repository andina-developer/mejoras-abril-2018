<!DOCTYPE html>
<html class="no-js">
<head>
	<title><?php wp_title('•', true, 'right'); bloginfo('name'); ?></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<!--<meta name="viewport" content="width=device-width, initial-scale=1.0">-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<?php wp_head(); ?>

    <!-- BEGIN GOOGLE ANALYTICS CODE -->
    <script type="text/javascript">
        //<![CDATA[
        var _gaq = _gaq || [];

        _gaq.push(['_setAccount', 'UA-69633964-1']);

        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

        //]]>
    </script>
    <!-- END GOOGLE ANALYTICS CODE -->
    <!-- Facebook Ads Extension for Magento -->
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','//connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1722364037784378', {}, {agent: 'exmagento-1.9.2.1-2.3.1' });
        fbq('track', 'PageView', {
            source: 'magento',
            version: "1.9.2.1",
            pluginVersion: "2.3.1"
        });
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1722364037784378&ev=PageView&noscript=1&cd[source]=magento&cd[version]=1.9.2.1&cd[pluginVersion]=2.3.1&a=exmagento-1.9.2.1-2.3.1"
        /></noscript>
    <!-- End Facebook Pixel Code -->
</head>

<body <?php body_class(); ?>>

<?php
/*
 * Header
 */
if ( wpmd_is_device() ):
    get_template_part( 'includes/movil/header' );
else:
    get_template_part( 'includes/desktop/header' );
endif;
?>

<?php
/*
 * Menú
 */
if ( wpmd_is_device() ):
    get_template_part( 'includes/movil/menu' );
else:
    get_template_part( 'includes/desktop/menu' );
endif;
?>





